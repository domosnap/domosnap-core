DomoSnap
======

Domotic Solution Management.



Project's goal is to provide a full solution at the begin around SmartHome but now
all smart* ecosystem (SmartCities, SmartHealth, SmartGrid, ...).

Different protocol are supported (Legrand/BTicino SCS BUS My Home, OpenWebNet, Hue, 
KNX, ...) and lot of new are coming. Therefore, it is easy to develop your own 
connector to support you own protocol.

The solution is based on different components:

- DomoSnap engine: the core library providing components
driving your device. Today some are manage as Light, Heating,
Gateway, ... The project is totally compliant with some of JAVA
stack (OSGi, Android, other java platform).

- DomoSnap adapter: additionnal adapter (support protocol).

- DomoSnap consumer: consume data from engine and stream them to external source.

- DomoSnap reverse gateway: make engine accessible from outside (for alexa
as example) without open your network. The engine will poll data from a cloud
provider.

Some other project are existing (android standalone application, web client, diy, etc...).

Today OpenWebNet protocol, Hue and first step of KNX are supported.
I2C, 1-wire, zigbee protocol are in beta.
The engine support light, shutter, heating, sensor, ...
The Android application version support light and shutter.

But since the plaftorm begin to be really strong our development are faster
and we have lot idea in the backlog!! 


API Management to pilot IoT object (down/up)
Discovery/Provisionning to discover/povisionning IoT object
Stream data (upstream)
Security - Pilot your house but without access to it
         - cryptage?
         
         
## Build:

    mvn clean package

## Quality:
    
    mvn sonar:sonar -Dsonar.host.url=<URL> -Dsonar.token=<TOKEN>
         
Update Version
=   
         
mvn versions:set -DnewVersion=1.0.3-SNAPSHOT

mvn versions:commit
