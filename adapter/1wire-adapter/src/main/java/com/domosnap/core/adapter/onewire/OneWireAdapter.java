package com.domosnap.core.adapter.onewire;

import java.io.File;

/*
 * #%L
 * DomoSnap 1wire Adapter
 * %%
 * Copyright (C) 2018 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.*;
import com.domosnap.engine.supervisor.OnScanDeviceListener;
import com.pi4j.io.w1.W1Master;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class OneWireAdapter implements IAdapter {

	private static final String SYS_BUS_W1_DEVICES = "/sys/bus/w1/devices";

	public static final String SCHEME = "ow";
	
	private static final Log log = new Log(OneWireAdapter.class.getSimpleName());
	
	private URI uri;
	
	private final OneWireCommanderConsumer commander = new OneWireCommanderConsumer();
	private final OneWireMonitorPublisher monitor = new OneWireMonitorPublisher();
	private final List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
	private boolean commanderConnected = false;
	
	@Override
	public URI getURI() {
		return uri;
	}

	@Override
	public void setURI(URI uri) {
		if (SCHEME.equals(uri.getScheme())) {
			this.uri = uri;
		} else {
			throw new IllegalArgumentException("Wrong schema. Expected: " + SCHEME + "; Received: " + uri.getScheme());
		}
	}

	@Override
	public PublishProcessor<Command> createCommander() {
		PublishProcessor<Command> result = PublishProcessor.create();
		result.subscribe(commander);
		return result;
	}

	@Override
	public PublishProcessor<Query> createQuery() {
		return null; // TODO
	}

	@Override
	public Flowable<Event> createMonitor() {
	
		ConnectableFlowable<Event> connectableFlowable = 
				Flowable.create(monitor, BackpressureStrategy.DROP)
								.share()
								.publish();
		connectableFlowable.connect();

		return connectableFlowable;
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		W1Master w1Master = new W1Master();

		List<com.pi4j.component.temperature.TemperatureSensor> lis =  w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class);
		int i = 1;
		for (com.pi4j.component.temperature.TemperatureSensor device : lis) {
			listener.progess(100/lis.size()*i); 
// TODO tester!refaire marcher
			monitor.addKnownDeviceId(device.getName().trim());
		}
		
		listener.progess(100);		
		listener.scanFinished();
	}

	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		try {
			File f = new File(SYS_BUS_W1_DEVICES);
			if (f.exists()) {
				listener.foundGateway(new URI(SCHEME, "localhost", "", "").toString());
			}
		} catch (URISyntaxException e) {
			log.warning(Session.GATEWAY, "Error during gateway scaning : " + e.getMessage() );
		}
	}

	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		return new ArrayList<>();
	}



	@Override
	public void addMonitorConnectionListener(OnConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(OnConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}

	@Override
	public void addCommanderConnectionListener(OnConnectionListener listener) {
		if (!connectionListenerList.contains(listener)) {
			connectionListenerList.add(listener);
		}
	}

	@Override
	public void removeCommanderConnectionListener(OnConnectionListener listener) {
		connectionListenerList.remove(listener);
	}

	@Override
	public void onCreatedDevice(Device device) {
		// Nothing to do on created device.
	}

	@Override
	public ConnectionStatusEnum connectCommander() {
		for (OnConnectionListener connectionListener : connectionListenerList) {
			connectionListener.onConnect(ConnectionStatusEnum.CONNECTED);
		}
        commanderConnected = true;
		return ConnectionStatusEnum.CONNECTED;
	}

	@Override
	public ConnectionStatusEnum connectMonitor() {
		return monitor.connect();
	}

	@Override
	public boolean isCommanderConnected() {
		return commanderConnected;
	}

	@Override
	public boolean isMonitorConnected() {
		return monitor.isConnected();
	}

	@Override
	public void disconnectCommander() {
		commanderConnected = false;
	}

	@Override
	public void disconnectMonitor() {
		monitor.disconnect();
	}

	@Override
	public void onDestroyedDevice(Device device) {

	}

	@Override
	public void close() throws IOException {

	}
}
