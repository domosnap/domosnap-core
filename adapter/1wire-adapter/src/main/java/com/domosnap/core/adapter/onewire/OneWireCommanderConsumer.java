package com.domosnap.core.adapter.onewire;

/*
 * #%L
 * DomoSnap 1wire Adapter
 * %%
 * Copyright (C) 2018 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.Command;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.functions.Consumer;

public class OneWireCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(OneWireCommanderConsumer.class.getSimpleName());
    
	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.COMMAND, "Command unsupported (null).", 1);
        } else if (!(command instanceof Query)) {
			log.severe(Session.COMMAND, "No action command supported [" + command + "].", 1);
        }
		// For status command we do nothing since monitor do polling...

	}
}
