package com.domosnap.core.adapter.onewire;

import java.net.URI;

/*
 * #%L
 * DomoSnap 1wire Adapter
 * %%
 * Copyright (C) 2018 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.device.temperature.TemperatureSensor.TemperatureSensorStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.TemperatureUnitState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.DisconnectionStatusEnum;
import com.domosnap.engine.gateway.Event;
import com.pi4j.io.w1.W1Master;
import com.pi4j.temperature.TemperatureScale;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class OneWireMonitorPublisher implements FlowableOnSubscribe<Event> {

	protected final Log log = new Log(OneWireMonitorPublisher.class.getSimpleName());

	private final List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
	private final List<FlowableEmitter<Event>> suscriberList = Collections
			.synchronizedList(new ArrayList<>());
	private final Set<String> deviceIdList = new HashSet<>();
	
	private W1Master w1Master;
	protected ExecutorService executor;
	protected boolean connected = false;
	
	private int poolingFrequency = 60000;
	
	private class MonitorHandler implements Runnable {
		@Override
		public void run() {
			while (connected) {
				for (com.pi4j.component.temperature.TemperatureSensor tempSensor : w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class)) {
					try {
							String id = tempSensor.getName().trim();
							Event commandTemp;
							
							// Temperature
							if (id.startsWith("28") || id.startsWith("10")) {
								commandTemp = new Event(TemperatureSensor.class, new Where(new URI(OneWireAdapter.SCHEME, null , "localhost", 0, id, null, null).toString()),
										new What(TemperatureSensorStateName.VALUE.name(), new DoubleState(tempSensor.getTemperature(TemperatureScale.CELSIUS))));
							} else {
								throw new UnknownWhoException();
							}

							for (FlowableEmitter<Event> suscriber : suscriberList) {
								suscriber.onNext(commandTemp);
								// Temperature unit
								Event commandUnit = new Event(TemperatureSensor.class, new Where(new URI(OneWireAdapter.SCHEME, null , "localhost", 0, id, null, null).toString()),
										new What(TemperatureSensorStateName.UNIT.name(), TemperatureUnitState.celsius()));
								suscriber.onNext(commandUnit);
							}
						} catch (Exception e) {
							log.severe(Session.MONITOR,
											"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
						}
					}
				try {
					Thread.sleep(poolingFrequency);
				} catch (InterruptedException e) {
					log.severe(Session.MONITOR, e.getMessage());
					// Restore interrupted state...
				    Thread.currentThread().interrupt();
				}
			}
        }
	}

	@Override
	public void subscribe(FlowableEmitter<Event> e) throws Exception {
		suscriberList.add(e);
	}

	public ConnectionStatusEnum connect() {
		if (isConnected())
			return ConnectionStatusEnum.CONNECTED;
		
		try {
			w1Master = new W1Master();
			
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(ConnectionStatusEnum.CONNECTED);
			}
			executor = Executors.newSingleThreadExecutor();
			executor.execute(new MonitorHandler());
			connected = true;
			return ConnectionStatusEnum.CONNECTED;
		} catch (Exception e) {
			log.severe(Session.MONITOR, e.getMessage());
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(ConnectionStatusEnum.UNKNOWN_ERROR);
			}
			return ConnectionStatusEnum.UNKNOWN_ERROR;
		} 
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		if (executor != null) {
			executor.shutdown();
			executor = null;
		}
		connected = false;
		w1Master = null;

		synchronized (connectionListenerList) {
			for (OnConnectionListener connectionListener : connectionListenerList) {
				try {
					connectionListener.onDisconnect(DisconnectionStatusEnum.CLOSED);
				} catch (Exception e) {
					log.severe(Session.COMMAND,
							"ConnectionListener raise an error [" + e.getMessage() + "]", 1);
				}
			}
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public void addConnectionListener(OnConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(OnConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}
	
	public void addKnownDeviceId(String adress) {
		deviceIdList.add(adress);
	}
	
	public void setFrequency(int frequency) {
		this.poolingFrequency = frequency;
	}
	
	public int getFrequency() {
		return poolingFrequency;
	}
}
