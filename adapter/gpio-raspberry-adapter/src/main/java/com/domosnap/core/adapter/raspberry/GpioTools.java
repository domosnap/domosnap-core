package com.domosnap.core.adapter.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.exception.InvalidPinException;
import com.pi4j.io.gpio.impl.PinImpl;

public class GpioTools {
	
	public static final int GPIO_00 = 0;
	public static final int GPIO_01 = 1; // supports PWM0 [ALT5]
	public static final int GPIO_02 = 2;
	public static final int GPIO_03 = 3;
	public static final int GPIO_04 = 4;
	public static final int GPIO_05 = 5;
	public static final int GPIO_06 = 6;
	public static final int GPIO_07 = 7;
	public static final int GPIO_08 = 8; // SDA.1 pin has a physical pull-up resistor
	public static final int GPIO_09 = 9; // SDC.1 pin has a physical pull-up resistor
	public static final int GPIO_10 = 10;
	public static final int GPIO_11 = 11;
	public static final int GPIO_12 = 12;
	public static final int GPIO_13 = 13;
	public static final int GPIO_14 = 14;
	public static final int GPIO_15 = 15;
	public static final int GPIO_16 = 16;
	// the following GPIO pins are only available on the Raspberry Pi Model A, B (revision 2.0)
	public static final int GPIO_17 = 17; // requires B rev2 or newer model (P5 header)
	public static final int GPIO_18 = 18; // requires B rev2 or newer model (P5 header)
	public static final int GPIO_19 = 19; // requires B rev2 or newer model (P5 header)
	public static final int GPIO_20 = 20; // requires B rev2 or newer model (P5 header)
    // the following GPIO pins are only available on the Raspberry Pi Model A+, B+, Model 2B, Model 3B, Zero
	public static final int GPIO_21 = 21; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_22 = 22; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_23 = 23; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header) : supports PWM1 [ALT0]
	public static final int GPIO_24 = 24; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header) : supports PWM1 [ALT5]
	public static final int GPIO_25 = 25; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_26 = 26; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header) : supports PWM0 [ALT0]
	public static final int GPIO_27 = 27; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_28 = 28; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_29 = 29; // requires 3B, 2B, Zero, A+, B+ or newer model (40 pin header)
	public static final int GPIO_30 = 30; // SDA.0 pin has a physical pull-up resistor
	public static final int GPIO_31 = 31; // SDC.0 pin has a physical pull-up resisto
	
	public Pin getPin(String i) {
		return getPin(Integer.parseInt(i));
	}
	
	public Pin getPin(int i) {
		switch(i) {
		case GpioTools.GPIO_00:
			return RaspiPin.GPIO_00;
		case GpioTools.GPIO_01:
			return RaspiPin.GPIO_01;
		case GpioTools.GPIO_02:
			return RaspiPin.GPIO_02;
		case GpioTools.GPIO_03:
			return RaspiPin.GPIO_03;
		case GpioTools.GPIO_04:
			return RaspiPin.GPIO_04;
		case GpioTools.GPIO_05:
			return RaspiPin.GPIO_05;
		case GpioTools.GPIO_06:
			return RaspiPin.GPIO_06;
		case GpioTools.GPIO_07:
			return RaspiPin.GPIO_07;
		case GpioTools.GPIO_08:
			return RaspiPin.GPIO_08;
		case GpioTools.GPIO_09:
			return RaspiPin.GPIO_09;
		case GpioTools.GPIO_10:
			return RaspiPin.GPIO_10;
		case GpioTools.GPIO_11:
			return RaspiPin.GPIO_11;
		case GpioTools.GPIO_12:
			return RaspiPin.GPIO_12;
		case GpioTools.GPIO_13:
			return RaspiPin.GPIO_13;
		case GpioTools.GPIO_14:
			return RaspiPin.GPIO_14;
		case GpioTools.GPIO_15:
			return RaspiPin.GPIO_15;
		case GpioTools.GPIO_16:
			return RaspiPin.GPIO_16;
		case GpioTools.GPIO_17:
			return RaspiPin.GPIO_17;
		case GpioTools.GPIO_18:
			return RaspiPin.GPIO_18;
		case GpioTools.GPIO_19:
			return RaspiPin.GPIO_19;
		case GpioTools.GPIO_20:
			return RaspiPin.GPIO_20;
		case GpioTools.GPIO_21:
			return RaspiPin.GPIO_21;
		case GpioTools.GPIO_22:
			return RaspiPin.GPIO_22;
		case GpioTools.GPIO_23:
			return RaspiPin.GPIO_23;
		case GpioTools.GPIO_24:
			return RaspiPin.GPIO_24;
		case GpioTools.GPIO_25:
			return RaspiPin.GPIO_25;
		case GpioTools.GPIO_26:
			return RaspiPin.GPIO_26;
		case GpioTools.GPIO_27:
			return RaspiPin.GPIO_27;
		case GpioTools.GPIO_28:
			return RaspiPin.GPIO_28;
		case GpioTools.GPIO_29:
			return RaspiPin.GPIO_29;
		case GpioTools.GPIO_30:	
			return RaspiPin.GPIO_30;
		case GpioTools.GPIO_31:
			return RaspiPin.GPIO_31;
		default :
			throw new InvalidPinException(new PinImpl(null, i, "" + i, null));
		} 
	}
}
