package com.domosnap.core.adapter.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.*;
import com.domosnap.engine.supervisor.OnScanDeviceListener;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class RaspberryAdapter implements IAdapter {

	private static final Log log = new Log(RaspberryAdapter.class.getSimpleName());

	private URI uri;
	public static final String SCHEME = "gpio-r";
	
	private GpioController gpio;
	
	private RaspberryCommanderConsumer commander;
	private RaspberryMonitorPublisher monitor;
	
	
	private final List<OnConnectionListener> commandConnectionListeners = Collections
			.synchronizedList(new ArrayList<>());
	private final List<OnConnectionListener> monitorConnectionListeners = Collections
			.synchronizedList(new ArrayList<>());

	
	public RaspberryAdapter(String uri) {
		setURI(URI.create(uri));
		log.finest(Session.OTHER, "RaspberryAdapter created.");
	}
	
	public RaspberryAdapter() {
		log.finest(Session.OTHER, "RaspberryAdapter created.");
	}
	
	@Override
	public URI getURI() {
		return uri;
	}

	@Override
	public void setURI(URI uri) {

		commander = new RaspberryCommanderConsumer();
		monitor = new RaspberryMonitorPublisher();

		if (SCHEME.equals(uri.getScheme())) {
			this.uri = uri;
		} else {
			throw new IllegalArgumentException("Wrong schema. Expected: " + SCHEME + "; Received: " + uri.getScheme());
		}
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		// TODO finir
		listener.progess(100);		
		listener.scanFinished();	
	}
	
	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		return new ArrayList<>();
	}

	@Override
	public Flowable<Event> createMonitor() {
		ConnectableFlowable<Event> connectableFlowable = 
				Flowable.create(monitor, BackpressureStrategy.DROP)
								.share()
								.publish();
		connectableFlowable.connect();

		return connectableFlowable;
	}

	@Override
	public PublishProcessor<Command> createCommander() {
		PublishProcessor<Command> result = PublishProcessor.create();
		result.subscribe(commander);
		return result;	
	}

	@Override
	public PublishProcessor<Query> createQuery() {
		return null; // TODO
	}

	@Override
	public void addMonitorConnectionListener(OnConnectionListener listener) {
		if (!monitorConnectionListeners.contains(listener)) {
			monitorConnectionListeners.add(listener);
		}
	}

	@Override
	public void removeMonitorConnectionListener(OnConnectionListener listener) {
		monitorConnectionListeners.remove(listener);
	}

	@Override
	public void addCommanderConnectionListener(OnConnectionListener listener) {
		if (!commandConnectionListeners.contains(listener)) {
			commandConnectionListeners.add(listener);
		}		
	}

	@Override
	public void removeCommanderConnectionListener(OnConnectionListener listener) {
		commandConnectionListeners.remove(listener);
	}

	@Override
	public void onCreatedDevice(Device device) {
		// Nothing to do on created device.
	}

	
	
	@Override
	public ConnectionStatusEnum connectCommander() {
		if (gpio != null) {
			gpio = GpioFactory.getInstance();
		}
		commander.connect(gpio);
		return ConnectionStatusEnum.CONNECTED;
	}

	@Override
	public ConnectionStatusEnum connectMonitor() {
		if (gpio != null) {
			gpio = GpioFactory.getInstance();
		}
		monitor.connect(gpio);
		return ConnectionStatusEnum.CONNECTED;
	}

	public boolean isCommanderConnected() {
		return commander.isConnected();
	}

	@Override
	public boolean isMonitorConnected() {
		return monitor.isConnected();
	}

	@Override
	public void disconnectCommander() {
		commander.disconnect();
	
		if (!isMonitorConnected()) {
			gpio.shutdown();
			gpio = null;
		}
		
	}

	@Override
	public void disconnectMonitor() {
		monitor.disconnect();

		if (!isCommanderConnected()) {
			gpio.shutdown();
			gpio = null;
		}
	}

	@Override
	public void onDestroyedDevice(Device device) {

	}

	@Override
	public void close() throws IOException {

	}
}
