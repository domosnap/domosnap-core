package com.domosnap.core.adapter.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.gateway.Command;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import io.reactivex.rxjava3.functions.Consumer;

public class RaspberryCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(this.getClass().getSimpleName());

	private GpioController gpio;
	
	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.COMMAND, "Command unsupported (null).");
			return;
		} else if (gpio == null) {
			log.info(Session.COMMAND, "Raspberry commander is not connected.");
			return;
		}
			
		GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.getPinByName(command.getWhere().getPath()));
	    pin.setShutdownOptions(true, PinState.LOW);
	    
		if (Light.class.equals(command.getWho())) {
			for (What what : command.getWhatList()) {
				if (LightStateName.STATUS.name().equals(what.getName())) {
					if(OnOffState.On().equals(what.getValue())) {
						// turn off gpio pin #01
						pin.high();
						System.out.println("--> GPIO state should be: ON");
					} else {
						// turn off gpio pin #01
				        pin.low();
				        System.out.println("--> GPIO state should be: OFF");
					}
				}	
			}
		}
	}
	
	public void connect(GpioController gpio) {
		this.gpio = gpio;
	}
	
	public void disconnect() {
		this.gpio = null;
	}
	
	public boolean isConnected() {
		return this.gpio != null;
	}
}
