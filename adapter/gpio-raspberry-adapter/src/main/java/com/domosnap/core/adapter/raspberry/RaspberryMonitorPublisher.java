package com.domosnap.core.adapter.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Event;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class RaspberryMonitorPublisher implements FlowableOnSubscribe<Event> {

	private List<FlowableEmitter<Event>> suscriberList = Collections.synchronizedList(new ArrayList<>());
	private GpioPinDigitalInput led;
    private GpioController gpio;
	protected final Log log = new Log(this.getClass().getSimpleName());
	protected final Object lock = new Object();
	protected boolean connected = true;

	void onMessageReceipt(GpioPinDigitalStateChangeEvent event) {
		try {
			for (FlowableEmitter<Event> suscriber : suscriberList) {
				Event e;
				if (event.getState().isHigh()) {
					e = new Event(Light.class, new Where("GPIO_01"), new What(LightStateName.STATUS.name(), OnOffState.On()));
				} else {
					e = new Event(Light.class, new Where("GPIO_01"), new What(LightStateName.STATUS.name(), OnOffState.Off()));
				}
				
				suscriber.onNext(e);
			}
		} catch (Exception e) {
			log.severe(Session.COMMAND, "Exception occurs with message ["
					+ event + "]. Message dropped. " + e.getMessage());
		}
	}

	private class MonitorHandler implements Runnable {

		@Override
		public void run() {
			System.out.println("<--Pi4J--> GPIO Listen Example ... started.");

	        // create gpio controller = ici prendre les controllers que l'on a déclarer => pas de unknow supporté ici!

	        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
	        led = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, PinPullResistance.PULL_DOWN);

	        // set shutdown state for this input pin
	        led.setShutdownOptions(true);

	        // create and register gpio pin listener
	        led.addListener(new GpioPinListenerDigital() {
	            @Override
	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
	                // display pin state on console
	                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
	                synchronized (lock) { // mutex on the main thread: only one connection or send message at the same time!
	                onMessageReceipt(event);
	                }
	            }

	        });

	        System.out.println(" ... complete the GPIO #02 circuit and see the listener feedback here in the console.");
			 
			// keep program running until user aborts (CTRL-C)
	        while(connected) {
	            try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					log.severe(Session.MONITOR, e.getMessage());
					// Restore interrupted state...
				    Thread.currentThread().interrupt();
				}
	        }
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Event> e) throws Exception {
		suscriberList.add(e);
	}
	
	public void connect(GpioController gpio) {
		this.gpio = gpio;
		connected = true;
		getExecutorService().execute(new MonitorHandler());
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		connected = false;
		gpio = null;
		led = null;
	}
	
	public boolean isConnected() {
		return this.gpio != null;
	}

	protected ExecutorService getExecutorService() {
		return Executors.newSingleThreadExecutor();
	}
}
