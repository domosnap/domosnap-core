package com.domosnap.core.adapter.i2c;

import java.io.IOException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.pi4j.io.i2c.I2CDevice;

public class EndianReaders {

	private final static Log log = new Log(EndianReaders.class.getSimpleName());
	
	public enum Endianness {
		LITTLE_ENDIAN,
		BIG_ENDIAN
	}

	/**
	 * Read an unsigned byte from the I2C device
	 */
	public static int readU8(I2CDevice device, int i2caddr, int reg) throws IOException {
		int result = 0;

		result = device.read(reg);
		log.finest(Session.DEVICE, "I2C: Device " + i2caddr + " (0x" + Integer.toHexString(i2caddr) +
					") returned " + result + " (0x" + Integer.toHexString(result) +
					") from reg " + reg + " (0x" + Integer.toHexString(reg) + ")");
		return result;
	}

	/**
	 * Read a signed byte from the I2C device
	 */
	public static int readS8(I2CDevice device, int i2caddr, int reg) throws IOException {
		int result = 0;

		result = device.read(reg);
		if (result > 127)
			result -= 256;
		log.finest(Session.DEVICE, "I2C: Device " + i2caddr + " returned " + result + " from reg " + reg);
		return result;
	}

	public static int readU16LE(I2CDevice device, int i2caddr, int register) throws IOException {
		return EndianReaders.readU16(device, i2caddr, register, Endianness.LITTLE_ENDIAN);
	}

	public static int readU16BE(I2CDevice device, int i2caddr, int register) throws IOException {
		return EndianReaders.readU16(device, i2caddr, register, Endianness.BIG_ENDIAN);
	}

	public static int readU16(I2CDevice device, int i2caddr, int register, Endianness endianness) throws IOException {
		int hi = EndianReaders.readU8(device, i2caddr, register);
		int lo = EndianReaders.readU8(device, i2caddr, register + 1);
		return ((endianness == Endianness.BIG_ENDIAN) ? (hi << 8) + lo : (lo << 8) + hi);
	}

	public static int readS16(I2CDevice device, int i2caddr, int register, Endianness endianness) throws IOException {
		int hi = 0, lo = 0;
		if (endianness == Endianness.BIG_ENDIAN) {
			hi = EndianReaders.readS8(device, i2caddr, register);
			lo = EndianReaders.readU8(device, i2caddr, register + 1);
		} else {
			lo = EndianReaders.readU8(device, i2caddr, register);
			hi = EndianReaders.readS8(device, i2caddr, register + 1);
		}
		return ((hi << 8) + lo); // & 0xFFFF;
	}

	public static int readS16LE(I2CDevice device, int i2caddr, int register) throws IOException {
		return EndianReaders.readS16(device, i2caddr, register, Endianness.LITTLE_ENDIAN);
	}

	public static int readS16BE(I2CDevice device, int i2caddr, int register) throws IOException {
		return EndianReaders.readS16(device, i2caddr, register, Endianness.BIG_ENDIAN);
	}
}
