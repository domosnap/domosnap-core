package com.domosnap.core.adapter.i2c;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.humidity.HumiditySensor;
import com.domosnap.engine.device.light.LightSensor;
import com.domosnap.engine.device.nfc.Nfc;
import com.domosnap.engine.device.pressure.PressureSensor;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.gateway.*;
import com.domosnap.engine.supervisor.OnScanDeviceListener;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class I2CAdapter implements IAdapter {

	protected final Log log = new Log(I2CAdapter.class.getSimpleName());
	
	private final List<OnConnectionListener> commanderListeners = new ArrayList<>();
	private final List<OnConnectionListener> monitorListeners = new ArrayList<>();
	
	public static final String SCHEME = "i2c"; // i2c://localhost/bme280/76
	
	protected static final String SCHEMA_BME280 = "bme280";
	protected static final int ADRESS_BME280 = 0x77;
	protected static final String SCHEMA_TSL2561 = "tsl2561";
	protected static final int ADRESS_TSL2561 = 0x39;
	protected static final String SCHEMA_PN532 = "pn532";
	protected static final int ADRESS_PN532 = 0x24;
	protected static final String SCHEMA_VEML6070 = "veml6070";
	protected static final int ADRESS_VEML6070 = 0x38;
	protected static final String SCHEMA_INA219 = "ina219";
	protected static final int ADRESS_INA219 = 0x40;
	
	private URI uri;

	private final I2CCommanderConsumer commander = new I2CCommanderConsumer();
	private final I2CMonitorPublisher monitor = new I2CMonitorPublisher();

	@Override
	public URI getURI() {
		return uri;
	}
	
	@Override
	public void setURI(URI uri) {
		if (SCHEME.equals(uri.getScheme())) {
			this.uri = uri;
		} else {
			throw new IllegalArgumentException("Wrong schema. Expected: " + SCHEME + "; Received: " + uri.getScheme());
		}
	}

	@Override
	public void scan(OnScanDeviceListener listener) {

		I2CBus i2cBus;
		try {
			i2cBus = I2CFactory.getInstance(I2CBus.BUS_1);
		} catch (UnsupportedBusNumberException | IOException e) {
			log.finest(Session.DEVICE, "No Bus found.");
			listener.progess(100);
			listener.scanFinished();
			return;
		}
			
	    log.finest(Session.DEVICE, "Connected to bus OK!!!");
		
		listener.progess(15);
		
		// BME280
		try { 
		    i2cBus.getDevice(ADRESS_BME280).read();
		    monitor.addBME280(ADRESS_BME280);
		} catch (Exception e) {
			log.finest(Session.DEVICE, "No BME found at " + Integer.toHexString(ADRESS_BME280) + ".");
		}

		try { 
			i2cBus.getDevice(0x76).read();
			monitor.addBME280(0x76);
		} catch (Exception e) {
			log.finest(Session.DEVICE, "No BME found at 76.");
		}
		
		listener.progess(30); 		
		
		//  TSL2561
		try {
			i2cBus.getDevice(ADRESS_TSL2561).read();
			monitor.addTSL2561(ADRESS_TSL2561);
		} catch (Exception e) {
			log.finest(Session.DEVICE, "No TSL2561 found at " + Integer.toHexString(ADRESS_TSL2561) + ".");
		} 
		listener.progess(60);
		
		// PN532
		try {
			I2CDevice dev = i2cBus.getDevice(ADRESS_PN532);
			Thread.sleep(500);
			dev.read();
			monitor.addPN532(ADRESS_PN532);
		} catch (InterruptedException e) {
			log.finest(Session.DEVICE, "No PN532found at " + Integer.toHexString(ADRESS_PN532) + " due to InterruptedExeption.");
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		} catch (Exception e) {
			log.finest(Session.DEVICE, "No PN532found at " + Integer.toHexString(ADRESS_PN532) + ".");
		}
		
		listener.progess(100);
		listener.scanFinished();
	}

	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		List<Class<? extends Device>> result = new ArrayList<>();
		result.add(HumiditySensor.class);
		result.add(PressureSensor.class);
		result.add(TemperatureSensor.class);
		result.add(LightSensor.class);
		result.add(Nfc.class);
		return result;
	}

	@Override
	public Flowable<Event> createMonitor() {
		ConnectableFlowable<Event> connectableFlowable = 
				Flowable.create(monitor, BackpressureStrategy.DROP)
								.share()
								.publish();
		connectableFlowable.connect();

		return connectableFlowable;
	}

	@Override
	public PublishProcessor<Command> createCommander() {
		PublishProcessor<Command> result = PublishProcessor.create();
		result.subscribe(commander);
		return result;
	}

	@Override
	public PublishProcessor<Query> createQuery() {
		return null; // TODO
	}

	/**
	 * Add a listener on new monitor stream connection
	 * @param listener the listener
	 */
	public void addMonitorConnectionListener(OnConnectionListener listener) {
		monitorListeners.add(listener);
	}

	/**
	 * Remove a listener on new monitor stream connection
	 * @param listener the listener
	 */
	public void removeMonitorConnectionListener(OnConnectionListener listener) {
		monitorListeners.remove(listener);
	}

	/**
	 * Add a listener on new command stream connection
	 * @param listener the listener
	 */
	public void addCommanderConnectionListener(OnConnectionListener listener) {
		commanderListeners.add(listener);
	}

	/**
	 * Remove a listener on new command stream connection
	 * @param listener the listener
	 */
	public void removeCommanderConnectionListener(OnConnectionListener listener) {
		commanderListeners.remove(listener);
	}

	@Override
	public void onCreatedDevice(Device device) {
		// Nothing to do on created device.		
	}

	@Override
	public void onDestroyedDevice(Device device) {

	}

	@Override
	public ConnectionStatusEnum connectCommander() {
		ConnectionStatusEnum connect = ConnectionStatusEnum.CONNECTED;
		if (!monitor.isConnected()) {
			connect = monitor.connect();
		}
		final ConnectionStatusEnum connected = connect;
		commanderListeners.forEach(connectionListener ->  {
			try {
				connectionListener.onConnect(connected);
			} catch (Exception e) {
				log.warning(Session.COMMAND, "Error in ConnectionListener during connect with error: " + e.getMessage());
			}
		});
		return connect;
	}

	@Override
	public ConnectionStatusEnum connectMonitor() {
		ConnectionStatusEnum connect = ConnectionStatusEnum.CONNECTED;
		if (!monitor.isConnected()) {
			connect = monitor.connect();
		}
		final ConnectionStatusEnum connected = connect;
		monitorListeners.forEach(connectionListener -> {
			try {
				connectionListener.onConnect(connected);
			} catch (Exception e) {
				log.warning(Session.MONITOR, "Error in ConnectionListener during connect with error: " + e.getMessage());
			}
		});
		return connect;
	}

	@Override
	public boolean isCommanderConnected() {
		return monitor.isConnected();
	}

	@Override
	public boolean isMonitorConnected() {
		return monitor.isConnected();
	}

	@Override
	public void disconnectCommander() {
		if (!isCommanderConnected()) {
			monitor.disconnect();
		}
	}

	@Override
	public void disconnectMonitor() {
		if (!isMonitorConnected()) {
			monitor.disconnect();
		}
	}

	@Override
	public void close() throws IOException {

	}
}
