package com.domosnap.core.adapter.i2c;

import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.core.adapter.i2c.bme280.BME280;
import com.domosnap.core.adapter.i2c.ina219.INA219;
import com.domosnap.core.adapter.i2c.ina219.INA219.Adc;
import com.domosnap.core.adapter.i2c.ina219.INA219.Address;
import com.domosnap.core.adapter.i2c.ina219.INA219.Brng;
import com.domosnap.core.adapter.i2c.ina219.INA219.Pga;
import com.domosnap.core.adapter.i2c.pn532.IPN532Interface;
import com.domosnap.core.adapter.i2c.pn532.PN532;
import com.domosnap.core.adapter.i2c.pn532.PN532I2C;
import com.domosnap.core.adapter.i2c.tsl2561.TSL2561;
import com.domosnap.core.adapter.i2c.veml6070.VEML6070;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.engine.device.counter.PowerCounter.CounterStateName;
import com.domosnap.engine.device.humidity.HumiditySensor;
import com.domosnap.engine.device.humidity.HumiditySensor.HumiditySensorStateName;
import com.domosnap.engine.device.light.LightSensor;
import com.domosnap.engine.device.light.LightSensor.LightSensorStateName;
import com.domosnap.engine.device.nfc.Nfc;
import com.domosnap.engine.device.nfc.Nfc.NfcStateName;
import com.domosnap.engine.device.pressure.PressureSensor;
import com.domosnap.engine.device.pressure.PressureSensor.PressureSensorStateName;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.device.temperature.TemperatureSensor.TemperatureSensorStateName;
import com.domosnap.engine.device.uv.UvSensor;
import com.domosnap.engine.device.uv.UvSensor.UvSensorStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.IntegerState;
import com.domosnap.engine.device.what.impl.PercentageState;
import com.domosnap.engine.device.what.impl.PressureUnitState;
import com.domosnap.engine.device.what.impl.StringState;
import com.domosnap.engine.device.what.impl.TemperatureUnitState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.Event;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class I2CMonitorPublisher implements FlowableOnSubscribe<Event> {

	private List<FlowableEmitter<Event>> suscriberList = Collections
			.synchronizedList(new ArrayList<FlowableEmitter<Event>>());

	private Map<String, BME280> bmeList = new HashMap<>();
	private Map<String, TSL2561> tslList = new HashMap<>();
	private Map<String, VEML6070> vemlList = new HashMap<>();
	private Map<String, INA219> inaList = new HashMap<>();
	private Map<String, PN532> pnList = new HashMap<>();
	protected ExecutorService pool = null;
	protected boolean connected = true;
	protected final Log log = new Log(I2CMonitorPublisher.class.getSimpleName());
	
	private static final int REFRESH_FREQUENCY = 60000;
	private static final int PN532_REFRESH_FREQUENCY = 500;
	
	private static final String ERR_PATTERN = "Exception occurs with message [{0}]. Message dropped. ";
	private static final String LOCALHOST = "localhost";
	
	private class MonitorHandler implements Runnable {

		
		static final byte PN532_MIFARE_ISO14443A = 0x00;
		
		@Override
		public void run() {
			while (connected) {
				
				for (Entry<String, BME280> entry : bmeList.entrySet()) {
					try {
						BME280 bme = entry.getValue();
						String adress = I2CAdapter.SCHEMA_BME280 + "/" + entry.getKey();
						for (FlowableEmitter<Event> suscriber : suscriberList) {
							
							// Temperature
							Event event = new Event(TemperatureSensor.class, new Where(new URI(I2CAdapter.SCHEME, "" , LOCALHOST, 0, adress, "", "").toString()), 
									new What(TemperatureSensorStateName.VALUE.name(), new DoubleState(bme.readTemperature())));
							suscriber.onNext(event);
							
							// Temperature unit
							event = new Event(TemperatureSensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()), 
									new What(TemperatureSensorStateName.UNIT.name(), TemperatureUnitState.celsius()));
							suscriber.onNext(event);
							
							// Humidity
							event = new Event(HumiditySensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()), 
									new What(HumiditySensorStateName.VALUE.name(), new PercentageState((double)bme.readHumidity())));
							suscriber.onNext(event);
							
							// Pressure unit
							event = new Event(PressureSensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()), 
									new What(PressureSensorStateName.UNIT.name(), PressureUnitState.pascal()));
							suscriber.onNext(event);
							
							// Pressure
							event = new Event(PressureSensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()), 
									new What(PressureSensorStateName.VALUE.name(), new DoubleState(bme.readPressure())));
							suscriber.onNext(event);
						
						}
						
					} catch (Exception e) {
						log.severe(Session.MONITOR, MessageFormat.format(ERR_PATTERN, e.getMessage()));
					}
				}
				for (Entry<String, TSL2561> entry : tslList.entrySet()) {
					TSL2561 tsl = entry.getValue();
					String adress = I2CAdapter.SCHEMA_TSL2561 + "/" + entry.getKey();
					try {
						for (FlowableEmitter<Event> suscriber : suscriberList) {
						
							// Lux
							Event event = new Event(LightSensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()),
									new What(LightSensorStateName.LUX.name(), new DoubleState(tsl.readLux())));
							suscriber.onNext(event);						
						}
						
					} catch (Exception e) {
						log.severe(Session.MONITOR,
								MessageFormat.format(ERR_PATTERN, e.getMessage()), 1);
					}
				}
				for (Entry<String, INA219> entry : inaList.entrySet()) {
					INA219 ina = entry.getValue();
					String adress = I2CAdapter.SCHEMA_INA219 + "/" + entry.getKey();
					try {
						for (FlowableEmitter<Event> suscriber : suscriberList) {
						
							// Power
							Event event = new Event(PowerCounter.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()), 
									new What(CounterStateName.ACTIVE_POWER.name(), new DoubleState(ina.getPower())));
							suscriber.onNext(event);						
						}
						
					} catch (Exception e) {
						log.severe(Session.MONITOR,
								MessageFormat.format(ERR_PATTERN, e.getMessage()), 1);
					}
				}
				for (Entry<String, VEML6070> entry : vemlList.entrySet()) {
					VEML6070 veml= entry.getValue();
					String adress = I2CAdapter.SCHEMA_VEML6070 + "/" + entry.getKey();
					try {
						for (FlowableEmitter<Event> suscriber : suscriberList) {
						
							// Uv
							Event event = new Event(UvSensor.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()),
									new What(UvSensorStateName.UVA.name(), new IntegerState(veml.readUv())));
							suscriber.onNext(event);						
						}
						
					} catch (Exception e) {
						log.severe(Session.MONITOR,
								MessageFormat.format(ERR_PATTERN, e.getMessage()), 1);
					}
				}
				int j = 0;
				while (connected && j < (REFRESH_FREQUENCY /PN532_REFRESH_FREQUENCY)) {
					j++;
					for (Entry<String, PN532> entry : pnList.entrySet()) {
						PN532 nfc = entry.getValue();
						String adress = I2CAdapter.SCHEMA_PN532 + "/" + entry.getKey();
						try {
							log.finest(Session.MONITOR, "Waiting for an ISO14443A Card ...");

							byte[] buffer = new byte[8];
							int readLength = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A,
									buffer);

							if (readLength > 0) {
								log.finest(Session.MONITOR, "Found an ISO14443A card");

								log.finest(Session.MONITOR, "  UID Length: ");
								log.finest(Session.MONITOR, "" + readLength);
								log.finest(Session.MONITOR, " bytes");

								log.finest(Session.MONITOR, "  UID Value: [");
								String value = "";
								for (int i = 0; i < readLength; i++) {
									value += Integer.toHexString(buffer[i]);
								}
								log.finest(Session.MONITOR, value);
								log.finest(Session.MONITOR, "]");
								
								for (FlowableEmitter<Event> suscriber : suscriberList) {
									
									// nfc reader value
									Event event = new Event(Nfc.class, new Where(new URI(I2CAdapter.SCHEME, null , LOCALHOST, 0, adress, null, null).toString()),
											new What(NfcStateName.PASSIVE_TARGET_ID.name(), new StringState(value)));
									suscriber.onNext(event);
								
								}
							}

						} catch (InterruptedException e) {
							log.severe(Session.MONITOR, e.getMessage());
							// Restore interrupted state...
						    Thread.currentThread().interrupt();
						} catch (Exception e) {
							log.severe(Session.MONITOR,
									MessageFormat.format(ERR_PATTERN, e.getMessage()), 1);
						}
					}
					try {
						Thread.sleep(PN532_REFRESH_FREQUENCY); // TODO make it parametrable
					} catch (InterruptedException e) {
						log.severe(Session.MONITOR, e.getMessage());
						// Restore interrupted state...
					    Thread.currentThread().interrupt();
					}
				}
			}
		}
	}
	
	@Override
	public void subscribe(FlowableEmitter<Event> e) throws Exception {
		suscriberList.add(e);
	}

	public ConnectionStatusEnum connect() {
		connected = true;
		getExecutorService().execute(new MonitorHandler());
		// TODO tester la connection....
		return ConnectionStatusEnum.CONNECTED;
	}

	public void disconnect() {
		connected = false;
	}
	
	public boolean isConnected() {
		return connected;
	}

	public void addBME280(int adress) throws UnsupportedBusNumberException {
		bmeList.put(Integer.toHexString(adress), new BME280(adress));
	}

	public void addTSL2561(int adress) throws UnsupportedBusNumberException {
		tslList.put(Integer.toHexString(adress), new TSL2561(adress));
	}
	
	public void addVEML6070(int adress) throws UnsupportedBusNumberException {
		vemlList.put(Integer.toHexString(adress), new VEML6070(adress));
	}
	
	public void addINA219(int adress) throws IOException {
		inaList.put(Integer.toHexString(adress), new INA219(Address.getAddress(adress), 0, 10, Brng.V16, Pga.GAIN_1, Adc.BITS_10, Adc.SAMPLES_128));
	}
	
	public void addPN532(int adress) {
		IPN532Interface pn532Interface = new PN532I2C(adress);
		PN532 nfc = new PN532(pn532Interface);
		
		
		try {
			log.finest(Session.DEVICE, "Starting up...");
			nfc.begin();
			Thread.sleep(1000);

			long versiondata = nfc.getFirmwareVersion();
			if (versiondata == 0) {
				log.finest(Session.DEVICE, "Didn't find PN53x board");
			}
			// Got ok data, print it out!
			log.finest(Session.DEVICE, "Found chip PN5");
			log.finest(Session.DEVICE, Long.toHexString((versiondata >> 24) & 0xFF));

			log.finest(Session.DEVICE, "Firmware ver. ");
			log.finest(Session.DEVICE, Long.toHexString((versiondata >> 16) & 0xFF));
			log.finest(Session.DEVICE, ".");
			log.finest(Session.DEVICE, Long.toHexString((versiondata >> 8) & 0xFF));

			// configure board to read RFID tags
			nfc.SAMConfig();
			pnList.put(Integer.toHexString(adress), nfc);
		} catch (InterruptedException e) {
			log.severe(Session.MONITOR, e.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}
	}
	
	protected ExecutorService getExecutorService() {
		if (pool == null) {
			pool = Executors.newFixedThreadPool(2);
		}

		return pool;
	}
}
