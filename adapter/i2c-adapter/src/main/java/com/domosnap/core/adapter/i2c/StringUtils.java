package com.domosnap.core.adapter.i2c;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class StringUtils {

	private StringUtils() {
	}
	
	/**
	 * Right pad, with blanks
	 *
	 */
	public static String rpad(String s, int len) {
		return rpad(s, len, " ");
	}

	public static String rpad(String s, int len, String pad) {
		StringBuilder str = new StringBuilder(s);
		while (str.length() < len) {
			str.append(pad);
		}
		return str.toString();
	}

	/**
	 * Left pad, with blanks
	 *
	 * @param s
	 * @param len
	 * @return
	 */
	public static String lpad(String s, int len) {
		return lpad(s, len, " ");
	}

	public static String lpad(String s, int len, String pad) {
		StringBuilder str = new StringBuilder(s);
		while (str.length() < len) {
			str.insert(0, pad);
		}
		return str.toString();
	}
}
