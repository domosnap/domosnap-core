package com.domosnap.core.adapter.i2c.bme280;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.domosnap.core.adapter.i2c.EndianReaders;
import com.domosnap.core.adapter.i2c.StringUtils;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.system.SystemInfo;

/*
 * Pressure, Altitude, Temperature, Humidity
 * Adapted from https://github.com/adafruit/Adafruit_Python_BME280
 */
public class BME280 {
	
	private static final Log log = new Log(BME280.class.getSimpleName());
	
//	private static final EndianReaders.Endianness BME280_ENDIANNESS = EndianReaders.Endianness.LITTLE_ENDIAN;
	/*
	Prompt> sudo i2cdetect -y 1
			 0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
	00:          -- -- -- -- -- -- -- -- -- -- -- -- --
	10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	70: -- -- -- -- -- -- -- 77
	 */
	// This next addresses is returned by "sudo i2cdetect -y 1", see above.
	public static final int BME280_I2CADDR = 0x77;

	// Operating Modes
	public static final int BME280_OSAMPLE_1 = 1;
	public static final int BME280_OSAMPLE_2 = 2;
	public static final int BME280_OSAMPLE_4 = 3;
	public static final int BME280_OSAMPLE_8 = 4;
	public static final int BME280_OSAMPLE_16 = 5;

	// BME280 Registers
	public static final int BME280_REGISTER_DIG_T1 = 0x88;  // Trimming parameter registers
	public static final int BME280_REGISTER_DIG_T2 = 0x8A;
	public static final int BME280_REGISTER_DIG_T3 = 0x8C;

	public static final int BME280_REGISTER_DIG_P1 = 0x8E;
	public static final int BME280_REGISTER_DIG_P2 = 0x90;
	public static final int BME280_REGISTER_DIG_P3 = 0x92;
	public static final int BME280_REGISTER_DIG_P4 = 0x94;
	public static final int BME280_REGISTER_DIG_P5 = 0x96;
	public static final int BME280_REGISTER_DIG_P6 = 0x98;
	public static final int BME280_REGISTER_DIG_P7 = 0x9A;
	public static final int BME280_REGISTER_DIG_P8 = 0x9C;
	public static final int BME280_REGISTER_DIG_P9 = 0x9E;

	public static final int BME280_REGISTER_DIG_H1 = 0xA1;
	public static final int BME280_REGISTER_DIG_H2 = 0xE1;
	public static final int BME280_REGISTER_DIG_H3 = 0xE3;
	public static final int BME280_REGISTER_DIG_H4 = 0xE4;
	public static final int BME280_REGISTER_DIG_H5 = 0xE5;
	public static final int BME280_REGISTER_DIG_H6 = 0xE6;
	public static final int BME280_REGISTER_DIG_H7 = 0xE7;

	public static final int BME280_REGISTER_CHIPID = 0xD0;
	public static final int BME280_REGISTER_VERSION = 0xD1;
	public static final int BME280_REGISTER_SOFTRESET = 0xE0;

	public static final int BME280_REGISTER_CONTROL_HUM = 0xF2;
	public static final int BME280_REGISTER_CONTROL = 0xF4;
	public static final int BME280_REGISTER_CONFIG = 0xF5;
	public static final int BME280_REGISTER_PRESSURE_DATA = 0xF7;
	public static final int BME280_REGISTER_TEMP_DATA = 0xFA;
	public static final int BME280_REGISTER_HUMIDITY_DATA = 0xFD;

	private int digT1 = 0;
	private int digT2 = 0;
	private int digT3 = 0;

	private int digP1 = 0;
	private int digP2 = 0;
	private int digP3 = 0;
	private int digP4 = 0;
	private int digP5 = 0;
	private int digP6 = 0;
	private int digP7 = 0;
	private int digP8 = 0;
	private int digP9 = 0;

	private int digH1 = 0;
	private int digH2 = 0;
	private int digH3 = 0;
	private int digH4 = 0;
	private int digH5 = 0;
	private int digH6 = 0;

	private float tFine = 0F;


	private I2CBus bus;
	private I2CDevice bme280Device;
	private int mode = BME280_OSAMPLE_8;

	public BME280() throws I2CFactory.UnsupportedBusNumberException {
		this(BME280_I2CADDR);
	}

	public BME280(int address) throws I2CFactory.UnsupportedBusNumberException {
		try {
			// Get i2c bus
			bus = I2CFactory.getInstance(I2CBus.BUS_1); // Depends onthe RasPI version
			log.finest(Session.DEVICE, "Connected to bus. OK.");

			// Get device itself
			bme280Device = bus.getDevice(address);
			log.finest(Session.DEVICE, "Connected to device. OK.");

			try {
				this.readCalibrationData();
			} catch (Exception ex) {
				log.severe(Session.DEVICE, ex.getMessage());
			}

			bme280Device.write(BME280_REGISTER_CONTROL, (byte) 0x3F);
			tFine = 0.0f;
		} catch (IOException e) {
			log.severe(Session.DEVICE, e.getMessage());
			throw new RuntimeException(e);
		}
	}

	private int readU8(int register) throws IOException {
		return EndianReaders.readU8(this.bme280Device, BME280_I2CADDR, register);
	}

	private int readS8(int register) throws IOException {
		return EndianReaders.readS8(this.bme280Device, BME280_I2CADDR, register);
	}

	private int readU16LE(int register) throws IOException {
		return EndianReaders.readU16LE(this.bme280Device, BME280_I2CADDR, register);
	}

	private int readS16LE(int register) throws IOException {
		return EndianReaders.readS16LE(this.bme280Device, BME280_I2CADDR, register);
	}

	public void readCalibrationData() throws Exception {
		// Reads the calibration data from the IC
		digT1 = readU16LE(BME280_REGISTER_DIG_T1);
		digT2 = readS16LE(BME280_REGISTER_DIG_T2);
		digT3 = readS16LE(BME280_REGISTER_DIG_T3);

		digP1 = readU16LE(BME280_REGISTER_DIG_P1);
		digP2 = readS16LE(BME280_REGISTER_DIG_P2);
		digP3 = readS16LE(BME280_REGISTER_DIG_P3);
		digP4 = readS16LE(BME280_REGISTER_DIG_P4);
		digP5 = readS16LE(BME280_REGISTER_DIG_P5);
		digP6 = readS16LE(BME280_REGISTER_DIG_P6);
		digP7 = readS16LE(BME280_REGISTER_DIG_P7);
		digP8 = readS16LE(BME280_REGISTER_DIG_P8);
		digP9 = readS16LE(BME280_REGISTER_DIG_P9);

		digH1 = readU8(BME280_REGISTER_DIG_H1);
		digH2 = readS16LE(BME280_REGISTER_DIG_H2);
		digH3 = readU8(BME280_REGISTER_DIG_H3);
		digH6 = readS8(BME280_REGISTER_DIG_H7);

		int h4 = readS8(BME280_REGISTER_DIG_H4);
		h4 = (h4 << 24) >> 20;
		digH4 = h4 | (readU8(BME280_REGISTER_DIG_H5) & 0x0F);

		int h5 = readS8(BME280_REGISTER_DIG_H6);
		h5 = (h5 << 24) >> 20;
		digH5 = h5 | (readU8(BME280_REGISTER_DIG_H5) >> 4 & 0x0F);

		if (log.finestLevel)
			showCalibrationData();
	}

	private String displayRegister(int reg) {
		return String.format("0x%s (%d)", StringUtils.lpad(Integer.toHexString(reg & 0xFFFF).toUpperCase(), 4, "0"), reg);
	}

	private void showCalibrationData() {
		// Displays the calibration values for debugging purposes
		
		log.finest(Session.DEVICE,"======================");
		log.finest(Session.DEVICE,"DBG: T1 = " + displayRegister(digT1));
		log.finest(Session.DEVICE,"DBG: T2 = " + displayRegister(digT2));
		log.finest(Session.DEVICE,"DBG: T3 = " + displayRegister(digT3));
		log.finest(Session.DEVICE,"----------------------");
		log.finest(Session.DEVICE,"DBG: P1 = " + displayRegister(digP1));
		log.finest(Session.DEVICE,"DBG: P2 = " + displayRegister(digP2));
		log.finest(Session.DEVICE,"DBG: P3 = " + displayRegister(digP3));
		log.finest(Session.DEVICE,"DBG: P4 = " + displayRegister(digP4));
		log.finest(Session.DEVICE,"DBG: P5 = " + displayRegister(digP5));
		log.finest(Session.DEVICE,"DBG: P6 = " + displayRegister(digP6));
		log.finest(Session.DEVICE,"DBG: P7 = " + displayRegister(digP7));
		log.finest(Session.DEVICE,"DBG: P8 = " + displayRegister(digP8));
		log.finest(Session.DEVICE,"DBG: P9 = " + displayRegister(digP9));
		log.finest(Session.DEVICE,"----------------------");
		log.finest(Session.DEVICE,"DBG: H1 = " + displayRegister(digH1));
		log.finest(Session.DEVICE,"DBG: H2 = " + displayRegister(digH2));
		log.finest(Session.DEVICE,"DBG: H3 = " + displayRegister(digH3));
		log.finest(Session.DEVICE,"DBG: H4 = " + displayRegister(digH4));
		log.finest(Session.DEVICE,"DBG: H5 = " + displayRegister(digH5));
		log.finest(Session.DEVICE,"DBG: H6 = " + displayRegister(digH6));
		log.finest(Session.DEVICE,"======================");
	}

	private int readRawTemp() throws Exception {
		// Reads the raw (uncompensated) temperature from the sensor
		int meas = mode;
		log.finest(Session.DEVICE, String.format("readRawTemp: 1 - meas=%d", meas));
		bme280Device.write(BME280_REGISTER_CONTROL_HUM, (byte) meas); // HUM ?
		meas = mode << 5 | mode << 2 | 1;
		log.finest(Session.DEVICE, String.format("readRawTemp: 2 - meas=%d", meas));
		bme280Device.write(BME280_REGISTER_CONTROL, (byte) meas);

		double sleepTime = 0.00125 + 0.0023 * (1 << mode);
		sleepTime = sleepTime + 0.0023 * (1 << mode) + 0.000575;
		sleepTime = sleepTime + 0.0023 * (1 << mode) + 0.000575;
		waitfor((long) (sleepTime * 1000L));
		int msb = readU8(BME280_REGISTER_TEMP_DATA);
		int lsb = readU8(BME280_REGISTER_TEMP_DATA + 1);
		int xlsb = readU8(BME280_REGISTER_TEMP_DATA + 2);
		int raw = ((msb << 16) | (lsb << 8) | xlsb) >> 4;
		log.finest(Session.DEVICE, "DBG: Raw Temp: " + (raw & 0xFFFF) + ", " + raw + String.format(", msb: 0x%04X lsb: 0x%04X xlsb: 0x%04X", msb, lsb, xlsb));
		return raw;
	}

	private int readRawPressure() throws Exception {
		// Reads the raw (uncompensated) pressure level from the sensor
		int msb = readU8(BME280_REGISTER_PRESSURE_DATA);
		int lsb = readU8(BME280_REGISTER_PRESSURE_DATA + 1);
		int xlsb = readU8(BME280_REGISTER_PRESSURE_DATA + 2);
		int raw = ((msb << 16) | (lsb << 8) | xlsb) >> 4;
		log.finest(Session.DEVICE, "DBG: Raw Press: " + (raw & 0xFFFF) + ", " + raw + String.format(", msb: 0x%04X lsb: 0x%04X xlsb: 0x%04X", msb, lsb, xlsb));
		return raw;
	}

	private int readRawHumidity() throws Exception {
		int msb = readU8(BME280_REGISTER_HUMIDITY_DATA);
		int lsb = readU8(BME280_REGISTER_HUMIDITY_DATA + 1);
		// raw:
		return (msb << 8) | lsb;
	}

	public float readTemperature() throws Exception {
		// Gets the compensated temperature in degrees celcius
		float ut = readRawTemp();
		float var1 = 0;
		float var2 = 0;
		float temp = 0.0f;

		// Read raw temp before aligning it with the calibration values
		var1 = (ut / 16384.0f - digT1 / 1024.0f) * digT2;
		var2 = ((ut / 131072.0f - digT1 / 8192.0f) * (ut / 131072.0f - digT1 / 8192.0f)) * digT3;
		tFine = (int) (var1 + var2);
		temp = (var1 + var2) / 5120.0f;
		log.finest(Session.DEVICE, "DBG: Calibrated temperature = " + temp + " C");
		return temp;
	}

	public float readPressure() throws Exception {
		// Gets the compensated pressure in pascal
		int adc = readRawPressure();
		log.finest(Session.DEVICE, "ADC:" + adc + ", tFine:" + tFine);
		float var1 = (tFine / 2.0f) - 64000.0f;
		float var2 = var1 * var1 * (digP6 / 32768.0f);
		var2 = var2 + var1 * digP5 * 2.0f;
		var2 = (var2 / 4.0f) + (digP4 * 65536.0f);
		var1 = (digP3 * var1 * var1 / 524288.0f + digP2 * var1) / 524288.0f;
		var1 = (1.0f + var1 / 32768.0f) * digP1;
		if (var1 == 0f)
			return 0f;
		float p = 1048576.0f - adc;
		p = ((p - var2 / 4096.0f) * 6250.0f) / var1;
		var1 = digP9 * p * p / 2147483648.0f;
		var2 = p * digP8 / 32768.0f;
		p = p + (var1 + var2 + digP7) / 16.0f;
		log.finest(Session.DEVICE, "DBG: Pressure = " + p + " Pa");
		return p;
	}

	public float readHumidity() throws Exception {
		int adc = readRawHumidity();
		float h = tFine - 76800.0f;
		h = (adc - (digH4 * 64.0f + digH5 / 16384.8f * h)) *
						(digH2 / 65536.0f * (1.0f + digH6 / 67108864.0f * h * (1.0f + digH3 / 67108864.0f * h)));
		h = h * (1.0f - digH1 * h / 524288.0f);
		if (h > 100)
			h = 100;
		else if (h < 0)
			h = 0;
		log.finest(Session.DEVICE, "DBG: Humidity = " + h);
		return h;
	}

	private int standardSeaLevelPressure = 101325;

	public void setStandardSeaLevelPressure(int standardSeaLevelPressure) {
		this.standardSeaLevelPressure = standardSeaLevelPressure;
	}

	public double readAltitude() throws Exception {
		// "Calculates the altitude in meters"
		double altitude = 0.0;
		float pressure = readPressure();
		altitude = 44330.0 * (1.0 - Math.pow(pressure / standardSeaLevelPressure, 0.1903));
		log.finest(Session.DEVICE, "DBG: Altitude = " + altitude);
		return altitude;
	}

	protected static void waitfor(long howMuch) {
		try {
			Thread.sleep(howMuch);
		} catch (InterruptedException ie) {
			log.severe(Session.DEVICE, ie.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}
	}

	public static void main(String[] args) throws I2CFactory.UnsupportedBusNumberException {
		final NumberFormat nf = new DecimalFormat("##00.00");
		BME280 sensor = new BME280();
		float press = 0;
		float temp = 0;
		float hum = 0;
//		double alt = 0;

		try {
			temp = sensor.readTemperature();
		} catch (Exception ex) {
			log.severe(Session.DEVICE, ex.getMessage());
		}
		try {
			press = sensor.readPressure();
		} catch (Exception ex) {
			log.severe(Session.DEVICE, ex.getMessage());
		}
	  /*
    sensor.setStandardSeaLevelPressure((int)press); // As we ARE at the sea level (in San Francisco).
    try { alt = sensor.readAltitude(); } 
    catch (Exception ex) 
    { 
      System.err.println(ex.getMessage()); 
      ex.printStackTrace();
    }
    */
		try {
			hum = sensor.readHumidity();
		} catch (Exception ex) {
			log.severe(Session.DEVICE, ex.getMessage());
		}

		log.info(Session.DEVICE, "Temperature: " + nf.format(temp) + " C");
		log.info(Session.DEVICE, "Pressure   : " + nf.format(press / 100) + " hPa");
		log.info(Session.DEVICE, "Humidity   : " + nf.format(hum) + " %");
		// Bonus : CPU Temperature
		try {
			log.info(Session.DEVICE, "CPU Temperature   :  " + SystemInfo.getCpuTemperature());
			log.info(Session.DEVICE, "CPU Core Voltage  :  " + SystemInfo.getCpuVoltage());
		} catch (InterruptedException ie) {
			log.severe(Session.DEVICE, ie.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		} catch (IOException e) {
			log.severe(Session.DEVICE, e.getMessage());
		}
	}
}
