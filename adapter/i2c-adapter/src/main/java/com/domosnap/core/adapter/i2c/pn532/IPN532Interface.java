package com.domosnap.core.adapter.i2c.pn532;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public interface IPN532Interface {
  
  byte PN532_PREAMBLE = 0x00;
  byte PN532_STARTCODE1 = 0x00;
  byte PN532_STARTCODE2 = (byte) 0xFF;
  byte PN532_POSTAMBLE = 0x00;

  byte PN532_HOSTTOPN532 = (byte) 0xD4;
  byte PN532_PN532TOHOST = (byte) 0xD5;

	void begin();

	void wakeup();

	CommandStatus writeCommand(byte[] header, byte[] body)
			throws InterruptedException;

	CommandStatus writeCommand(byte header[]) throws InterruptedException;

	int readResponse(byte[] buffer, int expectedLength,
			int timeout) throws InterruptedException;

	int readResponse(byte[] buffer, int expectedLength)
			throws InterruptedException;
	

}
