package com.domosnap.core.adapter.i2c.vcnl4000;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.IOException;

import com.domosnap.core.adapter.i2c.EndianReaders;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.system.SystemInfo;

/*
 * Proximity sensor
 */
public class VCNL4000 {
	
	private static Log log = new Log(VCNL4000.class.getSimpleName());
	
	private static final EndianReaders.Endianness VCNL4000_ENDIANNESS = EndianReaders.Endianness.BIG_ENDIAN;
	/*
	Prompt> sudo i2cdetect -y 1
			 0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
	00:          -- -- -- -- -- -- -- -- -- -- -- -- --
	10: -- -- -- 13 -- -- -- -- -- -- -- -- -- -- -- --
	20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
	70: -- -- -- -- -- -- -- --
	 */
	// This next addresses is returned by "sudo i2cdetect -y 1", see above.
	public static final int VCNL4000_ADDRESS = 0x13;
	// Commands
	public static final int VCNL4000_COMMAND = 0x80;
	public static final int VCNL4000_PRODUCTID = 0x81;
	public static final int VCNL4000_IRLED = 0x83;
	public static final int VCNL4000_AMBIENTPARAMETER = 0x84;
	public static final int VCNL4000_AMBIENTDATA = 0x85;
	public static final int VCNL4000_PROXIMITYDATA = 0x87;
	public static final int VCNL4000_SIGNALFREQ = 0x89;
	public static final int VCNL4000_PROXINITYADJUST = 0x8A;

	public static final int VCNL4000_3M125 = 0x00;
	public static final int VCNL4000_1M5625 = 0x01;
	public static final int VCNL4000_781K25 = 0x02;
	public static final int VCNL4000_390K625 = 0x03;

	public static final int VCNL4000_MEASUREAMBIENT = 0x10;
	public static final int VCNL4000_MEASUREPROXIMITY = 0x08;
	public static final int VCNL4000_AMBIENTREADY = 0x40;
	public static final int VCNL4000_PROXIMITYREADY = 0x20;


	private I2CBus bus;
	private I2CDevice vcnl4000Device;

	public VCNL4000() throws I2CFactory.UnsupportedBusNumberException {
		this(VCNL4000_ADDRESS);
	}

	public VCNL4000(int address) throws I2CFactory.UnsupportedBusNumberException {
		try {
			// Get i2c bus
			bus = I2CFactory.getInstance(I2CBus.BUS_1); // Depends onthe RasPi version
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "Connected to bus. OK.");
			}
			// Get device itself
			vcnl4000Device = bus.getDevice(address);
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "Connected to device. OK.");
			}
			vcnl4000Device.write(VCNL4000_IRLED, (byte) 20); // 20 * 10mA = 200mA. Range [10-200], by step of 10.
			try {
				int irLed = readU8(VCNL4000_IRLED);
				if (log.finestLevel) {
					log.finest(Session.DEVICE, "IR LED Current = " + (irLed * 10) + " mA");
				}
			} catch (Exception ex) {
				log.severe(Session.DEVICE, ex.getMessage());
			}

			try {
//      vcnl4000.write(VCNL4000_SIGNALFREQ, (byte)VCNL4000_390K625);
				int freq = readU8(VCNL4000_SIGNALFREQ);
				switch (freq) {
					case VCNL4000_3M125:
						log.finest(Session.DEVICE, "Proximity measurement frequency = 3.125 MHz");
						break;
					case VCNL4000_1M5625:
						log.finest(Session.DEVICE, "Proximity measurement frequency = 1.5625 MHz");
						break;
					case VCNL4000_781K25:
						log.finest(Session.DEVICE, "Proximity measurement frequency = 781.25 KHz");
						break;
					case VCNL4000_390K625:
						log.finest(Session.DEVICE, "Proximity measurement frequency = 390.625 KHz");
						break;
					default:
				}
			} catch (Exception ex) {
				log.severe(Session.DEVICE, ex.getMessage());
			}

			vcnl4000Device.write(VCNL4000_PROXINITYADJUST, (byte) 0x81);
			try {
				int reg = readU8(VCNL4000_PROXINITYADJUST);
				System.out.println("Proximity adjustment register = " + toHex(reg));
			} catch (Exception ex) {
				log.severe(Session.DEVICE, ex.getMessage());
			}
		} catch (IOException e) {
			log.severe(Session.DEVICE, e.getMessage());
		}
	}

	private int readU8(int reg) throws Exception {
		int result = this.vcnl4000Device.read(reg);
		try {
			Thread.sleep(0, 170_000);
		} catch (InterruptedException e) {
			log.severe(Session.DEVICE, e.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}  // 170 microseconds
		if (log.finestLevel) {
			log.finest(Session.DEVICE, "(U8) I2C: Device " + toHex(VCNL4000_ADDRESS) + " returned " + toHex(result) + " from reg " + toHex(reg));
		}
		return result;
	}

	private int readU16(int register) throws Exception {
		int hi = this.readU8(register);
		int lo = this.readU8(register + 1);
		int result = (VCNL4000_ENDIANNESS == EndianReaders.Endianness.BIG_ENDIAN) ? (hi << 8) + lo : (lo << 8) + hi; // Little endian for VCNL4000
		if (log.finestLevel) {
			log.finest(Session.DEVICE, "(U16) I2C: Device " + toHex(VCNL4000_ADDRESS) + " returned " + toHex(result) + " from reg " + toHex(register));
		}
		return result;
	}

	public int readProximity() throws Exception {
		int prox = 0;
		vcnl4000Device.write(VCNL4000_COMMAND, (byte) VCNL4000_MEASUREPROXIMITY);
		boolean keepTrying = true;
		while (keepTrying) {
			int cmd = this.readU8(VCNL4000_COMMAND);
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "DBG: Proximity: " + (cmd & 0xFFFF) + ", " + cmd + " (" + VCNL4000_PROXIMITYREADY + ")");
			}
			if (((cmd & 0xff) & VCNL4000_PROXIMITYREADY) != 0) {
				keepTrying = false;
				prox = this.readU16(VCNL4000_PROXIMITYDATA);
			} else {
				waitfor(10);  // Wait 10 ms
			}
		}
		return prox;
	}

	public int readAmbient() throws Exception {
		int ambient = 0;
		vcnl4000Device.write(VCNL4000_COMMAND, (byte) VCNL4000_MEASUREAMBIENT);
		boolean keepTrying = true;
		while (keepTrying) {
			int cmd = this.readU8(VCNL4000_COMMAND);
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "DBG: Ambient: " + (cmd & 0xFFFF) + ", " + cmd + " (" + VCNL4000_AMBIENTREADY + ")");
			}
			if (((cmd & 0xff) & VCNL4000_AMBIENTREADY) != 0) {
				keepTrying = false;
				ambient = this.readU16(VCNL4000_AMBIENTDATA);
			} else {
				waitfor(10);  // Wait 10 ms
			}
		}
		return ambient;
	}

	public static final int AMBIENT_INDEX = 0;
	public static final int PROXIMITY_INDEX = 1;

	public int[] readAmbientProximity() throws Exception {
		int prox = 0;
		int ambient = 0;
		vcnl4000Device.write(VCNL4000_COMMAND, (byte) (VCNL4000_MEASUREPROXIMITY | VCNL4000_MEASUREAMBIENT));
		boolean keepTrying = true;
		while (keepTrying) {
			int cmd = this.readU8(VCNL4000_COMMAND);
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "DBG: Proximity: " + (cmd & 0xFFFF) + ", " + cmd + " (" + VCNL4000_PROXIMITYREADY + ")");
			}
			if (((cmd & 0xff) & VCNL4000_PROXIMITYREADY) != 0 && ((cmd & 0xff) & VCNL4000_AMBIENTREADY) != 0) {
				keepTrying = false;
				ambient = this.readU16(VCNL4000_AMBIENTDATA);
				prox = this.readU16(VCNL4000_PROXIMITYDATA);
			} else {
				waitfor(10);  // Wait 10 ms
			}
		}
		return new int[]{ambient, prox};
	}

	private static String toHex(int i) {
		String s = Integer.toString(i, 16).toUpperCase();
		while (s.length() % 2 != 0)
			s = "0" + s;
		return "0x" + s;
	}

	private static boolean go = true;

	private static int minProx = Integer.MAX_VALUE;
	private static int minAmbient = Integer.MAX_VALUE;
	private static int maxProx = Integer.MIN_VALUE;
	private static int maxAmbient = Integer.MIN_VALUE;

	public static void main(String... args) throws I2CFactory.UnsupportedBusNumberException {
		VCNL4000 sensor = new VCNL4000();
		int prox = 0;
		int ambient = 0;

		// Bonus : CPU Temperature
		try {
			if (log.finestLevel) {
				log.finest(Session.DEVICE, "CPU Temperature   :  " + SystemInfo.getCpuTemperature());
				log.finest(Session.DEVICE, "CPU Core Voltage  :  " + SystemInfo.getCpuVoltage());
			}
		} catch (InterruptedException ie) {
			log.severe(Session.DEVICE, ie.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		} catch (IOException e) {
			log.severe(Session.DEVICE, e.getMessage());
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				go = false;
				if (log.finestLevel) {
					log.finest(Session.DEVICE, "\nBye");
					log.finest(Session.DEVICE, "Proximity between " + minProx + " and " + maxProx);
					log.finest(Session.DEVICE, "Ambient between " + minAmbient + " and " + maxAmbient);
				}
			}
		});
		log.finest(Session.DEVICE, "-- Ready --");
		while (go) { //  && i++ < 5)
			try {
				int[] data = sensor.readAmbientProximity();
				prox = data[PROXIMITY_INDEX];
				ambient = data[AMBIENT_INDEX];

				maxProx = Math.max(prox, maxProx);
				maxAmbient = Math.max(ambient, maxAmbient);
				minProx = Math.min(prox, minProx);
				minAmbient = Math.min(ambient, minAmbient);
			} catch (Exception ex) {
				log.severe(Session.DEVICE, ex.getMessage());
			}
			log.info(Session.DEVICE, "Ambient:" + ambient + ", Proximity: " + prox);
			try {
				Thread.sleep(100L);
			} catch (InterruptedException ex) {
				log.severe(Session.DEVICE, ex.getMessage());
				// Restore interrupted state...
			    Thread.currentThread().interrupt();
			}
		}
	}
	
	protected static void waitfor(long howMuch) {
		try {
			Thread.sleep(howMuch);
		} catch (InterruptedException ie) {
			log.severe(Session.DEVICE, ie.getMessage());
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}
	}
}
