package com.domosnap.core.adapter.i2c.veml6070;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     and helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class VEML6070 {

	private static Log log = new Log(VEML6070.class.getSimpleName());
	
	// This next addresses is returned by "sudo i2cdetect -y 1", see above.
	public static final int VEML6070_I2CADDR_READ = 0x38;
	
	private I2CBus bus;
	private I2CDevice veml6070Device;

	public VEML6070() throws I2CFactory.UnsupportedBusNumberException {
		this(VEML6070_I2CADDR_READ);
	}

	public VEML6070(int address) throws I2CFactory.UnsupportedBusNumberException {
		try {
			// Get i2c bus
			bus = I2CFactory.getInstance(I2CBus.BUS_1); // Depends onthe RasPI version
			log.finest(Session.DEVICE, "Connected to bus. OK.");

			// Get device itself
			veml6070Device = bus.getDevice(address);
			log.finest(Session.DEVICE, "Connected to device. OK.");

			// Select command register
			// Integration time = 0.5T, shutdown mode disabled
			veml6070Device.write(((byte)0x02));
			Thread.sleep(500);
			
		} catch (IOException e) {
			log.severe(Session.DEVICE, e.getMessage());
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			log.severe(Session.DEVICE, e.getMessage());
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	public int readUv() {
		try {
			// Read 2 bytes of data
			// uvlight msb, uvlight lsb
			byte[] data = new byte[2];
			data[0] = (byte)veml6070Device.read(0x73);
			data[1] = (byte)veml6070Device.read(0x71);
			// Convert the data to uvlight
			return (((data[0] & 0xFF)) * 256) + (data[1] & 0xFF);
		} catch (Exception e) {
			log.severe(Session.DEVICE, e.getMessage());
			return 0;
		}
	}

}
