package com.domosnap.core.adapter.i2c.vl53l0x;

import java.io.IOException;

import com.pi4j.io.i2c.I2CFactory;

public class Main {
	// For tests
		public static void main(String... args) {
			try {
				VL53L0X vl53l0x = new VL53L0X();
				int previousDist = -1;
				while (true) {
					int mm = vl53l0x.range();
					if (previousDist != mm) {
						System.out.println(String.format("Range: %d mm", mm));
					}
					previousDist = mm;
					try {
						Thread.sleep(50L);
					} catch (InterruptedException iex) {
						// Restore interrupted state...
					    Thread.currentThread().interrupt();
					}
				}
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} catch (I2CFactory.UnsupportedBusNumberException ubne) {
				ubne.printStackTrace();
			}
		}
}
