package com.domosnap.core.adapter.zigbee;

/*
 * #%L
 * DomoSnap zigbee adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.*;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class ZigbeeAdapter implements IAdapter {

	public static final String SCHEME = "zigbee";
	
	// zbee://dongle:10/1
	
	private ZigbeeMonitorPublisher monitor;
	private ZigbeeCommanderConsumer commander;
	private List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
	private boolean commanderConnected = false;
	
	@Override
	public URI getURI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setURI(URI uri) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Flowable<Event> createMonitor() {
		return Flowable.create(monitor, BackpressureStrategy.DROP).share();
	}

	@Override
	public PublishProcessor<Command> createCommander() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(commander);
		return commanderStream;
	}

	@Override
	public PublishProcessor<Query> createQuery() {
		return null; // TODO
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public void addCommanderConnectionListener(OnConnectionListener listener) {
		commander.addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(OnConnectionListener listener) {
		commander.removeConnectionListener(listener);
	}
	
	@Override
	public void addMonitorConnectionListener(OnConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(OnConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}

	@Override
	public void onCreatedDevice(Device device) {
		// Nothing to do on created device.	
	}
	
	@Override
	public ConnectionStatusEnum connectCommander() {
		for (OnConnectionListener connectionListener : connectionListenerList) {
			connectionListener.onConnect(ConnectionStatusEnum.CONNECTED);
		}
		commanderConnected = true;
		return ConnectionStatusEnum.CONNECTED;
	}

	@Override
	public ConnectionStatusEnum connectMonitor() {
		return monitor.connect(null);
	}

	@Override
	public boolean isCommanderConnected() {
		return commanderConnected;
	}

	@Override
	public boolean isMonitorConnected() {
		return monitor.isConnected();
	}

	@Override
	public void disconnectCommander() {
		commanderConnected = false;
	}

	@Override
	public void disconnectMonitor() {
		monitor.disconnect();
	}

	@Override
	public void onDestroyedDevice(Device device) {

	}

	@Override
	public void close() throws IOException {

	}
}
