package com.domosnap.core.adapter.zigbee;

/*
 * #%L
 * DomoSnap zigbee adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.zsmartsystems.zigbee.ZigBeeCommand;
import com.zsmartsystems.zigbee.ZigBeeNetworkManager;
import com.zsmartsystems.zigbee.dongle.cc2531.ZigBeeDongleTiCc2531;

import io.reactivex.rxjava3.functions.Consumer;

public class ZigbeeCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(this.getClass().getSimpleName());
	private final List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
       
	public ZigbeeCommanderConsumer() {
	    
	}

	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.MONITOR, "Command unsupported (null).");
			return;
		}
		
		
			ZigBeeCommand z = null;
			ZigBeeDongleTiCc2531 d = new ZigBeeDongleTiCc2531(null);
			
			ZigBeeNetworkManager nm = new ZigBeeNetworkManager(d);
			
//			nm.
//			ZigBeeDiscoveryExtension dic = new ZigBeeDiscoveryExtension();
//			dic.extensionInitialize(nm);
//			dic.
			
			
	}

	public boolean connect() {
		return true;
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
	}

	public boolean isConnected() {
		return true;
	}

	public void addConnectionListener(OnConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(OnConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}
	
}
