package com.domosnap.core.adapter.zigbee;

/*
 * #%L
 * DomoSnap zigbee adapter
 * %%
 * Copyright (C) 2016 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.Event;
import com.zsmartsystems.zigbee.ZigBeeCommand;
import com.zsmartsystems.zigbee.ZigBeeCommandListener;
import com.zsmartsystems.zigbee.ZigBeeNetworkManager;
import com.zsmartsystems.zigbee.ZigBeeStatus;
import com.zsmartsystems.zigbee.dongle.cc2531.ZigBeeDongleTiCc2531;
import com.zsmartsystems.zigbee.serial.ZigBeeSerialPort;
import com.zsmartsystems.zigbee.serialization.DefaultDeserializer;
import com.zsmartsystems.zigbee.serialization.DefaultSerializer;
import com.zsmartsystems.zigbee.transport.ZigBeePort.FlowControl;
import com.zsmartsystems.zigbee.zcl.clusters.general.ReportAttributesCommand;
import com.zsmartsystems.zigbee.zcl.field.AttributeReport;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class ZigbeeMonitorPublisher implements FlowableOnSubscribe<Event> {

	private List<FlowableEmitter<Event>> suscriberList = Collections.synchronizedList(new ArrayList<>());
	private List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
	protected final Log log = new Log(this.getClass().getSimpleName());
	private ZigBeeNetworkManager nm;
	private String device;
	
	public ZigbeeMonitorPublisher() {
	}

	void onMessageReceipt(ZigBeeCommand event) {
		try {
			
			if (event instanceof ReportAttributesCommand) {
				ReportAttributesCommand report = (ReportAttributesCommand) event;
				String where = ZigbeeAdapter.SCHEME + "://" + device + "/" + event.getSourceAddress().toString();
				AttributeReport att = report.getReports().get(0);
				if (att.getAttributeIdentifier() == 0) {
					boolean value = (Boolean) att.getAttributeValue();
					
					
					for (FlowableEmitter<Event> suscriber : suscriberList) {
//						Event command = new Event(DoorSensor.class, new Where(where), new What(DoorSensorStateName.status.name(), new OnOffState(String.valueOf(value)))); // TODO Replace null with component!!!
		//				if (event.getState().isHigh()) {
		//					command = new Command(Who.LIGHT, new What(LightStateName.status.name(), OnOffState.On()), new Where("GPIO_01", "GPIO_01"), Type.ACTION, null);
		//				} else {
		//					command = new Command(Who.LIGHT, new What(LightStateName.status.name(), OnOffState.Off()), new Where("GPIO_01", "GPIO_01"), Type.ACTION, null);
		//				}
//						suscriber.onNext(command);
					}
				}
			}
		} catch (Exception e) {
			log.severe(Session.COMMAND, "Exception occurs with message ["
					+ String.valueOf(event) + "]. Message dropped. " + e.getMessage());
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Event> e) throws Exception {
		suscriberList.add(e);
	}

	
	public ConnectionStatusEnum connect(String device) {
		this.device = device;
		ZigBeeSerialPort port = new ZigBeeSerialPort(device, 9600, FlowControl.FLOWCONTROL_OUT_NONE);
		ZigBeeDongleTiCc2531 d = new ZigBeeDongleTiCc2531(port);
		nm = new ZigBeeNetworkManager(d);
		ZigBeeStatus status =  nm.initialize();
		if (ZigBeeStatus.SUCCESS.equals(status)) {
			nm.startup(true); // TODO manage connected status from the return	
			
			nm.addCommandListener(new ZigBeeCommandListener() {
				
				@Override
				public void commandReceived(ZigBeeCommand command) {
					onMessageReceipt(command);
				}
			});
			
			nm.setSerializer(DefaultSerializer.class, DefaultDeserializer.class);
			log.finest(Session.COMMAND,"Connected on device [" + device + "].");
			return ConnectionStatusEnum.CONNECTED;
		} else {
			log.finest(Session.COMMAND,"Not connected on device [" + device + "].");
			return ConnectionStatusEnum.CONNECTION_REFUSED;
		}
		
//		nm.addAnnounceListener(new ZigBeeAnnounceListener() {
//			
//			@Override
//			public void deviceStatusUpdate(ZigBeeNodeStatus deviceStatus, Integer networkAddress, IeeeAddress ieeeAddress) {
//				System.out.println(deviceStatus);
//				
//			}
//		});
		
		
		
//		nm.permitJoin(254);

//		System.out.println("end");
		
		
		
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		nm.shutdown();
		nm = null;
		this.device = null;
	}

	public boolean isConnected() {
		return nm != null;
	}

	public void addConnectionListener(OnConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(OnConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	protected ExecutorService getExecutorService() {
		return Executors.newSingleThreadExecutor();
	}	
}
