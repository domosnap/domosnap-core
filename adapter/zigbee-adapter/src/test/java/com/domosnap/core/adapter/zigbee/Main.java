package com.domosnap.core.adapter.zigbee;

/*
 * #%L
 * DomoSnap zigbee adapter
 * %%
 * Copyright (C) 2016 - 2021 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.zsmartsystems.zigbee.IeeeAddress;
import com.zsmartsystems.zigbee.ZigBeeAnnounceListener;
import com.zsmartsystems.zigbee.ZigBeeCommand;
import com.zsmartsystems.zigbee.ZigBeeCommandListener;
import com.zsmartsystems.zigbee.ZigBeeEndpoint;
import com.zsmartsystems.zigbee.ZigBeeNetworkManager;
import com.zsmartsystems.zigbee.ZigBeeNetworkNodeListener;
import com.zsmartsystems.zigbee.ZigBeeNode;
import com.zsmartsystems.zigbee.ZigBeeNodeStatus;
import com.zsmartsystems.zigbee.dongle.cc2531.ZigBeeDongleTiCc2531;
import com.zsmartsystems.zigbee.serial.ZigBeeSerialPort;
import com.zsmartsystems.zigbee.serialization.DefaultDeserializer;
import com.zsmartsystems.zigbee.serialization.DefaultSerializer;
import com.zsmartsystems.zigbee.transport.ZigBeePort.FlowControl;
import com.zsmartsystems.zigbee.zcl.clusters.ZclDoorLockCluster;
import com.zsmartsystems.zigbee.zcl.clusters.doorlock.LockDoorCommand;
import com.zsmartsystems.zigbee.zcl.clusters.general.ReportAttributesCommand;

import jssc.SerialPortList;

public class Main {

	public static void main(String[] args) {
		
		  String[] portNames = SerialPortList.getPortNames();
		  System.out.println(portNames.length);
	        for(int i = 0; i < portNames.length; i++){
	            System.out.println(portNames[i]);
	        }
		
		ZigBeeSerialPort port = new ZigBeeSerialPort("/dev/ttyACM0", 9600, FlowControl.FLOWCONTROL_OUT_NONE);
		
		ZigBeeDongleTiCc2531 d = new ZigBeeDongleTiCc2531(port);
		
//		System.out.println(d.initialize());
//		System.out.println(d.getIeeeAddress());
//		System.out.println(d.getVersionString());
		ZigBeeNetworkManager nm = new ZigBeeNetworkManager(d);
		System.out.println(nm.initialize());
		System.out.println(nm.getNetworkState());
		for (ZigBeeNode node : nm.getNodes()) {
			System.out.println(node.getIeeeAddress().toString());
		}
		nm.addAnnounceListener(new ZigBeeAnnounceListener() {
			
			@Override
			public void deviceStatusUpdate(ZigBeeNodeStatus deviceStatus, Integer networkAddress, IeeeAddress ieeeAddress) {
				System.out.println("Announce: " + deviceStatus);
				
			}
		});
		
		
		nm.addCommandListener(new ZigBeeCommandListener() {
			
			@Override
			public void commandReceived(ZigBeeCommand command) {
				// TODO Auto-generated method stub
				System.out.println("Command: " + command);
				System.out.println("Aps security: " + command.getApsSecurity());
				System.out.println("Cluster id: " + command.getClusterId());
				System.out.println("destination: " + command.getDestinationAddress());
				System.out.println("source: " + command.getSourceAddress());
				System.out.println("transaction id: " + command.getTransactionId());
				if (command instanceof LockDoorCommand) {
					System.out.println("BINGO!!!");
				}
				if (command instanceof ReportAttributesCommand) {
					System.out.println("Direction: " + ((ReportAttributesCommand) command).getCommandDirection());
					System.out.println("Command: " + ((ReportAttributesCommand) command).getCommandId());
					System.out.println("Reports: " + ((ReportAttributesCommand) command).getReports());
					System.out.println("RValue: " + ((ReportAttributesCommand) command).getReports().get(0).getAttributeValue());
				}
			}
		});
		
		nm.addNetworkNodeListener(new ZigBeeNetworkNodeListener() {
			
			@Override
			public void nodeUpdated(ZigBeeNode node) {
				// TODO Auto-generated method stub
				System.out.println("updated" + node);
			}
			
			@Override
			public void nodeRemoved(ZigBeeNode node) {
				// TODO Auto-generated method stub
				System.out.println("remove: " + node);
			}
			
			@Override
			public void nodeAdded(ZigBeeNode node) {
				// TODO Auto-generated method stub
				System.out.println("added: " + node);
			}
		});

		
		nm.startup(true);
		System.out.println(nm.getNetworkState());
		System.out.println(nm.getLocalNwkAddress());
		System.out.println(nm.getZigBeeChannel());
//		while (ZigBeeTransportState.INITIALISING == nm.getNetworkState()) {
//			System.out.println("wait");
//		}
		System.out.println(nm.getNetworkState());
		for (ZigBeeNode node : nm.getNodes()) {
			System.out.println(node.getIeeeAddress().toString());
			node.getEndpoints().iterator().forEachRemaining(new Consumer<ZigBeeEndpoint>() {

				@Override
				public void accept(ZigBeeEndpoint t) {
					// TODO Auto-generated method stub
					System.out.println(t.toString());
				}
			});
		}

		nm.setSerializer(DefaultSerializer.class, DefaultDeserializer.class);
		nm.permitJoin(254);

		
//		System.out.println("extension");
//		ZigBeeDiscoveryExtension z = new ZigBeeDiscoveryExtension();
//		z.extensionInitialize(nm);
//		z.extensionStartup();
//		z.setUpdatePeriod(500);
//		z.refresh();

		System.out.println("end");
//		nm.addSupportedCluster(6);

		
		while(true) {
			try {
				Thread.sleep(35000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				// Restore interrupted state...
			    Thread.currentThread().interrupt();
			}
			ZclDoorLockCluster devi = new ZclDoorLockCluster(new ZigBeeEndpoint(nm.getNode(9693), 9693));
			System.out.println("State: " +devi.getActuatorEnabled(2000));
		}
		
//		while(true) {
//			try {
//				Thread.sleep(30000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			
//			System.out.println("Read attributes 0: ");
//			ReadAttributesCommand ra = new ReadAttributesCommand();
//			ra.setDestinationAddress(new ZigBeeEndpointAddress(39375, 1));
//			ra.setClusterId(6);
//			List<Integer> identifiers = new ArrayList<>();
//			identifiers.add(0);
//			ra.setIdentifiers(identifiers);
//			ra.setSourceAddress(new ZigBeeEndpointAddress(0, 1));
//
//			System.out.println(nm.sendCommand(ra));
//			
//			nm.sendTransaction(ra, new ZigBeeTransactionMatcher() {
//				
//				@Override
//				public boolean isTransactionMatch(ZigBeeCommand request, ZigBeeCommand response) {
//					System.out.println("request (read):" + request);
//					System.out.println("response (read):" + response);
//					if (request.getTransactionId() == response.getTransactionId())
//						return true;
//					return false;
//				}
//			});
//			
//			
//			System.out.println("Discover attributes:");
//			DiscoverAttributesCommand dac = new DiscoverAttributesCommand();
//			dac.setDestinationAddress(new ZigBeeEndpointAddress(39375, 1));
//			dac.setClusterId(19);
//			dac.setStartAttributeIdentifier(0);
//			dac.setMaximumAttributeIdentifiers(10);
//			dac.setSourceAddress(new ZigBeeEndpointAddress(10, 2));
////			System.out.println(nm.sendCommand(dac));
//			try {
//				System.out.println(	nm.sendTransaction(dac, new ZigBeeTransactionMatcher() {
//					
//					@Override
//					public boolean isTransactionMatch(ZigBeeCommand request, ZigBeeCommand response) {
//						System.out.println("request (discover):" + request);
//						System.out.println("response (discover):" + response);
//						if (request.getTransactionId() == response.getTransactionId())
//							return true;
//						return false;
//					}
//				}).get().isSuccess());
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}
//			
//		}
				
	}
}
