package com.domosnap.engine.api;

/*
 * #%L
 * DomoSnap Engine Rest API
 * %%
 * Copyright (C) 2017 - 2024 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.json.JSONObject;

import com.domosnap.engine.api.handler.DeviceHandler;
import com.domosnap.engine.api.tools.JSonTools;
import com.domosnap.engine.api.tools.Runner;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceHelper;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.CommandQueryJsonCodec;
import com.domosnap.engine.supervisor.DeviceRepository;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

public class ServerCoreAPI extends AbstractVerticle {
	
	private static final String APP_JSON = "application/json";
	private static final String TEXT_PLAIN = "text/plain; charset=utf-8";
	
	// Convenience method so you can run it in your IDE
	public static void main(String[] args) {
		Runner.runExample(ServerCoreAPI.class);
	}

	@Override
	public void start() {

		
		final Router router = Router.router(vertx);
		DeviceRepository dr = new DeviceRepository();
		DeviceHandler deviceHandler = new DeviceHandler(dr);
		// Authentication settings
		// final OAuth2Auth authProvider = OAuth2Auth.create(vertx, getOAuth2Options());
		
//		final JWTAuth authProvider = JWTAuth.create(vertx, getJWTAuthOptions());

		// allow route for CORS
		router.route()
			.handler(CorsHandler.create().addRelativeOrigin(".*.").allowedMethod(io.vertx.core.http.HttpMethod.GET)
				.allowedMethod(io.vertx.core.http.HttpMethod.POST)
				.allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
				.allowedMethod(io.vertx.core.http.HttpMethod.PUT).allowCredentials(true)
				.allowedHeader("Access-Control-Allow-Method").allowedHeader("Authorization")
				.allowedHeader("Access-Control-Allow-Credentials").allowedHeader("Content-Type"));
		
		// allow route for the API
		// router.route("/customers*").handler(JWTAuthHandler.create(authProvider));
		// with the front end server using the Jax-RS controller
		router.route().handler(BodyHandler.create());

		router.post("/devices").consumes(APP_JSON).produces(APP_JSON).handler(deviceHandler::createDevice);
		router.get("/devices").produces(APP_JSON).handler(deviceHandler::getAllDevices);
		router.get("/devices/:deviceId").produces(APP_JSON).handler(deviceHandler::getDeviceById);
//		router.delete("/devices").handler(controllerHandler::deleteControllers);
		router.delete("/devices/:deviceId").handler(deviceHandler::deleteDeviceById);
		router.put("/devices/:deviceId").consumes(APP_JSON).produces(APP_JSON).handler(deviceHandler::putDeviceById);
		router.get("/devices/:deviceId/:stateName").produces(TEXT_PLAIN).handler(deviceHandler::getState);
		router.put("/devices/:deviceId/:stateName").handler(deviceHandler::putState);
		
		
		// Healthcheck
		router.get("/health*")
			.handler(HealthCheckHandler.create(vertx)
					.register("health", result2 -> result2.complete(Status.OK())
		));

		// Start the front end server using the Jax-RS controller
		vertx.createHttpServer().requestHandler(router).listen(8080, ar -> 
			System.out.println("Server started on port " + ar.result().actualPort())
		);

		// Connect eventbus to websocket
		SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
		Router router2 = Router.router(vertx);
		router2.route("/command/*").handler(sockJSHandler);
		router2.route().handler(CorsHandler.create().addRelativeOrigin(".*.").allowedMethod(HttpMethod.GET));
		SockJSBridgeOptions options = new SockJSBridgeOptions().addInboundPermitted(new PermittedOptions().setAddressRegex(".*"))
				.addOutboundPermitted(new PermittedOptions().setAddressRegex(".*"));
		sockJSHandler.bridge(options);

		vertx.createHttpServer()
				// with websocket
				.requestHandler(router2).listen(8087, ar -> 
					System.out.println("Server started on port " + ar.result().actualPort())
				);


		// Start monitor websocket (controller to web)
		// TODO
//		for (Device controller : dr.getControllers()) {
//			new DeviceHelper(controller).addCommandConsumer(new Consumer<Command>() {
//				@Override
//				public void accept(Command command) {
//					vertx.eventBus().publish("domosnap-monitor-topic", command.toString());
//					System.out.println("COMMAND ENVOYEE " + command.toString());
//				}
//			});
//		}

		// Start commander websocket (web to controller)
		vertx.eventBus().consumer("domosnap-commander-topic", message -> {
			// {"timestamp":"1538753422143",
			//  "what":[{"name":"local_offset","value":"ON - 1"}],
			//  "where":openwebnet://12345@localhost:1234/2,
			//  "who":"HEATING_ADJUSTMENT",
			//  "type":"WRITE",
			//  "source":{"who":"HEATING_ADJUSTMENT",
			//            "title":"null",
			//            "description":"null",
			//            "where":"openwebnet://12345@localhost:1234/2",
			//            "states":{"measure_temperature":"20.0","local_offset":"ON - 1","status":"HEATING_MODE"}
			//	}
			// }
			JSONObject json = JSonTools.fromJson(String.valueOf(message.body()));
			Device controller = dr.getDevice(new Where(json.getString("where")));

			DeviceHelper helper = new DeviceHelper(controller);
			helper.pushCommand(CommandQueryJsonCodec.fromJson(String.valueOf(message.body())));

			System.out.println("COMMAND RECUE " + message.body());
		});

	}
}
