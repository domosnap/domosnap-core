package com.domosnap.engine.api.handler;

/*
 * #%L
 * DomoSnap Engine Rest API
 * %%
 * Copyright (C) 2017 - 2024 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import com.domosnap.engine.api.tools.ErrorTools;
import com.domosnap.engine.api.tools.HateoasTools;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceHelper;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceRepository;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class DeviceHandler {

	private static final String DEVICE_ID = "deviceId";
	private final DeviceRepository hr;
	
	public DeviceHandler(DeviceRepository dr) {
		this.hr = dr;
	}
	
	@SuppressWarnings("unchecked")
	public void createDevice(RoutingContext rc) {
		//Get customer to create
		JsonObject body = rc.getBodyAsJson();

		Optional<String> where = Optional.ofNullable(body.getString("where")); 
		Optional<String> who = Optional.ofNullable(body.getString("who"));
		Optional<String> description = Optional.ofNullable(body.getString("description"));
		Optional<String> title = Optional.ofNullable(body.getString("title"));
		
		if (where.isEmpty() || who.isEmpty())
			ErrorTools.SendError(rc, 400, "Where and Who required!");
		else {
			Device opt;
			Class<Device> deviceClass = (Class<Device>) DeviceMappingRegistry.getMapping(who.get());
			Where w = new Where(where.get());
			opt = hr.getDevice(deviceClass, w);
			if (opt == null) {
				try {
					hr.createDevice(deviceClass, w)
						.ifPresentOrElse(
							d -> {
								description.ifPresent(d::setDescription);
								title.ifPresent(d::setTitle);
								rc.response().setStatusCode(201).putHeader("Content-Type", "application/json").putHeader("Location", rc.request().absoluteURI() + "/" + URLEncoder.encode(d.getWhere().getURI().toString(), StandardCharsets.UTF_8)).end(HateoasTools.createLinkForDevice(rc, d));
							},
							() -> ErrorTools.SendError(rc, 400, "Error during creation...")
						);
				} catch (UnsupportedAdapter e) {
					ErrorTools.SendError(rc, 400, "Unsupported Adapter.");
				}
			} else {
				ErrorTools.SendError(rc, 400, "Device already existing.");
			}
		}
		
		// TODO ADD websocket!
	}
	
	public void getAllDevices(RoutingContext rc) {
		StringBuilder sb = new StringBuilder("[");

		hr.getAllDevicesMap().forEach((where, device) ->
				sb.append(HateoasTools.createLinkForDevice(rc, device)).append(",")
		);
		
		if (sb.length()>1) {
			sb.setLength(sb.length()-1);	
		}
		
		sb.append("]");
		rc.response().putHeader("Content-Type", "application/json").end(sb.toString());
	}	
	
	public void getDeviceById(RoutingContext rc)  {
		String deviceId = rc.pathParam(DEVICE_ID);
		Device d = hr.getDevice(new Where(URLDecoder.decode(deviceId, StandardCharsets.UTF_8)));
		if (d == null) {
			ErrorTools.SendError(rc, 404, "Device not found.");
		} else {
			rc.response().putHeader("Content-Type", "application/json").end(HateoasTools.createLinkForDevice(rc, d));
		}
	}

	public void deleteDeviceById(RoutingContext rc) {
		// TODO remove from websocket
		String deviceId = rc.pathParam(DEVICE_ID);
		Device result = hr.deleteDevice(new Where(URLDecoder.decode(deviceId, StandardCharsets.UTF_8)));
		if (result == null) {
			ErrorTools.SendError(rc, 404, "Device not found.");
		} else {
			rc.response().setStatusCode(204).end();
		}
	}
	
	public void putDeviceById(RoutingContext rc) {
		String deviceId = rc.pathParam(DEVICE_ID);
		
		JsonObject body = rc.getBodyAsJson();

		Optional<String> who = Optional.ofNullable(body.getString("who"));
		Optional<String> description = Optional.ofNullable(body.getString("description"));
		Optional<String> title = Optional.ofNullable(body.getString("title"));
		
		Device device = hr.getDevice(new Where(URLDecoder.decode(deviceId, StandardCharsets.UTF_8)));
		if (device == null) {
				if (who.isEmpty()) {
					ErrorTools.SendError(rc, 400, "Who is missing.");
				} else {
					try {
						@SuppressWarnings("unchecked")
						Optional<Device> d = hr.createDevice((Class<Device>) DeviceMappingRegistry.getMapping(who.get()), new Where(deviceId));
						if (d.isEmpty()) {
							ErrorTools.SendError(rc, 404, "Device not found.");
						} else {
							Device c = d.get();
							description.ifPresent(c::setDescription);
							title.ifPresent(c::setTitle);
							rc.response().setStatusCode(201).putHeader("Location", rc.request().absoluteURI() + "/" + URLEncoder.encode(c.getWhere().getURI().toString(), StandardCharsets.UTF_8)).end(HateoasTools.createLinkForDevice(rc, c));
						}
					} catch (UnsupportedAdapter e) {
						ErrorTools.SendError(rc, 400, "Unsupported adapter.");
					}

				}
		} else {
			if (who.isPresent() && !device.getClass().equals(DeviceMappingRegistry.getMapping(who.get()))) {
				ErrorTools.SendError(rc, 400, "Who doesn't match the existing device. Impossible to change who.");
			}
			description.ifPresent(device::setDescription);
			title.ifPresent(device::setTitle);
			rc.response().setStatusCode(200).end(HateoasTools.createLinkForDevice(rc, device));
		}
		// TODO ADD websocket!
	}

	
	public void getState(RoutingContext rc) {
		
		String deviceId = rc.pathParam(DEVICE_ID);
		String stateName = rc.pathParam("stateName");
		Device d = hr.getDevice(new Where(URLDecoder.decode(deviceId, StandardCharsets.UTF_8)));
		if (d == null) {
			rc.response().setStatusCode(404).end();
		} else {
			boolean stateExist = false;
			for (StateName sn : d.getStateList()) {
				if (sn.name().equalsIgnoreCase(stateName)) {
					stateExist = true;
				}
			}
			if (stateExist) {
				String state = String.valueOf(d.getStateMap().get(stateName.toUpperCase()));
				rc.response().setStatusCode(200).end(state);
			} else {
				ErrorTools.SendError(rc, 400, "Status not found.");
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void putState(RoutingContext rc) {
		String deviceId = rc.pathParam(DEVICE_ID);
		String stateName = rc.pathParam("stateName");
		String body = rc.getBodyAsString();
		Device d = hr.getDevice(new Where(URLDecoder.decode(deviceId, StandardCharsets.UTF_8)));
		if (d == null) {
			ErrorTools.SendError(rc, 404, "Device missing.");
		} else {
			Class<? extends State<?>> stateType = null;
			for (StateName sn : d.getStateList()) {
				if (sn.name().equalsIgnoreCase(stateName)) {
					stateType = sn.getType();
					break;
				}
			}
			if (stateType != null) {
				new DeviceHelper(d).set(stateName.toUpperCase(), body, (Class<State<?>>) stateType);
				String state = String.valueOf(d.getStateMap().get(stateName.toUpperCase()));
				rc.response().setStatusCode(200).end(state);
			} else {
				ErrorTools.SendError(rc, 400, "State not found.");
			}
		}
	}
}
