package com.domosnap.engine.api.tools;

/*
 * #%L
 * DomoSnap Engine Rest API
 * %%
 * Copyright (C) 2017 - 2024 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.ext.web.RoutingContext;

public class ErrorTools {

	private static final String ERR_MSG = "'{'\"timestamp\":\"{0}\",\"status\":{1},\"error\":\"{2}\",\"message\":\"{3}\",\"path\":\"{4}\"'}'";

	public static String formatError(int httpCode, String message, String path) {
		return MessageFormat.format(ERR_MSG, ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT), "" + httpCode, HttpResponseStatus.valueOf(httpCode).reasonPhrase(), message, path);
	}
	
	public static void SendError(RoutingContext rc, int httpCode, String message) {
		rc.response().putHeader("Content-Type", "application/json").setStatusCode(httpCode).end(ErrorTools.formatError(httpCode, message, rc.request().path()));
	}
}
