package com.domosnap.engine.api.tools;

/*
 * #%L
 * DomoSnap Engine Rest API
 * %%
 * Copyright (C) 2017 - 2024 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceJsonCodec;

import io.vertx.ext.web.RoutingContext;

public class HateoasTools {

	public static String createLinkForDevice(RoutingContext rc, Device d) {
		
		String self = "\",\"href\": \"".concat(rc.request().absoluteURI()).concat("/").concat(URLEncoder.encode(d.getWhere().getURI().toString(), StandardCharsets.UTF_8));
		
		StringBuilder result = new StringBuilder();
		result.append("\"_links\": [ {\"rel\": \"self").append(self).append( "\"},");
		d.getStateList().forEach(stateName ->
			result.append(" {\"rel\": \"").append(stateName.name().toLowerCase()).append(self).append("/").append(stateName.name().toUpperCase()).append("\"},")
		);
		result.setLength(result.length()-1);
		result.append("]");
		return toJson(d, result.toString());
	}
	
	private static String toJson(Device d, String link) {
		StringBuilder sb = new StringBuilder();
		DeviceJsonCodec.buildJson(d, false, sb);
		
		sb.setLength(sb.length()-1);
		if (link != null) {
			sb.append(",").append(link);
		}
		sb.append("}");
		return sb.toString();
	}
}
