         package com.domosnap.engine.api.tools;

/*
 * #%L
 * DomoSnap Engine Rest API
 * %%
 * Copyright (C) 2017 - 2024 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.json.JSONObject;

public class JSonTools {

	public final static String ERROR = "error";
	public final static String ERROR_CLASSNAME = "className";
	public final static String ERROR_MESSAGE = "message";

	public final static String formatException(Exception e) {
		return "{" + ERROR + ": [{" + ERROR_CLASSNAME + ":'" + JSONObject.quote(e.getClass().getSimpleName()) + "', " + ERROR_MESSAGE + " :'" + JSONObject.quote(e.getMessage())+ "'}]}";
	}

	public final static String formatException(String classname, String errorMsg) {
		return "{" + ERROR + ": [{" + ERROR_CLASSNAME + ":'" + JSONObject.quote(classname) + "', " + ERROR_MESSAGE + " :'" + JSONObject.quote(errorMsg)+ "'}]}";
	}

	public final static String formatNull() {
		return JSONObject.NULL.toString();
	}

	public final static JSONObject fromJson(String value)  {
		if (formatNull().equals(value))
			return null;
		return new JSONObject(value);
	}
}
