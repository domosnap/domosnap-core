package com.domosnap.webserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import com.domosnap.engine.api.ServerCoreAPI;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(VertxExtension.class)
class DeviceRestAPITest {

//	private static final String HEADER_LOCATION = "Location";
	private static final String ATTRIBUTE_WHERE = "where";
	private static final String ATTRIBUTE_WHO = "who";
	private static final String ATTRIBUTE_DESCRIPTION = "description";
	private static final String ATTRIBUTE_TITLE = "title";


	private final String host = "localhost";
	private final int port = 8080;
	
	private final String jwt ="eyJhbGciOiJSUzI1NiIsImtpZCI6ImFiZGY2YzhiY2NiNDk4NzAzZGZjOWRmYjgwNGI0ZTE2YmIyNDI1MzQiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiT2xpdmllciBEcmllc2JhY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1CSVJmT3J5dThVYy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JlSUVVd21VSldqbTNoWmlFRXZCNjlJb2N1OHN3L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9jdXJhbnRpLW12cCIsImF1ZCI6ImN1cmFudGktbXZwIiwiYXV0aF90aW1lIjoxNTYxNDk2NTQxLCJ1c2VyX2lkIjoibjVNSEcyVEx1cVliemM4QXJoNkx3VkxmaVp1MiIsInN1YiI6Im41TUhHMlRMdXFZYnpjOEFyaDZMd1ZMZmladTIiLCJpYXQiOjE1NjE0OTY1NDEsImV4cCI6MTU2MTUwMDE0MSwiZW1haWwiOiJvbGl2aWVyLmRyaWVzYmFjaEBjdXJhbnRpLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTE1Mjc0NTI4ODU2NDY0NjQ0MDUyIl0sImVtYWlsIjpbIm9saXZpZXIuZHJpZXNiYWNoQGN1cmFudGkuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.UurHAtR1EV-CR7YMpbhGmS0akgHCh4nIgV3cMQlo8GbnKBoQEKLW3gBx_PiFvxTghVRfmoKeyBw9lGt-0NZZ4ylYFyXBRWHeJYsThugmVuPQdDP2Ec4_FLcK15kGS6z2WYgK9ZFyVakQXrjUrMHe727NbN9LaaqVSzq3Il51gm1196W_fzgP22k5Lnadc53-Ghaeqg0cTjabKBrHFCmgg9cKfFmFgYtSdBohSCzozeoKolnHrCfZGBjjU2-vs_orDeyDvzElHli7Cyw7f5mjStB2te7fyD928wia4srZ9MAHJdKXlttJeKgBbn5LlSWJFdu88dX8U2qV4Jo96NtXdw";

	@BeforeAll
	@DisplayName("Deploy a verticle")
	static void prepare(Vertx vertx, VertxTestContext testContext) {
		vertx.deployVerticle(new ServerCoreAPI(), testContext.succeedingThenComplete());
	}

	@AfterEach
	@DisplayName("Check that the verticle is still there")
	void lastChecks(Vertx vertx) {
		assertThat(vertx.deploymentIDs()).isNotEmpty().hasSize(1);
	}

	@RepeatedTest(3)
	@DisplayName("Test connection")
	@Order(1)
	void testConnection(Vertx vertx, VertxTestContext testContext) {
		HttpClient client = vertx.createHttpClient();
		client.request(HttpMethod.GET, 8080, "localhost", "/health")
				.compose(req -> req.send().compose(HttpClientResponse::body))
				.onComplete(testContext.succeeding(buffer -> testContext.verify(() -> {
					assertThat(new JsonObject(buffer).getString("status")).isEqualTo("UP");
					testContext.completeNow();
				})));
	}

	@Test
	@DisplayName("Test Creation Device")
	@Order(10)
	void testCreateDevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(3);

		WebClient client = WebClient.create(vertx);
		// Create SmartDevice
		client.post(port, host, "/devices").bearerTokenAuthentication(jwt)
				.sendJsonObject(
						new JsonObject()
							.put(ATTRIBUTE_WHO, "Light")
							.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/12")
							.put(ATTRIBUTE_DESCRIPTION, "Une descrption")
							.put(ATTRIBUTE_TITLE, "title"),
						testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(201, ar.statusCode());
							smartDeviceCreated.flag();
						})));

		client.post(port, host, "/devices").bearerTokenAuthentication(jwt)
				.sendJsonObject(
						new JsonObject()
							.put(ATTRIBUTE_WHO, "Light")
							.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
							.put(ATTRIBUTE_DESCRIPTION, "Description 2")
							.put(ATTRIBUTE_TITLE, "Title 1"),
						testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(201, ar.statusCode());
							smartDeviceCreated.flag();
						})));
		
		//	Try to recreate the same..client.
		client.post(port, host, "/devices").bearerTokenAuthentication(jwt)
				.sendJsonObject(
						new JsonObject()
								.put(ATTRIBUTE_WHO, "Light")
								.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
								.put(ATTRIBUTE_DESCRIPTION, "Description 2")
								.put(ATTRIBUTE_TITLE, "Title 1"),
						testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(400, ar.statusCode());
							smartDeviceCreated.flag();
						})));
	}
	
	@Test
	@DisplayName("Test get all Device")
	@Order(20)
	void testGetAllDevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(1);

		WebClient client = WebClient.create(vertx);
		// Create SmartDevice
		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
				.send(testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(200, ar.statusCode());
							JsonArray listDevice = ar.bodyAsJsonArray();
							assertEquals(2, listDevice.size());
							assertEquals("Light", listDevice.getJsonObject(0).getString("who"));
							assertEquals("scs://12345@localhost:1234/13", listDevice.getJsonObject(0).getString("where"));
							assertEquals("\\\\type:OnOffState\\\\false", listDevice.getJsonObject(0).getJsonObject("states").getString("STATUS"));
							smartDeviceCreated.flag();
						})));
	}

	@Test
	@DisplayName("Test get a Device")
	@Order(30)
	void testGetADevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(2);

		WebClient client = WebClient.create(vertx);
		// Get unexisting device
		client.get(port, host, "/devices/13").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(404, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		// Get SmartDevice
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
				.send(testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(200, ar.statusCode());
							JsonObject listDevice = ar.bodyAsJsonObject();
							assertEquals("Light", listDevice.getString("who"));
							assertEquals("scs://12345@localhost:1234/13", listDevice.getString("where"));
							assertEquals("\\\\type:OnOffState\\\\false", listDevice.getJsonObject("states").getString("STATUS"));
							smartDeviceCreated.flag();
						})));
	}
	
	@Test
	@DisplayName("Test delete a Device")
	@Order(40)
	void testDeleteADevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(4);

		WebClient client = WebClient.create(vertx);
		// Delete unexisting device
		client.delete(port, host, "/devices/13").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(404, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		// Delete SmartDevice
		client.delete(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
				.send(testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(204, ar.statusCode());
							smartDeviceCreated.flag();
						})));
		
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(404, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
				.send(testContext.succeeding(ar -> testContext.verify(() -> {
							assertEquals(200, ar.statusCode());
							JsonArray listDevice = ar.bodyAsJsonArray();
							assertEquals(1, listDevice.size());
							smartDeviceCreated.flag();
						})));
	}

	@Test
	@DisplayName("Test modify a Device")
	@Order(50)
	void testModifyADevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(8);

		WebClient client = WebClient.create(vertx);
		// Modify unexisting device
		client.put(port, host, "/devices/13").bearerTokenAuthentication(jwt)
		.sendJsonObject(
				new JsonObject()
					.put(ATTRIBUTE_WHO, "Light")
					.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/12")
					.put(ATTRIBUTE_DESCRIPTION, "Une descrption")
					.put(ATTRIBUTE_TITLE, "title"),
				testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(404, ar.statusCode());
					assertEquals("Not Found", ar.bodyAsJsonObject().getString("error"));
					smartDeviceCreated.flag();
				})));
		
		// Modify/create unexisting device
		client.put(port, host, "/devices/13").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(400, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.sendJsonObject(
				new JsonObject()
					.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
					.put(ATTRIBUTE_DESCRIPTION, "Description 2")
					.put(ATTRIBUTE_TITLE, "Title 1"),
				testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(400, ar.statusCode());
					assertEquals("Bad Request", ar.bodyAsJsonObject().getString("error"));
					smartDeviceCreated.flag();
				})));
		
		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.sendJsonObject(
				new JsonObject()
					.put(ATTRIBUTE_WHO, "Light")
					.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
					.put(ATTRIBUTE_DESCRIPTION, "Description 2")
					.put(ATTRIBUTE_TITLE, "Title 1"),
				testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(201, ar.statusCode());
					JsonObject listDevice = ar.bodyAsJsonObject();
					assertEquals("Light", listDevice.getString("who"));
					assertEquals("scs://12345@localhost:1234/13", listDevice.getString("where"));
					assertEquals("Description 2", listDevice.getString(ATTRIBUTE_DESCRIPTION));
					assertEquals("Title 1", listDevice.getString(ATTRIBUTE_TITLE));
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonArray listDevice = ar.bodyAsJsonArray();
					assertEquals(2, listDevice.size());
					smartDeviceCreated.flag();
				})));

		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.sendJsonObject(
				new JsonObject()
					.put(ATTRIBUTE_WHO, "Light")
					.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
					.put(ATTRIBUTE_DESCRIPTION, "Description 4")
					.put(ATTRIBUTE_TITLE, "Title 3"),
				testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonObject listDevice = ar.bodyAsJsonObject();
					assertEquals("Light", listDevice.getString("who"));
					assertEquals("scs://12345@localhost:1234/13", listDevice.getString("where"));
					assertEquals("Description 4", listDevice.getString(ATTRIBUTE_DESCRIPTION));
					assertEquals("Title 3", listDevice.getString(ATTRIBUTE_TITLE));
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonArray listDevice = ar.bodyAsJsonArray();
					assertEquals(2, listDevice.size());
					smartDeviceCreated.flag();
				})));
		
		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.sendJsonObject(
				new JsonObject()
					.put(ATTRIBUTE_WHO, "Shutter")
					.put(ATTRIBUTE_WHERE, "scs://12345@localhost:1234/13")
					.put(ATTRIBUTE_DESCRIPTION, "Description 4")
					.put(ATTRIBUTE_TITLE, "Title 3"),
				testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(400, ar.statusCode());
					JsonObject listDevice = ar.bodyAsJsonObject();
					assertEquals("Bad Request", listDevice.getString("error"));
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonArray listDevice = ar.bodyAsJsonArray();
					assertEquals(2, listDevice.size());
					smartDeviceCreated.flag();
				})));
	}

	
	@Test
	@DisplayName("Test get a Device state")
	@Order(60)
	void testGetStateDevice(Vertx vertx, VertxTestContext testContext) {

		Checkpoint smartDeviceCreated = testContext.checkpoint(5);

		WebClient client = WebClient.create(vertx);
		// Get unexisting device
		client.get(port, host, "/devices/13/status").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(404, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)+ "/statuswrong").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(400, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8) + "/status").bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					assertEquals("false", ar.bodyAsString());
					smartDeviceCreated.flag();
				})));
		
		
		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8) + "/status").bearerTokenAuthentication(jwt)
		.sendBuffer(Buffer.buffer("true") ,testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonObject listDevice = ar.bodyAsJsonObject();
					assertEquals("Light", listDevice.getString("who"));
					assertEquals("scs://12345@localhost:1234/13", listDevice.getString("where"));
					assertEquals("\\\\type:OnOffState\\\\true", listDevice.getJsonObject("states").getString("STATUS"));
					smartDeviceCreated.flag();
				})));
		
		client.put(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8) + "/status").bearerTokenAuthentication(jwt)
		.sendBuffer(Buffer.buffer("false") ,testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					smartDeviceCreated.flag();
				})));
		
		client.get(port, host, "/devices/" + URLEncoder.encode("scs://12345@localhost:1234/13", StandardCharsets.UTF_8)).bearerTokenAuthentication(jwt)
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
					assertEquals(200, ar.statusCode());
					JsonObject listDevice = ar.bodyAsJsonObject();
					assertEquals("false", listDevice.getJsonObject("states").getString("STATUS"));
					smartDeviceCreated.flag();
				})));
	}
}
