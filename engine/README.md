# DomoSnap  Engine


Domosnap engine is part of domosnap, a Domotic Solution Management.
Engine is the core system to implement digital twin.

![Domosnap Engine Architecture](schema-architecture.png)

# QuickStart

## Lunch a virtual smart home installation

Clone the domosnap-core project and launch with command:

    git clone git@gitlab.com:domosnap/domosnap-core.git
    cd domosnap-cpre/OpenWebNet-Emulator
    java -jar bin/felix.jar

Requierement: JDK installed (at least JDK 13)

## Code your first interaction with light

In you pom.xml you need to add:

    <dependencies>
        <dependency>
            <groupId>com.domosnap.core</groupId>
            <artifactId>engine</artifactId>
            <version>0.2.9</version>
        </dependency>

    </dependencies>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/groups/6848729/-/packages/maven</url>
        </repository>
    </repositories>


After you can code

    try (DeviceFactory df = new DeviceFactory()) { // 1. Create a device factory
        final Light light = df.createDevice(Light.class, new Where("scs://12345@localhost:1234/12")).get(); // 2. Instanciate a light

        OnOffState status = light.getStatus(); // 3. Get the status of the light
        light.setStatus(OnOffState.off()); // 4, Turn off the light
    }

1. Create a device factory: a device factory make easy to instanciate a device because under the wood a device is composed from different elements (notably an [Adapter](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/adapter/IAdapter.java) which will translate digital signals to physical protocol).
2. Instanciate a light with by indicating the who (Light.class) and the [Where](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/device/where/Where.java) which describe the adress/id of the device. During instanciation, the device will [Query](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Query.java) the physical device to get all its status.
3. Get the status of the light: read from memory the status.
4. Turn off the light: write in memory the status off and will send a [Command](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Command.java) to change the status. The command will be translated by the Gateway/Adapter to the physical protocol.



# Device

A [Device](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/device/Device.java) is digital twin of an IoT object (physical device). It is synchronized with the physical object and keep representation of different status of that object.
This Device can be connected with physical object throws a [Gateway](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Gateway.java) which convert digital [command](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Command.java) or [query](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Query.java) to physical order, and physical Event from Iot Object to digital twin [Event](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Event.java).  
The device doesn't care about the physical protocole: a light device can have a KNX physical twin or a Philips Hue physical twin.

It is composed by three information:

- **Who**:  the type of the device.
- **Where**: the identifier of the device
- **What**: one or more information of the device


## Who
The who is the type/class of a device.
For example: a light or a shutter or a humidity sensor, etc...

There is a [dictionnary](https://gitlab.com/domosnap/domosnap-core/-/tree/master/model/src/main/avro/devices) of already supported device and you can add your own device.

## Where
The [Where](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/device/where/Where.java) is a description of the device adress. It is composed by different information inspire from URI:

    protocole://user:password@server:port/path/to/device

1. Protocole (required): it will the protocole used to communicate with the physical device
2. User (optional): the login to connect to the device
3. Password (optional): the password to connect to the device
4. server (required): the ip or dnsname or other thing to identify the physical gateway (or directly the physical device)
5. port (optional): in case of ip, the port to connect
6. path/to/device (required): the path to identify the specific device (a physical gateway is connected to more than once physical device)

The where is too the id of the device.
Remark: It means that if a physical device is connected by different protocole, there will be a different digital twin for each uri and they will not be identified as the same one.

## What
A [What](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/device/what/What.java) is a state. A device can have one or more what.


## Listener


# Gateway/Adapter
To communicate with a physical device, digital device use [Gateway](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/gateway/Gateway.java).

Gateway implements the communication between the digital world and the physical world. To do that Gateway will use an [Adapter](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/adapter/IAdapter.java) which is a translator between digital command/query/event and the physical world.

Normally you will use Device which hide internal mechanism to communicate with physical device. But if you don't need shadow device feature (capacity to get information in memory to make your code working even if connection with the device is lost) you can use gateway directly.

To do that you need to instanciate the gateway with an adapter implementation:

    IAdapter adapter = new OpenWebNetAdapter("scs://12345@localhost:1234");
    Gateway g = new Gateway(adapter); // Instanciate a gateway
    g.connect(); // Connect to the gateway
    g.getMonitor().subscribe(event -> System.out.println("Event received: " + String.valueOf(event))); // Subscribe to event
    g.getCommander().offer(new Command(Light.class, new Where("12"), new What("STATUS", OnOffState.On()), null)); // Send a command

# Service

## Device Factory
[DeviceFactory](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/supervisor/DeviceFactory.java) will help you to create a device.
With DeviceFactory, you can create multiple instance of the same device.

## Device Repository
[DeviceRepository](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/supervisor/DeviceRepository.java) will help you to create a device.
With DeviceRepository, if you call some creation of the same device, you will always get the same instance. Another feature of DeviceRepository is that you can add listener on UnknowDevice: it means that if an event from a physical device which haven't been instanciated, your listener will be call.

## Device Discovery
[DeviceDiscovery](https://gitlab.com/domosnap/domosnap-core/-/blob/master/engine/src/main/java/com/domosnap/engine/supervisor/DeviceDiscovery.java) will scan device (Adapter must support this feature).

## Gateway Discovery


# Create your own device

# Create your own adapter
