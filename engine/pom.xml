<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.domosnap.core</groupId>
		<artifactId>domosnap-core</artifactId>
		<version>0.2.10-SNAPSHOT</version>
	</parent>

	<artifactId>engine</artifactId>
	<packaging>bundle</packaging>
	<name>DomoSnapEngine</name>
	<description>Engine to send command to different domotic protocol</description>

	<properties>
		<sonar.exlusions>${project.basedir}/engine/src/main/java/com/domosnap/engine/adapter/openwebnet/conversion/core/parser/**/*,
						${project.basedir}/engine/src/main/java/com/domosnap/engine/adapter/**/*
		</sonar.exlusions>
	</properties>
	
	<!-- Dependecies Management -->
	<dependencies>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>5.7.1</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.awaitility</groupId>
			<artifactId>awaitility</artifactId>
			<version>4.0.3</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<!-- must be on the classpath -->
			<groupId>org.jacoco</groupId>
			<artifactId>org.jacoco.agent</artifactId>
			<classifier>runtime</classifier>
			<version>${sonar.jacoco.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.9</version>
		</dependency>

		<dependency>
			<groupId>com.squareup.okhttp3</groupId>
			<artifactId>okhttp</artifactId>
			<version>3.6.0</version>
		</dependency>

		<dependency>
			<groupId>io.reactivex.rxjava3</groupId>
			<artifactId>rxjava</artifactId>
			<version>3.1.6</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<version>5.1.1</version>
				<artifactId>maven-bundle-plugin</artifactId>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Bundle-SymbolicName>com.domosnap.${project.artifactId}</Bundle-SymbolicName>
						<Bundle-Name>${project.artifactId}</Bundle-Name>
						<Bundle-Description/>
						<Bundle-Vendor>ADG Software</Bundle-Vendor>
						<Bundle-Version>${project.version}</Bundle-Version>
						<Export-Package>
							com.domosnap.engine;uses:="com.domosnap.engine.controller",
							com.domosnap.engine.adapter,
							com.domosnap.engine.adapter.core,
							com.domosnap.engine.adapter.impl.openwebnet,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.shutter,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.core,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.core.parser,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.heating,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.light,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.counter,
							com.domosnap.engine.adapter.impl.openwebnet.conversion.counter.dimension,
							com.domosnap.engine.controller,
							com.domosnap.engine.controller.shutter,
							com.domosnap.engine.controller.gateway,
							com.domosnap.engine.controller.heating,
							com.domosnap.engine.controller.heating.stateValue,
							com.domosnap.engine.controller.light;uses:="com.domosnap.engine.openwebnet.controller",
							com.domosnap.engine.controller.light.stateValue,
							com.domosnap.engine.controller.what,
							com.domosnap.engine.controller.what.impl,
							com.domosnap.engine.controller.where,
							com.domosnap.engine.house,
							com.domosnap.engine.services,
							com.domosnap.engine.services.impl
						</Export-Package>
						<Import-Package>
							javax.xml.parsers,
							org.xml.sax.helpers,
							org.xml.sax,
							io.reactivex,
							io.reactivex.functions,
							io.reactivex.flowables,
							io.reactivex.processors,
							org.reactivestreams
						</Import-Package>

					</instructions>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>license-maven-plugin</artifactId>
				<version>${licence.plugin.version}</version>
				<executions>
					<execution>
						<id>first</id>
						<goals>
							<goal>update-file-header</goal>
						</goals>
						<phase>process-sources</phase>
						<configuration>
							<licenseName>gpl</licenseName>
							<licenseResolver>${project.baseUri}/src/license</licenseResolver>
							<canUpdateCopyright>true</canUpdateCopyright>
							<canUpdateDescription>true</canUpdateDescription>
							<roots>
								<root>src/main/java</root>
							</roots>
							<excludes>
								<exclude>org/json/**</exclude>
							</excludes>
							<encoding>UTF-8</encoding>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${sonar.jacoco.version}</version>
				<executions>
					<execution>
						<id>default-prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>default-report</id>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
					<execution>
						<id>default-check</id>
						<goals>
							<goal>check</goal>
						</goals>
						<configuration>
							<rules>
								<!-- implementation is needed only for Maven 2 -->
								<rule implementation="org.jacoco.maven.RuleConfiguration">
									<element>BUNDLE</element>
									<limits>
										<!-- implementation is needed only for Maven 2 -->
										<limit implementation="org.jacoco.report.check.Limit">
											<counter>COMPLEXITY</counter>
											<value>COVEREDRATIO</value>
											<minimum>0.00</minimum>
										</limit>
									</limits>
								</rule>
							</rules>
						</configuration>
					</execution>
					<execution>
						<id>merge</id>
						<goals>
							<goal>merge</goal>
						</goals>
						<configuration>
							<fileSets>
								<fileSet
									implementation="org.apache.maven.shared.model.fileset.FileSet">
									<directory>${project.basedir}</directory>
									<includes>
										<include>**/*.exec</include>
									</includes>
								</fileSet>
							</fileSets>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.codehaus.mojo</groupId>
										<artifactId>license-maven-plugin</artifactId>
										<versionRange>[1.5,)</versionRange>
										<goals>
											<goal>
												update-file-header
											</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore/>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

</project>
