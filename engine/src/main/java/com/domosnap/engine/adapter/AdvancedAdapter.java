package com.domosnap.engine.adapter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.gateway.*;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.reactivestreams.Publisher;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.flowables.ConnectableFlowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.processors.PublishProcessor;

import java.io.IOException;

public abstract class AdvancedAdapter<E, C, Q> implements IAdapter {

	private final Log log = new Log(this.getClass().getSimpleName());
	
	public abstract ICommanderStream<C, Q> getPhysicalCommander();
	public abstract IMonitorStream<E> getPhysicalMonitor();

	private Disposable streamCommand;
	private Disposable streamQuery;
	
	/**
	 * Add a listener on new monitor stream connection
	 * @param listener listener to listen monitor connection
	 */
	public void addMonitorConnectionListener(OnConnectionListener listener) {
		getPhysicalMonitor().addConnectionListener(listener);
	}

	/**
	 * Remove a listener on new monitor stream connection
	 * @param listener listener to listen monitor connection
	 */
	public void removeMonitorConnectionListener(OnConnectionListener listener) {
		getPhysicalMonitor().removeConnectionListener(listener);
	}

	/**
	 * Add a listener on new command stream connection
	 * @param listener listener to listen commander connection
	 */
	public void addCommanderConnectionListener(OnConnectionListener listener) {
		getPhysicalCommander().addConnectionListener(listener);
	}

	/**
	 * Remove a listener on new command stream connection
	 * @param listener listener to listen commander connection
	 */
	public void removeCommanderConnectionListener(OnConnectionListener listener) {
		getPhysicalCommander().removeConnectionListener(listener);
	}
	
	public Flowable<Event> createMonitor() {
		
		Flowable<Event> result;
		if (getPhysicalMonitor() != null) {
			FlowableOnSubscribe<E> monitorStream = getPhysicalMonitor().getPhysicalMonitorStream();
			Function<E, Publisher<Event>> mapper = getPhysicalMonitor().getMapper();
			
			ConnectableFlowable<Event> connectableFlowable = 
					Flowable.create(monitorStream, BackpressureStrategy.DROP)
									.flatMap(mapper, true, Integer.MAX_VALUE, Integer.MAX_VALUE)
									.share()
									.publish();
			connectableFlowable.connect();

			result = connectableFlowable;
		} else {
			result = PublishProcessor.create();
		}

		return result;
	}
	
	public PublishProcessor<Command> createCommander() {

		final PublishProcessor<Command> result = PublishProcessor.create();
		final ICommanderStream<C, Q> physicalCommander = getPhysicalCommander();

		if (physicalCommander != null) {

			Consumer<C>  commanderStream;
			if (physicalCommander.getCommandConsumer() != null) {
				commanderStream = physicalCommander.getCommandConsumer();
			} else {
				commanderStream = command -> log.finest(Session.COMMAND, "Command Stream not implemented for gateway [" + getURI() + "]");
			}

			Function<Command, Publisher<C>> commandMapper;
			if (physicalCommander.getCommandMapper() != null) {
				commandMapper = physicalCommander.getCommandMapper();
			} else {
				commandMapper = command -> {
					log.finest(Session.COMMAND, "Command Mapper not implemented for gateway [" + getURI() + "]" );
					return null;
				};
			}

			streamCommand = result.observeOn(Schedulers.io())
					.subscribeOn(Schedulers.newThread(), false)
					.flatMap(commandMapper, true, Integer.MAX_VALUE, Integer.MAX_VALUE)
					.subscribe( event -> {
								commanderStream.accept(event);
								if (log.finestLevel) {
									log.finest(Session.COMMAND, "Commander send [".concat(String.valueOf(event)).concat("]"));
								}
							},
							error -> {if (log.errorLevel) {
								log.warning(Session.COMMAND, "Processor not able to send command: ".concat(error.getMessage()));
							}
					});
		}
		return result;	
	}

	@Override
	public PublishProcessor<Query> createQuery() {
		final PublishProcessor<Query> result = PublishProcessor.create();
		final ICommanderStream<C, Q> physicalCommander = getPhysicalCommander();

		if (physicalCommander != null) {

			Consumer<Q> queryStream;
			if (physicalCommander.getQueryConsumer() != null) {
				queryStream = physicalCommander.getQueryConsumer();
			} else {
				queryStream = query -> log.finest(Session.COMMAND, "Query Stream not implemented for gateway [" + getURI() + "]");
			}

			Function<Query, Publisher<Q>> queryMapper;
			if (physicalCommander.getQueryMapper() != null) {
				queryMapper = physicalCommander.getQueryMapper();
			} else {
				queryMapper = command -> {
					log.finest(Session.COMMAND, "Query Mapper not implemented for gateway [" + getURI() + "]");
					return null;
				};
			}

			streamQuery = result.observeOn(Schedulers.io())
					// Lunch all command on the same thread to avoit conflict later...
					.subscribeOn(Schedulers.newThread(), false)
					.flatMap(queryMapper, true, Integer.MAX_VALUE, Integer.MAX_VALUE)
					.subscribe(event -> {
								queryStream.accept(event);
								if (log.finestLevel) {
									log.finest(Session.QUERY, "Query send [".concat(String.valueOf(event)).concat("]"));
								}
							},
							error -> {
								if (log.errorLevel) {
									log.warning(Session.QUERY, "Processor not able to send query: ".concat(error.getMessage()));
								}
							}
					);
		}
		return result;
	}

	/**
	 * Force connection for Commander and Monitor stream if there is no connection.
	 */
	@Override
	public ConnectionStatusEnum connectCommander() {
		ConnectionStatusEnum connectCommander;
		
		if (!getPhysicalCommander().isConnected()) {
			connectCommander = getPhysicalCommander().connect();
		} else {
			connectCommander = ConnectionStatusEnum.CONNECTED;
		}
		
        return connectCommander;
	}

	/**
	 * Force connection for Commander and Monitor stream if there is no connection.
	 */
	@Override
	public ConnectionStatusEnum connectMonitor() {
		ConnectionStatusEnum connectMonitor;
					
		if (!getPhysicalMonitor().isConnected()) {
			connectMonitor = getPhysicalMonitor().connect();
		} else {
			connectMonitor = ConnectionStatusEnum.CONNECTED;
		}
		
        return connectMonitor;
	}
	
	/**
	 *  Disconnect Commander and Monitor stream and clean up all resources used.
	 */
	@Override
	public void disconnectMonitor() {
		if (getPhysicalMonitor() != null) {
			getPhysicalMonitor().disconnect();
		}		
	}
	
	/**
	 *  Disconnect Commander and Monitor stream and clean up all resources used.
	 */
	@Override
	public void disconnectCommander() {
		if (getPhysicalCommander() != null) {
			getPhysicalCommander().disconnect();
		}
	}
	
	/**
	 * Return true if all commander and monitor are connected (else false)
	 * @return true if all commander and monitor are connected (else false)
	 */
	@Override
	public boolean isMonitorConnected() {
		return getPhysicalMonitor().isConnected();
	}	
	
	/**
	 * Return true if all commander and monitor are connected (else false)
	 * @return true if all commander and monitor are connected (else false)
	 */
	@Override
	public boolean isCommanderConnected() {
		return getPhysicalCommander().isConnected();
	}	
	
	@Override
	public void onCreatedDevice(Device device) {
		// Nothing to do on created device.
	}

	@Override
	public void onDestroyedDevice(Device device) {
		// Nothing to do on destroyed device.
	}

	@Override
	public void close() throws IOException {
		if (streamCommand != null) {
			streamCommand.dispose();
		}
		if (streamQuery != null) {
			streamQuery.dispose();
		}
	}
}
