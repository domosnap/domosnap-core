package com.domosnap.engine.adapter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Closeable;
import java.net.URI;
import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.*;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

/**
 * {@link IAdapter} is a protocol abstraction.<br/>
 * <br/>
 * {@link Gateway} is the bridge between logical (digital twin {@link Device}) and
 * physical (iot device).<br/>
 * {@link Gateway} send logical {@link Command}/{@link Query} to stream provided by {@link IAdapter#createCommander()}.
 * The stream will transform {@link Command}/{@link Query} to physical order for the physical device.<br/>
 * Another side, {@link Gateway} will get {@link Event} from the stream provided
 * by {@link IAdapter#createMonitor()}. The stream will transform a physical order from
 * physical device to {@link Event}.<br/>
 * 
 * {@link IAdapter} provides other protocol abstraction (Scan, connection, etc...).
 * <br/>
 * To resume : one adapter means one protocol.
 */
public interface IAdapter extends Closeable {
	
	/**
	 * Return the uri of the physical gateway
	 * @return the uri of the physical gateway
	 */
	URI getURI();

	/**
	 * Define the uri of the physical gateway
	 * @param uri the uri of the physical gateway
	 */
	void setURI(URI uri);
	
	/**
	 * Return the monitor stream. Monitor stream
	 * must emit an {@link Event} each time physical
	 * device's state change.
	 * @return the monitor stream.
	 */
	Flowable<Event> createMonitor();

	/**
	 * Return a @{@link PublishProcessor} stream. The stream
	 * must process a {@link Command} each time a
	 * {@link Gateway} write a state to a physical device.
	 * @return the commander stream.
	 */
	PublishProcessor<Command> createCommander();

	/**
	 * Return @{@link PublishProcessor} stream. The stream
	 * must process a {@link Query} each time a
	 * {@link Gateway} read a physical device's state.
	 * @return the commander stream.
	 */
	PublishProcessor<Query> createQuery();

	/**
	 * Scan the list of physical device.
	 * @param listener listener to listen device found
	 */
	void scan(OnScanDeviceListener listener);
	
	/**
	 * Lunch a scan to find the gateway of the protocol.
	 */
	void scanGateway(OnScanGatewayListener listener);

	/**
	 * Called when a device is created.
	 * @param device new device created.
	 */
	void onCreatedDevice(Device device);

	/**
	 * Called when a device is destroyed.
	 * @param device device destroyed.
	 */
	void onDestroyedDevice(Device device);

	/**
	 * Return the list of supported device.
	 * @return list ot supported device.
	 */
	List<Class<? extends Device>> getSupportedDevice();
	
	/**
	 * Connect Commander stream.
	 * @return status of connection.
	 */
	ConnectionStatusEnum connectCommander();

	/**
	 * Connect Monitor stream.
	 * @return status of monitor.
	 */
	ConnectionStatusEnum connectMonitor();

	/**
	 * Return true if commander is connected (else false)
	 * @return true if commander is connected (else false)
	 */
	boolean isCommanderConnected();
	
	/**
	 * Return true if monitor is connected (else false)
	 * @return true if monitor is connected (else false)
	 */
	boolean isMonitorConnected();
	
	/**
	 *  Disconnect Commander stream and clean up all resources used.
	 */
	void disconnectCommander();
	
	/**
	 *  Disconnect Monitor stream and clean up all resources used.
	 */
	void disconnectMonitor();
	
	/**
	 * Add a listener on new monitor stream connection
	 * @param listener to add
	 */
	void addMonitorConnectionListener(OnConnectionListener listener);

	/**
	 * Remove a listener on new monitor stream connection
	 * @param listener to remove
	 */
	void removeMonitorConnectionListener(OnConnectionListener listener);

	/**
	 * Add a listener on new command stream connection
	 * @param listener to add
	 */
	void addCommanderConnectionListener(OnConnectionListener listener);

	/**
	 * Remove a listener on new command stream connection
	 * @param listener to remove
	 */
	void removeCommanderConnectionListener(OnConnectionListener listener);
}
