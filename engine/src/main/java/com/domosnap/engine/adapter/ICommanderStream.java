package com.domosnap.engine.adapter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.reactivestreams.Publisher;

import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Query;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;

public interface ICommanderStream<C, Q> {

	/**
	 * Return the flow which received logical command to send it to the physical device.
	 * @return the consumer.
	 */
	Consumer<C> getCommandConsumer();

	/**
	 * Return the flow which received logical query to send it to the physical device.
	 * @return the consumer.
	 */
	Consumer<Q> getQueryConsumer();
	
	/**
	 * Return the mapper which transform {@link Command} to raw command.
	 * @return the mapper.
	 */
	Function<Command, Publisher<C>> getCommandMapper();

	/**
	 * Return the mapper which transform {@link Query} to raw query.
	 * @return the mapper.
	 */
	Function<Query, Publisher<Q>> getQueryMapper();

	/**
	 * Force connection for Commander stream if there is no connection.
	 */
	ConnectionStatusEnum connect();
	
	/**
	 *  Disconnect Commander stream and clean up all resources used.
	 */
	void disconnect();

	/**
	 * Return true if all commander are connected (else false)
	 * @return true if all commander are connected (else false)
	 */
	boolean isConnected();
	
	/**
	 * Add a listener on connection
	 * @param listener listener to add on connection.
	 */
	void addConnectionListener(OnConnectionListener listener);

	/**
	 * Remove a listener on connection
	 * @param listener listener on connection to remove
	 */
	void removeConnectionListener(OnConnectionListener listener);
}
