package com.domosnap.engine.adapter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.reactivestreams.Publisher;

import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.Event;

import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.functions.Function;

public interface IMonitorStream<E> {

	/**
	 * Return the flow which received raw command to transform to event to the logical gateway.
	 *  
	 * @return the flow.
	 */
	FlowableOnSubscribe<E> getPhysicalMonitorStream();
	
	/**
	 * Return the mapper which transform a raw command to an {@link Event}.
	 * 
	 * @return the mapper.
	 */
	Function<E, Publisher<Event>> getMapper();

	/**
	 * Force connection for Monitor stream if there is no connection.
	 */
	ConnectionStatusEnum connect();
	
	/**
	 *  Disconnect Monitor stream and clean up all resources used.
	 */
	void disconnect();
	
	/**
	 * Return true if all monitor are connected (else false)
	 * @return true if all monitor are connected (else false)
	 */
	boolean isConnected();

	/**
	 * Add a listener on connection
	 * @param listener
	 */
	void addConnectionListener(OnConnectionListener listener);

	/**
	 * Remove a listener on connection
	 * @param listener
	 */
	void removeConnectionListener(OnConnectionListener listener);
}
