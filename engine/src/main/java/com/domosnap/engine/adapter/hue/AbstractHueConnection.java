package com.domosnap.engine.adapter.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.Log;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.DisconnectionStatusEnum;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AbstractHueConnection {

	protected final OkHttpClient client = new OkHttpClient();
	protected final Log log = new Log(this.getClass().getSimpleName());
	private boolean connected = false;

	
	final Object lock = new Object();
	protected final Object lockPool = new Object();
	protected ExecutorService pool = null;
	protected boolean forceConnection = true;
	private static final int POOL_SIZE = 5;
	private String user = "w6uChF4XDX-WEN9mm1Wx5BatBOrZLTos3W-p2H75";
	private final String ip;
	private final int port;
	private int timeout = 5000;
	private final List<OnConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<>());
	private final Log.Session session;

	/**
	 * 
	 * @param ip
	 *            the ip or dns name of the open server
	 * @param port
	 *            the port number of the open server
	 * @param user
	 *            not supported actually
	 * @param session the session (COMMAND or MONITOR)
	 */
	public AbstractHueConnection(String ip, int port, String user, Log.Session session) {
		this.ip = ip;
		this.port = port;
		this.user = user;
		this.session = session;

	}

	/**
	 * Return the ip of the open server.
	 * 
	 * @return the ip of the open server.
	 */
	protected String getIp() {
		return ip;
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return port;
	}


	public String getUser() {
		return user;
	}

	/**
	 * Return the timeout of the connection to open server in millisecond.
	 * 
	 * @return the timeout of the connection to open server in millisecond.
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Define the timeout of the connection to open server in millisecond.
	 * 
	 * @param timeout
	 *            the timeout of the connection to open server in millisecond.
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		if (connected) {
			connected = false;
			forceConnection = false;
			synchronized (connectionListenerList) {
				for (OnConnectionListener connectionListener : connectionListenerList) {
					try {
						connectionListener.onDisconnect(DisconnectionStatusEnum.CLOSED);
					} catch (Exception e) {
						log.severe(session, getFormattedLog(client, 1,
								"ConnectionListener raise an error [" + e.getMessage() + "]"));
					}
				}
			}
			log.fine(session,
					getFormattedLog(client, 0, "End connection: " + session.name() + " session closed..."));

			synchronized (lockPool) {
				if (pool != null) {
					pool.shutdown();
					try {
						if (!pool.awaitTermination(500, TimeUnit.MILLISECONDS)) {
							List<Runnable> droppedTasks = pool.shutdownNow();
							log.warning(session,
									"Executor did not terminate in the specified time.");
							log.warning(session, "Executor was abruptly shut down. "
									+ droppedTasks.size() + " tasks will not be executed.");
						}
					} catch (InterruptedException e) {
						log.severe(session,
								"Executor error during abruptly shut down. " + e.getMessage());
						// Restore interrupted state...
					    Thread.currentThread().interrupt();
					}
					pool = null;
				}
			}
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public void addConnectionListener(OnConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(OnConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	protected ExecutorService getExecutorService() {
		synchronized (lockPool) {
			if (pool == null) {
				pool = Executors.newFixedThreadPool(POOL_SIZE);
			}
		}

		return pool;
	}

	/**
	 * Open the connection. If connection is not possible it will try again
	 * later:
	 * 
	 * @return @{@link ConnectionStatusEnum#CONNECTED} if connection is working
	 */
	public ConnectionStatusEnum connect() {
		synchronized (lock) { // mutex on the main thread: only one
								// connection or send message at the same
								// time!
			forceConnection = true;
			if (!isConnected()) { // Test again since with the lock, maybe a
									// previous thread has opened the
									// connection!

				if (log.finestLevel) {
					log.fine(session,
						getFormattedLog(client, 1, "HttpClient created [" + ip + "]"));
				}

				log.finest(session,
						getFormattedLog(client, 1, " ----- Step Connection ----- "));
				
				Request request = new Request.Builder()
					      .url("http://" + ip + ":" + port + "/api/" + user + "/lights")
					      .build();

				Response response;
				try {
					response = client.newCall(request).execute();
				} catch (IOException e) {
					return ConnectionStatusEnum.UNKNOWN_ERROR;
				}

				if (!response.isSuccessful()) {
					// Bad return message
					log.severe(session,
							getFormattedLog(client, 1, "Bad message [" + response.body().toString() + "] received from [" + ip + "]"));
					return ConnectionStatusEnum.UNKNOWN_ERROR;
				}

				log.fine(session, getFormattedLog(client, 1, "Connection OK"));
				connected=true;
				return ConnectionStatusEnum.CONNECTED;
			} else {
				return ConnectionStatusEnum.CONNECTED;
			}
		}
	}

	private static int countConnection = 0;
	private static int countSession = 0;
	private static final Map<Integer, String> mapSessionInstance = new ConcurrentHashMap<>();
	private static final Map<Integer, String> mapConnectionInstance = new ConcurrentHashMap<>();

	public String getFormattedLog(Object object, int level, String msg) {

		String connectionNum = "Connection null";
		if (object != null) {
			if (mapConnectionInstance.containsKey(object.hashCode())) {
				connectionNum = mapConnectionInstance.get(object.hashCode());
			} else {
				connectionNum = "Connection " + pad(countConnection++, 4);
				mapConnectionInstance.put(object.hashCode(), connectionNum);
			}
		}

		String sessionNum;
		if (mapSessionInstance.containsKey(this.hashCode())) {
			sessionNum = mapSessionInstance.get(this.hashCode());
		} else {
			sessionNum = session.name() + pad(countSession++, 5);
			mapSessionInstance.put(this.hashCode(), sessionNum);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("[").append(sessionNum).append("]-");
		sb.append("[").append(connectionNum).append("] : ");

		for (int i = 0; i < level; i++) {
			sb.append("   ");
		}

		sb.append(msg);
		return sb.toString();

	}

	private String pad(int i, int pad) {
		String s = "" + i;
		while (s.length() < pad) {
			s = " ".concat(s);
		}
		return s;
	}
}
