package com.domosnap.engine.adapter.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.AdvancedAdapter;
import com.domosnap.engine.adapter.ICommanderStream;
import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.adapter.hue.conversion.CommandToHueRequestFunction;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Query;
import com.domosnap.engine.gateway.OnScanGatewayListener;
import com.domosnap.engine.supervisor.OnScanDeviceListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class HueAdapter extends AdvancedAdapter<Response, Request, Request> {

	private static final Log log = new Log(HueAdapter.class.getSimpleName());
	
	public static final String SCHEME = "hue";
	private URI uri;

	private final HueCommanderStream commander;
	private final HueMonitorStream monitor;
	
	public HueAdapter(String uri) {
		setURI(URI.create(uri));
		String ip = this.uri.getHost();
		int port = this.uri.getPort();
		String user = this.uri.getUserInfo();
		commander = new HueCommanderStream(ip, port, user);
		monitor = new HueMonitorStream(ip, port, user);
		log.finest(Session.OTHER, "HueAdapter created.");
	}

	public URI getURI() {
		return uri;
	}
	
	@Override
	public void setURI(URI uri) {
		if (SCHEME.equals(uri.getScheme())) {
			this.uri = uri;
		} else {
			throw new IllegalArgumentException("Wrong schema. Expected: " + SCHEME + "; Received: " + uri.getScheme());
		}
	}
	
	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		List<Class<? extends Device>> result = new ArrayList<>();
		result.add(Light.class);
		return result;
	}

	@Override
	public ICommanderStream<Request, Request> getPhysicalCommander() {
		return commander;
	}

	@Override
	public IMonitorStream<Response> getPhysicalMonitor() {
		return monitor;
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url("http://" + uri.getHost() + ":" + uri.getPort() + "/api/" + uri.getUserInfo() + "/lights")
				.build();
		try {
			
			Response response = client.newCall(request).execute();
			
			String json = response.body().string();
			Gson gson = new Gson();
			JsonObject lights = gson.fromJson(json, JsonObject.class);
			int counter = 0;
			
			for (Entry<String, JsonElement> entryLight : lights.entrySet()) {

				String lightKey = entryLight.getKey();
				
				Where where = new Where(new URI(HueAdapter.SCHEME, uri.getUserInfo(), uri.getHost(), uri.getPort(), lightKey, null, null).toString());
				
				CommandToHueRequestFunction convert = (CommandToHueRequestFunction) this.getPhysicalCommander().getCommandMapper();
				
				// Send a command on command stream to get a response on monitor stream and discored the device!
				getPhysicalCommander().getCommandConsumer().accept(convert.createMessage(new Query(Light.class, where, LightStateName.STATUS.name())));
				
//				Light light = createController(Light.class, where);
				
				

//				listener.foundController(light);
				// currentMapLight.get(lightKey).get("state").get("on")
				listener.progess(counter++ * 100 / lights.size());
			}
			listener.progess(100);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}

		listener.scanFinished();

	}


	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
		
	}
}
