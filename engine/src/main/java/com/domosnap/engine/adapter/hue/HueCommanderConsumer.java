package com.domosnap.engine.adapter.hue;

import java.io.IOException;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log.Session;

import io.reactivex.rxjava3.functions.Consumer;
import okhttp3.Request;

public class HueCommanderConsumer extends AbstractHueConnection implements Consumer<Request> {

	public HueCommanderConsumer(String host, int port, String user) {
		super(host, port, user, Session.COMMAND);
		HueHub.getInstance().registerCommander(this, host);
	}

	@Override
	public void accept(Request command) {
		if (command == null) {
			log.severe(Session.COMMAND, getFormattedLog(client, 1, "Command unsupported (null)."));
			return;
		}
		getExecutorService().execute(new CommandHandler(command, this));
	}

	private class CommandHandler implements Runnable {

		private final Request request;
		private final HueCommanderConsumer commander;

		public CommandHandler(Request command, HueCommanderConsumer commander) {
			this.request = command;
			this.commander = commander;
		}

		@Override
		public void run() {
			// mutex on the main thread: only one connection or send message at the same
			// time!
			synchronized (lock) {
				// Check that it is possible to connect!
				if (!isConnected() && forceConnection) {
					connect();
				}

				if (isConnected()) {
					log.fine(Session.COMMAND,
							getFormattedLog(client, 1, "Sending command ".concat(request.toString()).concat("]")));
					// 1. Call Rest API

					try {
						client.newCall(request).execute();
					} catch (IOException e) {
						log.fine(Session.COMMAND, e.getMessage());
						return;
					}

					log.finest(Session.COMMAND,

							getFormattedLog(client, 0, "Command [" + request + "] sent."));

					// Propagate result to listening monitors
					HueHub.getInstance().pingMonitor(commander);

					log.finest(Session.COMMAND,
							getFormattedLog(client, 0, "Command [" + request + "] sent."));

				} else { // connection closed... => means impossible to call REST API
					log.severe(Session.COMMAND,
							getFormattedLog(client, 0, "Command [" + request + "] failed (Connection closed)."));
				}
			}
		}
	}
}
