package com.domosnap.engine.adapter.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.hue.conversion.QueryToHueRequestFunction;
import com.domosnap.engine.gateway.Query;
import org.reactivestreams.Publisher;

import com.domosnap.engine.adapter.ICommanderStream;
import com.domosnap.engine.adapter.hue.conversion.CommandToHueRequestFunction;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import okhttp3.Request;

public class HueCommanderStream implements ICommanderStream<Request, Request> {

	private final HueCommanderConsumer commander;
	private final CommandToHueRequestFunction hueConvertToCommandFunction;
	private final QueryToHueRequestFunction queryToHueRequestFunction;


	public HueCommanderStream(String ip, int port, String user) {
		hueConvertToCommandFunction = new CommandToHueRequestFunction(ip, port, user);
		queryToHueRequestFunction = new QueryToHueRequestFunction(ip, port, user);
		commander = new HueCommanderConsumer(ip, port, user);
	}

	@Override
	public Consumer<Request> getCommandConsumer() {
		return commander;
	}

	@Override
	public Consumer<Request> getQueryConsumer() {
		return getCommandConsumer();
	}

	@Override
	public Function<Command, Publisher<Request>> getCommandMapper() {
		return hueConvertToCommandFunction;
	}

	@Override
	public Function<Query, Publisher<Request>> getQueryMapper() {
		return queryToHueRequestFunction;
	}

	@Override
	public boolean isConnected() {
		return commander.isConnected();
	}
	
	@Override
	public ConnectionStatusEnum connect() {
		return commander.connect();
	}
	
	@Override
	public void disconnect() {
		commander.disconnect();
	}
	

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		commander.addConnectionListener(listener);
		
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		commander.removeConnectionListener(listener);
	}
}
