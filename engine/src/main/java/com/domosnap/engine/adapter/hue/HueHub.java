package com.domosnap.engine.adapter.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

import okhttp3.Request;
import okhttp3.Response;

public class HueHub {
	
	private static final HueHub hueHub = new HueHub();
	
	private static Log log = new Log(HueHub.class.getSimpleName());
	
	private Map<HueCommanderConsumer, String> commanderList = new HashMap<>();
	private Map<HueMonitorPublisher, String> monitorList = new HashMap<>();
	
	private HueHub() {
	}

	public static HueHub getInstance() {
		return hueHub;
	}
	
	public void pingMonitor(HueCommanderConsumer commander) {
		
		for (HueMonitorPublisher monitor : getMonitor(commander)) {
//			monitor.lock.notify(); // wake up monitor to get the response NOW and not wait next pull
			// TODO A REVOIR
			 Request request = new Request.Builder()
				      .url("http://" + monitor.getIp() + ":" + monitor.getPort() + "/api/" + monitor.getUser() + "/lights")
				      .build();
			 Response response = null;

			 try {
					// 1. Call Rest API
					response = monitor.client.newCall(request).execute();
					monitor.onMessageReceipt(response);
			} catch (Exception e) {
				 log.warning(Session.MONITOR, "Error to call hue monitor: " + e.getMessage());
			}
		}
	}

	private List<HueMonitorPublisher> getMonitor(HueCommanderConsumer commander) {
		
		List<HueMonitorPublisher> result = new ArrayList<>();
		String host = commanderList.get(commander);
		
		for (Entry<HueMonitorPublisher, String> entry : monitorList.entrySet()) {
			if (host.equals(entry.getValue())) {
				result.add(entry.getKey());
			}
		}
		return result;
	}
	
	public void registerMonitor(HueMonitorPublisher monitor, String host) {
		monitorList.put(monitor, host);
	}
	
	public void removeMonitor(HueMonitorPublisher monitor) {
		monitorList.remove(monitor);
	}

	public void registerCommander(HueCommanderConsumer commander, String host) {
		commanderList.put(commander, host);
	}
	
	public void removeCommander(HueCommanderConsumer commander) {
		commanderList.remove(commander);
	}
}
