package com.domosnap.engine.adapter.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.reactivestreams.Publisher;

import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.adapter.hue.conversion.HueResponseToEventFunction;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.Event;

import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.functions.Function;
import okhttp3.Response;

public class HueMonitorStream implements IMonitorStream<Response> {

	private final HueMonitorPublisher monitor;
	private final HueResponseToEventFunction hueConvertToEventFunction;
	
	public HueMonitorStream(String ip, int port, String user) {
		monitor = new HueMonitorPublisher(ip, port, user);
		hueConvertToEventFunction = new HueResponseToEventFunction(ip, port, user);
	}
		
	@Override
	public FlowableOnSubscribe<Response> getPhysicalMonitorStream() {
		return monitor;
	}
	
	@Override
	public Function<Response, Publisher<Event>> getMapper() {
		return hueConvertToEventFunction;
	}

	@Override
	public boolean isConnected() {
		return monitor.isConnected();
	}
	
	@Override
	public ConnectionStatusEnum connect() {
		return monitor.connect();
	}
	
	@Override
	public void disconnect() {
		monitor.disconnect();
	}

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		monitor.addConnectionListener(listener);
		
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}
}
