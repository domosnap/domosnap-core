package com.domosnap.engine.adapter.hue.conversion;

import com.domosnap.engine.gateway.Query;
import org.reactivestreams.Publisher;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.gateway.Command;

import io.reactivex.rxjava3.functions.Function;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class CommandToHueRequestFunction implements Function<Command, Publisher<Request>> {

	private final Log log = new Log(CommandToHueRequestFunction.class.getSimpleName());
	
	private final String host;
	private final int port;
	private final String user;
	
	public CommandToHueRequestFunction(String host, int port, String user) {
		this.host = host;
		this.port = port;
		this.user = user;
	}
	
	@Override
	public Publisher<Request> apply(final Command command) throws Exception {
		return arg0 -> {
			log.fine(Session.COMMAND, "Prepare to send command [" + command + "].");
			final Request converted = createMessage(command);
			if (converted == null) {
				if(log.finestLevel) {
					log.fine(Session.COMMAND, "No translation command for : " + command, 1);
				}
			} else {
				if(log.finestLevel) {
					log.fine(Session.COMMAND, "Command translated.", 1);
				}
				
				arg0.onNext(converted);
			}
		};
	}
		
	/**
	 * Create hue requests for action or status.
	 */
	// TODO make it protected!
	public final Request createMessage(Command command) {

		if (command.getWhere() == null || command.getWhere().getPath() == null) {
			log.fine(Session.COMMAND, "Command must contain a where.");
			throw new IllegalArgumentException("Command must contain a where");
		}

		// Actually only LIGHT is supported by hue => check that Domosnap doesn't ask another device to Hue protocole
		if (!Light.class.equals(command.getWho())) {
			throw new IllegalArgumentException("Controller Who unknown. [" + command.getWho() + "]");
		}

		final String who = "lights";
		String where = command.getWhere().getPath();
		String whatName;
		String whatValue;
		Request request;

		if (command instanceof Query) { // Read/Status request
			request = new Request.Builder()
					.url("http://".concat(host).concat(":").concat(Integer.toString(port)).concat("/api/").concat(user).concat("/").concat(who).concat("/").concat(where))
					.build();
		} else { // Write/Action request

			String body = "{";

			for (What what : command.getWhatList()) {
				String stateName = what.getName();
				if (LightStateName.STATUS.name().equals(stateName)) {
					whatName="on";
					if (Boolean.TRUE.equals(((OnOffState)what.getValue()).getValue())) {
						whatValue = "true";
					} else {
						whatValue = "false";
					}
				} else  {
					throw new IllegalArgumentException("Controller status unknown [" + what.getName() + "]");
				}

				body = body.concat("\"").concat(whatName).concat("\":").concat(whatValue).concat(",");

			}
			if (body.length()>1)
				body = body.substring(0, body.length()-1);
			body = body.concat("}");

			RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), body);
			request = new Request.Builder()
					.url("http://".concat(host).concat(":").concat(Integer.toString(port)).concat("/api/").concat(user).concat("/").concat(who).concat("/").concat(where).concat("/state"))
					.put(requestBody)
					.build();

		}

		return request;
	}
}
