package com.domosnap.engine.adapter.hue.conversion;

import java.net.URI;
import java.util.ArrayList;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;
import java.util.Map.Entry;

import org.reactivestreams.Publisher;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Event;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.reactivex.rxjava3.functions.Function;
import okhttp3.Response;

public class HueResponseToEventFunction implements Function<Response, Publisher<Event>> {

	private final Log log = new Log(HueResponseToEventFunction.class.getSimpleName());
	private final String ip;
	private final int port;
	private final String user;
	
	public HueResponseToEventFunction(String ip, int port, String user) {
		super();
		this.ip = ip;
		this.port = port;
		this.user = user;
	}

	@Override
	public Publisher<Event> apply(final Response message) throws Exception {

		return subscriber ->  {
			try {
				if (message.body() == null) {
					log.warning(Session.MONITOR, "Response body empty: event discarded.");
					return;
				}
				if (200 != message.code()) {
					if (log.warningLevel)
						log.warning(Session.MONITOR, "Response code diffrent from 200 [".concat(Integer.toString(message.code())).concat("] : event discarded."));
					return;
				}

				Class<? extends Device> who;
				What whatOnOff;
				What whatReachable;
				Where where;

				List<String> pathList = message.request().url().pathSegments();

				// Who
				if ("lights".equals(pathList.get(2))) {
					who = Light.class;
				} else {
					log.severe(Session.MONITOR, pathList.get(2) + ": unsupported device.");
					throw new UnSupportedStateException();
				}

				// Where

				// What
				if ("GET".equals(message.request().method())) { // status
					if (pathList.size() == 3) { // monitor is reading status from ALL device
						String bodyJson = message.body().string();
						log.finest(Session.MONITOR, bodyJson);
						JsonObject lights = JsonParser.parseString(bodyJson).getAsJsonObject(); // Each property name is a
						// key, and property value
						// the light entity

						for (Entry<String, JsonElement> key : lights.entrySet()) {
							where = new Where(new URI("hue", user, ip, port, key.getKey(), null, null).toString());
							JsonObject light = lights.getAsJsonObject(key.getKey());
							JsonObject state = light.getAsJsonObject("state");
							whatOnOff = createWhat("on", state.get("on").getAsString());
							whatReachable = createWhat("reachable", String.valueOf(state.get("reachable")));
							ArrayList<What> whatList = new ArrayList<>();
							whatList.add(whatOnOff);
							whatList.add(whatReachable);
							subscriber.onNext(new Event(who, where, whatList));
						}
						// {"1":
						// {"modelid":"LCT001","name":"Hue Lamp
						// 1","swversion":"65003148","state":{"xy":[0,0],"ct":0,"alert":"none","sat":254,"effect":"none","bri":254,"hue":4444,"colormode":"hs","reachable":true,"on":true},"type":"Extended
						// color
						// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0a"},
						// "2":
						// {"modelid":"LCT001","name":"Hue Lamp
						// 2","swversion":"65003148","state":{"xy":[0.346,0.3568],"ct":201,"alert":"none","sat":144,"effect":"none","bri":254,"hue":23536,"colormode":"hs","reachable":true,"on":true},"type":"Extended
						// color
						// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0b"},
						// "3":
						// {"modelid":"LCT001","name":"Hue Lamp
						// 3","swversion":"65003148","state":{"xy":[0.346,0.3568],"ct":201,"alert":"none","sat":254,"effect":"none","bri":254,"hue":65136,"colormode":"hs","reachable":true,"on":true},"type":"Extended
						// color
						// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0c"}
						//
						// }

					} else { // Command is read the status of ONE device
						where = new Where(new URI("hue", user, ip, port, pathList.get(3), null, null).toString());
						String bodyJson = message.body().string();
						JsonObject ligth = JsonParser.parseString(bodyJson).getAsJsonObject();

						JsonObject state = ligth.getAsJsonObject("state");
						whatOnOff = createWhat("on", state.get("on").getAsString());
						whatReachable = createWhat("reachable", String.valueOf(state.get("reachable")));
						ArrayList<What> whatList = new ArrayList<>();
						whatList.add(whatOnOff);
						whatList.add(whatReachable);
						subscriber.onNext(new Event(who, where, whatList));
					}
				} else { // Action

					where = new Where(new URI("hue", user, ip, port, pathList.get(3), null, null).toString());
					String property = pathList.get(4);
					if (!"state".equals(property)) {
						log.severe(Session.MONITOR, property + ": unsupported state.");
						throw new UnSupportedStateException();
					}
					String bodyJson = message.body().string();

					JsonArray ajs = JsonParser.parseString(bodyJson).getAsJsonArray();
					JsonObject ojs = ajs.get(0).getAsJsonObject();
					JsonObject state = ojs.getAsJsonObject("success");
					// for (String string : ojs.keySet()) {
					whatOnOff = createWhat("on", String.valueOf(state.get("on")));

					subscriber.onNext(new Event(who, where, whatOnOff));

				}
				// 4. End
				message.body().close();
				message.close();
			} catch (Exception e) {
				log.severe(Session.MONITOR, "Problem with Response: event discarded. Original error is: " + e.getMessage());
			}
		};
	}

	private What createWhat(String name, String value) throws UnSupportedStateException {
		
		if(name.equals("on")) {
			
			if ("true".equals(value)) {
				return new What("status", OnOffState.On());
			} else if ("false".equals(value)) {
				return new What("status", OnOffState.Off());
			} else {
				throw new UnSupportedStateException(); 
			}
		}
		if(name.equals("reachable")) {
			if ("true".equals(value)) {
				return new What(name, BooleanState.TRUE);
			} else if ("false".equals(value)) {
				return new What(name, BooleanState.FALSE);
			} else {
				throw new UnSupportedStateException(); 
			}
			
		}
		
		throw new UnSupportedStateException();
	}
}
