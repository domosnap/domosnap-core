package com.domosnap.engine.adapter.hue.conversion;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.functions.Function;
import okhttp3.Request;
import org.reactivestreams.Publisher;

public class QueryToHueRequestFunction implements Function<Query, Publisher<Request>> {

	private final Log log = new Log(QueryToHueRequestFunction.class.getSimpleName());

	private final String host;
	private final int port;
	private final String user;

	public QueryToHueRequestFunction(String host, int port, String user) {
		this.host = host;
		this.port = port;
		this.user = user;
	}
	
	@Override
	public Publisher<Request> apply(final Query query) throws Exception {
		return arg0 -> {
			log.fine(Session.COMMAND, "Prepare to send command [" + query + "].");
			final Request converted = createMessage(query);
			if (converted == null) {
				if(log.finestLevel) {
					log.fine(Session.COMMAND, "No translation command for : " + query, 1);
				}
			} else {
				if(log.finestLevel) {
					log.fine(Session.COMMAND, "Command translated.", 1);
				}
				
				arg0.onNext(converted);
			}
		};
	}
		
	/**
	 * Create hue requests for action or status.
	 */
	protected final Request createMessage(Query command) {

		if (command.getWhere() == null || command.getWhere().getPath() == null) {
			log.fine(Session.COMMAND, "Command must contain a where.");
			throw new IllegalArgumentException("Command must contain a where");
		}

		// Actually only LIGHT is supported by hue => check that Domosnap doesn't ask another device to Hue protocol
		if (!Light.class.equals(command.getWho())) {
			throw new IllegalArgumentException("Device unknown. [" + command.getWho() + "]");
		}

		final String who = "lights";
		String where = command.getWhere().getPath();
		Request request;

		request = new Request.Builder()
				.url("http://".concat(host).concat(":").concat(Integer.toString(port)).concat("/api/").concat(user).concat("/").concat(who).concat("/").concat(where))
				.build();

		return request;
	}
}
