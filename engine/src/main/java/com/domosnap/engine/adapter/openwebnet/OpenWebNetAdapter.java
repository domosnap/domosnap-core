package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.AdvancedAdapter;
import com.domosnap.engine.adapter.ICommanderStream;
import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.adapter.openwebnet.connector.Commander;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.engine.device.gateway.Gateway;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.shutter.Shutter;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.gateway.OnScanGatewayListener;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

/**
 * Implements the adapter for openwebnet protocol <br/>
 * <a href="https://en.wikipedia.org/wiki/OpenWebNet">https://en.wikipedia.org/wiki/OpenWebNet</a>
 */
public class OpenWebNetAdapter extends AdvancedAdapter<String, String, String> {

	private static final Log log = new Log(OpenWebNetAdapter.class.getSimpleName());
	
	private URI uri;
	public static final String SCHEME = "scs";
	private OpenwebnetCommanderStream commander;
	private OpenwebnetMonitorStream monitor;
	private String ip;
	private int port;
	private Integer password;
	
	public OpenWebNetAdapter(String uri) {
		setURI(URI.create(uri));
		log.finest(Session.OTHER, "OpenWebNetAdapter created.");
	}
	
	public OpenWebNetAdapter() {
		log.finest(Session.OTHER, "OpenWebNetAdapter created.");
	}
	
	public URI getURI() {
		return uri;
	}
	
	@Override
	public void setURI(URI uri) {
		ip = uri.getHost();
		port = uri.getPort();
		String userInfo = uri.getUserInfo();
		if (userInfo == null) {
			throw new IllegalArgumentException("User info missing to get gateway password.  Received: " + uri);
		}
		userInfo = userInfo.contains(":") ? userInfo.substring(userInfo.indexOf(":")) : userInfo; // get only the password
		try {
			password = Integer.decode(userInfo);
		} catch (Exception e) {
			log.warning(Session.OTHER, "Password is wrong [" + userInfo + "]. We try without password.");
		}
		commander = new OpenwebnetCommanderStream(ip, port, password);
		monitor = new OpenwebnetMonitorStream(uri);

		if (SCHEME.equals(uri.getScheme())) {
			this.uri = uri;
		} else {
			throw new IllegalArgumentException("Wrong schema. Expected: " + SCHEME + "; Received: " + uri.getScheme());
		}
	}
	
	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		List<Class<? extends Device>> result = new ArrayList<>();
		result.add(TemperatureSensor.class);
		result.add(Light.class);
		result.add(Shutter.class);
		result.add(PowerCounter.class);
		result.add(Gateway.class);
		return result;
	}

	@Override
	public ICommanderStream<String, String> getPhysicalCommander() {
		return commander;
	}

	@Override
	public IMonitorStream<String> getPhysicalMonitor() {
		return monitor;
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		Commander command = new Commander(this.ip, this.port, this.password);
		command.connect();
		// TODO controller unknown = pb with heating parsing. 
		// *#4*88*0*0140##
		// *#4*2#1*20*1##
		
		// wrong light command *1*1000#1*42##
		
		// Light
		String commandLight = "*#1*0##";
		try {
			command.sendCommand(commandLight);
		} catch (Exception e) {
			log.severe(Session.COMMAND, e.getMessage());
		}
		
		listener.progess(10);
		
		// Shutter
		String commandAutomation = "*#2*0##";
		try {
			command.sendCommand(commandAutomation);
		} catch (Exception e) {
			log.severe(Session.COMMAND, e.getMessage());
		}
		
		listener.progess(30);
		// Heating
		String commandHeating = "*#4*0*0##";
		try {
			command.sendCommand(commandHeating);
		} catch (Exception e) {
			log.severe(Session.COMMAND, e.getMessage());
		}
		
		listener.progess(50);
		// PowerCounter
		try {
			for (int i = 1; i<=255;i++) {
				command.sendCommand("*#18*5"+i+"*113##");
			}
			
		} catch (Exception e) {
			log.severe(Session.COMMAND, e.getMessage());
		}

		listener.progess(75);
		// Gateway
		String commandGateway = "*#13**0##";
		try {
			command.sendCommand(commandGateway);
		} catch (Exception e) {
			log.severe(Session.COMMAND, e.getMessage());
		}
		command.disconnect();
		listener.progess(100);
		listener.scanFinished();
		
	}

	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCreatedDevice(Device device) {
		if (device instanceof PowerCounter) { // Hack to manage energy management refresh...
			if (log.finestLevel) {
				log.finest(Session.MONITOR, "Declare an energy management device [" + device.getWhere().toString() +"].");
			}
			((OpenWebNetQueryConsumer) commander.getQueryConsumer()).addPowerCounter(device.getWhere().getPath());
		}
	}

	@Override
	public void onDestroyedDevice(Device device) {
//		TODO if (device instanceof PowerCounter) { // Hack to manage energy management refresh...
//			if (log.finestLevel) {
//				log.finest(Session.MONITOR, "Declare an energy management device [" + device.getWhere().toString() +"].");
//			}
//			((OpenWebNetCommanderConsumer) commander.getCommanderConsumer()).removePowerCounter(device.getWhere().getPath());
//		}
	}
}
