package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.openwebnet.connector.Commander;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

import io.reactivex.rxjava3.functions.Consumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OpenWebNetCommandConsumer implements Consumer<String> {

	private static final Log log = new Log(OpenWebNetCommandConsumer.class.getSimpleName());

	private final Commander commander;
	private final ExecutorService service;

	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen the password
	 */
	public OpenWebNetCommandConsumer(String ip, int port, Integer passwordOpen) {
			commander = new Commander(ip, port, passwordOpen);
			service = Executors.newSingleThreadExecutor();
	}
	
	@Override
	public void accept(String command) throws Exception {
		commander.sendCommand(command);
	}
	
	/**
	 * Return the ip of the open server.
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return commander.getIp();
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return commander.getPort();
	}
	
	public Integer getPasswordOpen() {
		return commander.getPasswordOpen();
	}
	
	public ConnectionStatusEnum connect() {
		service.execute(new PingHandler()); // Ping to keep the connection up (gateway seems to close the connection)
		return commander.connect();
	}
	
	public boolean isConnected() {
		return commander.isConnected();
	}
	
	public void disconnect() {
		commander.disconnect();
	}
	
	public void addConnectionListener(
			OnConnectionListener connectionListener) {
		commander.addConnectionListener(connectionListener);
	}
	
	public void removeConnectionListener(OnConnectionListener connectionListener) {
		commander.removeConnectionListener(connectionListener);
	}

	private class PingHandler implements Runnable {

		int timer = 5000;

		@Override
		public void run() {
			while (isConnected()) {
						try {
							Thread.sleep(timer);
						} catch (InterruptedException e) {
							log.severe(Log.Session.COMMAND,
									"Ping Handler thread stopped. " + e.getMessage());
							// Restore interrupted state...
							Thread.currentThread().interrupt();
							return;
						}
				commander.sendCommand("*#13**0##"); // Time request
			}

		}
	}
}
