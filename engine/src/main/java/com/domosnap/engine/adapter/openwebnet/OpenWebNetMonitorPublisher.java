package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.openwebnet.connector.Monitor;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;

public class OpenWebNetMonitorPublisher implements FlowableOnSubscribe<String>{

	protected Monitor monitor;
	
	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen not supported actually
	 */
	public OpenWebNetMonitorPublisher(String ip, int port, Integer passwordOpen) { 
		monitor = new Monitor(ip, port, passwordOpen);
	}

	/**
	 * Return the ip of the open server.
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return monitor.getIp();
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return monitor.getPort();
	}
	
	public Integer getPasswordOpen() {
		return monitor.getPasswordOpen();
	}
	
	public ConnectionStatusEnum connect() {
		return monitor.connect();
	}
	
	public void disconnect() {
		monitor.disconnect();
	}
	
	public boolean isConnected() {
		return monitor.isConnected();
	}

	public void addConnectionListener(
			OnConnectionListener connectionListener) {
		monitor.addConnectionListener(connectionListener);
	}
	
	public void removeConnectionListener(OnConnectionListener connectionListener) {
		monitor.removeConnectionListener(connectionListener);
	}
	
	@Override
	public void subscribe(FlowableEmitter<String> e) throws Exception {
		monitor.subscribe(e::onNext);
	}	
}
