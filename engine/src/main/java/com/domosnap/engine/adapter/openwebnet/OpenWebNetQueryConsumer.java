package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.openwebnet.connector.Commander;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.OnConnectionListener;
import io.reactivex.rxjava3.functions.Consumer;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class OpenWebNetQueryConsumer implements Consumer<String> {

	private static final Log log = new Log(OpenWebNetQueryConsumer.class.getSimpleName());
	private final Commander commander;
	private Thread energyDeviceRefreshThread;

	/**
	 *
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen the password
	 */
	public OpenWebNetQueryConsumer(String ip, int port, Integer passwordOpen) {
			commander = new Commander(ip, port, passwordOpen);
	}
	
	@Override
	public void accept(String command) throws Exception {
		commander.sendCommand(command);
	}
	
	private final List<String> address = new ArrayList<>();
	
	public void addPowerCounter(String address) {
		synchronized (this.address) {
			this.address.add(address);
		}
	}
	
	/**
	 * Thread to refresh energy management device (WHO=18) since openwebnet need to activate for a specific duration...
	 * More simple to request value.
	 */
	private class EnergyDeviceRefreshHandler implements Runnable {
		
		int timeActivePower = 5000;
		int timeTotalPower = 1000*60*5; // 5 min
		int timeMonthlyPower = 1000*60*60; // 1 hours

		@Override
		public void run() {
			
			while (isConnected()) {
				int time1 = 0;
				while (time1 < timeMonthlyPower) {
					time1 += timeTotalPower;
					int time = 0;
					while(time < timeTotalPower) {
						time += timeActivePower;
						try {
							Thread.sleep(timeActivePower);
						} catch (InterruptedException e) {
							log.severe(Session.COMMAND,
									"EnergyDeviceRefresh Handler thread stopped. " + e.getMessage());
							// Restore interrupted state...
						    Thread.currentThread().interrupt();
						    return;
						}
						// Request active power all 5 seconde
						commandToRefresh("*#18*{0}*113##");
					}
					// Request daily consumption all 5 minute
					commandToRefresh("*#18*{0}*54##");
					// Request total power all 5 minute
					commandToRefresh("*#18*{0}*51##");
				}
				// Request monthly consumption all hours
				commandToRefresh("*#18*{0}*53##");
			}
		}
		
		private void commandToRefresh(String command) {
			synchronized (address) {
				address.forEach(where -> commander.sendCommand(MessageFormat.format(command, where)));
			}
		}
	}
	
	/**
	 * Return the ip of the open server.
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return commander.getIp();
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return commander.getPort();
	}
	
	public Integer getPasswordOpen() {
		return commander.getPasswordOpen();
	}
	
	public ConnectionStatusEnum connect() {
		ConnectionStatusEnum result = commander.connect();
		if (ConnectionStatusEnum.CONNECTED.equals(result)) { // TODO mettre dans un listener plutot!
			// TODO energyDeviceRefreshThread = new Thread(new EnergyDeviceRefreshHandler());
			// TODO energyDeviceRefreshThread.start();
		}
		return result;
	}
	
	public boolean isConnected() {
		return commander.isConnected();
	}
	
	public void disconnect() {
		commander.disconnect();
		if (energyDeviceRefreshThread != null) {
			energyDeviceRefreshThread.interrupt();
		}
	}
	
	public void addConnectionListener(
			OnConnectionListener connectionListener) {
		commander.addConnectionListener(connectionListener);
	}
	
	public void removeConnectionListener(OnConnectionListener connectionListener) {
		commander.removeConnectionListener(connectionListener);
	}
}
