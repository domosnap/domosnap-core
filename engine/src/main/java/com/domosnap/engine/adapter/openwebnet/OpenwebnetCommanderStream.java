package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.openwebnet.conversion.core.QueryToOpenWebNetFunction;
import com.domosnap.engine.gateway.Query;
import org.reactivestreams.Publisher;

import com.domosnap.engine.adapter.ICommanderStream;
import com.domosnap.engine.adapter.openwebnet.conversion.core.CommandToOpenWebNetFunction;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;

public class OpenwebnetCommanderStream implements ICommanderStream<String, String> {

	private final OpenWebNetCommandConsumer commander;
	private final OpenWebNetQueryConsumer query;

	
	public OpenwebnetCommanderStream(String ip, int port, Integer passwordOpen) {
		commander = new OpenWebNetCommandConsumer(ip, port, passwordOpen);
		query = new OpenWebNetQueryConsumer(ip, port, passwordOpen);
	}

	@Override
	public Consumer<String> getCommandConsumer() {
		return commander;
	}

	@Override
	public Consumer<String> getQueryConsumer() {
		return query;
	}

	@Override
	public Function<Command, Publisher<String>> getCommandMapper() {
		return new CommandToOpenWebNetFunction();
	}

	@Override
	public Function<Query, Publisher<String>> getQueryMapper() {
		return new QueryToOpenWebNetFunction();
	}

	@Override
	public boolean isConnected() {
		return commander.isConnected() && query.isConnected();
	}
	
	@Override
	public ConnectionStatusEnum connect() {
		ConnectionStatusEnum resultQuery = query.connect();
		ConnectionStatusEnum resultCommand = commander.connect();
		if (ConnectionStatusEnum.CONNECTED == resultQuery && ConnectionStatusEnum.CONNECTED == resultCommand ) {
			// Connected
			return ConnectionStatusEnum.CONNECTED;
		} else if (resultCommand == resultQuery ) {
			// Not connected but same symptom
			return resultCommand;
		} else if (resultCommand == ConnectionStatusEnum.CONNECTED) {
			// If resultCommand is connected so pb come from resultQuery
			return resultQuery;
		} else {
			// Else maybe resultQuery is on error too but in all case we return error from resultCommand
			// since probability is really low...
			return resultCommand;
		}
	}
	
	@Override
	public void disconnect() {
		commander.disconnect();
		query.disconnect();
	}

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		commander.addConnectionListener(listener);
		// TODO query.addConnectionListener(listener);
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		commander.removeConnectionListener(listener);
		// TODO query.removeConnectionListener(listener);
	}
}
