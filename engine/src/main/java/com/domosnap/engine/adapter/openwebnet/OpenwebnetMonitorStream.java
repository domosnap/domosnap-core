package com.domosnap.engine.adapter.openwebnet;

import java.net.URI;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.reactivestreams.Publisher;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.adapter.openwebnet.conversion.core.OpenWebNetToEventFunction;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.Event;

import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.functions.Function;

public class OpenwebnetMonitorStream implements IMonitorStream<String> {

	private final OpenWebNetMonitorPublisher monitor;
	private final OpenWebNetToEventFunction openWebNetConvertToCommandFunction;
	
	private static final Log log = new Log(OpenwebnetMonitorStream.class.getSimpleName());
	
	public OpenwebnetMonitorStream(URI uri) {
		String ip = uri.getHost();
		int port = uri.getPort();
		String userInfo = uri.getUserInfo();
		userInfo = userInfo.contains(":") ? userInfo.substring(userInfo.indexOf(":")) : userInfo; // get only the password
		Integer password = null;
		try {
			password = Integer.decode(userInfo);
		} catch (Exception e) {
			log.warning(Session.OTHER, "Password is wrong [" + userInfo + "]. We try without password.");
		}
		monitor = new OpenWebNetMonitorPublisher(ip, port, password);
		// push the gateway uri for where
		openWebNetConvertToCommandFunction = new OpenWebNetToEventFunction(uri.getScheme()+"://" + uri.getAuthority());
	}
		
	@Override
	public FlowableOnSubscribe<String> getPhysicalMonitorStream() {
		return monitor;
	}
	
	@Override
	public Function<String, Publisher<Event>> getMapper() {
		return openWebNetConvertToCommandFunction;
	}

	@Override
	public boolean isConnected() {
		return monitor.isConnected();
	}
	
	@Override
	public ConnectionStatusEnum connect() {
		return monitor.connect();
	}
	
	@Override
	public void disconnect() {
		monitor.disconnect();
	}

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}
}
