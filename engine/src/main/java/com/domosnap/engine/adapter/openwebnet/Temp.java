package com.domosnap.engine.adapter.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.net.URISyntaxException;

public class Temp {

	public static void main(String[] args) throws URISyntaxException {
		// TODO Auto-generated method stub
		URI uri = new URI("scs://12345:pwd@localhost:1234/path?query=12&t=12");

		System.out.println(uri.getPath());
		System.out.println(uri.getScheme()+"://" + uri.getAuthority());
		System.out.println(uri.toString().substring(0, uri.toString().length()-uri.getPath().length()));
		
	}

}
