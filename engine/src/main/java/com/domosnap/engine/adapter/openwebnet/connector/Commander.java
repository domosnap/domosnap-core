package com.domosnap.engine.adapter.openwebnet.connector;

import java.text.MessageFormat;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/**
 * Commander open a COMMAND session to an SCS gateway.
 * Once connected you are able to send command to gateway.
 * 
 * @author adegiuli
 *
 */
public class Commander extends Connection {

	private static final String COMMAND_FAIL_ERR_MSG = "Command [{0}] failed.";
	private static final String COMMAND_SEND_MSG = "Command [{0}] sent.";
	private static final String COMMAND_RECEIVED_MSG = "Result of [{0}] received [{1}]";
	
	/**
	 * Constructor
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen the password
	 */
	public Commander(String ip, int port, Integer passwordOpen) {
		super(ip, port, passwordOpen, Log.Session.COMMAND);
	}
	
	/**
	 * Send a command (in SCS format <a href="https://fr.wikipedia.org/wiki/Openwebnet">https://fr.wikipedia.org/wiki/Openwebnet</a>)
	 * @param command the SCS command to send
	 * @return true if command has been sent, false else.
	 */
	public synchronized boolean sendCommand(String command) {
		if (command == null) {
			log.severe(Session.COMMAND, getFormattedLog(1, "Command unsupported (null)."));
			return false;
		}

		if (log.finestLevel) {
			log.fine(Session.COMMAND, getFormattedLog(0, "Sending command [".concat(command).concat("]")));
		}

		if (isConnected()) { // Test again since with the lock, maybe a previous thread has closed the connection!
			writeMessage(command);
			String msg = readMessage();

			if(msg == null){
				log.severe(Session.COMMAND, getFormattedLog(0, MessageFormat.format(COMMAND_FAIL_ERR_MSG, command)));
				return false;
			}

			if (OpenWebNetConstant.ACK.equals(msg)) {
				if (log.finestLevel) {
					log.finest(Session.COMMAND, getFormattedLog(0, MessageFormat.format(COMMAND_SEND_MSG, command)));
				}
				return true;
			} else if (OpenWebNetConstant.NACK.equals(msg)) {
				log.severe(Session.COMMAND, getFormattedLog(0, MessageFormat.format(COMMAND_FAIL_ERR_MSG, command)));
				return false;
			} else { // First return was information. The next should be result (1 or n) and the acknowledgment
				boolean result = false;
				boolean continu = true;
					String actionReturn = msg;
					while (continu) {
						msg = readMessage();
						if(OpenWebNetConstant.ACK.equals(msg)){
							// Case where command is ok
							if (log.finestLevel) {
								log.finest(Session.COMMAND, getFormattedLog(1, MessageFormat.format(COMMAND_RECEIVED_MSG, command, actionReturn)));
								log.finest(Session.COMMAND, getFormattedLog(0, MessageFormat.format(COMMAND_SEND_MSG, command)));
							}
							continu = false;
							result = true;
						} else if (OpenWebNetConstant.NACK.equals(msg)) {
							// Case where command is ko
							log.severe(Session.COMMAND, getFormattedLog(0, MessageFormat.format(COMMAND_FAIL_ERR_MSG, command)));
							continu = false;
						} else {
							// case where command is partial: we get more than once return. This can occurs for exemple when we send a command to a group: for each controller we get a return and the acknoledge at the end.
							if (log.finestLevel) {
								log.finest(Session.COMMAND, getFormattedLog(1, MessageFormat.format(COMMAND_RECEIVED_MSG, command, actionReturn)));
							}
							actionReturn = msg;
						}
					}
					return result;
				}
		} else { // connection closed...
			log.severe(Session.COMMAND, getFormattedLog(0, "Command [" + command +  "] failed (Connection closed)."));
			return false;
		}
	}
}
