package com.domosnap.engine.adapter.openwebnet.connector;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.domosnap.engine.Log;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.DisconnectionStatusEnum;

public abstract class Connection {

	protected final Log log = new Log(this.getClass().getSimpleName());
	private final Object lockConnection = new Object();
	protected Socket socket;
	protected boolean handshakeFinished = false; // important = to only send message when handshake is finished and not just only socket open.
	private final Integer passwordOpen;
	private BufferedReader fromClient;
	private PrintWriter toClient;
	private final String ip;
	private final int port;
	private int timeout = 5000;
	private final Password passwordEncoder = new Password();
	protected final List<OnConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<>());
	protected final Log.Session session;
	
	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen not supported actually
	 */
	protected Connection(String ip, int port, Integer passwordOpen, Log.Session session){ 
		this.ip = ip;
		this.port = port;
		this.passwordOpen = passwordOpen;
		this.session = session;
		
	}

	/**
	 * Return the ip of the open server.
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return port;
	}
	
	public Integer getPasswordOpen() {
		return passwordOpen;
	}
	
	/**
	 * Return the timeout of the connection to open server in millisecond.
	 * @return the timeout of the connection to open server in millisecond.
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Define the timeout of the connection to open server in millisecond.
	 * @param timeout the timeout of the connection to open server in millisecond.
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Open the connection.
	 * @return the status of connection
	 */
	public synchronized ConnectionStatusEnum connect() {
		if (!isConnected()) { 
			ConnectionStatusEnum result =  connection();
			for (OnConnectionListener connectionListener : connectionListenerList) {
				try {
					connectionListener.onConnect(result);
				} catch (Exception e) {
					log.severe(session, getFormattedLog(1, "ConnectionListener raise an error [" + e.getMessage() +"]"));
				}
			}
			return result;
		}
		return ConnectionStatusEnum.CONNECTED;
	}
	
	/**
	 * Close the socket and stop the thread.<br/>
	 */
	public synchronized void disconnect(){
		if (isConnected()) { 
			disconnected(DisconnectionStatusEnum.CLOSED);
		} else {
			log.fine(session, getFormattedLog(0, "End connection: " + session.name() + " session already closed..."));
		}
	}
	
	/**
	 * Close the socket and stop the thread on an error.<br/>
	 */
	protected synchronized void disconnected(DisconnectionStatusEnum status){
		if (log.fineLevel) {
			log.fine(session, getFormattedLog(0, "End connection: " + session.name() + " session closed with event: " + status.name()));
		}
		synchronized (lockConnection) {
			close();
			onDisconnect();
		}
		for (OnConnectionListener connectionListener : connectionListenerList) {
			try {
				connectionListener.onDisconnect(status);
			} catch (Exception e) {
				log.severe(session, getFormattedLog(1, "ConnectionListener raise an error [" + e.getMessage() +"]"));
			}
		}
		
		
	}
	
	private void close() {
		handshakeFinished = false;
		if(socket != null){
			try {
				socket.close();
			} catch (IOException e) {
				log.severe(session, getFormattedLog(0, "End connection:  Error during closing socket [" + e.getMessage() + "]"));
			} finally {
				socket = null;
			}
		}
	}

	protected void onDisconnect() {
	}
	
	public boolean isConnected() {
		synchronized (lockConnection) {
			return handshakeFinished;
		}
	}

	public void addConnectionListener(
			OnConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}
	
	public void removeConnectionListener(OnConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}
	
	protected void writeMessage(String msg) {
		if (toClient != null) {
			if(socket != null && !socket.isOutputShutdown() && !socket.isClosed()) {
				toClient.print(msg);
				toClient.flush();
				log.fine(session, getFormattedLog(1, "Send to ".concat(session.name().toUpperCase()).concat(" server: ").concat(msg)));
			} else {
				disconnected(DisconnectionStatusEnum.CONNECTION_RESET_BY_PEER);
			}
		}
	}
	
	protected String readMessage(){
		int index = 0;
		char[] respond = new char[1024];
		char c;
		int ci;

		try{
			do { 
				if(socket != null && !socket.isInputShutdown() && !socket.isClosed()) {
					ci = fromClient.read();
					if (ci == -1) {
						log.finest(session, getFormattedLog(1, "End of read from monitor socket."));
						disconnected(DisconnectionStatusEnum.UNEXPECTEDLY_END_OF_STREAM);
						return null;
					} else { 
						c = (char) ci;			        				        
						if (c == '#' && index > 1 && '#' == respond[index-1]) {
							respond[index] = c;
							String responseString = new String(respond,0,index+1);
							log.fine(session, getFormattedLog(1, "Read from " + session.name().toUpperCase() + " server: " + responseString));
							return responseString;
						} else {
							respond[index] = c;
							index = index + 1;
						}
					}
				} else {
					disconnected(DisconnectionStatusEnum.CONNECTION_RESET_BY_PEER);
					log.finest(session, getFormattedLog(1, "Socket shutdown unexpectedly."));
					return null;
				}
			} while(true);
		}catch(IOException e){
			log.severe(session, getFormattedLog(0, "Socket not available"));
		}
		return null;
	}
	
	protected ConnectionStatusEnum connection() {
		synchronized (lockConnection) {
			try {

				if (ip == null || port == 0) {
					log.fine(session, getFormattedLog(0, "Not enough information to initiate connection to socket [" + ip + ":" + port + "]"));
					return ConnectionStatusEnum.INVALID_ADDRESS;
				}

				socket = new Socket(); // log use socket! So need to instanciate socket before.

				log.fine(session, getFormattedLog(0, "Begin connection to socket [" + ip + ":" + port + "]"));
				InetSocketAddress address = new InetSocketAddress(ip, port);
				socket.connect(address, getTimeout());
				fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				toClient = new PrintWriter(socket.getOutputStream(), true);

				log.finest(session, getFormattedLog(1, " ----- Step Connection ----- "));
				String msg = readMessage();
				if (!OpenWebNetConstant.ACK.equals(msg)) {
					// Bad return message
					log.severe(session, getFormattedLog(1, "Bad message [" + msg + "] received from [" + ip + "]"));
					close();
					return ConnectionStatusEnum.WRONG_ACKNOWLEDGEMENT;
				}

				log.finest(session, getFormattedLog(1, " ----- Step Identification -----"));
				if (Log.Session.MONITOR.equals(session)) {
					writeMessage(OpenWebNetConstant.MONITOR_SESSION);
				} else {
					writeMessage(OpenWebNetConstant.COMMAND_SESSION);
				}

				if (passwordOpen != null) {
					log.finest(session, getFormattedLog(1, " ----- Step authentication -----"));
					msg = readMessage().substring(2); // Remove *#
					msg = msg.substring(0, msg.length() - 2); // Remove last ##
					writeMessage("*#".concat(passwordEncoder.calcPass(getPasswordOpen(), msg).concat("##"))); // Send encoded password
				}

				log.finest(session, getFormattedLog(1, " ----- Step Final -----"));
				msg = readMessage();

				if (!OpenWebNetConstant.ACK.equals(msg)) {
					log.severe(session, getFormattedLog(1, "Problem during connection to [" + getIp() + "] with message [" + msg + "] - Wrong password"));
					close();
					return ConnectionStatusEnum.WRONG_ACKNOWLEDGEMENT;
				}

				handshakeFinished = true;
				log.fine(session, getFormattedLog(1, "Connection OK"));
				return ConnectionStatusEnum.CONNECTED;

			} catch (SocketTimeoutException e) {
				log.severe(session, getFormattedLog(1, "Impossible to connect to the server [" + ip + ":" + port + "]:" + e.getMessage()));
				if (socket != null && socket.isConnected()) {
					close();
				}
				return ConnectionStatusEnum.TIMEOUT;
			} catch (UnknownHostException e) {
				log.severe(session, getFormattedLog(1, "Impossible to connect to the server [" + ip + ":" + port + "]:Unknown host."));
				if (socket != null && socket.isConnected()) {
					close();
				}
				return ConnectionStatusEnum.INVALID_ADDRESS;
			} catch (ConnectException e) {
				log.severe(session, getFormattedLog(1, "Impossible to connect to the server [" + ip + ":" + port + "]:" + e.getMessage()));
				if (socket != null && socket.isConnected()) {
					close();
				}
				return ConnectionStatusEnum.CONNECTION_REFUSED;
			} catch (Exception e) {
				log.severe(session, getFormattedLog(1, "Impossible to connect to the server [" + ip + ":" + port + "]:" + e.getMessage()));
				if (socket != null && socket.isConnected()) {
					close();
				}
				return ConnectionStatusEnum.UNKNOWN_ERROR;
			}
		}
		
	}
		
	private static int countSocket = 0;
	private static int countSession = 0;
	private static final Map<Integer, String> mapSessionInstance = new ConcurrentHashMap<>();
	private static final Map<Integer, String> mapSocketInstance = new ConcurrentHashMap<>();
	
	public String getFormattedLog(int level, String msg) {
		
		
		String socketNum = "Socket null";
		if(socket != null) {
			if (mapSocketInstance.containsKey(socket.hashCode())) {
				socketNum = mapSocketInstance.get(socket.hashCode());
			} else {
				socketNum = "Socket ".concat(pad(countSocket++, 4));
				mapSocketInstance.put(socket.hashCode(),socketNum);
			}
		}

		String sessionNum;
		if (mapSessionInstance.containsKey(this.hashCode())) {
			sessionNum = mapSessionInstance.get(this.hashCode());
		} else {
			sessionNum = session.name() + pad(countSession++, 5);
			mapSessionInstance.put(this.hashCode(),sessionNum);
		}

		return "[".concat(sessionNum).concat("]-")
				.concat("[").concat(socketNum).concat("] : ")
				.concat("   ".repeat(level)).concat(msg);
		
	}
	
	private String pad(int i, int pad) {
		String s = String.valueOf(i);
		while (s.length() < pad) {
			s = " ".concat(s);
		}
		return s;
	}
}
