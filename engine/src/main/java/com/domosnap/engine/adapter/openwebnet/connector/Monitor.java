package com.domosnap.engine.adapter.openwebnet.connector;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.Log;
import com.domosnap.engine.gateway.ConnectionStatusEnum;

public class Monitor extends Connection {

	private final List<Consumer> suscriberList = new ArrayList<>();
	protected ExecutorService pool = null;
	
	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen not supported actually
	 */
	public Monitor(String ip, int port, Integer passwordOpen) { 
		super(ip, port, passwordOpen, Log.Session.MONITOR);
	}

	@Override
	public synchronized ConnectionStatusEnum connect() {
		ConnectionStatusEnum result = super.connect();
		getExecutorService().execute(new MonitorHandler());
		return result;
	}

	protected synchronized ExecutorService getExecutorService() {
		if (pool == null) {
			pool = Executors.newSingleThreadExecutor();
		}

		return pool;
	}
	
	@Override
	protected void onDisconnect() {
		if(pool!=null){
			pool.shutdown();
			try {
				if (!pool.awaitTermination(500, TimeUnit.MILLISECONDS)) {
					List<Runnable> droppedTasks = pool.shutdownNow();
					log.warning(session, "Executor was shut down. "
							+ droppedTasks.size() + " tasks will not be executed.");

				}
			} catch (InterruptedException e) {
				log.severe(session,
						"Executor error during abruptly shut down. " + e.getMessage());
				// Restore interrupted state...
			    Thread.currentThread().interrupt();
			}
			pool = null;
		}
		
	}
	
	private class MonitorHandler implements Runnable {

		@Override
		public void run() {
			while(isConnected()) {
				// If connected read data
				String msg = readMessage();
				if (msg != null && !OpenWebNetConstant.ACK.equals(msg)) {
					for (Consumer consumer : suscriberList) {
						consumer.accept(msg);
					}
				}
			}
		}
	}

	public boolean subscribe(Consumer c) {
		return suscriberList.add(c);
	}
	
	public interface Consumer {
		
		void accept(String msg);
	}
}
