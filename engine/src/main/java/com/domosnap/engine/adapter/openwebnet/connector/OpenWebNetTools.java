package com.domosnap.engine.adapter.openwebnet.connector;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.Socket;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

public class OpenWebNetTools {
	
	private static Log log = new Log(OpenWebNetTools.class.getSimpleName());

	private String ip;
	private int port;
	private int password;
	
	public static void main(String[] args) {

		OpenWebNetTools owc = new OpenWebNetTools("192.168.1.35", 20000, 12345);
		owc.sendCommand("*18*58#1*5101##");
		owc.sendCommand("*#18*5111*53##");
		owc.sendCommand("*#18*5111*52#20#12##");
		owc.sendCommand("*#13**22##");
		owc.sendCommand("*#18*5111*#1200#1*3##");
		
	}
	
	public OpenWebNetTools(String ip, int port, int password) {
		this.ip = ip;
		this.port = port;
		this.password = password;
	}
	
	public void sendCommand(String command) {
		Monitor owp = new Monitor(ip, port, password);
		owp.connect();
		Commander owc = new Commander(ip, port, password);
		owc.connect();
		
		owc.sendCommand(command);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.severe(Session.OTHER, e.getMessage());
			Thread.currentThread().interrupt();
		}
	}	
	
	public static boolean is1234Open() {
		try (Socket socket  = new Socket("127.0.0.1", 1234)) {
			return socket.isConnected();
		} catch (Exception e) {
			log.severe(Session.OTHER, "OpenWebEmulator not lunch. Test won't be executed.");
			return false;
		}
	}
	
	public boolean isServerAvailable() {
		try (Socket socket  = new Socket(ip, port)){
			return socket.isConnected();
		} catch (Exception e) {
			log.severe(Session.OTHER, "OpenWebNet server not lunch. Test won't be executed.");
			return false;
		}
	}
}
