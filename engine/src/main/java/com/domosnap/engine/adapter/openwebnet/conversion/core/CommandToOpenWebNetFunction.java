package com.domosnap.engine.adapter.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.reactivestreams.Publisher;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetConstant;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.device.gateway.Gateway;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.gateway.Command;

import io.reactivex.rxjava3.functions.Function;

public class CommandToOpenWebNetFunction implements Function<Command, Publisher<String>> {

	private final Log log = new Log(this.getClass().getSimpleName());

	@Override
	public Publisher<String> apply(final Command command) throws Exception {
		return subscriber -> {
			final List<String> converted = createMessage(command);
			if (converted.isEmpty()) {
				if(log.finestLevel) {
					log.fine(Session.COMMAND, "No translation command for : " + command, 1);
				}
			} else {
				for (String string : converted) {
					if(log.finestLevel) {
						log.fine(Session.COMMAND, "Lunch command.", 1);
					}
					subscriber.onNext(string);
				}
			}
		};
	}

	/**
	 * Create the open message for action or status.
	 * 
	 * @return open web net message.
	 */
	protected final List<String> createMessage(Command command) {
		if (command.getWhere() == null || command.getWhere().getPath() == null
			&& !Gateway.class.equals(command.getWho())) {
			log.fine(Session.COMMAND, "Command must contain a where.", 1);
			throw new IllegalArgumentException("Command must contain a where");
		}
		List<String> commandList  = new ArrayList<>();
		for (What what : command.getWhatList()) {
			try {
				Converter converter = OpenWebNetConverterRegistry.getConverter(command.getWho());
				String who =  converter.getOpenWebWho();
				String where = command.getWhere().getPath();

				if (converter.isDimension(what)) { // Dimension
					List<DimensionStatus<?>> dimensionStatus = converter.convertDimension(what);
					StringBuilder sb = new StringBuilder();

					for (DimensionStatus<?> dim : dimensionStatus) {
						for (DimensionValue dimension : dim.getValueList()) {
							sb.append(dimension.getValue());
							sb.append(OpenWebNetConstant.DIMENSION_SEPARATOR);
						}
						sb.setLength(sb.length() - 1);
						commandList.add(MessageFormat.format(
								OpenWebNetConstant.DIMENSION_COMMAND,
								who, where, dim.getCode(), sb.toString()
						));
					}
				} else {
					List<String> whatList = converter.convert(what);
					for (String stringWhat : whatList) {
						commandList.add(
								MessageFormat.format(
										OpenWebNetConstant.COMMAND,
										who, stringWhat, where
								)
						);
					}
				}

			} catch (UnknownStateException e) {
				log.fine(Session.COMMAND,"Device status unknown [" + what.getName() + "]", 1);
			} catch (UnknownWhoException e) {
				throw new IllegalArgumentException("Device unknown. [" + command.getWho() + "]");
			} catch (UnSupportedStateException e) {
				log.fine(Session.COMMAND,"Device status unsupported. [" + what.getName() + "]", 1);
			} catch (UnknownStateValueException e) {
				log.fine(Session.COMMAND,"Device status value unknown [" + what.getValue() + "]", 1);
			}
		}
		return commandList;
	}
}
