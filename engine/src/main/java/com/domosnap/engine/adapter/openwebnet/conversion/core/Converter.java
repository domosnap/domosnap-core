package com.domosnap.engine.adapter.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.What;

public interface Converter {

	List<String> convert(What what) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	List<DimensionStatus<?>> convertDimension(What what) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	List<What> convert(String code) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	List<What>  convertDimension(String dimension, List<DimensionValue> dimensionValueList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	boolean isDimension(What what);
	String getOpenWebWho();
	Class<? extends Device> getHomeSnapWho();
}
