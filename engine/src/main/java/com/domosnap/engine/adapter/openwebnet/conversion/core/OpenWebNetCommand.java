package com.domosnap.engine.adapter.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.CommandParser;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.ParseException;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.heating.HeatingZone;
import com.domosnap.engine.device.what.What;

public class OpenWebNetCommand {
	
	private static Log log = new Log(OpenWebNetCommand.class.getSimpleName());

	private String command;
	private CommandParser parser;

	public OpenWebNetCommand(String command) throws ParseException {
		try {
			this.command = command;
			parser = CommandParser.parse(command);
		} catch (ParseException e) {
			log.finest(Session.COMMAND,"Invalid command [" + command + "].");
			throw e;
		}
	}

	public boolean isStandardCommand() {
		return CommandEnum.STANDARD_COMMAND.equals(parser.getType());
	}

	public boolean isDimensionCommand() {
		return CommandEnum.DIMENSION_COMMAND.equals(parser.getType());
	}

	public boolean isGeneralCommand() {
		// TODO manage the heating case in the parser!!!
		try {
			if (HeatingZone.class.equals(getWho())) {
				return false;
			}
		} catch (UnknownWhoException e) {
			log.severe(Session.OTHER, e.getMessage());
		}
		return WhereType.GENERAL.equals(parser.getWhereType());
	}

	public boolean isGroupCommand() {
		// TODO manage the heating case in the parser!!!
		try {
			if (HeatingZone.class.equals(getWho())) {
				return false;
			}
		} catch (UnknownWhoException e) {
			log.severe(Session.OTHER, e.getMessage());
		}
		return WhereType.GROUP.equals(parser.getWhereType());
	}

	public boolean isEnvironmentCommand() {
		// TODO manage the heating case in the parser!!!
		try {
			if (HeatingZone.class.equals(getWho())) {
				return false;
			}
		} catch (UnknownWhoException e) {
			log.severe(Session.OTHER, e.getMessage());
		}
		return WhereType.ENVIRONMENT.equals(parser.getWhereType());
	}

	public List<What> getWhat() throws UnknownStateException, UnknownWhoException, UnSupportedStateException, UnknownStateValueException {
		return OpenWebNetConverterRegistry.getConverter(getWho()).convert(parser.getWhat());
	}

	public Class<? extends Device> getWho() throws UnknownWhoException{
		return OpenWebNetConverterRegistry.getConverter(parser.getWho()).getHomeSnapWho();
	}

	public String getWhere() {
		return parser.getWhere();
	}

	public String getGroup() {
		return parser.getGroup();
	}

	public String getEnvironment() {
		return parser.getEnvironment();
	}

	public SpecialCommand getSpecialCommand() {
		return new SpecialCommand(parser);
	}

	public List<What> getDimension() throws UnknownStateException, UnknownWhoException, UnSupportedStateException, UnknownStateValueException {
		return OpenWebNetConverterRegistry.getConverter(getWho()).convertDimension(parser.getDimension(), parser.getDimensionList());
	}

	public String toString() {
		return command;
	}
	
	public class SpecialCommand {

		private CommandParser parser;

		protected SpecialCommand(CommandParser parser) {
			this.parser = parser;
		}
		
		public String getActuator() {
			return parser.getActuator();
		}

		public String getZone() {
			return parser.getZone();
		}
	}
}

