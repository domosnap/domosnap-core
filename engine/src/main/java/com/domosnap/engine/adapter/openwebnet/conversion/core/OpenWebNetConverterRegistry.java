package com.domosnap.engine.adapter.openwebnet.conversion.core;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.adapter.openwebnet.conversion.counter.CounterConverter;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.HeatingZoneConverter;
import com.domosnap.engine.adapter.openwebnet.conversion.light.LightStatusConverter;
import com.domosnap.engine.adapter.openwebnet.conversion.shutter.ShutterStatusConverter;
import com.domosnap.engine.device.Device;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class OpenWebNetConverterRegistry {

	static {
		converterList = new ArrayList<Converter>();
		registerConverter(new LightStatusConverter());			//		WHO_LIGHTING("1"),
		registerConverter(new ShutterStatusConverter()); 	//		WHO_AUTOMATION("2"),
		// WHO 28 €
//		WHO_SCENARIO("0"),
//		WHO_POWER_MANAGEMENT("3"),
		registerConverter(new HeatingZoneConverter());			//		WHO_HEATING_ADJUSTMENT("4"),
//		WHO_BURGLAR_ALARM("5"), // not supported
//		WHO_DOOR_ENTRY("6"), // not supported
//		WHO_MULTIMEDIA("7"),
//		WHO_AUXILIARY("9"), // not supported
		registerConverter(new GatewayStatusConverter());		//		WHO_GATEWAY("13"),
//		WHO_LIGHT_SHUTTERS_ACTUATORS_LOCK("14"), // not supported
//		WHO_CEN_SCENARIO_SCHEDULER_SWITCH("15"), // not supported
//		WHO_SOUND_SYSTEM("16"),
//		WHO_SCENARIO_PROGRAMMING("17"), // not supported
		registerConverter(new CounterConverter()); //		WHO_ENERGY_MANAGEMENT("18"),
//		WHO_LIGHT_MANAGEMENT("24"), // not supported
//		WHO_CEN_SCENARIO_SCHEDULER_BUTTONS("25"), // not supported
//		WHO_DIAGNOSTIC("1000"), // not supported
//		WHO_AUTOMATION_DIAGNOSTIC("1001"), // not supported
//		WHO_DIAGNOSTIC_OF_HEATING_ADJUSTMENT("1004"),
//		WHO_DEVICE_DIAGNOSTIC("1013"); // not supported
	}
	
	private static List<Converter> converterList;
	
	private static void registerConverter(Converter c)  {
		converterList.add(c);
	}
	
	public static Converter getConverter(String who) throws UnknownWhoException {
		for (Converter converter : converterList) {
			if (converter.getOpenWebWho().equals(who)) {
				return converter;
			}
		}
		throw new UnknownWhoException();
	}
	
	public static Converter getConverter(Class<? extends Device> who) throws UnknownWhoException {
		for (Converter converter : converterList) {
			if (converter.getHomeSnapWho().equals(who)) {
				return converter;
			}
		}
		throw new UnknownWhoException();
	}
}
