package com.domosnap.engine.adapter.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.ParseException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.TokenMgrError;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Event;

import io.reactivex.rxjava3.functions.Function;

public class OpenWebNetToEventFunction implements Function<String, Publisher<Event>> {

	private final Log log = new Log(OpenWebNetToEventFunction.class.getSimpleName());
	private final String uri;
	
	
	public OpenWebNetToEventFunction(String uri) {
		super();
		if (uri.endsWith("/")) {
			this.uri = uri;
		} else {
			this.uri = uri+"/";
		}
	}

	@Override
	public Publisher<Event> apply(final String message) throws Exception {
		return event -> {
				try {
					OpenWebNetCommand command = new OpenWebNetCommand(message);

					if (!(command.isDimensionCommand() || command.isStandardCommand())) {
						log.severe(Session.MONITOR, "Message received [" + message +"] don't match with standard command or dimension command.", 1);
						return;
					}

					// TODO manage message on other bus
					if (command.isGeneralCommand()) {
						// We send command to all correct address
						for (int i = 11; i < 100; i++) {
							if (i % 10 != 0) { // group address (20, 30, ..) are not correct
								updateController( ""+i, command, event);
							}
						}
					} else if (command.isGroupCommand()) {
						// We send command to group address
// TODO reactivate group
//							for (Label label : controller.getLabels()) {
//								if (label.getId().equals(command.getGroup()) &&
//										controller.getClass().equals(command.getWho())) {
//									if (command.isStandardCommand()) {
//										for(What what : command.getWhat()) {
//											Event c = new Event(command.getWho(), what, new Where(command.getWhere()), Type.COMMAND);
//											arg0.onNext(c);
//										}
//									} else {
//										for(What what : command.getDimension()) {
//											Event c = new Event(command.getWho(), what, new Where(command.getWhere()), Type.COMMAND);
//											arg0.onNext(c);
//										}
//									}
//									log.finest(Session.Monitor, "Message ["+ command.toString() + "] forwarded to controller.", 1);
//								}
//							}
					} else if (command.isEnvironmentCommand()) {
						String environment = command.getEnvironment();
						// We send ambiance command to address
						for (int i = 1; i < 10; i++) {
							updateController(environment+i, command, event);
						}
					} else {
						// Command direct on a controller
						updateController(command.getWhere(), command, event);
					}

			} catch (ParseException e) {
				log.warning(Session.MONITOR, "Unknown message received [" + message +"]. Message dropped.", 1);
			} catch (UnknownStateException e) {
				log.warning(Session.MONITOR, "Unknown state received [" + message +"]. Message dropped.", 1);
			} catch (UnknownWhoException e) {
				log.warning(Session.MONITOR, "Unknown who received [" + message +"]. Message dropped.", 1);
			} catch (UnSupportedStateException e) {
				log.warning(Session.MONITOR, "UnSupportedState ["+ message + "]. Message dropped.", 1);
			} catch (UnknownStateValueException e) {
				log.warning(Session.MONITOR, "UnknowStateValue ["+ message + "]. Message dropped.", 1);
			} catch (TokenMgrError e) {
				log.severe(Session.MONITOR, "Received unexpected message ["+ message + "]. Message dropped but probably there is a serious problem with protocol implementation. ", 1);
				throw e;
			} catch (Exception e) {
				log.severe(Session.MONITOR, "Exception occurs with message [" + message +"]. Message dropped. " +e.getMessage(), 1);
			}
		};
	}
		
	private void updateController(String where, OpenWebNetCommand command, Subscriber<? super Event> arg0) throws UnknownStateException, UnknownWhoException, UnSupportedStateException, UnknownStateValueException {

		// Manage what command 
			if (command.isStandardCommand()) {
				for(What what : command.getWhat()) {
					Event c = new Event(command.getWho(), new Where(uri+where), what);
					log.finest(Session.MONITOR, "Message ["+ command + "] transformed to command [" + c + "].", 1);
					arg0.onNext(c);
				}
			} else {
				for(What what : command.getDimension()) {
					Event c = new Event(command.getWho(), new Where(uri+where), what);
					log.finest(Session.MONITOR, "Message ["+ command + "] transformed to command [" + c + "].", 1);
					arg0.onNext(c);
				}
			}
	}
}
