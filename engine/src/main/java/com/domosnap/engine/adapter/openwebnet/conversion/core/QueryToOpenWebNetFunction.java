package com.domosnap.engine.adapter.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.UnknownWhoException;
import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetConstant;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.device.gateway.Gateway;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.functions.Function;
import org.reactivestreams.Publisher;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class QueryToOpenWebNetFunction implements Function<Query, Publisher<String>> {

	private final Log log = new Log(this.getClass().getSimpleName());
	private static final String STATUS = "STATUS";

	@Override
	public Publisher<String> apply(final Query query) throws Exception {
		return subscriber -> {
			final List<String> converted = createMessage(query);
			if (converted.isEmpty()) {
				if(log.finestLevel) {
					log.fine(Session.QUERY, "No translation command for : " + query, 1);
				}
			} else {
				for (String string : converted) {
					if(log.finestLevel) {
						log.fine(Session.QUERY, "Lunch command.", 1);
					}
					subscriber.onNext(string);
				}
			}
		};
	}

	/**
	 * Create the open message for action or status.
	 * 
	 * @return open web net message.
	 */
	protected final List<String> createMessage(Query query) {
		if (query.getWhere() == null || query.getWhere().getPath() == null
			&& !Gateway.class.equals(query.getWho())) {
			log.fine(Session.QUERY, "Query must contain a where.", 1);
			throw new IllegalArgumentException("Query must contain a where");
		}
		List<String> commandList  = new ArrayList<>();
		for (What what : query.getWhatList()) {
			try {
				Converter converter = OpenWebNetConverterRegistry.getConverter(query.getWho());
				String who =  converter.getOpenWebWho();
				String where = query.getWhere().getPath();
				
				if (converter.isDimension(what)) { // Dimension
					List<DimensionStatus<?>> dimensionStatus = converter.convertDimension(what);
					for (DimensionStatus<?> dim : dimensionStatus) {
						commandList.add(MessageFormat.format(
								OpenWebNetConstant.DIMENSION_STATUS, who, where, dim.getCode()));
					}
				} else if (STATUS.equals(what.getName())) {
					// Only one command even if what list is > 1 since in SCS only has one status
					// Other (level, blink, etc... ) are just a status! So it is unusefull to send more
					// than once the same command
					// If need more than one status = use a dimension!
					commandList.add(
							MessageFormat.format(
									OpenWebNetConstant.STATUS,
									who, where
							)
					);
				}

			} catch (UnknownStateException e) {
				log.fine(Session.QUERY,"Device status unknown [" + what.getName() + "]", 1);
			} catch (UnknownWhoException e) {
				throw new IllegalArgumentException("Device unknown. [" + query.getWho() + "]");
			} catch (UnSupportedStateException e) {
				log.fine(Session.QUERY,"Device status unsupported. [" + what.getName() + "]", 1);
			} catch (UnknownStateValueException e) {
				log.fine(Session.QUERY,"Device status value unknown [" + what.getValue() + "]", 1);
			}
		}
		return commandList;
	}
}
