package com.domosnap.engine.adapter.openwebnet.conversion.counter;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.counter.dimension.Power;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.engine.device.counter.PowerCounter.CounterStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.WattState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class CounterConverter implements Converter {
	
	public static final String OPEN_WEB_WHO = "18";
	
//	public enum CounterStatus {
//		// ACTIVATION_AUTOMATIC_RESET("26"),
//		// DEACTIVATION_AUTOMATIC_RESET("27"),
//		// START_SENDING_DAILY_TOTALIZERS("57"),
//		// START_SENDING_MONTHLY_ON_AN_HOURLY_BASIS("58"),
//		// START_SENDING_MONTHLY_ON_DAILY_BASIS("59"),
//		// ENABLE_ACTUATOR("71"),
//		// FORCE_ACTUATOR("73"),
//		// END_FORCED_ACTUATOR("74"),
//		// RESET_TOTALIZERS("75");
//
//		private String openWebNetCode;
//
//		private CounterStatus(String code) {
//			this.openWebNetCode = code;
//		}
//
//		public String getCode() {
//			return openWebNetCode;
//		}
//		
//		public static CounterStatus fromValue(String code) {
//			for (CounterStatus zone: CounterStatus.values()) {
//				if (zone.getCode().equals(code))
//					return zone;
//			}
//			return null;
//		}
//	}
	
	public enum CounterDimension {
		ACTIVE_POWER("113"), 
		//END_AUTOMATIC_UPDATE_SIZE("1200"),
		ENERGY_UNIT_TOTALIZER("51"),
		//ENERGY_UNIT_BY_MONTH("52"),
		PARTIAL_TOTALIZER_FOR_CURRENT_MONTH("53"),
		PARTIAL_TOTALIZER_FOR_CURRENT_DAY("54");
		//ACTUATORS_INFO("71"),
		//TOTALIZERS("72"),
		//DIFFERENTIAL_CURRENT_LEVEL("73"),
		//DAILY_TOTALIZERS_ON_AN_HOURLY_BASIS("511"),
		//MONTHLY_AVERAGE_ON_AN_HOURLY_BASIS("512"),
		//MONTHLY_TOTALIZERS_CURRENT_YEAR_ON_A_DAILY_BASIS("513"),
		//MONTHLY_TOTALIZERS_LAS_YEAR_ON_A_DAILY_BASIS("514");
	
		private final String openWebNetCode;
	
		CounterDimension(String openWebNetCode) {
			this.openWebNetCode = openWebNetCode;
		}
	
		public String getCode() {
			return openWebNetCode;
		}
		
		public static CounterDimension fromValue(String code) {
			for (CounterDimension gd : CounterDimension.values()) {
				if (gd.getCode().equals(code))
					return gd;
			}
			
			return null;
		}
	}

	@Override
	public List<String> convert(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		if (what == null) {
			return new ArrayList<>();
		}
		
		throw new UnknownStateException();
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

		CounterStateName s = CounterStateName.valueOf(what.getName());
		List<DimensionStatus<?>> result = new ArrayList<>();
		
		switch (s) {
			case ACTIVE_POWER:
				Power mt = new Power(CounterDimension.ACTIVE_POWER.getCode());
				if (what.getValue() != null) {
					mt.setStateValue((WattState) what.getValue());
				}
				result.add(mt);
				break;
			case CURRENT_DAY:
				Power cd= new Power(CounterDimension.PARTIAL_TOTALIZER_FOR_CURRENT_DAY.getCode());
				if (what.getValue() != null) {
					cd.setStateValue((WattState) what.getValue());
				}
				result.add(cd);
				break;
			case CURRENT_MONTH:
				Power cm= new Power(CounterDimension.PARTIAL_TOTALIZER_FOR_CURRENT_MONTH.getCode());
				if (what.getValue() != null) {
					cm.setStateValue((WattState) what.getValue());
				}
				result.add(cm);
				break;
			case TOTAL:
				Power tm= new Power(CounterDimension.ENERGY_UNIT_TOTALIZER.getCode());
				if (what.getValue() != null) {
					tm.setStateValue((WattState) what.getValue());
				}
				result.add(tm);
				break;
			default:
				throw new UnknownStateException();
		}
		
		return result;
	}

	@Override
	public List<What> convert(String code)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

		throw new UnknownStateException();
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		
		CounterDimension gd = CounterDimension.fromValue(dimension);
		List<What> l = new ArrayList<>();
		if (gd != null) {
			What what;
			switch (gd) {
			case ACTIVE_POWER:
				Power activePower = new Power(CounterDimension.ACTIVE_POWER.getCode());
				activePower.setValueList(dimensionValueList);
				what = new What(CounterStateName.ACTIVE_POWER.name(), activePower.getStateValue());	
				l.add(what);
				break;
			case PARTIAL_TOTALIZER_FOR_CURRENT_MONTH:
				Power montlyPower = new Power(CounterDimension.PARTIAL_TOTALIZER_FOR_CURRENT_MONTH.getCode());
				montlyPower.setValueList(dimensionValueList);
				what = new What(CounterStateName.CURRENT_MONTH.name(), montlyPower.getStateValue());	
				l.add(what);
				break;
			case PARTIAL_TOTALIZER_FOR_CURRENT_DAY:
				Power dayPower = new Power(CounterDimension.PARTIAL_TOTALIZER_FOR_CURRENT_DAY.getCode());
				dayPower.setValueList(dimensionValueList);
				what = new What(CounterStateName.CURRENT_DAY.name(), dayPower.getStateValue());	
				l.add(what);
				break;
			case ENERGY_UNIT_TOTALIZER:
				Power totalPower = new Power(CounterDimension.ENERGY_UNIT_TOTALIZER.getCode());
				totalPower.setValueList(dimensionValueList);
				what = new What(CounterStateName.TOTAL.name(), totalPower.getStateValue());	
				l.add(what);
				break;	
			default:
				throw new UnknownStateException();
			}
			
			return l;
		
		} else {
			throw new UnknownStateException();
		}
	}

	@Override
	public boolean isDimension(What what) {
		
		if (CounterStateName.ACTIVE_POWER.name().equals(what.getName())) {
			return true;
		} else if (CounterStateName.CURRENT_DAY.name().equals(what.getName())) {
			return true;
		} else if (CounterStateName.CURRENT_MONTH.name().equals(what.getName())) {
			return true;
		} else if (CounterStateName.TOTAL.name().equals(what.getName())) {
			return true;
		} 	
		return false;
	}

	@Override
	public String getOpenWebWho() {
		return OPEN_WEB_WHO;
	}

	@Override
	public Class <? extends Device> getHomeSnapWho() {
		return PowerCounter.class;
	}
}
