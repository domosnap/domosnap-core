package com.domosnap.engine.adapter.openwebnet.conversion.counter.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.device.what.impl.WattState;

public class Power extends DimensionStatusImpl<WattState> {

	private int POWER_POS = 0;
	
	public Power(String code) {
		super(new DimensionValue[] { 
				new DimensionValueImpl() // T: temperature not adjust by local offset. Composed by 4 digit: c1c2c3c4 included between "0050" (5° temperature) and "0400" (40° temperature). c1 is always equals to 0, it indicates a positive temperature. The c2c3 couple indicates the temperature between [05°-40°]. c4 indicates the decimal Celsius degree by 0.5° step.
				},
				code
		);
	}

	public Integer getPower() {
		return getIntValue(POWER_POS);
	}
	
	public void setPower(Integer value) {
		setIntValue(value, POWER_POS, 0);
	}

	@Override
	public WattState getStateValue() {
		return new WattState(getPower());
	}

	@Override
	public void setStateValue(WattState value) {
		setPower(((WattState)value).getValue());
	}
}
