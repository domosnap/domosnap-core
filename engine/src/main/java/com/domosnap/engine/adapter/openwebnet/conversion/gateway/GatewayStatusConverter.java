package com.domosnap.engine.adapter.openwebnet.conversion.gateway;

import java.util.ArrayList;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.Date;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.DateTime;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.DistributionVersion;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.FirmwareVersion;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.IpAddress;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.KernelVersion;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.MacAddress;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.Model;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.NetMask;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.Time;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension.UpTime;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.gateway.Gateway;
import com.domosnap.engine.device.gateway.Gateway.GatewayStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.DateState;
import com.domosnap.engine.device.what.impl.IpAddressState;
import com.domosnap.engine.device.what.impl.MacAddressState;
import com.domosnap.engine.device.what.impl.StringState;
import com.domosnap.engine.device.what.impl.TimeState;
import com.domosnap.engine.device.what.impl.VersionState;

public class GatewayStatusConverter implements Converter {
	
	public static final String OPEN_WEB_WHO = "13";
	
	public enum GatewayDimension {
		DATE("1"),
		DATETIME("22"),
		DISTRIBUTION_VERSION("24"),
		FIRMWARE_VERSION("16"),
		IP_ADDRESS("10"),
		KERNEL_VERSION("23"),
		MAC_ADDRESS("12"),
		MODEL("15"),
		NETMASK("11"),
		TIME("0"),
		UPTIME("19");
		
		private final String openWebNetCode;
	
		GatewayDimension(String code) {
			this.openWebNetCode = code;
		}
	
		public String getCode() {
			return openWebNetCode;
		}
		
		public static GatewayDimension fromValue(String value) {
			for (GatewayDimension c : GatewayDimension.values()) {
				if (c.getCode().equals(value)) {
					return c;
				}
			}
			return null;
		}
	
	}

	@Override
	public List<String> convert(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		// Gateway has no state... only dimension.
		throw new UnSupportedStateException();
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		GatewayStateName c = GatewayStateName.valueOf(what.getName());
		
		
		List<DimensionStatus<?>> l = new ArrayList<>();
		switch (c) {
		case DATE:
			Date dd = new Date();
			if (what.getValue() != null) dd.setStateValue((DateState) what.getValue());
			l.add(dd);
			break;

		case DATE_TIME:
			DateTime dt = new DateTime();
			if (what.getValue() != null) dt.setStateValue((DateState) what.getValue());
			l.add(dt);
			break;
			
		case DISTRIBUTION_VERSION:
			DistributionVersion dv = new DistributionVersion();
			if (what.getValue() != null) dv.setStateValue((VersionState) what.getValue());
			l.add(dv);
			break;
			
		case FIRMWARE_VERSION:
			FirmwareVersion fv = new FirmwareVersion();
			if (what.getValue() != null) fv.setStateValue((VersionState) what.getValue());
			l.add(fv);
			break;
			
		case IP_ADDRESS:
			IpAddress ai = new IpAddress();
			if (what.getValue() != null) ai.setStateValue((IpAddressState) what.getValue());
			l.add(ai);
			break;
			
		case KERNEL_VERSION:
			KernelVersion kv = new KernelVersion();
			if (what.getValue() != null) kv.setStateValue((VersionState) what.getValue());
			l.add(kv);
			break;
			
		case MAC_ADDRESS:
			MacAddress ma = new MacAddress();
			if (what.getValue() != null) ma.setStateValue((MacAddressState) what.getValue());
			l.add(ma);
			break;
			
		case MODEL:
			Model m = new Model();
			if (what.getValue() != null) m.setStateValue((StringState) what.getValue());
			l.add(m);
			break;
			
		case NETMASK:
			NetMask nm = new NetMask();
			if (what.getValue() != null) nm.setStateValue((IpAddressState) what.getValue());
			l.add(nm);
			break;
			
		case TIME:
			Time t = new Time();
			if (what.getValue() != null) t.setStateValue((TimeState) what.getValue());
			l.add(t);
			break;
			
		case UPTIME:
			UpTime ut = new UpTime();
			if (what.getValue() != null) ut.setStateValue((DateState) what.getValue());
			l.add(ut);
			break;
			
		default:
			break;
		}
		
		return l;
	}

	@Override
	public List<What> convert(String code)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

		GatewayDimension gd = GatewayDimension.fromValue(dimension);
		List<What> l = new ArrayList<>();
		if (gd == null) {
			return l;
		}
		What what;
		switch (gd) {
		case DATE:
			Date dd = new Date();
			dd.setValueList(dimensionValueList);
			what = new What(GatewayStateName.DATE.name(), dd.getStateValue());
			l.add(what);
			break;

		case DATETIME:
			DateTime dt = new DateTime();
			dt.setValueList(dimensionValueList);
			what = new What(GatewayStateName.DATE_TIME.name(), dt.getStateValue());
			l.add(what);
			break;
			
		case DISTRIBUTION_VERSION:
			DistributionVersion dv = new DistributionVersion();
			dv.setValueList(dimensionValueList);
			what = new What(GatewayStateName.DISTRIBUTION_VERSION.name(), dv.getStateValue());
			l.add(what);
			break;
			
		case FIRMWARE_VERSION:
			FirmwareVersion fv = new FirmwareVersion();
			fv.setValueList(dimensionValueList);
			what = new What(GatewayStateName.FIRMWARE_VERSION.name(), fv.getStateValue());
			l.add(what);
			break;
			
		case IP_ADDRESS:
			IpAddress ia = new IpAddress();
			ia.setValueList(dimensionValueList);
			what = new What(GatewayStateName.IP_ADDRESS.name(), ia.getStateValue());
			l.add(what);
			break;
			
		case KERNEL_VERSION:
			KernelVersion kv = new KernelVersion();
			kv.setValueList(dimensionValueList);
			what = new What(GatewayStateName.KERNEL_VERSION.name(), kv.getStateValue());
			l.add(what);
			break;
			
		case MAC_ADDRESS:
			MacAddress ma = new MacAddress();
			ma.setValueList(dimensionValueList);
			what = new What(GatewayStateName.MAC_ADDRESS.name(), ma.getStateValue());
			l.add(what);
			break;
			
		case MODEL:
			Model m = new Model();
			m.setValueList(dimensionValueList);
			what = new What(GatewayStateName.MODEL.name(), m.getStateValue());
			l.add(what);
			break;
			
		case NETMASK:
			NetMask nm = new NetMask();
			nm.setValueList(dimensionValueList);
			what = new What(GatewayStateName.NETMASK.name(), nm.getStateValue());
			l.add(what);
			break;
			
			
		case TIME:
			Time t = new Time();
			t.setValueList(dimensionValueList);
			what = new What(GatewayStateName.TIME.name(), t.getStateValue());
			l.add(what);
			break;
			
		case UPTIME:
			UpTime ut = new UpTime();
			ut.setValueList(dimensionValueList);
			what = new What(GatewayStateName.UPTIME.name(), ut.getStateValue());
			l.add(what);
			break;
			
		default:
			break;
		}
		return l;
	}

	@Override
	public boolean isDimension(What what) {
		return true;
	}

	@Override
	public String getOpenWebWho() {
		return OPEN_WEB_WHO;
	}

	@Override
	public Class<? extends Device> getHomeSnapWho() {
		return Gateway.class;
	}
}
