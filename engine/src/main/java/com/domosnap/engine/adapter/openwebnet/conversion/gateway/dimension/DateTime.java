package com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.device.what.impl.DateState;

public class DateTime extends DimensionStatusImpl<DateState> {
	
	private static final int HOURS_POS = 0;
	private static final int MINUTES_POS = 1;
	private static final int SECONDS_POS = 2;
	private static final int TIMEZONE_POS = 3;
	private static final int DAYOFWEEK_POS = 4;
	private static final int DAY_POS = 5;
	private static final int MONTH_POS = 6;
	private static final int YEAR_POS = 7;
	
	public DateTime() {
		super(new DimensionValue[] { 
				
				new DimensionValueImpl(), // Hours
				new DimensionValueImpl(), // Minute
				new DimensionValueImpl(), // Second
				new DimensionValueImpl(), // Timezone
				new DimensionValueImpl(), // Day of week
				new DimensionValueImpl(), // Day
				new DimensionValueImpl(), // Month
				new DimensionValueImpl()  // Year
				},
			GatewayDimension.DATETIME.getCode()
		);
	}

	private java.util.Date getDate() {
		Calendar c = new GregorianCalendar();
		c.set(Calendar.DAY_OF_WEEK, getIntValue(DAYOFWEEK_POS));
		c.set(Calendar.DAY_OF_MONTH, getIntValue(DAY_POS));
		c.set(Calendar.MONTH, getIntValue(MONTH_POS));
		c.set(Calendar.YEAR, getIntValue(YEAR_POS));
		c.set(Calendar.HOUR, getIntValue(HOURS_POS));
		c.set(Calendar.MINUTE, getIntValue(MINUTES_POS));
		c.set(Calendar.SECOND, getIntValue(SECONDS_POS));
		c.setTimeZone(getTimeZoneValue(TIMEZONE_POS));

		return c.getTime();
	}

	public void setDate(java.util.Date date) {
		Calendar c = new GregorianCalendar();
		c.setTime(date);
		setIntValue(c.get(Calendar.DAY_OF_WEEK), DAYOFWEEK_POS, 2);
		setIntValue(c.get(Calendar.DAY_OF_MONTH), DAY_POS, 2);
		setIntValue(c.get(Calendar.MONTH), MONTH_POS, 2);
		setIntValue(c.get(Calendar.YEAR), YEAR_POS, 4);
		setIntValue(c.get(Calendar.HOUR), HOURS_POS, 2);
		setIntValue(c.get(Calendar.MINUTE), MINUTES_POS, 2);
		setIntValue(c.get(Calendar.SECOND), SECONDS_POS, 2);
		setTimeZoneValue(c.getTimeZone(), TIMEZONE_POS);
	}

	@Override
	public DateState getStateValue() {
		return new DateState(getDate());
	}

	@Override
	public void setStateValue(DateState value) {
		setDate(value.getValue());
	}
}
