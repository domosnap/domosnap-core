package com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.device.what.impl.IncorrectMacAddressException;
import com.domosnap.engine.device.what.impl.MacAddressState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class MacAddress extends DimensionStatusImpl<MacAddressState> {

	private Log log = new Log(MacAddress.class.getSimpleName());

	public MacAddress() {
		super(new DimensionValue[] {
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl()
				}, GatewayDimension.MAC_ADDRESS.getCode());
	}

	@Override
	public MacAddressState getStateValue() {
		try {
			return new MacAddressState(new Byte[] {
					getByteValue(0),
					getByteValue(1),
					getByteValue(2),
					getByteValue(3),
					getByteValue(4),
					getByteValue(5)
			});
		} catch (IncorrectMacAddressException e) {
			log.severe(Session.OTHER, "Wrong Mac address."); // Impossible normally...
			return null;
		}
		
	}

	@Override
	public void setStateValue(MacAddressState value) {
		log.severe(Session.OTHER, "Try to modify MacAddress state: but it is read-only...");
	}
	
	public void setMacAddress(byte addr1, byte addr2, byte addr3, byte addr4, byte addr5, byte addr6) {
		setByteValue(addr1, 0, 0);
		setByteValue(addr2, 1, 0);
		setByteValue(addr3, 2, 0);
		setByteValue(addr4, 3, 0);
		setByteValue(addr5, 4, 0);
		setByteValue(addr6, 5, 0);
	}
}
