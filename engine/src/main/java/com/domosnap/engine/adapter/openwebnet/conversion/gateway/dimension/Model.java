package com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.device.what.impl.StringState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class Model extends DimensionStatusImpl<StringState> {
	
	private static final int MODEL_POS = 0;
	
	public static final int MHSERVER = 2;
	public static final int MH200 = 4;
	public static final int F452 = 6;
	public static final int F452V = 7;
	public static final int MHServer2 = 11;
	public static final int H4684 = 13;
	public static final int ADGTESTSERVER = 19;
	
	
	public Model() {
		super(new DimensionValue[] { 
				new DimensionValueImpl(), // Model
				},
			GatewayDimension.MODEL.getCode()
		);
	}

	@Override
	public StringState getStateValue() {
		int model = getIntValue(MODEL_POS);
		switch (model) {
		case MHSERVER:
			return new StringState("MHServer");
		case MH200:
			return new StringState("MH200");
		case F452:
			return new StringState("F452");
		case F452V:
			return new StringState("F452V");
		case MHServer2:
			return new StringState("MHServer2");
		case H4684:
			return new StringState("H4684");
		case ADGTESTSERVER:
			return new StringState("ADG test Server");
		default:
			return new StringState("Unknown");
		}
	}

	@Override
	public void setStateValue(StringState value) {
		new Log(Model.class.getSimpleName()).severe(Session.OTHER, "Try to modify Model state: but it is read-only...");
	}
	
	public void setModel(int value) {
		setIntValue(value, MODEL_POS, 0);
	}
}
