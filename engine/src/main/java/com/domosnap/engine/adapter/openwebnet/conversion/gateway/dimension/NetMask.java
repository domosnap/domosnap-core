package com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.device.what.impl.IpAddressState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class NetMask extends DimensionStatusImpl<IpAddressState> {

	private Log log = new Log(NetMask.class.getSimpleName());

	public NetMask() {
		super(new DimensionValue[] {
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl(),
				new DimensionValueImpl()
				}, GatewayDimension.NETMASK.getCode());
	}

	@Override
	public IpAddressState getStateValue() {
		
		try {
			return new IpAddressState(Inet4Address.getByAddress(new byte[] {
					getByteValue(0),
					getByteValue(1),
					getByteValue(2),
					getByteValue(3)
			}));
		} catch (UnknownHostException e) {
			log.severe(Session.OTHER, "Unknown host."); // Impossible normally...
			return null;
		}
	}

	@Override
	public void setStateValue(IpAddressState value) {
		log.severe(Session.OTHER, "Try to modify NetMask state: but it is read-only...");
	}

	public void setIpAddress(byte addr1, byte addr2, byte addr3, byte addr4) {
		setByteValue(addr1, 0, 0);
		setByteValue(addr2, 1, 0);
		setByteValue(addr3, 2, 0);
		setByteValue(addr4, 3, 0);
	}
}
