package com.domosnap.engine.adapter.openwebnet.conversion.gateway.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.device.what.impl.TimeState;


public class Time extends DimensionStatusImpl<TimeState> {
	
	private static final int HOURS_POS = 0;
	private static final int MINUTES_POS = 1;
	private static final int SECONDS_POS = 2;
	private static final int TIMEZONE_POS = 3;
	
	public Time() {
		super(new DimensionValue[] { 
				new DimensionValueImpl(), // Hours
				new DimensionValueImpl(), // Min
				new DimensionValueImpl(), // Seconds
				new DimensionValueImpl()  // Timezone
				},
				GatewayDimension.TIME.getCode()
		);
	}

	@Override
	public TimeState getStateValue() {
		return new TimeState(getIntValue(HOURS_POS), getIntValue(MINUTES_POS), getIntValue(SECONDS_POS), getTimeZoneValue(TIMEZONE_POS));
	}

	@Override
	public void setStateValue(TimeState value) {
		setTime(((TimeState)value).getTime());
	}

	public void setTime(java.util.Date time) {
		Calendar c = new GregorianCalendar();
		c.setTime(time);
		setIntValue(c.get(Calendar.HOUR), HOURS_POS, 2);
		setIntValue(c.get(Calendar.MINUTE), MINUTES_POS, 2);
		setIntValue(c.get(Calendar.SECOND), SECONDS_POS, 2);
		setTimeZoneValue(c.getTimeZone(), TIMEZONE_POS);
	}
}
