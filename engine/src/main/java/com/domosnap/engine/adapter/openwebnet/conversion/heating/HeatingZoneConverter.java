package com.domosnap.engine.adapter.openwebnet.conversion.heating;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension.ActuatorStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension.DesiredTemperature;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension.MeasureTemperature;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension.ProbeTemperature;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension.SetOffset;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.heating.HeatingZone;
import com.domosnap.engine.device.heating.HeatingZone.HeatingZoneStateName;
import com.domosnap.engine.device.heating.statevalue.HeatingZoneState;
import com.domosnap.engine.device.heating.statevalue.OffsetState;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.OnOffState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class HeatingZoneConverter implements Converter {
	
	public static final String OPEN_WEB_WHO = "4";
	
	public enum HeatingZoneStatus {
		HEATING_MODE("1"),
		HEATING_OFF("303"),
		THERMAL_PROTECTION("202");

		private final String openWebNetCode;

		HeatingZoneStatus(String code) {
			this.openWebNetCode = code;
		}

		public String getCode() {
			return openWebNetCode;
		}
		
		public static HeatingZoneStatus fromValue(String code) {
			for (HeatingZoneStatus zone: HeatingZoneStatus.values()) {
				if (zone.getCode().equals(code))
					return zone;
			}
			return null;
		}
	}
	
	public enum HeatingZoneDimension {
		MEASURE_TEMPERATURE("0"), 
		FAN_COIL_SPEED("11"),
		PROBE_STATUS("12"),
		LOCAL_OFFSET("13"),
		SET_TEMPERATURE("14"),
		VALVE_STATUS("19"),
		ACTUATOR_STATUS("20");
	
		private final String openWebNetCode;
	
		HeatingZoneDimension(String openWebNetCode) {
			this.openWebNetCode = openWebNetCode;
		}
	
		public String getCode() {
			return openWebNetCode;
		}
		
		public static HeatingZoneDimension fromValue(String code) {
			for (HeatingZoneDimension gd : HeatingZoneDimension.values()) {
				if (gd.getCode().equals(code))
					return gd;
			}
			
			return null;
		}
	}

	@Override
	public List<String> convert(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		if (what == null) {
			return new ArrayList<>();
		}
		
		List<String> result = new ArrayList<>();
		HeatingZoneStateName s = HeatingZoneStateName.valueOf(what.getName());
		
		switch (s) {
			case STATUS:
				HeatingZoneState state = (HeatingZoneState) what.getValue();
				switch (state) {
					case HEATING_MODE:
						result.add(HeatingZoneStatus.HEATING_MODE.getCode());
						break;
					case OFF:
						result.add(HeatingZoneStatus.HEATING_OFF.getCode());
						break;
					case THERMAL_PROTECTION:
						result.add(HeatingZoneStatus.THERMAL_PROTECTION.getCode());
						break;
					default:
						throw new UnknownStateException();
				}
				break;
			default:
				throw new UnknownStateException();
		}		
		return result;
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

		HeatingZoneStateName s = HeatingZoneStateName.valueOf(what.getName());
		List<DimensionStatus<?>> result = new ArrayList<>();
		
		switch (s) {
			case LOCAL_OFFSET:
				SetOffset so = new SetOffset();
				if (what.getValue() != null) {
					so.setStateValue((OffsetState) what.getValue());
				}
				result.add(so);
				break;
			case MEASURE_TEMPERATURE:
				MeasureTemperature mt = new MeasureTemperature();
				if (what.getValue() != null) {
					mt.setStateValue((DoubleState) what.getValue());
				}
				result.add(mt);
				break;
			case SET_TEMPERATURE:
				DesiredTemperature dt = new DesiredTemperature();
				if (what.getValue() != null) {
					dt.setStateValue((DoubleState) what.getValue());
					dt.setMode(1); // TODO Actually heating mode is hard coded...
				}
				result.add(dt);
				break;
			case ACTUATOR_STATUS:
				ActuatorStatus as = new ActuatorStatus();
				if (what.getValue() != null) {
					as.setActuatorStatus(((BooleanState) what.getValue()).getValue());
				}
				result.add(as);
				break;
			case PROBE_TEMPERATURE:
				throw new UnSupportedStateException(); // not writable TODO: needed to query... even if not writable...
				
			default:
				throw new UnknownStateException();
		}
		
		return result;
	}

	@Override
	public List<What> convert(String code)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		HeatingZoneStatus c = HeatingZoneStatus.fromValue(code);
		List<What> result = new ArrayList<>();
		if (c == null) {
			throw new UnknownStateException();
		}
		
		switch (c) {
			case HEATING_MODE:
				result.add(new What(HeatingZoneStateName.STATUS.name(), HeatingZoneState.HEATING_MODE));
				break;
			case HEATING_OFF:
				result.add(new What(HeatingZoneStateName.STATUS.name(), HeatingZoneState.OFF));
				break;
			case THERMAL_PROTECTION:
				result.add(new What(HeatingZoneStateName.STATUS.name(), HeatingZoneState.THERMAL_PROTECTION));
				break;
			default:
				throw new UnknownStateException();
		}
		
		return result;
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		HeatingZoneDimension gd = HeatingZoneDimension.fromValue(dimension);
		List<What> l = new ArrayList<>();
		if (gd != null) {
			What what;
			switch (gd) {
				case ACTUATOR_STATUS:
					if ("1".equals(dimensionValueList.get(0).getValue())) {
						what = new What(HeatingZoneStateName.ACTUATOR_STATUS.name(), OnOffState.On());
					} else {
						what = new What(HeatingZoneStateName.ACTUATOR_STATUS.name(), OnOffState.Off());
					}
					l.add(what);
					break;

				case LOCAL_OFFSET:
					SetOffset so = new SetOffset();
					so.setValueList(dimensionValueList);
					what = new What(HeatingZoneStateName.LOCAL_OFFSET.name(), so.getStateValue());
					l.add(what);
					break;

				case MEASURE_TEMPERATURE:
					MeasureTemperature mt = new MeasureTemperature();
					mt.setValueList(dimensionValueList);
					what = new What(HeatingZoneStateName.MEASURE_TEMPERATURE.name(), mt.getStateValue());
					l.add(what);
					break;

				case PROBE_STATUS:
					ProbeTemperature pt = new ProbeTemperature();
					pt.setValueList(dimensionValueList);
					what = new What(HeatingZoneStateName.PROBE_TEMPERATURE.name(), pt.getStateValue());
					l.add(what);
					break;

				case SET_TEMPERATURE:
					DesiredTemperature dt = new DesiredTemperature();
					dt.setValueList(dimensionValueList);
					what = new What(HeatingZoneStateName.SET_TEMPERATURE.name(), dt.getStateValue());
					l.add(what);
					break;

				case VALVE_STATUS:
				case FAN_COIL_SPEED:
					throw new UnSupportedStateException();

				default:
					throw new UnknownStateException();
			}
			
			return l;
		
		} else {
			throw new UnknownStateException();
		}
	}

	@Override
	public boolean isDimension(What what) {
		
		if (HeatingZoneStateName.LOCAL_OFFSET.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.MEASURE_TEMPERATURE.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.SET_TEMPERATURE.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.STATUS.name().equals(what.getName())) {
			return false;
		} else if (HeatingZoneStateName.ACTUATOR_STATUS.name().equals(what.getName())) {
			return false;
		} 	
		return false;
	}

	@Override
	public String getOpenWebWho() {
		return OPEN_WEB_WHO;
	}

	@Override
	public Class <? extends Device> getHomeSnapWho() {
		return HeatingZone.class;
	}
}
