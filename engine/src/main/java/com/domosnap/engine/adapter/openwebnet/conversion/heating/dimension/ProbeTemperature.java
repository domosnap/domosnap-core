package com.domosnap.engine.adapter.openwebnet.conversion.heating.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.openwebnet.conversion.heating.HeatingZoneConverter.HeatingZoneDimension;
import com.domosnap.engine.device.what.impl.DoubleState;

public class ProbeTemperature extends DimensionStatusImpl<DoubleState> {

	private static final int TEMPERATURE_POS = 0;
	private static final int MODE_POS = 1;

	public ProbeTemperature() {
				super(new DimensionValue[] { 
						new DimensionValueImpl(), // T: temperature not adjust by local offset. Composed by 4 digit: c1c2c3c4 included between "0050" (5° temperature) and "0400" (40° temperature). c1 is always equals to 0, it indicates a positive temperature. The c2c3 couple indicates the temperature between [05°-40°]. c4 indicates the decimal Celsius degree by 0.5° step.
						new DimensionValueImpl()  // 3: always 3
						},
						HeatingZoneDimension.SET_TEMPERATURE.getCode()
				);
	}

	public Double getDesiredTemperature() {
		return getTemperatureValue(TEMPERATURE_POS);
	}

	public void setDesiredTemperature(Double temperature) {
		setTemperatureValue(temperature, TEMPERATURE_POS);
	}

	public int getMode() {
		return getIntValue(MODE_POS);
	}

	public void setMode(int mode) {
		// Check mode
		setIntValue(mode, MODE_POS, 1);
	}

	@Override
	public DoubleState getStateValue() {
		return new DoubleState(getDesiredTemperature());
	}

	@Override
	public void setStateValue(DoubleState value) {
		setDesiredTemperature(value.getDoubleValue());
	}

}
