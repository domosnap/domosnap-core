package com.domosnap.engine.adapter.openwebnet.conversion.light;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.UnSupportedStateException;
import com.domosnap.engine.adapter.UnknownStateException;
import com.domosnap.engine.adapter.UnknownStateValueException;
import com.domosnap.engine.adapter.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.what.impl.PercentageState;

// LIGHT
public class LightStatusConverter implements Converter {

	public enum LightStatus {
		// LEgrand Light Status
		LIGHT_OFF("0"), // TODO manage speed 0 to 255!
		LIGHT_ON("1"), // TODO manage speed!
		LIGHT_ON_20_PERCENT("2"), LIGHT_ON_30_PERCENT("3"), LIGHT_ON_40_PERCENT("4"), LIGHT_ON_50_PERCENT("5"),
		LIGHT_ON_60_PERCENT("6"), LIGHT_ON_70_PERCENT("7"), LIGHT_ON_80_PERCENT("8"), LIGHT_ON_90_PERCENT("9"),
		LIGHT_ON_100_PERCENT("10"), LIGHT_ON_DURING_1_MIN("11"), LIGHT_ON_DURING_2_MIN("12"),
		LIGHT_ON_DURING_3_MIN("13"), LIGHT_ON_DURING_4_MIN("14"), LIGHT_ON_DURING_5_MIN("15"),
		LIGHT_ON_DURING_15_MIN("16"), LIGHT_ON_DURING_30_SEC("17"), LIGHT_ON_DURING_HALF_SEC("18"), LIGHT_ERROR("19"),
		LIGHT_ON_BLINKING_HALF_SEC("20"), LIGHT_ON_BLINKING_1_SEC("21"), LIGHT_ON_BLINKING_1_AND_HALF_SEC("22"),
		LIGHT_ON_BLINKING_2_SEC("23"), LIGHT_ON_BLINKING_2_AND_HALF_SEC("24"), LIGHT_ON_BLINKING_3_SEC("25"),
		LIGHT_ON_BLINKING_3_AND_HALF_SEC("26"), LIGHT_ON_BLINKING_4_SEC("27"), LIGHT_ON_BLINKING_4_AND_HALF_SEC("28"),
		LIGHT_ON_BLINKING_5_SEC("29"), LIGHT_ON_UP_ONE_LEVEL("30"), // TODO manage speed!
		LIGHT_OFF_ONE_LEVEL("31"), // TODO manage speed!

		LIGHT_FORCE_ON("1000#1"), LIGHT_FORCE_OFF("1000#0");

		private final String value;

		LightStatus(String value) {
			this.value = value;
		}

		public String getCode() {
			return value;
		}

		public static LightStatus fromValue(String value) {
			for (LightStatus c : LightStatus.values()) {
				if (c.getCode().equals(value)) {
					return c;
				}
			}
			return null;
		}
	}

	@Override
	public List<What> convert(String code) {

		LightStatus c = LightStatus.fromValue(code);
		List<What> result = new ArrayList<>();
		if (c == null) {
			return result;
		}

		switch (c) {
			case LIGHT_FORCE_OFF:
			case LIGHT_OFF:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.Off()));
				break;
			case LIGHT_FORCE_ON:
			case LIGHT_ON:
			case LIGHT_ON_BLINKING_1_AND_HALF_SEC:
			case LIGHT_ON_BLINKING_1_SEC:
			case LIGHT_ON_BLINKING_2_AND_HALF_SEC:
			case LIGHT_ON_BLINKING_2_SEC:
			case LIGHT_ON_BLINKING_3_AND_HALF_SEC:
			case LIGHT_ON_BLINKING_3_SEC:
			case LIGHT_ON_BLINKING_4_AND_HALF_SEC:
			case LIGHT_ON_BLINKING_4_SEC:
			case LIGHT_ON_BLINKING_5_SEC:
			case LIGHT_ON_BLINKING_HALF_SEC:
			case LIGHT_ON_DURING_15_MIN:
			case LIGHT_ON_DURING_1_MIN:
			case LIGHT_ON_DURING_2_MIN:
			case LIGHT_ON_DURING_30_SEC:
			case LIGHT_ON_DURING_3_MIN:
			case LIGHT_ON_DURING_4_MIN:
			case LIGHT_ON_DURING_5_MIN:
			case LIGHT_ON_DURING_HALF_SEC:
			case LIGHT_ON_UP_ONE_LEVEL:
					result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				break;
			case LIGHT_ON_100_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(100d)));
				break;
			case LIGHT_ON_20_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(20d)));
				break;
			case LIGHT_ON_30_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(30d)));
				break;
			case LIGHT_ON_40_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(40d)));
				break;
			case LIGHT_ON_50_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(50d)));
				break;
			case LIGHT_ON_60_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(60d)));
				break;
			case LIGHT_ON_70_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(70d)));
				break;
			case LIGHT_ON_80_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(80d)));
				break;
			case LIGHT_ON_90_PERCENT:
				result.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
				result.add(new What(LightStateName.LEVEL.name(), new PercentageState(90d)));
				break;
			case LIGHT_ERROR:
			case LIGHT_OFF_ONE_LEVEL:
			default:
				break;
		}

		return result;
	}

	@Override
	public List<String> convert(What what) {
		if (what == null) {
			return null;
		}

		List<String> result = new ArrayList<>();
		LightStateName s = LightStateName.valueOf(what.getName());

		switch (s) {
			case REACHABLE:
				// We do nothing on reachable
				return result;
			case COLOR:
				return result;
			case STATUS:
				OnOffState status = (OnOffState) what.getValue();
				if (OnOffState.Off().getValue().equals(status.getValue())) {
					result.add(LightStatus.LIGHT_OFF.value);
				} else {
					result.add(LightStatus.LIGHT_ON.value);
				}
				return result;
			case LEVEL:
				PercentageState level = (PercentageState) what.getValue();
				result.add(getLevel(level));

				break;
		}
		// TODO for the moment only support status....
		return new ArrayList<>();
	}

	private String getLevel(PercentageState value) {
		double i = value.getValue();
		if (i == 0) {
			return LightStatus.LIGHT_OFF.name();
		} else if (i < 21) {
			return LightStatus.LIGHT_ON_20_PERCENT.name();
		} else if (i < 31) {
			return LightStatus.LIGHT_ON_30_PERCENT.name();
		} else if (i < 41) {
			return LightStatus.LIGHT_ON_40_PERCENT.name();
		} else if (i < 51) {
			return LightStatus.LIGHT_ON_50_PERCENT.name();
		} else if (i < 61) {
			return LightStatus.LIGHT_ON_60_PERCENT.name();
		} else if (i < 71) {
			return LightStatus.LIGHT_ON_70_PERCENT.name();
		} else if (i < 81) {
			return LightStatus.LIGHT_ON_80_PERCENT.name();
		} else if (i < 91) {
			return LightStatus.LIGHT_ON_90_PERCENT.name();
		} else {
			return LightStatus.LIGHT_ON_100_PERCENT.name();
		}
	}

	@Override
	public boolean isDimension(What what) {
		return false; // no dimension for light
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public String getOpenWebWho() {
		return "1";
	}

	@Override
	public Class<? extends Device> getHomeSnapWho() {
		return Light.class;
	}

}
