package com.domosnap.engine.device;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;
import com.domosnap.engine.gateway.Query;
import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.Label;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.processors.PublishProcessor;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Controller is the abstract class for all device supporter by SnapHome.
 * Each subclass represent a device. Controller contains the main logic to manage status.
 *
 */
public abstract class Device implements Closeable {

	private final Log log = new Log(this.getClass().getSimpleName());

	// Model
	private String title; // Title of the controller
	private String description; // Description of the controller
	private final LabelList labelList = new LabelList(this); // List of label where the controller appears
	private Group group; // Group containing this controller
	protected Where where; // Address of the controller
	protected Map<String, State<?>> whatList = Collections.synchronizedMap(new HashMap<>()); //List of all state names with their current values of the controller. This map represents the complete status of the controller.

	// Stream
	protected PublishProcessor<Command> serverCommand; // Stream to send Command
	protected PublishProcessor<Query> serverQuery; // Stream to send Query (separate from command since on big installation when requestAllStatus queue stuck)
	protected List<Disposable> serverHook = new ArrayList<>();// Hook for send command!
	protected Disposable monitor;

	protected final static ExecutorService executorCommand = Executors.newFixedThreadPool(5); // to send command
	protected final static ExecutorService executorQuery = Executors.newFixedThreadPool(5); // to send query
	
	// Listener
	private final List<OnDeviceChangeListener> controllerChangeListenerList = Collections.synchronizedList(new ArrayList<>()); // list of listener
	private final List<Consumer<Command>> commandListenerList = Collections.synchronizedList(new ArrayList<>());

	protected Device(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> qs) {
		addCommander(cs);
		addMonitor(ms);
		addQuery(qs);
		DeviceMappingRegistry.registerDevice(this.getClass());
	}
	
	private final Class<? extends Device> myclass = this.getClass();

	protected void addCommander(PublishProcessor<Command> commandStream) {
		this.serverCommand = commandStream;
	}

	protected void addQuery(PublishProcessor<Query> queryStream) {
		this.serverQuery = queryStream;
	}
	
	protected void addMonitor(Flowable<Event> monitorStream) {
		if (monitorStream != null) {
			this.monitor = monitorStream.subscribe(event -> {
				final Where w = event.getWhere();
				if (myclass.equals(event.getWho()) && (w != null && w.getPath() != null && w.getPath().equals(getWhere().getPath()))) {
					notifyStateChange(event);
				}
			}, error -> log.severe(Session.MONITOR, "Error when receiving event : " + error.getMessage())
			);
		} else if (this.monitor != null){
			this.monitor.dispose();
		}
	}

	@Override
	public void close() throws IOException {
		serverQuery.onComplete();
		serverCommand.onComplete();
		monitor.dispose();
		serverHook.forEach(Disposable::dispose);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	/**
	 * Return the description of the controller
	 * @return description of the controller
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Define the description of the controller.
	 * @param description of the controller
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Return the unique id of the controller
	 * @return unique id of the controller
	 */
	public String getId() {
		return getWhere().getURI().toString();
	}

	/**
	 * Return the list of label associated to this component.
	 * @return list of label.
	 */
	public LabelList getLabels() {
		return labelList;
	}

	/**
	 * Return the group associated to this component.
	 * @return the group associated to this component.
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * Defines the group associated to this component.
	 * <br/><b>Warning</b> Set null here is like to delete physically 
	 * the controller = we remove it from label
	 * @param group the group associated to this component.
	 */
	public void setGroup(Group group) {
		if (group == null){
			// Set group to null is as delete it: remove from label.
			for (Label label : getLabels()) {
				label.remove(this);
			}
		}
		this.group = group;
	}

	/**
	 * Returns the address of the targeted device
	 * @return the address of the targeted device
	 */
	public Where getWhere() {
		return where;
	}

	/**
	 * Defines the address of the targeted device to control
	 * @param newValue the address of the targeted device to control
	 */
	public void setWhere(Where newValue) {
		if ((where == null && newValue != null)
				|| (where != null && !where.equals(newValue))) // Optimize: don't request status if where is the same
		{
			this.where = newValue;
			requestAllStatus();
		}
	}
	
	/**
	 * Clear the cache list of status and request them all.
	 * <b>Careful:</b> request are asynchronously done so even
	 * if the method return, during some millisecond all
	 * status will be not available. 
	 */
	public void requestAllStatus() {
		if (where != null) {
			List<String> whatListToRequest = new ArrayList<>(); // Construct query list
			for (StateName state : getStateList()) {
				whatListToRequest.add(state.name());
			}
			pushQuery(new Query(this.getClass(), where, whatListToRequest));
		}
	}

	public abstract List<StateName> getStateList();

	public Map<String, State<?>> getStateMap() {
		return whatList;
	}

	public BooleanState isReachable() {
		return (BooleanState) get(Light.LightStateName.REACHABLE.name()); // TODO REACHABLE IS NOT "LIGHT" state...
	}

	/**
	 * If current value of a state name is known, provide it directly 
	 * without request the server. 
	 * 
	 * @param name The state name
	 * @return The current value of a state name or null if a request to the server
	 * is done (when current value is null).
	 */
	protected State<?> get(final String name) {
		if (name == null) {
			throw new NullPointerException("Could not get null state name.");
		}

		State<?> value = whatList.get(name);

		if (value == null) { // If value = null we lunch a query/refresh and return null
			pushQuery(new Query(this.getClass(), where, name));
		}

		return value;
	}

	/**
	 * Update a value of the status.
	 * @param name The state name to update
	 * @param state The new value of the state name
	 */
	protected void set(String name, State<?> state) {

		if (state == null || name == null) {
			throw new NullPointerException("Could not set state name [" + name + "," + state + "]");
		}

		pushCommand(new Command(this.getClass(), where, new What(name, state)));
	}

	protected void pushQuery(final Query query) {
		if (serverQuery == null) {
			log.info(Session.QUERY, "Impossible to send query since queryStream is null. Maybe your build your own Device and forget to provide a Query stream before set where? Else there is a problem with the builder.");
			return;
		}
		executorQuery.execute(() -> {
			// The query is sent to the gateway. Gateway transmits it to the actuator.
			// If everything is fine, Gateway provides through the monitor session
			// the new status => not need to set it here since it will be set by the
			// monitor way.
			if (!serverQuery.offer(query)) {
				log.warning(Session.QUERY, "Query [" + query + "] was not publish to all device subscriber.");
			}

			for (Consumer<Command> listener: commandListenerList) {
				try {
					listener.accept(query);
				} catch (Throwable e) {
					if (log.errorLevel) {
						log.severe(Session.QUERY, "Error in Consumer<Command> with error: " + e.getMessage());
					}
				}
			}

			if (log.finestLevel) {
				log.fine(Session.QUERY, "Query sent to adapter [".concat(query.toString()).concat("]"));
			}
		});

	}

	protected void pushCommand(final Command command) {
		if (serverCommand == null) {
			log.info(Session.COMMAND, "Impossible to send command since commandStream is null. Maybe your build your own Device and forget to provide a Command stream before set where? Else there is a problem with the builder.");
			return;
		}
		executorCommand.execute(() -> {
			// The command is sent to the gateway. Gateway transmits it to the actuator.
			// If everything is fine, Gateway provides through the monitor session
			// the new status => not need to set it here since it will be set by the
			// monitor way.
			if (!serverCommand.offer(command)) {
				log.warning(Session.COMMAND, "Command [" + command + "] was not publish to all device subscriber.");
			}

			for (Consumer<Command> listener: commandListenerList) {
				try {
					listener.accept(command);
				} catch (Throwable e) {
					if (log.errorLevel) {
						log.severe(Session.COMMAND, "Error in Consumer<Command> with error: " + e.getMessage());
					}
				}
			}

			if (log.finestLevel) {
				log.fine(Session.COMMAND, "Command sent to adapter [".concat(command.toString()).concat("]"));
			}
		});

	}

	protected void addCommandProvider(Flowable<Command> hook) {
		serverHook.add(hook.subscribe(this::pushCommand));
	}

	protected void addCommandConsumer(Consumer<Command> hook) {
		this.commandListenerList.add(hook);
	}

	protected void removeCommandConsumer(Consumer<Command> hook) {
		this.commandListenerList.remove(hook);
	}
	
	/**
	 * @param listener
	 *            the new change listener.
	 */
	public void addControllerChangeListener(OnDeviceChangeListener listener) {
		controllerChangeListenerList.add(listener);
	}

	/**
	 * @param listener
	 *            the change listener to remove.
	 */
	public void removeControllerChangeListener(OnDeviceChangeListener listener) {
		controllerChangeListenerList.remove(listener);
	}
	
	protected void notifyStateChange(Event command) {
		for (What what : command.getWhatList()) {
			State<?> oldState = whatList.get(what.getName());
			whatList.put(what.getName(), what.getValue());
			if (log.finestLevel) {
				log.fine(Session.MONITOR, "Controller [" + getWhere().toString() + "] updated with command [" + command + "]");
			}
			for (OnDeviceChangeListener listener : controllerChangeListenerList) {
				listener.onStateChange(this,new What(what.getName(), oldState) , what);
			}
		}
	}
	
	@Override
	public String toString() {
		return DeviceJsonCodec.toJson(this);
	}
	
	public String getType() {
		return DeviceMappingRegistry.getMapping(this.getClass());
	}
}
