package com.domosnap.engine.device;

import java.lang.reflect.InvocationTargetException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;

public class DeviceHelper {

	private static final Log log = new Log(DeviceHelper.class.getSimpleName());
	
	private final Device c;
	
	public DeviceHelper(Device c) {
		this.c=c;
	}
	
	public void set(String name, State<?> status) {
		c.set(name, status);
	}
	
	public void set(String name, String value, Class<State<?>> statusType) {
		try {
			if (BooleanState.class.equals(statusType)) {
				if ("true".equalsIgnoreCase(value)) {
					set(name, BooleanState.TRUE);
				} else if ("false".equalsIgnoreCase(value)) {
					set(name, BooleanState.FALSE);
				}
			} else {
				State<?> status = (State<?>) statusType.getConstructors()[0].newInstance(value);
				set(name, status);
			}
			
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| SecurityException e) {
			log.warning(Session.COMMAND, "Impossible to write state [" + name + "] to value [" + value + "]. Original error: " + e.getMessage());
		}
	}

	public State<?> get(String name) {
		return c.get(name);
	}
	
	public void addCommandProvider(Flowable<Command> commandFlowable) {
		c.addCommandProvider(commandFlowable);
	}
	
	public void addCommandConsumer(Consumer<Command> commandListener) {
		c.addCommandConsumer(commandListener);
	}
	
	public void removeCommandConsumer(Consumer<Command> commandListener) {
		c.removeCommandConsumer(commandListener);
	}
	
	public void pushCommand(Command command) {
		c.pushCommand(command);
	}
	
	public void notifyChange(Event event) {
		c.notifyStateChange(event);
	}

	/**
	 * Return the status of a device. If the state is null, this method
	 * will retry to get the status 10 times all 10ms.
	 * Warning: this method block the thread!
	 * @param name the name of the state.
	 * @return the value of the state or null.
	 */
	public State<?> getValue(String name) {
		State<?> result = c.get(name);
		if (result == null) {

			int i = 0;
			  while (i<20) { // after 10 try, I will return value (null if no state)
				  try {
					  i++;
					  if (c.whatList.get(name) != null) {
						  result = c.whatList.get(name);
						  break;
					  }
					  Thread.sleep(100);
					} catch (InterruptedException e) {
					  log.warning(Session.DEVICE, e.getMessage());
					  Thread.currentThread().interrupt();
					}
			  }
		}

		return result;

	}
}
