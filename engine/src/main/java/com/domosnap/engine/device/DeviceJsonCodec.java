package com.domosnap.engine.device;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.what.WhatJsonCodec;
import com.domosnap.engine.device.where.Where;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DeviceJsonCodec {

	private static final String JSON_SEPARATOR = "\":\"";
	public static final String JSON_TITLE = "title";
	public static final String JSON_STATES = "states";
	public static final String JSON_WHERE = "where";
	public static final String JSON_WHO = "who";
	public static final String JSON_DESCRIPTION = "description";
	
	private DeviceJsonCodec() {
	}
	
	public static String toJson(Device device) {
		return toJson(device, true);
	}

	public static String toJson(Device device, boolean hidePassword) {
		StringBuilder result = new StringBuilder();
		buildJson(device, hidePassword, result);
		return result.toString();
	}
	
	public static void buildJson(Device device, boolean hidePassword, StringBuilder sb) {
	
		sb.append("{\"").append(JSON_WHO).append(JSON_SEPARATOR).append(device.getType()).append("\"");
		if (device.getTitle() == null) {
			sb.append(",\"").append(JSON_TITLE).append("\":null");
		} else {
		sb.append(",\"").append(JSON_TITLE).append(JSON_SEPARATOR).append(device.getTitle()).append("\"");
		}
		if (device.getDescription() == null) {
			sb.append(",\"").append(JSON_DESCRIPTION).append("\":null");
		} else {
			sb.append(",\"").append(JSON_DESCRIPTION).append(JSON_SEPARATOR).append(device.getDescription()).append("\"");
		}

		if (device.getWhere() != null) {
			sb.append(",\"").append(JSON_WHERE).append(JSON_SEPARATOR).append(hidePassword ? device.getWhere().toString() : device.getId() ).append("\"");
		}

		sb.append(",\"").append(JSON_STATES).append("\":");
		if (!device.whatList.isEmpty()) {
			sb.append(WhatJsonCodec.toJSon(device.whatList));
		}

		sb.append("}");
	}

	public static Device fromJson(Device device, String json) {
		
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		
		if (jsonObject.has(JSON_TITLE)) {
			JsonElement element = jsonObject.get(JSON_TITLE);
			if (element == null) {
				device.setTitle(null);
			} else {
				device.setTitle(element.getAsString());
			}
		}
	
		if (jsonObject.has(JSON_DESCRIPTION)) {
			JsonElement element = jsonObject.get(JSON_DESCRIPTION);
			if (element == null) {
				device.setDescription(null);
			} else {
				device.setDescription(element.getAsString());
			}
		}

		if (jsonObject.has(JSON_WHERE)) {
			JsonElement element = jsonObject.get(JSON_WHERE);
			if (element != null) {
				device.setWhere(new Where(element.getAsString()));
			}
		}
	
		// We don't deserialized state since device we get from physical world!
		
		return device;
	}
}
