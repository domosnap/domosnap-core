package com.domosnap.engine.device;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.engine.device.gateway.Gateway;
import com.domosnap.engine.device.heating.HeatingZone;
import com.domosnap.engine.device.humidity.HumiditySensor;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.LightSensor;
import com.domosnap.engine.device.nfc.Nfc;
import com.domosnap.engine.device.pressure.PressureSensor;
import com.domosnap.engine.device.shutter.Shutter;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.device.uv.UvSensor;

public class DeviceMappingRegistry {

	private static Log log = new Log(DeviceMappingRegistry.class.getSimpleName());
	
	private static MappingRegistry<Device> map = new MappingRegistry<>();

	static {
		registerDevice(PowerCounter.class);
		registerDevice(Gateway.class);
		registerDevice(HeatingZone.class);
		registerDevice(HumiditySensor.class);
		registerDevice(Light.class);
		registerDevice(LightSensor.class);
		registerDevice(Nfc.class);
		registerDevice(PressureSensor.class);
		registerDevice(Shutter.class);
		registerDevice(TemperatureSensor.class);
		registerDevice(UvSensor.class);
	}
	
	private DeviceMappingRegistry() {
	}
	
	public static String registerDevice(Class<? extends Device> clazz) {
		return map.register(clazz);
	}
	
	@SuppressWarnings("unchecked")
	public static Class<? extends Device> getMapping(String name) {
		Class<? extends Device> result = map.getMapping(name);
		try {
			return result == null ? (Class<? extends Device>) Class.forName(name) : result;
		} catch (ClassNotFoundException | ClassCastException e) {
			log.finest(Session.DEVICE, e.getMessage());
			return null;
		}
	}
	
	public static String getMapping(Class<? extends Device> clazz) {
		String result = map.getMapping(clazz);
		return result == null ? clazz.getName() : result;
	}
}
