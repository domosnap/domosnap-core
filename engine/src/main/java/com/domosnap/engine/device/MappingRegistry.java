package com.domosnap.engine.device;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

public class MappingRegistry<T> {
	
	private Map<String, Class<? extends T>> map = new HashMap<>();
	private Map<Class<? extends T>, String> map2 = new HashMap<>();
	
	
	private static Log log = new Log(MappingRegistry.class.getSimpleName());
		
	public String register(Class<? extends T> clazz) {
		if (map2.containsKey(clazz)) {
			return map2.get(clazz);
		}
		
		Mapping tag = clazz.getAnnotation(Mapping.class);
		if (tag == null) {
			return 	register(clazz.getSimpleName(), clazz);
		} else {
			return 	register(tag.name(), clazz);
		}
	}

	private String register(String name, Class<? extends T> clazz) {
		map.put(name, clazz);
		map2.put(clazz, name);
		if (log.finestLevel) {
			log.finest(Session.DEVICE, MessageFormat.format("What registered [{0}].", name));
		}
		return name;
	}
	
	public Class<? extends T> getMapping(String name) {
		return map.get(name);
	}
	
	public String getMapping(Class<? extends T> clazz) {
		String result = map2.get(clazz);
		
		if (result == null)
			return register(clazz);
		else
			return result;
	}
}
