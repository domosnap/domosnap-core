package com.domosnap.engine.device.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.impl.DateState;
import com.domosnap.engine.device.what.impl.IpAddressState;
import com.domosnap.engine.device.what.impl.MacAddressState;
import com.domosnap.engine.device.what.impl.StringState;
import com.domosnap.engine.device.what.impl.TimeState;
import com.domosnap.engine.device.what.impl.VersionState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

@Mapping(name="Gateway")
public class Gateway extends Device {

	public Gateway(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	public Date getDate() {
		return ((DateState) get(GatewayStateName.DATE.name())).getValue();
	}

	public void setDate(Date newDate) {
		set(GatewayStateName.DATE.name(), new DateState(newDate));
	}
	
	public InetAddress getIpAddress() {
		return ((IpAddressState) get(GatewayStateName.IP_ADDRESS.name())).getValue();
	}

	public InetAddress getNetMask() {
		return ((IpAddressState) get(GatewayStateName.NETMASK.name())).getValue();
	}

	public String getDeviceType() {
		// TODO Protect if get return null...
		return ((StringState) get(GatewayStateName.MODEL.name())).getValue();
	}

	public VersionState getFirmwareVersion() {
		return (VersionState) get(GatewayStateName.FIRMWARE_VERSION.name());
	}

	public DateState getUpTime() {
		return (DateState) get(GatewayStateName.UPTIME.name());
	}

	public VersionState getKernelVersion() {
		return (VersionState) get(GatewayStateName.KERNEL_VERSION.name());
	}

	public VersionState getDistributionVersion() {
		return (VersionState) get(GatewayStateName.DISTRIBUTION_VERSION.name());
	}

	@Override
	public List<StateName> getStateList() {
		return Arrays.asList(GatewayStateName.values());
	}
	
	public enum GatewayStateName implements StateName {
		DATE(DateState.class),
		DATE_TIME(DateState.class),
		DISTRIBUTION_VERSION(VersionState.class),
		FIRMWARE_VERSION(VersionState.class),
		IP_ADDRESS(IpAddressState.class),
		KERNEL_VERSION(VersionState.class),
		MAC_ADDRESS(MacAddressState.class),
		MODEL(StringState.class),
		NETMASK(IpAddressState.class),
		TIME(TimeState.class),
		UPTIME(DateState.class);
		
		private final Class<? extends State<?>> state;
		
		GatewayStateName(Class<? extends State<?>> state) {
			this.state = state;
		}
		
		public Class<? extends State<?>> getType() {
			return state;
		}
	}
}
