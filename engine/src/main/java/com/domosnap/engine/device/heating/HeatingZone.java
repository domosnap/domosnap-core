package com.domosnap.engine.device.heating;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Arrays;
import java.util.List;

import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.heating.statevalue.HeatingZoneState;
import com.domosnap.engine.device.heating.statevalue.OffsetState;
import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.TemperatureUnitState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

@Mapping(name="Heating")
public class HeatingZone extends TemperatureSensor { 

	public HeatingZone(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	public HeatingZoneState getStatus() {
		return (HeatingZoneState) get(HeatingZoneStateName.STATUS.name());
	}

	@Override
	public DoubleState getTemperature() {
		return getMeasureTemperature();
	}

	@Override
	public TemperatureUnitState getUnit() {
		return TemperatureUnitState.celsius();
	}

	@Override
	public void setUnit(TemperatureUnitState value) {
		throw new UnsupportedOperationException();
	}

	public void setStatus(HeatingZoneState value) {
		set(HeatingZoneStateName.STATUS.name(), value);
	}

	public DoubleState getDesiredTemperature() {
		return (DoubleState) get(HeatingZoneStateName.SET_TEMPERATURE.name());
	}

	public void setOffset(OffsetState offset) {
		set(HeatingZoneStateName.LOCAL_OFFSET.name(), offset);
	}

	public OffsetState getOffset() {
		return (OffsetState) get(HeatingZoneStateName.LOCAL_OFFSET.name());
	}

	public DoubleState getMeasureTemperature() {
		return (DoubleState) get(HeatingZoneStateName.MEASURE_TEMPERATURE.name());
	}

	@Override
	public List<StateName> getStateList() {
		return Arrays.asList(HeatingZoneStateName.values());
	}
	
	public enum HeatingZoneStateName implements StateName {
		
		MEASURE_TEMPERATURE(DoubleState.class), 
		LOCAL_OFFSET(OffsetState.class),
		SET_TEMPERATURE(DoubleState.class),
		STATUS(HeatingZoneState.class),
		ACTUATOR_STATUS(null),  // TODO manage different zone
		PROBE_TEMPERATURE(null);
		
		private final Class<? extends State<?>> state;
		
		HeatingZoneStateName(Class<? extends State<?>> state) {
			this.state = state;
		}
		
		public Class<? extends State<?>> getType() {
			return state;
		}
	}
}
