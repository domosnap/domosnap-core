package com.domosnap.engine.device.light;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.what.impl.PercentageState;
import com.domosnap.engine.device.what.impl.RGBState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

@Mapping(name="Light")
public class Light extends Device {

	public Light(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	public OnOffState getStatus() {
		return (OnOffState) get(LightStateName.STATUS.name());
	}

	public void setStatus(OnOffState status) {
		set(LightStateName.STATUS.name(), status);
	}

	public PercentageState getLevel() {
		return (PercentageState) get(LightStateName.LEVEL.name());
	}
	
	public void setLevel(PercentageState value) {
		set(LightStateName.LEVEL.name(), value);
	}
	
	public RGBState getColor() {
		return (RGBState) get(LightStateName.COLOR.name());
	}

	@Override
	public List<StateName> getStateList() {
		return Arrays.asList(LightStateName.values());
	}
	
	/**
	 * Enumeration of state names for Light controller.
	 */
	public enum LightStateName implements StateName {
		/** explain the availability to reach the light with the protocol */
		REACHABLE(OnOffState.class),
		/** Contains the hexadecimal color code for RGB devices. */
		COLOR(RGBState.class),
		/** Contains the current level of light for dimmable devices. */
		LEVEL(PercentageState.class),
		/** Contains the current status of the light (ie: on, off, blinking ...) depending on vendor capabilities. */
		STATUS(OnOffState.class);

		private final Class<? extends State<?>> state;
		
		LightStateName(Class<? extends State<?>> state) {
			this.state = state;
		}
		
		public Class<? extends State<?>> getType() {
			return state;
		}
	}
}
