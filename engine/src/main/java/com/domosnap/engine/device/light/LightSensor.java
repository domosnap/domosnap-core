package com.domosnap.engine.device.light;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

@Mapping(name="LightSensor")
public class LightSensor extends Device {

	public LightSensor(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	public DoubleState getLux() {
		return (DoubleState) get(LightSensorStateName.LUX.name());
	}

	@Override
	public List<StateName> getStateList() {
		return Arrays.asList(LightSensorStateName.values());
	}

	public enum LightSensorStateName implements StateName {
		
		/** Contains the current lux value */
		LUX(DoubleState.class),
		
		/** Contains the current IR value */
		IR(DoubleState.class);
		
		private final Class<? extends State<?>> state;
		
		LightSensorStateName(Class<? extends State<?>> state) {
			this.state = state;
		}
		
		public Class<? extends State<?>> getType() {
			return state;
		}
	}
}
