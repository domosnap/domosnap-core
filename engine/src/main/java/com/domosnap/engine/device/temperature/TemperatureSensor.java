package com.domosnap.engine.device.temperature;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.TemperatureUnitState;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

@Mapping(name="TemperatureSensor")
public class TemperatureSensor extends Device {

	public TemperatureSensor(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	public DoubleState getTemperature() {
		return (DoubleState) get(TemperatureSensorStateName.VALUE.name());
	}

	public TemperatureUnitState getUnit() {
		return (TemperatureUnitState) get(TemperatureSensorStateName.UNIT.name());
	}
	
	public void setUnit(TemperatureUnitState value) {
		set(TemperatureSensorStateName.UNIT.name(), value);
	}

	@Override
	public List<StateName> getStateList() {
		return Arrays.asList(TemperatureSensorStateName.values());
	}
	
	public enum TemperatureSensorStateName implements StateName {
		
		/** Contains the current temperature value */
		VALUE(DoubleState.class),
		
		/** Defines the unit of temperature of the values read on this sensor */
		UNIT(TemperatureUnitState.class);
		
		private final Class<? extends State<?>> state;
		
		TemperatureSensorStateName(Class<? extends State<?>> state) {
			this.state = state;
		}
		
		public Class<? extends State<?>> getType() {
			return state;
		}
	}

}
