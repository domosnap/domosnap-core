package com.domosnap.engine.device.what;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * State represents an instance of a status which is define by a key/value pair.
 *
 */
public interface State<T> extends Serializable {
	
	Log log = new Log(State.class.getName());
	
	T getValue();
	
	void setValue(T value);

	/**
	 * Define the value of the status.
	 * Override it to manage different type.
	 */
	void fromString(String value);
	
	/**
	 * The value of the status
	 * @return the value of the status
	 */
	String toString();
	
	static String toJson(State<?> state) {
		return state.toString();
	}
	
	static <R extends State<?>> R fromString(Class<R> clazz, String state) {
		try {
			return clazz.getConstructor(String.class).newInstance(state);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | InstantiationException e) {
			log.severe(Session.OTHER, e.getMessage());
			return null;
		}
	}
}

	/*	TODO:
	    - discuter des states avec Laurent:
		 Par exemple pour le Powercounter: doit-on porter par l'unité par le state (Watt) ou est-ce un status du device?
		 Dans le cas du power counter c'est le state qui le porte!
		 Dans le cas du pressureSensor c'est le device qui le porte... (plus flixible)

		- Discuter aussi du mapping entre notre modèle et le monde physique = aujourd'hui j'envoi la hastable dans la command et la query pour faire un mapping... flexible/puissant mais surtout trop lourd je pense


		- Le fait d'avoir le who dans la commande... = j'en ai besoin pour traduire la commande... on pourrait avec un dico mais pas pragmatique...

		- Présenter le modèle avro du core (meta modèle)!!! Mais ensuite il y a le modèle = des instances de Who + liste d'état (et leur type) => les states names
	*/
