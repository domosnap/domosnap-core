package com.domosnap.engine.device.what;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.Map.Entry;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.what.impl.*;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class WhatJsonCodec {

	public final static String TYPE_SEPARATOR = "\\\\";
	public final static String TYPE_TEMPLATE = TYPE_SEPARATOR + "type:";

	private WhatJsonCodec() {
	}

	private final static Log log = new Log(WhatJsonCodec.class.getSimpleName());

	public static String toJSon(What what) {
		return toJSon(Collections.singletonList(what));
	}

	public static String toJSon(Map<String, State<?>> whats) {
		List<What> l = new ArrayList<>();
		whats.forEach((name, state) -> l.add(new What(name, state)));
		return toJSon(l);
	}

	public static String toJSon(List<What> whats) {
		StringBuilder sb = new StringBuilder("{");

		whats.forEach( what -> {

			String value;
			State<?> state = what.getValue();
			if (state == null) {
				value = "null";
			} else if (state instanceof BooleanState ||
					state instanceof DoubleState ||
					state instanceof IntegerState) {
				value = String.valueOf(state);
			} else if (state instanceof StringState) {
				value = "\"".concat(String.valueOf(state)).concat("\"");
			} else {
				String type = state.getClass().getSimpleName();
				value = "\"".concat(TYPE_SEPARATOR).concat(TYPE_TEMPLATE).concat(type).concat(TYPE_SEPARATOR).concat(TYPE_SEPARATOR).concat(String.valueOf(state)).concat("\"");

			}
			sb.append("\"").append(what.getName()).append("\":").append(value).append(",");
		});

		if (!whats.isEmpty()) {
			sb.setLength(sb.length() - 1); // remove last comma
		}
		return sb.append("}").toString();
	}

	public static What oneFromJson(String json) {
		return fromJson(json).get(0);
	}

	public static List<What> fromJson(String json) {
		JsonObject je = JsonParser.parseString(json).getAsJsonObject();
		return fromJson(je);
	}
	public static List<What> fromJson(JsonObject je) {
		List<What> result = new ArrayList<>();

		Set<Entry<String, JsonElement>> properties = je.entrySet();

		for (Entry<String, JsonElement> entry : properties) {
			String name = entry.getKey();
			State<?> state;



			if (entry.getValue().isJsonNull()) {
				state = null;
			}
			else {
				JsonPrimitive value = entry.getValue().getAsJsonPrimitive();
				if (value.isBoolean()) {
					state = value.getAsBoolean() ? BooleanState.TRUE : BooleanState.FALSE;
				} else if (value.isNumber()) {
					// Integer || double
					if (value.getAsString().indexOf('.') != -1) { // Double
						state = new DoubleState(value.getAsDouble());
					} else { // int
						state = new IntegerState(value.getAsInt());
					}
				} else if (value.isString() && value.getAsString().startsWith(TYPE_TEMPLATE)) {
					String stringValue = value.getAsString();
					stringValue = stringValue.substring(TYPE_TEMPLATE.length());
					String classs = stringValue.substring(0, stringValue.indexOf(TYPE_SEPARATOR));
					stringValue = stringValue.substring(stringValue.indexOf(TYPE_SEPARATOR) + 2);

					try {
						Class<?> claname = Class.forName(BooleanState.class.getPackage().getName() + "." + classs);
						state = (State<?>) claname.getConstructor(String.class).newInstance(stringValue);
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException |
							 IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
							 SecurityException e) {
						log.warning(Session.OTHER, "Error when deserialized What : " + e.getMessage());
						state = null;
					}
				} else if (value.isString()) {
					state = new StringState(entry.getValue().getAsString());
				} else {
					log.warning(Session.DEVICE, "Impossible to deserialize what with key [" + name + "] and value [" + value.getAsString() + "]");
					break;
				}
			}

			result.add(new What(name, state));
		}
		return result;
	}
}
