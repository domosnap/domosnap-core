package com.domosnap.engine.device.what.impl;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.StringTokenizer;

import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;

@Mapping(name="MacAddress")
public class MacAddressState implements State<Byte[]> {

	private static final long serialVersionUID = 1L;
	
	private Byte[] macAdress = new Byte[] {0, 0, 0, 0, 0, 0};
	
	public MacAddressState(Byte[] address) {
		macAdress = address;
	}

	public MacAddressState(String address) {
		fromString(address);
	}
	
	@Override
	public Byte[] getValue() {
		return macAdress;
	}

	@Override
	public void setValue(Byte[] address) {
		if (address == null || address.length != 6) {
			throw new IncorrectMacAddressException();
		} else {
			macAdress = address;
		}
	}

	@Override
	public void fromString(String value) {
		StringTokenizer st = new StringTokenizer(value, ".");
		macAdress[0] = hexStringToByte(st.nextToken());
		macAdress[1] = hexStringToByte(st.nextToken());
		macAdress[2] = hexStringToByte(st.nextToken());
		macAdress[3] = hexStringToByte(st.nextToken());
		macAdress[4] = hexStringToByte(st.nextToken());
		macAdress[5] = hexStringToByte(st.nextToken());
	}
	
	@Override
	public String toString() {
		return "".concat(bytesToHexString(macAdress[0]))
				.concat(".").concat(bytesToHexString(macAdress[1]))
				.concat(".").concat(bytesToHexString(macAdress[2]))
				.concat(".").concat(bytesToHexString(macAdress[3]))
				.concat(".").concat(bytesToHexString(macAdress[4]))
				.concat(".").concat(bytesToHexString(macAdress[5]));
	}
	
	
	public byte hexStringToByte(String s) {
		if (s.length() > 2) 
			throw new IncorrectMacAddressException();
		
	    return (byte) ((Character.digit(s.charAt(0), 16) << 4)
	                             + Character.digit(s.charAt(1), 16));
	}
	
	private static final char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHexString(byte bytes) {
	    char[] hexChars = new char[2];
	    int v = bytes & 0xFF;
	    hexChars[0] = hexArray[v >>> 4];
	    hexChars[1] = hexArray[v & 0x0F];
	    return new String(hexChars);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(macAdress);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MacAddressState other = (MacAddressState) obj;
		return Arrays.equals(macAdress, other.macAdress);
	}
}
