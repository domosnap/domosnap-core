package com.domosnap.engine.device.what.impl;

import com.domosnap.engine.device.Mapping;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.what.State;

@Mapping(name="OnOff")
public class OnOffState implements State<Boolean> {

	private static final long serialVersionUID = 1L;
	
	private Boolean value;
	
	public OnOffState(String value) {
		fromString(value);
	}
	
	private OnOffState(Boolean value) {
		this.value = value;
	}

	public Boolean getValue() {
		return value;
	}

	@Override
	public void setValue(Boolean value) {
		this.value = value;
	}

	@Override
	public void fromString(String value) {
		this.value = Boolean.valueOf(value);
	}
	
	@Override
	public String toString() {
		return Boolean.toString(value); 
	}
	
	public static OnOffState On() {
		// Static method to generate a on state.
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new OnOffState(true); 
	}
	
	public static OnOffState Off() {
		// Static method to generate a off state.
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new OnOffState(false);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OnOffState other = (OnOffState) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
