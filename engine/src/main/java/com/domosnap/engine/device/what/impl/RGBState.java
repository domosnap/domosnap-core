package com.domosnap.engine.device.what.impl;

import java.util.Arrays;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.regex.Pattern;

import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;

@Mapping(name="RGB")
public class RGBState implements State<String> {
	
	private static final long serialVersionUID = 1L;
	
	private static final Pattern HEX_COLOR_PATTERN = Pattern.compile("^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
	
	private int[] color = new int[] {0, 0, 0};

	public RGBState(String value) {
		fromString(value);
	}
	
	public String getValue() {
		return String.format("#%02x%02x%02x", color[0], color[1], color[2]);
	}

	@Override
	public void setValue(String value) {
		if (value.length() > 0) {
			String hexCode = value;
			if (value.charAt(0) == '#') {
				hexCode = value.substring(1);
			}
			if (HEX_COLOR_PATTERN.matcher(hexCode).matches()) {
				int rgb = Integer.parseInt(hexCode, 16);
			    color[0] = (rgb & 0xFF0000) >> 16;
			    color[1] = (rgb & 0xFF00) >> 8;
			    color[2] = (rgb & 0xFF);
			    return;
			}
		}
		throw new IllegalArgumentException("Value "+ value +" is not a valid hexadecimal color code.");
	}
	
	public String toString() {
		return getValue();
	}

	@Override
	public void fromString(String value) {
		setValue(value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(color);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RGBState other = (RGBState) obj;
		return Arrays.equals(color, other.color);
	}
}
