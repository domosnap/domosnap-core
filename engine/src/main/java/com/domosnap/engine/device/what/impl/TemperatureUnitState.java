package com.domosnap.engine.device.what.impl;

import com.domosnap.engine.device.Mapping;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.what.State;


@Mapping(name="TemperatureUnit")
public class TemperatureUnitState implements State<TemperatureUnitState.TemperatureUnit> {

	private static final long serialVersionUID = 1L;
	
	public enum TemperatureUnit {CELSIUS, FAHREINHEIT, KELVIN, REAUMUR, PLANCK}	
	
	private TemperatureUnit value;
	
	public TemperatureUnitState(String value) {
		fromString(value);
	}
	
	private TemperatureUnitState(TemperatureUnit value) {
		this.value = value;
	}

	public TemperatureUnit getValue() {
		return value;
	}

	@Override
	public void setValue(TemperatureUnit value) {
		this.value = value;
	}

	@Override
	public void fromString(String value) {
		this.value = TemperatureUnit.valueOf(value);
	}
	
	@Override
	public String toString() {
		return value.name(); 
	}
	
	public static TemperatureUnitState celsius() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.CELSIUS); 
	}
	
	public static TemperatureUnitState fahreinheit() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.FAHREINHEIT);
	}

	public static TemperatureUnitState kelvin() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.KELVIN);
	}
	
	public static TemperatureUnitState reaumur() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.REAUMUR);
	}
	
	public static TemperatureUnitState planck() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.PLANCK);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemperatureUnitState other = (TemperatureUnitState) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
