package com.domosnap.engine.device.what.impl;

import java.util.StringTokenizer;

import com.domosnap.engine.device.Mapping;
import com.domosnap.engine.device.what.State;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

@Mapping(name="Version")
public class VersionState implements State<String> {

	private static final long serialVersionUID = 1L;
	
	private int version;
	private int release;
	private int build;
	
	public VersionState (int version, int release, int build) {
		this.version = version;
		this.release = release;
		this.build = build;
	}
	
	public VersionState (String value) {
		fromString(value);
	}
	
	public int getVersion() {
		return version;
	}

	public int getRelease() {
		return release;
	}

	public int getBuild() {
		return build;
	}

	@Override
	public String getValue() {
		return "".concat(String.valueOf(version))
				.concat(".").concat(String.valueOf(release))
				.concat(".").concat(String.valueOf(build));
	}

	@Override
	public void setValue(String value) {
		StringTokenizer st = new StringTokenizer(value, ".");
		version = Integer.valueOf(st.nextToken());
		release = Integer.valueOf(st.nextToken());
		build = Integer.valueOf(st.nextToken());
	}

	@Override
	public void fromString(String value) {
		setValue(value);
	}
	
	@Override
	public String toString() {
		return getValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + build;
		result = prime * result + release;
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionState other = (VersionState) obj;
		if (build != other.build)
			return false;
		if (release != other.release)
			return false;
		return version == other.version;
	}

}
