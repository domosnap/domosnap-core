package com.domosnap.engine.event;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.Event;


public class EventFactory {

	private static Log log = new Log(EventFactory.class.getSimpleName());
	private static List<Consumer<Event>> cloudServices = new ArrayList<>(); 

	private EventFactory() {
	}
	
	public static void addConsumer(Consumer<Event> consumer) {
		EventFactory.cloudServices.add(consumer);
	}

	public static void removeConsumer(Consumer<Event> consumer) {
		EventFactory.cloudServices.remove(consumer);
	}
	
	/**
	 * Call the web service to harvest all event. 
	 * @param origin 
	 * @param command
	 * @return The response from the server => check API. If cloud services are disactivated, return is null.
	 */
	public static Future<Boolean> sendEvent(final Event event) {
		
		FutureTask<Boolean> task;
		
		if (cloudServices.isEmpty()) {
			log.finest(Session.MONITOR, "No Consumer<Event> activated: Event not sent.");
			task = new FutureTask<>(() -> false);	
		} else if (event == null) {
			log.finest(Session.MONITOR, "Null event received. Abort.");
			task = new FutureTask<>(() -> false);
		} else {
			task = new FutureTask<>(() ->{
					for (Consumer<Event> consumer : cloudServices) {
						try {
							consumer.accept(event);
							log.finest(Session.MONITOR, "Event processed [" + event + "]");
						} catch (Exception e) {
							log.severe(Session.MONITOR, "Event not send [" + event + "] - " + e.getMessage());
						}
					}
					return true;
				}
			);
		}
		
		Executors.newSingleThreadExecutor().execute(task);
		return task;
	}
}
