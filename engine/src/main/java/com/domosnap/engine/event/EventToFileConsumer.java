package com.domosnap.engine.event;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.Event;
import com.domosnap.engine.gateway.EventJsonCodec;

public class EventToFileConsumer implements Consumer<Event> {

	private static final Log log = new Log(EventToFileConsumer.class.getSimpleName());
	
	private FileWriter file;
	private Date dateNextFile;
	private final String dirDest;
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	
	/**
	 * Contructor.
	 * @param dirDest directory in which store event files,
	 */
	public EventToFileConsumer(String dirDest) {
		if (dirDest.endsWith("/")) {
			this.dirDest = dirDest;
		} else {
			this.dirDest = dirDest.concat("/");
		}
		
		Path path = Paths.get(dirDest);
		if (!path.toFile().exists()) {
			  try {
				Files.createDirectories(path);
			} catch (IOException e) {
				log.severe(Session.MONITOR, "Error during creation of this directory:" + e.getMessage());
			}
		}
	}
	
	@Override
	public void accept(final Event event) {
		try {
			// TODO regarder le fichier 20230427 => a la fin pas fini = j'ai du mal à croire à un pb de synchro...
			if (dateNextFile == null || new Date().after(dateNextFile)) {
				Date currentDate = new Date();
		        Calendar c = Calendar.getInstance();
		        c.setTime(currentDate);

				file = new FileWriter(dirDest+"domosnapEvent-" + dateFormat.format(c.getTime()) + ".json");
		        
		        // manipulate date
		        c.add(Calendar.DATE, 1);
		        c.set(Calendar.HOUR_OF_DAY, 0);
		        c.set(Calendar.MINUTE, 0);
		        c.set(Calendar.SECOND, 0);

				dateNextFile = c.getTime();
			}
			file.write(EventJsonCodec.toJSon(event, false));
			file.write("\n");
		} catch (IOException e) {
			log.severe(Session.MONITOR, "Error with file: " + e.getMessage());
		}
	}
}
