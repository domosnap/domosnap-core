package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.*;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.where.Where;

/**
 * {@link Command} represent an IoT event. {@link Device} raise a command to change a state.
 * This command is send to Command stream which send it to the physical gateway.
 * When gateway has changed the status, it raise the command to monitor stream which send it to
 * Controller which change the state.
 */
public class Command implements Serializable {

	private static final long serialVersionUID = 1L;

	protected Long timestamp = new Date().getTime(); // timeline of the command
	private final Class<? extends Device> who; 			// Device type
	private final List<What> whatList;				   	// list of event = new status
	private final Where where;							// Device address

	/**
	 * Constructor for an event.
	 * @param who Controller type
	 * @param what an event: new state value
	 * @param where Controller address
	 */
	public Command(Class<? extends Device> who, Where where, What what) {
		this.whatList = Collections.singletonList(what);
		this.who = who;
		this.where = where;
	}
	
	/**
	 * Contructor for some events.
	 * @param who Controller type
	 * @param what list of event: new state value
	 * @param where Controller address
	 */
	public Command(Class<? extends Device> who, Where where, List<What> what) {
		this.whatList = what;
		this.who = who;
		this.where = where;
	}

	protected Command(Class<? extends Device> who, List<String> what, Where where) {
		List<What> whatList = new ArrayList<>();
		what.forEach(w -> whatList.add(new What(w, null)));
		this.whatList = whatList;
		this.who = who;
		this.where = where;
	}
	
	public Class<? extends Device> getWho() {
		return who;
	}

	public List<What> getWhatList() {
		return whatList;
	}

	public Where getWhere() {
		return where;
	}

	public String toString() {
		return CommandQueryJsonCodec.toJSon(this);
	}
}
