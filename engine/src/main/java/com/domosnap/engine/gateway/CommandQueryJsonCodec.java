package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.WhatJsonCodec;
import com.domosnap.engine.device.where.Where;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

public class CommandQueryJsonCodec {

	private final static Log log = new Log(CommandQueryJsonCodec.class.getSimpleName());
	
	private CommandQueryJsonCodec() {
		// Tooling class
	}
	
	public static String toJSon(Command command) {
		if (command == null) {
			log.finest(Session.COMMAND, "Command null to serialized... => return empty string.");
			return "null";
		}
		
		final StringBuilder result = 
			// Add Timestamp
			new StringBuilder("{").append("\"when\":").append(command.timestamp)
			// Add Who
			.append(",\"who\":\"").append(DeviceMappingRegistry.getMapping(command.getWho()))
			// Add Where
			.append("\",\"where\":\"").append(command.getWhere().toString())
			// Add List of what
			.append("\",\"what\":");
			
		if (command instanceof Query) { // Query/read
			result.append("[");
			command.getWhatList().forEach(what -> result.append("\"").append(what.getName()).append("\","));

			if (!command.getWhatList().isEmpty()) {
				result.setLength(result.length()-1);
			}
			result.append("]");
		} else { // Command/write
			result.append(WhatJsonCodec.toJSon(command.getWhatList()));
		}

		return result.append("}").toString();

	}
	
	public static Command fromJson(String json) {
		JsonObject je = JsonParser.parseString(json).getAsJsonObject();
		Class<? extends Device> who;

		Where where;

		// Add Where
		where = new Where(je.get("where").getAsString());
	
		// Add Who
		who = DeviceMappingRegistry.getMapping(je.get("who").getAsString());


		// Add What
		if (je.get("what").isJsonArray()) { // if array of name = query
			// Add List of what
			final List<String> whats = new ArrayList<>();
			JsonArray ja = je.getAsJsonArray("what");
			ja.forEach(jsonElement -> whats.add(jsonElement.getAsString()));

			Query q = new Query(who, where, whats);
			q.timestamp = je.get("when").getAsLong();
			return q;
		} else { // else it is a map of state
			// Add List of what
			JsonObject ja = je.getAsJsonObject("what");
			List<What> whats =  WhatJsonCodec.fromJson(ja);

			Command c = new Command(who, where, whats);
			c.timestamp = je.get("when").getAsLong();

			return c;
		}
	}
}
