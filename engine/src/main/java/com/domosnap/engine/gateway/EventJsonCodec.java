package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.what.WhatJsonCodec;
import com.domosnap.engine.device.where.Where;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class EventJsonCodec {

	private final static Log log = new Log(EventJsonCodec.class.getSimpleName());
	
	private EventJsonCodec() {
	}
	
	public static String toJSon(Event event) {
		return toJSon(event, true);
	}
	
	public static String toJSon(Event event, boolean hidePassword) {
		if (event == null) {
			log.finest(Session.MONITOR, "Event null to serialized... Return empty string.");
			return "";
		}

		return "{".concat("\"when\":").concat(String.valueOf(event.timestamp))
				// Add Who
				.concat(",\"who\":\"").concat(DeviceMappingRegistry.getMapping(event.getWho()))
				// Add Where
				.concat("\",\"where\":\"").concat(hidePassword ? event.getWhere().toString() : event.getWhere().getURI().toString())
				// Add List of what
				.concat("\",\"what\":").concat(WhatJsonCodec.toJSon(event.getWhatList())).concat("}");
	}
		
	public static Event fromJson(String json) {
		JsonObject je = JsonParser.parseString(json).getAsJsonObject();
		
			// Retrieve Where
			Where where = new Where(je.get("where").getAsString());
	
			// Retrieve Who
			Class<? extends Device> who = DeviceMappingRegistry.getMapping(je.get("who").getAsString());
	
			// Retrieve List of what
			Event c = new Event(who, where, WhatJsonCodec.fromJson(je.getAsJsonObject("what")));
			// Retrieve when
			c.timestamp = je.get("when").getAsLong();
			
			return c;
	}
}
