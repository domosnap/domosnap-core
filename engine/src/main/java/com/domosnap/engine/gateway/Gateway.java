package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.event.EventFactory;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class Gateway implements Closeable {

	private static final Log log = new Log(Gateway.class.getSimpleName());
	
	protected static final String SCHEME_PATTERN = "[a-z0-9+.-]+"; // scheme pattern in uri

	private final Flowable<Event>  monitor;
	private final PublishProcessor<Command> commander;
	private final PublishProcessor<Query> query;
	private Disposable monitorLogger;
	private boolean connectionRetry = false;
	private final int retryDelay = 5000;

	private final List<OnGatewayConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<>());

	private final IAdapter adapter;

	public Gateway(IAdapter adapter) {
		if (adapter == null) {
			if (log.errorLevel)
				log.severe(Session.GATEWAY, "No adapter provided. Gateway will not be created.");
			throw new IllegalArgumentException("No adapter provided. Gateway will do nothing.");
		} else if (adapter.getURI() == null || adapter.getURI().getScheme() == null) {
			throw new IllegalArgumentException("Adapter provided with wrong URI [" + adapter.getURI() + "]");
		} else {
			this.adapter = adapter;
			this.monitor = createMonitor();
			this.adapter.addMonitorConnectionListener(new OnConnectionListener() {
				@Override
				public void onDisconnect(DisconnectionStatusEnum disconnectionStatus) {
					log.finest(Session.MONITOR, "Monitor " + adapter.getURI() + " disconnected with " + disconnectionStatus.name() + " (retry: " + connectionRetry + ").");

					// Lunch gateway monitor connection listener  on disconnect
					for (OnGatewayConnectionListener connectionListener : connectionListenerList) {
						try {
							connectionListener.onDisconnect(Session.MONITOR, disconnectionStatus);
						} catch (Exception e) {
							log.severe(Session.MONITOR, "ConnectionListener raise an error [" + e.getMessage() +"]");
						}
					}

					// Lunch retry
					if (connectionRetry) {
						log.finest(Session.MONITOR, "Retry to connect.");
						 connect();
					}
				}

				@Override
				public void onConnect(ConnectionStatusEnum connectionStatus) {
					log.severe(Session.MONITOR, "Monitor " + adapter.getURI() + " connected with " + connectionStatus.name() + " (retry: " + connectionRetry + ").");

					// Lunch gateway monitor connection listener  on connect
					for (OnGatewayConnectionListener connectionListener : connectionListenerList) {
						try {
							connectionListener.onConnect(Session.MONITOR, connectionStatus);
						} catch (Exception e) {
							log.severe(Session.MONITOR, "ConnectionListener raise an error [" + e.getMessage() +"]");
						}
					}
				}
			});

			this.query = createQuery();
			this.commander = createCommander();
			this.adapter.addCommanderConnectionListener(new OnConnectionListener() {

				@Override
				public void onDisconnect(DisconnectionStatusEnum disconnectionStatus) {
					log.severe(Session.COMMAND, "Commander " + adapter.getURI() + " disconnected with " + disconnectionStatus.name() + " (retry: " + connectionRetry + ").");
					// Lunch gateway commander connection listener  on disconnect
					for (OnGatewayConnectionListener connectionListener : connectionListenerList) {
						try {
							connectionListener.onDisconnect(Session.COMMAND, disconnectionStatus);
						} catch (Exception e) {
							log.severe(Session.MONITOR, "ConnectionListener raise an error [" + e.getMessage() +"]");
						}
					}
					// Lunch retry
					if (connectionRetry) {
						connect();
					}
				}

				@Override
				public void onConnect(ConnectionStatusEnum connectionStatus) {
					log.severe(Session.COMMAND, "Commander " + adapter.getURI() + " connected with " + connectionStatus.name() + " (retry: " + connectionRetry + ").");
					// Lunch gateway commander connection listener  on connect
					for (OnGatewayConnectionListener connectionListener : connectionListenerList) {
						try {
							connectionListener.onConnect(Session.COMMAND, connectionStatus);
						} catch (Exception e) {
							log.severe(Session.MONITOR, "ConnectionListener raise an error [" + e.getMessage() +"]");
						}
					}
				}
			});
			if (log.finestLevel) 
				log.finest(Session.GATEWAY, "Gateway [".concat(adapter.getURI().getScheme()).concat("] created."));
		}
	}
	
	private Flowable<Event> createMonitor() {
		
		Flowable<Event> result = adapter.createMonitor();
		if (result != null) {
			monitorLogger = result.subscribe(event -> {
				if (log.finestLevel) {
					log.finest(Session.MONITOR, "Monitor received [".concat(event.toString()).concat("]"));
				}
				EventFactory.sendEvent(event);
			});
		}
		return result;
	}
	
	
	private PublishProcessor<Command> createCommander() {
		return adapter.createCommander();
	}

	private PublishProcessor<Query> createQuery() {
		return adapter.createQuery();
	}
	
	/**
	 * return the protocol abbreviation in uri.<br/>
	 * Example: knx://server => knx is the scheme.
	 * @return protocol abbreviation in uri.
	 */
	public String getScheme() {
		try {
			return adapter.getURI().getScheme();
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during calling isConnected on adapter [%s]:%s", getScheme(), e.getMessage()));
			return null;
		}
	}

	/**
	 * Return the uri of the gateway.<br/>
	 * @return the uri of the gateway.
	 */
	public URI getUri() {
		return adapter.getURI();
	}
	
	/**
	 * Add a listener on new connection (connection can be monitored and/or commanded).
	 * So if two connection are lost (monitor and commander) the listener will be call 2 times!
	 * @param listener listener to listen connection
	 */
	public Gateway addConnectionListener(OnGatewayConnectionListener listener) {
		connectionListenerList.add(listener);
		return this;
	}

	/**
	 * Remove a listener on connection
	 * @param listener listener to listen connection
	 */
	public Gateway removeConnectionListener(OnGatewayConnectionListener listener) {
		connectionListenerList.remove(listener);
		return this;
	}

	/**
	 * Force connection for Commander and Monitor stream if there is no connection.
	 */
	public Future<Boolean> connect() {
		connectionRetry = true;
		return Executors.newSingleThreadExecutor().submit(() -> {
			try {
					boolean commanderConnected = ConnectionStatusEnum.CONNECTED.equals(adapter.connectCommander());
					boolean monitorConnected = ConnectionStatusEnum.CONNECTED.equals(adapter.connectMonitor());

					if (!commanderConnected && connectionRetry) {
						Executors.newSingleThreadExecutor().submit(() -> {
							try {
								// we begin to wait before try again to connect!
								Thread.sleep(retryDelay);
							} catch (InterruptedException e) {
								log.warning(Session.MONITOR, "Thread interrupted.");
								Thread.currentThread().interrupt();
							}
							while (!adapter.isCommanderConnected() && connectionRetry) {
								adapter.connectCommander();
								try {
									Thread.sleep(retryDelay);
								} catch (InterruptedException e) {
									log.warning(Session.MONITOR, "Thread interrupted.");
									Thread.currentThread().interrupt();
								}
							}
						});
					}

					if (!monitorConnected && connectionRetry) {
						Executors.newSingleThreadExecutor().submit(() -> {
							try {
								// we begin to wait before try again to connect!
								Thread.sleep(retryDelay);
							} catch (InterruptedException e) {
								log.warning(Session.MONITOR, "Thread interrupted.");
								Thread.currentThread().interrupt();
							}
							while (!adapter.isMonitorConnected() && connectionRetry) {
								adapter.connectMonitor();
								try {
									Thread.sleep(retryDelay);
								} catch (InterruptedException e) {
									log.warning(Session.MONITOR, "Thread interrupted.");
									Thread.currentThread().interrupt();
								}
							}
						});
					}

					return  commanderConnected && monitorConnected;

			} catch (Exception e) {
				log.severe(Session.GATEWAY, String.format("Exception during calling connect on gateway [%s]:%s", getScheme(), e.getMessage()));
				return false;
			}
		});


	}
	
	/**
	 *  Disconnect Commander and Monitor stream and clean up all resources used.
	 */
	public void disconnect() {
		connectionRetry = false;
		try {
			adapter.disconnectCommander();
			adapter.disconnectMonitor();
		} catch (Exception e) {
			log.severe(Session.GATEWAY, "Error during disconnection for adapter " + getScheme() + ":" + e.getMessage());
		}
	}
	
	/**
	 * Return true if all commander and monitor are connected (else false)
	 * @return true if all commander and monitor are connected (else false)
	 */
	public boolean isConnected() {
		try {
			return adapter.isCommanderConnected() && adapter.isMonitorConnected();
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during calling isMonitorConnected or isCommanderConnected on adapter [%s]:%s", getScheme(), e.getMessage()));
			return false;
		}
	}

	/**
	 * Return true if the gateway is trying to connect (else false). Trying to connect
	 * means that the retry connection until it's successful.
	 * When you use connect, gateway always retries. To stop to retry you need to
	 * call @{@link Gateway#disconnect()}
	 *
	 * @return true if the gateway is trying to connect (else false).
	 */
	public boolean isConnectingTry() {
		return !isConnected() && connectionRetry;
	}
	
	public Flowable<Event> getMonitor() {
		return monitor;
	}
	
	public PublishProcessor<Command> getCommander() {
		return commander;
	}

	public PublishProcessor<Query> getQuery() {
		return query;
	}

	public void scan(OnScanDeviceListener listener) {
		try {
			adapter.scan(listener);
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during scan on adapter [%s]:%s", getScheme(), e.getMessage()));
			listener.progess(100);
			listener.scanFinished();
		}
	}
	
	public List<Class<? extends Device>> getSupportedDevice() {
		try {
			return adapter.getSupportedDevice();
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during calling getSupportedDevice on adapter [%s]:%s", getScheme(), e.getMessage()));
			return new ArrayList<>();
		}
	}
	
	public Gateway sendCommand(Command command) {
		if (command != null) {
			if (command instanceof Query) {
				if (!query.offer((Query) command)) {
					log.warning(Session.GATEWAY, "This Query [" + command + "] was not emit to all subscriber of the commander [".concat(getScheme().concat("].")));
				}
			} else {
				if (!commander.offer(command)) {
					log.warning(Session.GATEWAY, "This Command [" + command + "] was not emit to all subscriber of the commander [".concat(getScheme().concat("].")));
				}
			}
		}
		return this;
	}
	
	public void onCreatedDevice(Device device) {
		try {
			adapter.onCreatedDevice(device);
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during calling onCreatedDevice on adapter [%s]:%s", getScheme(), e.getMessage()));

		}

	}

	public void onDestroyedDevice(Device device) {
		try {
			adapter.onDestroyedDevice(device);
		} catch (Exception e) {
			log.severe(Session.GATEWAY, String.format("Exception during calling onDestroyedDevice on adapter [%s]:%s", getScheme(), e.getMessage()));
		}
	}

	@Override
	public void close() throws IOException {
		disconnect();
		adapter.close();
		if (monitorLogger != null) {
			monitorLogger.dispose();
		}

	}
}
