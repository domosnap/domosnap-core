package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.adapter.hue.HueAdapter;
import com.domosnap.engine.adapter.openwebnet.OpenWebNetAdapter;

public class GatewayBuilder {

	private static final Log log = new Log(GatewayBuilder.class.getSimpleName());
	
	protected static final String SCHEME_PATTERN = "[a-z0-9+.-]+"; // scheme pattern in uri
	private static final Map<String, Class<? extends IAdapter>> adapterList = new HashMap<>(); // protocole supported

	static {
		try {
			registerProtocole(OpenWebNetAdapter.SCHEME, OpenWebNetAdapter.class);
			registerProtocole(HueAdapter.SCHEME, HueAdapter.class);
		} catch (UnsupportedScheme e) {
			log.severe(Session.OTHER, e.getMessage());
		}
	}
	
	/**
	 * Register a new protocol.
	 * @param scheme Protocol name.
	 * @param adapter Class of the ControllerAdapter associated to the protocol
	 * @throws UnsupportedScheme the scheme of the uri doesn't respect the pattern {@link GatewayBuilder#SCHEME_PATTERN}
	 */
	public static void registerProtocole(String scheme, Class<? extends IAdapter> adapter) throws UnsupportedScheme {
		Pattern p = Pattern.compile(SCHEME_PATTERN);
		Matcher matcher = p.matcher(scheme);
		if (matcher.matches()) {
			adapterList.put(scheme, adapter);
		} else {
			throw new UnsupportedScheme(scheme);
		}
	}
	
	/**
	 * Lunch a scan to find the gateway of the protocole.
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}. Some device can have been created.
	 */
	public void scanGateway(String schema, OnScanGatewayListener listener) throws UnsupportedAdapter {
		
		newAdapter(schema).scanGateway(listener);
	}
	
	public Gateway build(String uri) throws UnsupportedAdapter, IllegalArgumentException {
		try {
			if (uri == null) {
				throw new IllegalArgumentException("Uri is null");
			}
			return build(new URI(uri));
		} catch (URISyntaxException e ) {
			log.severe(Session.OTHER, e.getMessage());
			throw new IllegalArgumentException("Wrong uri [" + uri + "].");
		}
	}

	public Gateway build(URI uri) throws UnsupportedAdapter, IllegalArgumentException {
		if (uri == null) {
			throw new IllegalArgumentException("Uri is null");
		}
		
		IAdapter ad = newAdapter(uri.getScheme());
		ad.setURI(uri);
		
		return new Gateway(ad);
	}

	private IAdapter newAdapter(String schema) throws UnsupportedAdapter {
		try {
			Class<? extends IAdapter> adapterClass = adapterList.get(schema);
			if (adapterClass == null) {
				throw new UnsupportedAdapter("Adapter [" + schema + "] not found.");
			}
			return adapterClass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			log.severe(Session.OTHER, e.getMessage());
			throw new UnsupportedAdapter("Adapter [" + schema + "] impossible to instanciate.");
		}
	}	
}
