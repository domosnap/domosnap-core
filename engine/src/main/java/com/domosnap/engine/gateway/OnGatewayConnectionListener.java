package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;

/**
 * ConnectionListener manage close and connect event.
 */
public interface OnGatewayConnectionListener {
 
	/**
	 * Raise each time a connection to the gateway is try with the result of it,
	 * @param session the session (monitor or command) which raise the event
	 * @param connectionStatus the status of the event
	 */
	public void onConnect(Log.Session session, ConnectionStatusEnum connectionStatus);
	
	/**
	 * Raise each time a connection is closed (unexpectedly or not).
	 * @param session the session (monitor or command) which raise the event
	 * @param disconnectionStatus the status of the event
	 */
	public void onDisconnect(Log.Session session, DisconnectionStatusEnum disconnectionStatus);
	 
}
