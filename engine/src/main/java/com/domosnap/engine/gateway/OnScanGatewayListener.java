package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * {@link OnScanGatewayListener} must be provided to {@link com.domosnap.engine.adapter.IAdapter#scanGateway(OnScanGatewayListener)}.
 * It act as a callback when an unknown controller is found.
 */
public interface OnScanGatewayListener {

	/**
	 * Called each time a new gateway is found.
	 * @param uri uri of the gateway
	 */
	void foundGateway(String uri);

	/**
	 * Called by {@link com.domosnap.engine.adapter.IAdapter#scanGateway(OnScanGatewayListener)} by providing percent progress.
	 * When percent = 100, scan SHOULD be finished but without any warranty.
	 * @param percent indicator of progress
	 */
	void progess(int percent);
	
	/**
	 *  Called by {@link com.domosnap.engine.adapter.IAdapter#scanGateway(OnScanGatewayListener)} when percent = 100.
	 *  Scan SHOULD be finished but without any warranty.
	 */
	void scanFinished();
}
