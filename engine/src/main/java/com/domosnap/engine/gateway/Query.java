package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.where.Where;

import java.util.*;

/**
 * {@link Query} represent an IoT event. {@link Device} raise a command to change a state.
 * This command is send to Command stream which send it to the physical gateway.
 * When gateway has changed the status, it raise the command to monitor stream which send it to
 * Controller which change the state.
 */
public class Query extends Command {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for an event.
	 * @param who Controller type
	 * @param what an event: new state value
	 * @param where Controller address
	 */
	public Query(Class<? extends Device> who, Where where, String what) {
		super(who, where, new What(what, null));
	}

	/**
	 * Contructor for some events.
	 * @param who Controller type
	 * @param what list of event: new state value
	 * @param where Controller address
	 */
	public Query(Class<? extends Device> who, Where where, List<String> what) {
		super(who, what, where);
	}

	public List<String> getWhatNameList() {
		List<String> whatList = new ArrayList<>();
		getWhatList().forEach(w -> whatList.add(w.getName()));
		return whatList;
	}
}
