package com.domosnap.engine.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.device.where.Where;

import java.util.ArrayList;
import java.util.List;

public class QueryBuilder {

	private final Class<? extends Device> who;

	public static QueryBuilder create (Class<? extends Device> who) {
		return new QueryBuilder(who);
	}

	private QueryBuilder(Class<? extends Device> who) {
		if  (who == null) {
			throw new NullPointerException();
		}
		this.who = who;
	}
	
	public WhatBuilder setWhere(Where where) {
		if (where == null) {
			throw new NullPointerException();
		}
		
		return new WhatBuilder(who, where);
	}
	
	public WhatBuilder setWhere(String uri) {
		if (uri == null) {
			throw new NullPointerException();
		}
		return setWhere(new Where(uri));
	}
	
	public static class WhatBuilder {
		private final Class<? extends Device> who;
		private final Where where;

		private final List<String> whatList = new ArrayList<>();
		
		private WhatBuilder(Class<? extends Device> who, Where where) {
			this.who = who;
			this.where = where;
		}
		
		public WhatBuilder addWhatName(String whatName) {
			whatList.add(whatName);
			return this;
		}

		public WhatBuilder addWhatName(StateName whatName) {
			whatList.add(whatName.name());
			return this;
		}

		public Query build() {
			if (whatList.isEmpty()) {
				throw new NullPointerException("Command needs at least one what");
			}
			return new Query(who, where, whatList);
		}
	}
	
}
