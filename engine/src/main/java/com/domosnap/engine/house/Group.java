package com.domosnap.engine.house;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import com.domosnap.engine.device.Device;

/**
 * {@link Group} represents a physical domain of {@link Device}. It can be
 * compare to an internet network mask: a group of devices. When a
 * {@link Device} is added, it can be contains in one {@link Group} only! So
 * a check is done to avoid to add a controller to most than one
 * {@link Group}.<br/>
 * <br/>
 * If you want organized your {@link Device} in another way than a physical
 * {@link Group}, you can use {@link Label} which is a logical representation of
 * {@link Device} domain.<br/>
 * <br/>
 * When you create a controller it must be associated to one {@link Group}. It
 * is the job of the {@link ControllerAdapter}. So user has no choice.<br/>
 * Now to let the user able to organize {@link Device}, he must use
 * {@link Label}.<br/>
 * <u>Example:</u>
 * <ul>
 * <li>User can organized {@link House} by floor: he creates a {@link Label} for
 * the first floor, another for the second floor, etc... Now he could put for
 * each floor, {@link Device} located in them-</li>
 * <li>User can organized {@link House} by type of {@link Device}: he create
 * a {@link Label} for light, another for shutter, etc... Now he could put
 * for each type corresponding {@link Device}</li>
 * <li>User can organized {@link House} by room: he create a {@link Label} for
 * each room. Now he could put in each rrom the corresponding
 * {@link Device}</li>
 * <li>... all other possibilit</li>
 * </ul>
 * 
 * @see House
 */
public class Group implements Serializable {

	public static final String JSON_CONTROLLERS = "controllers";
	public static final String JSON_DESCRIPTION = "description";
	public static final String JSON_TITLE = "title";
	public static final String JSON_ID = "id";

	/** uuid */
	private static final long serialVersionUID = 1L;

	private List<Device> controllerList = new ArrayList<>();
	private String title;
	private String description;
	private String id;

	/**
	 * Return the id of the Group. If no Id has been assigned, a UUID one is
	 * created.
	 * 
	 * @return the id of the Group.
	 */
	public String getId() {
		if (id == null) {
			id = UUID.randomUUID().toString();
		}
		return id;
	}

	/**
	 * Defines the id of the group.
	 * 
	 * @param id
	 *            id of the group.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the title of the group.
	 * 
	 * @return the title of the group.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Define the title of the group.
	 * 
	 * @param title
	 *            the title of the group.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the description of the group.
	 * 
	 * @return the description of the group.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Defines the description of the group.
	 * 
	 * @param description
	 *            the description of the group.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Return a copy of the list of controller contains in this group. The same
	 * controller can't be present in different group.
	 * 
	 * @return the list of controller.
	 */
	public List<Device> getControllerList() {
		return new ArrayList<>(controllerList);
	}

	/**
	 * Add the controller to the list only if its group property is null.
	 * 
	 * @param controller
	 *            controller to add to the group
	 * @return true if controller has been added, else false! If the
	 *         controller's property group is not null return false too.
	 */
	public boolean add(Device controller) {
		if (controller.getGroup() != null) {
			return false;
		} else {
			return controllerList.add(controller);
		}
	}

	/**
	 * Add the controller to the list only if its group property is null.
	 * 
	 * @param location
	 *            the location to add to the group
	 * @param controller
	 *            controller controller to add to the group
	 */
	public void add(int location, Device controller) {
		if (controller.getGroup() == null) {
			controllerList.add(location, controller);
		}
	}

	/**
	 * Add controllers to the list only if there is no group property define in
	 * the list.
	 * 
	 * @param collection
	 *            controllers to add to the group
	 * @return true if all controllers has been added, else false! If at least
	 *         one controller's property group is not null return false too and
	 *         no one controller is added to the list..
	 */
	public boolean addAll(Collection<? extends Device> collection) {
		for (Device controller : collection) {
			if (controller.getGroup() != null)
				return false;
		}
		return controllerList.addAll(collection);
	}

	/**
	 * Add controllers to the list only if there is no group property define in
	 * the list.
	 * 
	 * @param location
	 *            location to insert the list
	 * @param collection
	 *            controllers to add to the group
	 * @return true if all controllers has been added, else false! If at least
	 *         one controller's property group is not null return false too and
	 *         no one controller is added to the list..
	 */
	public boolean addAll(int location, Collection<? extends Device> collection) {
		for (Device controller : collection) {
			if (controller.getGroup() != null)
				return false;
		}
		return controllerList.addAll(location, collection);
	}

	/**
	 * Remove all the controller. <b>Warning</b> Since group is a physical
	 * location, if we remove all controller from it,it's like if we delete all
	 * controller. So in that logic we remove them from logical view = we remove
	 * controller from all label conaining them.
	 */
	public void clear() {
		for (Device controller : controllerList) {
			controller.setGroup(null);
		}
		controllerList.clear();
	}

	/**
	 * Return true if the group contains the controller.
	 * 
	 * @param controller
	 * @return true if the group contains the controller.
	 */
	public boolean contains(Device controller) {
		return controllerList.contains(controller);
	}

	/**
	 * Return true if contains all Controller.
	 * 
	 * @param arg0
	 *            controller to check
	 * @return true if contains all Controller.
	 */
	public boolean containsAll(Collection<?> arg0) {
		return controllerList.containsAll(arg0);
	}

	/**
	 * Return the Controller at the location.
	 * 
	 * @param location
	 * @return the Controller at the location.
	 */
	public Device get(int location) {
		return controllerList.get(location);
	}

	/**
	 * Returns the indexof the controller.
	 * 
	 * @param object
	 *            the controller.
	 * @return the indexof the controller.
	 */
	public int indexOf(Device object) {
		return controllerList.indexOf(object);
	}

	/**
	 * Return true if the group is empty.
	 * 
	 * @return true if the group is empty.
	 */
	public boolean isEmpty() {
		return controllerList.isEmpty();
	}

	public Iterator<Device> iterator() {
		return controllerList.iterator();
	}

	public int lastIndexOf(Object object) {
		return controllerList.lastIndexOf(object);
	}

	public ListIterator<Device> listIterator() {
		return controllerList.listIterator();
	}

	public ListIterator<Device> listIterator(int location) {
		return controllerList.listIterator(location);
	}

	/**
	 * Remove the controller at the location. Since group is a physical
	 * representation, remove a {@link Device} from the group is like to
	 * delete the Controller and make it removed too from labels.
	 * 
	 * @param location
	 * @return controller that have been removed with not group and no labels.
	 */
	public Device remove(int location) {
		Device c = controllerList.remove(location);
		// Since we remove the physical
		for (Label l : c.getLabels()) {
			l.remove(c);
		}
		c.setGroup(null);
		return c;
	}

	/**
	 * Remove the controller at the location. Since group is a physical
	 * representation, remove a {@link Device} from the group is like to
	 * delete the Controller and make it removed too from labels.
	 * 
	 * @param controller
	 *            the controller to remove.
	 * @return true if Controller has been removed.
	 */
	public boolean remove(Device controller) {
		// Since we remove the physical
		for (Label l : controller.getLabels()) {
			l.remove(controller);
		}
		controller.setGroup(null);
		return controllerList.remove(controller);
	}

	/**
	 * Remove controllers from this group. Since group is a physical
	 * representation, remove a {@link Device} from the group is like to
	 * delete the Controller and make it removed too from labels.
	 * 
	 * @param controllers
	 *            controllers to remove.
	 * @return true if all Controllers has been removed.
	 */
	public boolean removeAll(Collection<Device> controllers) {
		for (Device controller : controllers) {
			controller.setGroup(null);
		}
		return controllerList.removeAll(controllers);
	}

	/**
	 * Remove controllers which are not in the collection. Since group is a
	 * physical representation, remove a {@link Device} from the group is
	 * like to delete the Controller and make it removed too from labels.
	 * 
	 * @param controllers
	 *            controllers to keep.
	 * @return true if this list changed as a result of the call
	 */
	public boolean retainAll(Collection<?> controllers) {
		for (Device controller : controllerList) {
			if (!controllers.contains(controller)) {
				controller.setGroup(null);
			}
		}
		return controllerList.retainAll(controllers);
	}

	/**
	 * Replace the controller at the location. Since group is a physical
	 * representation, the replaced {@link Device} will be removed from the group 
	 * AND removed too from labels.
	 * 
	 * @param location location of the controller to replace
	 * @param controller the new controller
	 * @return the element previously at the specified position
	 */
	public Device set(int location, Device controller) {
		Device c = controllerList.set(location, controller);
		c.setGroup(null);
		return c;
	}

	public int size() {
		return controllerList.size();
	}

	public List<Device> subList(int start, int end) {
		return controllerList.subList(start, end);
	}

	public Object[] toArray() {
		return controllerList.toArray();
	}

	public <T> T[] toArray(T[] array) {
		return controllerList.toArray(array);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Group other = (Group) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}


}
