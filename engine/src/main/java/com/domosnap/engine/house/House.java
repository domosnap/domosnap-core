package com.domosnap.engine.house;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.device.Device;

/**
 * House is a contains all {@link Device}.
 * 
 * It is subdivided in two categories:
 * 
 * <ul>
 * <li>{@link Group} that is a physical organization of {@link Device}: it isn't possible to modify it and should be only use by technical layer. FOr user it is read only.</li>
 * <li>{@link Label} that is a logical organization of {@link Device}: user can organized them as he want.</li>
 * </ul>
 * 
 * @see Group
 * @see Label
 */
public class House implements Serializable {

	/** serial uid */
	private static final long serialVersionUID = 1L;
	private List<Group> groups; // Physical model
	private List<Label> labels; // Logical model

	public List<Group> getGroups() {
		if (groups == null) {
			groups = new ArrayList<>();
		}
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Label> getLabels() {
		if (labels == null) {
			labels = new ArrayList<>();
		}
		return labels;
	}

	public void setLabels(List<Label> labels) {
		this.labels = new ArrayList<>();
		this.labels.addAll(labels);
	}
}
