package com.domosnap.engine.house;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public enum Icon {

	CHAMBER("icn_categories"),
	GARAGE(""),
	HOUSE(""), 
	ROOM(""), 
	KITCHEN(""),
	BATHROOM(""),
	SHOWROOM(""),
	MOVIE(""),
	TV(""),
	LIGHT(""),
	HEATING(""),
	LIVINGROOM(""),
	OUTLET(""),
	AUTOMATION(""),
	CELLAR(""),
	STOREROOM(""),
	DOOR(""),
	WINDOW(""),
	FIREPLACE(""),
	GARDEN(""),
	OUTDOOR("");

	private String className;
	
	private Icon(String className)  {
		this.className = className;
	}
	
	public String getClassName() {
		return className;
	}
}
