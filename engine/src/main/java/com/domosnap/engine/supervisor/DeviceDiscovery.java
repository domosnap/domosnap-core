package com.domosnap.engine.supervisor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.Gateway;
import com.domosnap.engine.gateway.GatewayBuilder;
import com.domosnap.engine.gateway.UnsupportedAdapter;

import java.net.URI;

public class DeviceDiscovery {

	private final DeviceRepository dr;

	private final Log log = new Log(DeviceDiscovery.class.getSimpleName());

	public DeviceDiscovery() {
		dr = new DeviceRepository();
	}
	
	public DeviceDiscovery(DeviceRepository dr) {
		this.dr = dr;
	}
	
	/**
	 * Lunch a scan. This feature scan the physical device to find Controller. When a controller
	 * is find, {@link OnScanDeviceListener#foundController(Device)} is called.
	 * @param listener callback when a controller is found.
	 */
	public void scan(URI uri, OnScanDeviceListener listener) throws UnsupportedAdapter, IllegalArgumentException {
		dr.connectGateway(uri);
		dr.addUnknownDeviceListener(event -> {
			try {
				Device d = dr.createDevice(event.getWho(), event.getWhere()).get();
				listener.foundController(d); // TODO test is listener is not null...
			} catch (UnsupportedAdapter e) {
				log.severe(Log.Session.GATEWAY, "Unknown adapter for the unknown device + " + event.getWhere());
			}
		});

		dr.getGateway(uri).scan(listener);// TODO pourquoi le meme listener alors que la gateway ne gère pas les devices find?
		//Gateway gateway = new GatewayBuilder().build(uri);
		//gateway.scan(listener);
	}	
}
