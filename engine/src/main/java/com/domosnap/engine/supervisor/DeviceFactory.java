package com.domosnap.engine.supervisor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.DeviceHelper;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.*;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

/**
 * DeviceFactory build device. Building a device means instanciate it
 * and connect with stream to gateway.
 */
public class DeviceFactory implements AutoCloseable {

	private static final String REACHABLE = "REACHABLE";

	private final Log log = new Log(DeviceFactory.class.getSimpleName());

	protected final Map<String, Gateway> gatewayList;
	private final List<OnDeviceCreatedListener> deviceListenerList;
	private final List<OnDeviceDestroyedListener> deviceDestroyedListenerList;
	private final List<OnGatewayCreatedListener> gatewayListenerList;

	private final List<OnGatewayDestroyedListener> gatewayDestroyedListeners;

	private final Map<Device, OnGatewayConnectionListener> connectionListenerMap;

	public DeviceFactory() {
		gatewayList = new HashMap<>();
		deviceListenerList = new ArrayList<>();
		deviceDestroyedListenerList = new ArrayList<>();
		gatewayListenerList = new ArrayList<>();
		connectionListenerMap = new HashMap<>();
		gatewayDestroyedListeners = new ArrayList<>();
	}

	/**
	 * Return the {@link Gateway} corresponding to the URI. If gateway doesn't exist, it
	 * will be instanciate and keep in cache.
	 * @param uri Address of the gateway
	 * @return the gateway
	 * @throws IllegalArgumentException there is a problem with the uri
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}
	 */
	public Gateway getGateway(URI uri) throws IllegalArgumentException, UnsupportedAdapter {
		if (uri == null)
			throw new IllegalArgumentException("Uri can't be null.");
		Gateway result = gatewayList.get(uri.getHost()); // TODO check here = we only distinct gateway by host... need to use port or schema too...
		if (result == null) {
			result = new GatewayBuilder().build(uri);
			this.onCreatedGateway(result);
			gatewayList.put(uri.getHost(), result);
		}

		return result;
	}

	/**
	 * Return all the gateway created.
	 * @return list of created gateway;
	 */
	public List<Gateway> getAllGateway() {
		List<Gateway> result = new ArrayList<>();
		gatewayList.forEach((key, gateway) ->  result.add(gateway));
		return result;
	}

	/**
	 * Create a {@link Device}. Device created is already connected
	 * to physical gateway (with a monitor session and a command session):
	 * this means that when you read a status or write it,
	 * the {@link Query}/{@link Command} is sent to the gateway.
	 * @param clazz the class of the controller you would like create.
	 * @param where the address of the device
	 * @return the new created and initialized device
	 * @throws IllegalArgumentException there is a problem with the uri
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}
	 */
	public <T extends Device> Optional<T> createDevice(
			Class<T> clazz, Where where) throws IllegalArgumentException, UnsupportedAdapter {

		Gateway g = getGateway(where.getURI());
		try {
			T device = clazz.getDeclaredConstructor(Flowable.class, PublishProcessor.class, PublishProcessor.class).newInstance(g.getMonitor(), g.getCommander(), g.getQuery());
			onCreatedDevice(device); // Before setWhere since setWhere call updatestatus = sometime so fast that reponse arrive before added to knownController....
			device.setWhere(where);
			addReachableState(device, g);
			g.onCreatedDevice(device);
			return Optional.of(device);
		} catch (Exception e) {
			log.severe(Session.DEVICE, "Error when instanciate the controller [" + where + "]. " + e.getMessage());
			return Optional.empty();
		}
	}

	/**
	 * Create a {@link Device}. {@link Device} created is already connected
	 * to gateway (with a stream to send command and a stream to received command):
	 * this means that when you read or write the status the 
	 * {@link Query}/{@link Command} is sent to the gateway. 
	 * @param clazz the class of the controller you would like create.
	 * @param wheres address of the device
	 * @return the new created and initialized controller
	 * @throws IllegalArgumentException there is a problem with the uri. Some device can have been created.
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}. Some device can have been created.
	 */
	public <T extends Device> List<Optional<T>> createDevices(
			Class<T> clazz, Where... wheres) throws IllegalArgumentException, UnsupportedAdapter {
		List<Optional<T>> result = new ArrayList<>();
		for (Where where : wheres) {
			result.add(createDevice(clazz, where));
		}
		return result;
	}

	/**
	 * Add the mechanism to keep device's state REACHABLE (depending if gateway is connected or not)
	 * up to date.
	 * @param d the device
	 * @param g the gateway
	 */
	private void addReachableState(Device d, Gateway g) {
		DeviceHelper dh = new DeviceHelper(d);
		What what = new What(REACHABLE, g.isConnected()? BooleanState.TRUE : BooleanState.FALSE);
		Event event = new Event(d.getClass(), d.getWhere(), what);
		dh.notifyChange(event);


		OnGatewayConnectionListener connectionListener =  new OnGatewayConnectionListener() {

			@Override
			public void onDisconnect(Session session, DisconnectionStatusEnum disconnectionStatus) {
				What what = new What(REACHABLE, g.isConnected()? BooleanState.TRUE : BooleanState.FALSE);
				Event event = new Event(d.getClass(), d.getWhere(), what);
				dh.notifyChange(event);
			}

			@Override
			public void onConnect(Session session, ConnectionStatusEnum connectionStatus) {
				What what = new What(REACHABLE, g.isConnected()? BooleanState.TRUE : BooleanState.FALSE);
				Event event = new Event(d.getClass(), d.getWhere(), what);
				dh.notifyChange(event);

				if (ConnectionStatusEnum.CONNECTED.equals(connectionStatus)) {
					d.requestAllStatus();
				}
			}
		};

		connectionListenerMap.put(d, connectionListener);
		g.addConnectionListener(connectionListener);
	}

	/**
	 * Destroy the {@link Device}. It means that gateway is informed that the device
	 * is destroyed (sometimes needing for some protocol to clean the device).
	 * The synchronisation with gateway's connected status is removed for this device.
	 * @param d the device to destroyed.
	 * @return true if the device has been destroyed (or if null).
	 */
	public boolean destroyDevice(Device d) {
		if (d == null) {
			return true;
		}

		try {
			if (d.getWhere() == null)
				return false;
			Gateway g = getGateway(d.getWhere().getURI());
			//T device = clazz.getDeclaredConstructor(Flowable.class, PublishProcessor.class).newInstance(g.getMonitor(), g.getCommander());
			onDestroyedDevice(d); // Before setWhere since setWhere call updatestatus = sometime so fast that reponse arrive before added to knownController....
			//d.setWhere(where);
			removeReachableState(d, g); // remove connection listener for commander & monitor
			g.onDestroyedDevice(d);
			return true;
		} catch (Exception e) {
			log.severe(Session.DEVICE, "Error when destroying the device [" + d.getWhere() + "]. " + e.getMessage());
			return false;
		}
	}

	/**
	 * Destroy the {@link Gateway}. It means that gateway is disconnected and informed that the device
	 * is destroyed (sometimes needing for some protocol to clean the device).
	 * The synchronisation with gateway's connected status is removed for this device.
	 * @param g the device to destroyed.
	 * @return true if the device has been destroyed (or if null).
	 */
	public boolean destroyGateway(Gateway g) {
		if (g == null) {
			return true;
		}

		try {
			onDestroyedGateway(g);
			g.close();
			return true;
		} catch (Exception e) {
			log.severe(Session.DEVICE, "Error when destroying the gateway [" + g.getUri() + "]. " + e.getMessage());
			return false;
		}
	}

	private void removeReachableState(Device d, Gateway g) {
		g.removeConnectionListener(connectionListenerMap.get(d));
		connectionListenerMap.remove(d);
	}

	/**
	 * Add a listener triggered when a device is created.
	 * @param listener to trigger when a device is created.
	 * @return the instance
	 */
	public DeviceFactory addOnDeviceCreatedListener(OnDeviceCreatedListener listener) {
		deviceListenerList.add(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a device is created.
	 * @param listener to removed.
	 * @return the instance
	 */
	public DeviceFactory removeOnDeviceCreatedListener(OnDeviceCreatedListener listener) {
		deviceListenerList.remove(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a device is destroyed.
	 * @param listener to trigger when a device is destroyed.
	 * @return the instance
	 */
	public DeviceFactory addOnDeviceDestroyedListener(OnDeviceDestroyedListener listener) {
		deviceDestroyedListenerList.add(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a device is destroyed.
	 * @param listener to removed.
	 * @return the instance
	 */
	public DeviceFactory removeOnDeviceDestroyedListener(OnDeviceDestroyedListener listener) {
		deviceDestroyedListenerList.remove(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a gateway is created.
	 * @param listener to trigger when a gateway is created.
	 * @return the instance
	 */
	public DeviceFactory addOnGatewayCreatedListener(OnGatewayCreatedListener listener) {
		gatewayListenerList.add(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a gateway is created.
	 * @param listener to removed.
	 * @return the instance
	 */
	public DeviceFactory removeOnGatewayCreatedListener(OnGatewayCreatedListener listener) {
		gatewayListenerList.remove(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a gateway is destroyed.
	 * @param listener to trigger when a gatewqy is destroyed.
	 * @return the instance
	 */
	public DeviceFactory addOnGatewayDestroyedListener(OnGatewayDestroyedListener listener) {
		gatewayDestroyedListeners.add(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a gateway is destroyed.
	 * @param listener to removed.
	 * @return the instance
	 */
	public DeviceFactory removeOnGatewayDestroyedListener(OnGatewayDestroyedListener listener) {
		gatewayDestroyedListeners.remove(listener);
		return this;
	}

	protected void onCreatedDevice(Device c) {
		for (OnDeviceCreatedListener onDeviceCreatedListener : deviceListenerList) {
			try {
				onDeviceCreatedListener.onCreated(c);
			} catch (Exception e) {
				log.severe(Session.OTHER, "Error during call listener on device creation [" + e.getMessage() + "].");
			}
		}
	}

	protected void onDestroyedDevice(Device c) {
		for (OnDeviceDestroyedListener onDeviceDestroyedListener : deviceDestroyedListenerList) {
			try {
				onDeviceDestroyedListener.onDestroyed(c);
			} catch (Exception e) {
				log.severe(Session.OTHER, "Error during call listener on device destroyed [" + e.getMessage() + "].");
			}
		}
	}

	protected void onDestroyedGateway(Gateway g) {
		for (OnGatewayDestroyedListener onGatewayDestroyedListener : gatewayDestroyedListeners) {
			try {
				onGatewayDestroyedListener.onGatewayDestroyed(g);
			} catch (Exception e) {
				log.severe(Session.OTHER, "Error during call listener on gateway destroyed [" + e.getMessage() + "].");
			}
		}
	}
	
	protected void onCreatedGateway(Gateway g) {
		g.connect();
		for (OnGatewayCreatedListener onGatewayCreatedListener : gatewayListenerList) {
			try {
				onGatewayCreatedListener.onCreated(g);
			} catch (Exception e) {
				log.severe(Session.OTHER, "Error during call listener on gateway creation [" + e.getMessage() + "].");
			}
		}
	}

	/**
	 * Disconnect all gateway (and so all device connected
	 * to the gateway are not reachable (but not destroyed).
	 */
	@Override
	public void close() {
		this.gatewayList.forEach((key, gateway) -> destroyGateway(gateway));
	}
}
