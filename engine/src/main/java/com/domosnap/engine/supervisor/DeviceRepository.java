package com.domosnap.engine.supervisor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.*;
import io.reactivex.rxjava3.disposables.Disposable;

/**
 * DeviceRepository build device and keep it in cache.
 * It means instanciate a device, connect with stream to gateway
 * and put the device in cache. If you ask to create the same we
 * get it from cache and so you never had two same instance of the
 * same device.
 * Another feature of DeviceRepository is to be able to inform you
 * when gateway received message for an unknown device.
 */
public class DeviceRepository extends DeviceFactory {

	private final List<OnUnknownDeviceListener> unknownDeviceListenerList = Collections.synchronizedList(new ArrayList<>());
	
	protected final Map<Class<? extends Device>, Map<Where, Device>> deviceMap = new ConcurrentHashMap<>();

	protected final List<Device> deviceList = Collections.synchronizedList(new ArrayList<>());

	private Disposable unknownDetector;

	private final Log log = new Log(DeviceRepository.class.getSimpleName());
	
	public DeviceRepository() {
		super();
		addOnGatewayCreatedListener( g ->
			unknownDetector = g.getMonitor().subscribe(event -> {
				final Where w = event.getWhere();
				if (deviceMap.get(event.getWho()) == null || !deviceMap.get(event.getWho()).containsKey(w)) {
					if (log.finestLevel) {
						log.finest(Session.MONITOR, "Unknown controller detected [" + event + "].");
					}

					unknownDeviceListenerList.forEach(listener -> {
						try {
							listener.foundUnknownDevice(event);
						} catch (Exception e) {
							log.warning(Session.MONITOR, "Error with a listener when found an unknown controller on message [" + event +"]. Message dropped.");
						}
					});
				}
			})
		);
	}
	
	/**
	 * Add a listener on unknown {@link Device} instance detected. An unknown controller is detected
	 * when monitor stream get a message and doesn't know the where.
	 * @param unknownDeviceListener the listener when an unknown device is discovered (message received for an unknown device)
	 * @return the instance of device repository
	 */
	public DeviceRepository addUnknownDeviceListener(OnUnknownDeviceListener unknownDeviceListener) {
		if (!unknownDeviceListenerList.contains(unknownDeviceListener)) {
			unknownDeviceListenerList.add(unknownDeviceListener);
		}
		return this;
	}
	
	/**
	 * Remove a listener on unknown {@link Device} detected. An unknown controller is detected
	 * when monitor stream get a message and doesn't know the where.
	 * @param unknownDeviceListener the listener when an unknown device is discovered (message received for an unknown device)
	 * @return the instance of device repository
	 */
	public DeviceRepository removeUnknowDeviceListener(OnUnknownDeviceListener unknownDeviceListener) {
		unknownDeviceListenerList.remove(unknownDeviceListener);
		return this;
	}

	/**
	 * Connect to a gateway without create a device.
	 * When you create a device for a new gateway, the gateway
	 * is created and connected automatically.
	 * With this method you can connect a gateway without create a device.
	 * @param uri Address of the gateway
	 * @return a future with true if connection is done.
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}
	 */
	public Future<Boolean> connectGateway(URI uri) throws UnsupportedAdapter {
		return getGateway(uri).connect();
	}

	/**
	 * Disconnect a gateway (and all device associated).
	 * @param uri Address of the gateway
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}
	 */
	public void disconnectGateway(URI uri) throws UnsupportedAdapter {
		getGateway(uri).disconnect();
	}

	/**
	 * Create a {@link Device}. {@link Device} created is already connected
	 * to gateway (with a stream to send command and a stream to received command):
	 * this means that when you read or write the status the
	 * {@link Query}/{@link Command} is sent to the gateway.
	 *  If the device is already existing from cache, you get it.
	 * @param clazz the class of the controller you would like create.
	 * @param wheres address of the device
	 * @return the new created and initialized controller
	 * @throws IllegalArgumentException there is a problem with the uri. Some device can have been created.
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}. Some device can have been created.
	 */
	@Override
	public <T extends Device> List<Optional<T>> createDevices(
			Class<T> clazz, Where... wheres) throws IllegalArgumentException, UnsupportedAdapter {
		List<Optional<T>> list = new ArrayList<>();
		for (Where where : wheres) {
			list.add(createDevice(clazz, where));
		}
		return list;
	}

	/**
	 * Create a {@link Device}. Device created is already connected
	 * to physical gateway (with a monitor session and a command session):
	 * this means that when you read a status or write it,
	 * the {@link Query}/{@link Command} is sent to the gateway.
	 * If the device is already existing from cache, you get it.
	 * @param clazz the class of the controller you would like create.
	 * @param where the address of the device
	 * @return the new created or already existing initialized device
	 * @throws IllegalArgumentException there is a problem with the uri
	 * @throws UnsupportedAdapter the protocol (uri's schema) is not registered to {@link GatewayBuilder}. See {@link GatewayBuilder#registerProtocole(String, Class)}
	 */
	@Override
	public <T extends Device> Optional<T> createDevice (
			Class<T> clazz, Where where) throws IllegalArgumentException, UnsupportedAdapter {

		if (clazz == null || where == null) {
			return Optional.empty();
		}

		synchronized (deviceMap) {
			Map<Where, Device> map = deviceMap.computeIfAbsent(clazz, k -> new ConcurrentHashMap<>());

			Device d = map.get(where);
			if (d == null) {
				Optional<T> result = super.createDevice(clazz, where);
				if (result.isPresent()) {
					d = result.get();
					map.put(where, d);
					deviceList.add(d);
				}
				return result;
			} else {
				return (Optional<T>) Optional.of(d);
			}
		}
	}

	/**
	 * Add a listener triggered when a device is created.
	 * @param listener to trigger when a device is created.
	 * @return the instance
	 */
	@Override
	public DeviceRepository addOnDeviceCreatedListener(OnDeviceCreatedListener listener) {
		super.addOnDeviceCreatedListener(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a device is created.
	 * @param listener to removed.
	 * @return the instance
	 */
	@Override
	public DeviceRepository removeOnDeviceCreatedListener(OnDeviceCreatedListener listener) {
		super.removeOnDeviceCreatedListener(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a device is destroyed.
	 * @param listener to trigger when a device is destroyed.
	 * @return the instance
	 */
	@Override
	public DeviceRepository addOnDeviceDestroyedListener(OnDeviceDestroyedListener listener) {
		super.addOnDeviceDestroyedListener(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a device is destroyed.
	 * @param listener to removed.
	 * @return the instance
	 */
	@Override
	public DeviceFactory removeOnDeviceDestroyedListener(OnDeviceDestroyedListener listener) {
		super.removeOnDeviceDestroyedListener(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a gateway is created.
	 * @param listener to trigger when a gateway is created.
	 * @return the instance
	 */
	@Override
	public DeviceRepository addOnGatewayCreatedListener(OnGatewayCreatedListener listener) {
		super.addOnGatewayCreatedListener(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a gateway is created.
	 * @param listener to removed.
	 * @return the instance
	 */
	@Override
	public DeviceRepository removeOnGatewayCreatedListener(OnGatewayCreatedListener listener) {
		super.removeOnGatewayCreatedListener(listener);
		return this;
	}

	/**
	 * Add a listener triggered when a gateway is destroyed.
	 * @param listener to trigger when a gatewqy is destroyed.
	 * @return the instance
	 */
	public DeviceFactory addOnGatewayDestroyedListener(OnGatewayDestroyedListener listener) {
		super.addOnGatewayDestroyedListener(listener);
		return this;
	}

	/**
	 * Remove a listener triggered when a gateway is destroyed.
	 * @param listener to removed.
	 * @return the instance
	 */
	public DeviceFactory removeOnGatewayDestroyedListener(OnGatewayDestroyedListener listener) {
		super.removeOnGatewayDestroyedListener(listener);
		return this;
	}

	/**
	 * Return a map of map of devices who has been created.
	 * @return map of device of the same type
	 */
	public Map<Class<? extends Device>, Map<Where, Device>> getAllDevicesByClass() {
		return Collections.unmodifiableMap(deviceMap);
	}

	/**
	 * Return a map of devices of the same type who has been created.
	 * @return map of device.
	 */
	public Map<Where, Device> getDevicesByClassMap(Class<? extends Device> clazz) {
		return Collections.unmodifiableMap(deviceMap.get(clazz));
	}

	/**
	 * Return a map of all devices who has been created.
	 * @return map of device.
	 */
	public Map<Where, Device> getAllDevicesMap() {
		Map<Where, Device> result = new ConcurrentHashMap<>();
		deviceMap.forEach((clazz, list) -> result.putAll(list));
		return result;
	}

	/**
	 * Return a list of all devices who has been created.
	 * @return list of device.
	 */
	public List<Device> getAllDevicesList() {
		return Collections.synchronizedList(new ArrayList<>(deviceList));
	}

	/**
	 * Remove all device without disconnection.
	 */
	public void clear() {
		deviceList.clear();
		deviceMap.clear();
	}

	/**
	 * Destroy the {@link Gateway}. It means that gateway is disconnected and
	 * all device created with this gateway are destroyed too..
	 * @param g the gateway to destroyed.
	 * @return true if the gateway has been destroyed (or if null).
	 */
	public boolean destroyGateway(Gateway g) {

		if (g == null) {
			return true;
		}

		getAllDevicesMap().forEach((where, device) -> {
			try {
				if (g.equals(getGateway(device.getWhere().getURI()))) {
					destroyDevice(device);
				}
			} catch (UnsupportedAdapter e) {
				throw new RuntimeException(e);
			}
		});
		return super.destroyGateway(g);
	}

	/**
	 * Disconnect all gateway (and so all device connected
	 * o the gateway are not reachable (but not destroyed).
	 * And remove all device and disconnect them.
	 */
	@Override
	public void close() {
		super.close();
		getAllGateway().forEach(this::destroyGateway);
		deviceMap.clear();
		unknownDetector.dispose();
	}

	public Device getDevice(Where where) {
		return getAllDevicesMap().get(where);
	}

	@SuppressWarnings("unchecked")
	public <T extends Device> T getDevice(Class<T> clazz, Where where) {
		Map<Where, Device> l = this.deviceMap.get(clazz);
		if (l == null) {
			return null;
		} else {
			return (T) l.get(where);
		}
	}

	public Device deleteDevice(Where where) {
		for (Entry<Class<? extends Device>, Map<Where, Device>> entry : this.deviceMap.entrySet()) {
			Map<Where, Device> map = entry.getValue();
			for (Entry<Where, Device> device: map.entrySet()) {
				if (device.getKey().equals(where)) {
					Device result = map.remove(where);
					deviceList.remove(result);
					return result;
				}
			}
		}
		return null;
	}

	public <T extends Device> void deleteDevice(Class<T> clazz, Where where) {
		this.deviceMap.get(clazz).remove(where);

	}

	/**
	 * Delete the device from the repository and destroy it.
	 * Destroy means that the device will be deconnected from the gateway.
	 * @param d Device to destroy.
	 * @return true if the device has been remove from the repository (or if device is null).
	 */
	@Override
	public boolean destroyDevice(Device d) {
		return super.destroyDevice(deleteDevice(d.getWhere()));
	}
}
