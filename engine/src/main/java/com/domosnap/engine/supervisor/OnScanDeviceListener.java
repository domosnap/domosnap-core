package com.domosnap.engine.supervisor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.device.Device;

import java.net.URI;

/**
 * {@link OnScanDeviceListener} must be provided to {@link com.domosnap.engine.supervisor.DeviceDiscovery#scan(URI, OnScanDeviceListener)}.
 * It act as a callback when an unknown controller is found.
 */
public interface OnScanDeviceListener {

	/**
	 * Called each time a new Controller is found.
	 * @param controller
	 */
	public void foundController(Device controller);

	/**
	 * Called by {@link com.domosnap.engine.supervisor.DeviceDiscovery#scan(URI, OnScanDeviceListener)} by providing percent progress.
	 * When percent = 100, scan SHOULD be finished but without any warranty.
	 * @param percent
	 */
	public void progess(int percent);
	
	/**
	 *  Called by {@link com.domosnap.engine.supervisor.DeviceDiscovery#scan(URI, OnScanDeviceListener)} when percent = 100.
	 *  Scan SHOULD be finished but without any warranty.
	 */
	public void scanFinished();
}
