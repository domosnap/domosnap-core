package com.domosnap.engine.supervisor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.gateway.Event;

/**
 * Each time a monitor session ( {@link IMonitorStream} ) get a message and isn't able to 
 * found a controller, {@link OnUnknownDeviceListener#foundUnknownController(String, String, String, String, List)}
 * is called.
 */
public interface OnUnknownDeviceListener {

	public void foundUnknownDevice(Event event);
}
