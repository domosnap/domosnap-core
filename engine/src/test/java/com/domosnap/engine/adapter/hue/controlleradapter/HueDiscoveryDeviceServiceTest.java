//package com.domosnap.engine.adapter.hue.controlleradapter;
//
//import static org.junit.jupiter.api.Assumptions.assumeTrue;
//
//import java.net.Socket;
//import java.time.Duration;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import com.domosnap.engine.adapter.hue.old.HueDiscoveryService;
//import com.domosnap.engine.supervisor.DiscoveryDeviceServiceGenericTest;
//import com.domosnap.engine.supervisor.DiscoveryDeviceServiceGenericTest.IDiscoveryDeviceServiceTest;
//
//class HueDiscoveryDeviceServiceTest implements IDiscoveryDeviceServiceTest {
//
//	private DiscoveryDeviceServiceGenericTest caTest = new DiscoveryDeviceServiceGenericTest();
//	
//	@Test
//	@Override
//	public void testScan() throws InterruptedException {
//		Assertions.assertTimeout(Duration.ofMillis(20000), () -> {
//		// when
//			caTest.testScan( new HueDiscoveryService("newdeveloper@127.0.0.1:8080"));
//		});
//	}
//	
//	@BeforeAll
//	public static void assume() {
//		assumeTrue(is8080Open());
//	}
//
//	private static boolean is8080Open() {
//		try {
//			Socket socket  = new Socket("127.0.0.1", 8080);
//			boolean result = socket.isConnected();
//			socket.close();
//			return result;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//}
