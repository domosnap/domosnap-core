package com.domosnap.engine.adapter.moc.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.what.impl.PercentageState;
import com.domosnap.engine.device.what.impl.RGBState;
import com.domosnap.engine.gateway.*;
import org.reactivestreams.Publisher;

import com.domosnap.engine.adapter.ICommanderStream;

import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;

public class CommanderStreamMoc implements ICommanderStream<String, String> {

	private ConnectionStatusEnum connect = ConnectionStatusEnum.UNKNOWN_ERROR;
	private final Consumer<String> consumer;
	private final Function<Command, Publisher<String>> mapper = CommanderStreamMoc::applyCommand;
	private final Function<Query, Publisher<String>> queryMapper = CommanderStreamMoc::applyQuery;
	protected List<OnConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<>());
	
	public CommanderStreamMoc() {
		consumer = MonitorStreamMoc::push;

	}

	private static Publisher<String> applyQuery(Query query) {
		return s -> {
			List<What> result = new ArrayList<>();
			query.getWhatNameList().forEach(whatName -> {
				if (Light.class.equals(query.getWho())) {
					if (Light.LightStateName.STATUS.name().equals(whatName)) {
						result.add(new What(Light.LightStateName.STATUS.name(), OnOffState.Off()));
					} else if (Light.LightStateName.COLOR.name().equals(whatName)) {
						result.add(new What(Light.LightStateName.COLOR.name(), new RGBState("FFFFFF")));
					} else if (Light.LightStateName.LEVEL.name().equals(whatName)) {
						result.add(new What(Light.LightStateName.LEVEL.name(), new PercentageState("100 %")));
					} else if (Light.LightStateName.REACHABLE.name().equals(whatName)) {
						result.add(new What(Light.LightStateName.REACHABLE.name(), BooleanState.FALSE));
					}
				}
			});
			// Recreate command as a write command => else event have no value
			Command c = new Command(query.getWho(), query.getWhere(), result);
			s.onNext(CommandQueryJsonCodec.toJSon(c));
		};
	}

	private static Publisher<String> applyCommand(Command command) {
		return s -> s.onNext(CommandQueryJsonCodec.toJSon(command));
	}

	@Override
	public Consumer<String> getCommandConsumer() {
		return consumer;
	}


	@Override
	public Consumer<String> getQueryConsumer() {
		return getCommandConsumer();
	}

	@Override
	public Function<Command, Publisher<String>> getCommandMapper() {
		return mapper;
	}

	@Override
	public Function<Query, Publisher<String>> getQueryMapper() {
		return queryMapper;
	}

	
	@Override
	public ConnectionStatusEnum connect() {
		if (!ConnectionStatusEnum.CONNECTED.equals(connect)) {
			connect = ConnectionStatusEnum.CONNECTED;
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(connect);
			}
		}
		return connect;
		
	}

	@Override
	public void disconnect() {
		if (ConnectionStatusEnum.CONNECTED.equals(connect)) {
			connect = ConnectionStatusEnum.UNKNOWN_ERROR;
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onDisconnect(DisconnectionStatusEnum.CLOSED);
			}
		}
	}

	@Override
	public boolean isConnected() {
		return ConnectionStatusEnum.CONNECTED.equals(connect);
	}

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		connectionListenerList.add(listener);
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		connectionListenerList.remove(listener);
	}

}
