package com.domosnap.engine.adapter.moc.core;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.AdvancedAdapter;
import com.domosnap.engine.adapter.ICommanderStream;
import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.shutter.Shutter;
import com.domosnap.engine.gateway.OnScanGatewayListener;
import com.domosnap.engine.supervisor.OnScanDeviceListener;

public class FullAdapterMoc extends AdvancedAdapter<String, String, String> {

	private static final Log log = new Log(FullAdapterMoc.class.getSimpleName());
	
	private URI uri;
	
	public static String SCHEME = "fullmoc"; 
	private ICommanderStream<String, String> commander;
	private IMonitorStream<String> monitor;
	
	public FullAdapterMoc() {
		try {
			setURI(new URI(SCHEME, "ssp", "fragment"));
			commander = new CommanderStreamMoc();
			monitor = new MonitorStreamMoc();
		} catch (URISyntaxException e) {
			log.severe(Session.OTHER, "Wrong URI");
		}
	}
	
	@Override
	public URI getURI() {
		return uri;
	}

	@Override
	public void setURI(URI uri) {
		this.uri = uri;
	}

	@Override
	public ICommanderStream<String, String> getPhysicalCommander() {
		return commander;
	}

	@Override
	public IMonitorStream<String> getPhysicalMonitor() {
		return monitor;
	}

	@Override
	public List<Class<? extends Device>> getSupportedDevice() {
		List<Class<? extends Device>> list = new ArrayList<>();
		list.add(Light.class);
		list.add(Shutter.class);
		return list;
	}

	@Override
	public void scan(OnScanDeviceListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scanGateway(OnScanGatewayListener listener) {
		// TODO Auto-generated method stub
		
	}
}
