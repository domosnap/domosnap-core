package com.domosnap.engine.adapter.moc.core;

import static org.junit.jupiter.api.Assertions.fail;

import com.domosnap.engine.gateway.GatewayBuilder;
import com.domosnap.engine.gateway.UnsupportedScheme;

public class MocTools {

	public static void registerMocAdapter() {
		try {
			GatewayBuilder.registerProtocole(FullAdapterMoc.SCHEME, FullAdapterMoc.class);
		} catch (UnsupportedScheme e) {
			fail("GatewayBuilder must support a correct schema.");
		}
	}
}
