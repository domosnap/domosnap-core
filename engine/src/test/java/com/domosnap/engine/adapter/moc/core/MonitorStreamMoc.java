package com.domosnap.engine.adapter.moc.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import com.domosnap.engine.adapter.IMonitorStream;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.DisconnectionStatusEnum;
import com.domosnap.engine.gateway.Event;
import com.domosnap.engine.gateway.EventJsonCodec;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.FlowableEmitter;
import io.reactivex.rxjava3.core.FlowableOnSubscribe;
import io.reactivex.rxjava3.functions.Function;

public class MonitorStreamMoc implements IMonitorStream<String> {

	private ConnectionStatusEnum connect = ConnectionStatusEnum.UNKNOWN_ERROR;
	private final Function<String, Publisher<Event>> mapper;
	
	private final FlowableOnSubscribe<String> monitor;
	private final static List<FlowableEmitter<@NonNull String>> emmiters = Collections.synchronizedList(new ArrayList<>());
	protected List<OnConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<>());
	
	public MonitorStreamMoc() {
		mapper = s -> {
			return new Publisher<Event>() {
				@Override
				public void subscribe(Subscriber<? super Event> t) {
					t.onNext(EventJsonCodec.fromJson(s));
				}
			};
		};
		monitor = new FlowableOnSubscribe<String>() {

			@Override
			public void subscribe(@NonNull FlowableEmitter<@NonNull String> emitter) throws Throwable {
				synchronized (emmiters) {
					emmiters.add(emitter);
				}
			}
		};
	}
	
	static void push(String event) {
		synchronized (emmiters) {
			for (FlowableEmitter<String> flowableEmitter : emmiters) {
				flowableEmitter.onNext(event);
			}
		}
	}
	
	@Override
	public FlowableOnSubscribe<String> getPhysicalMonitorStream() {
		return monitor;
	}

	@Override
	public Function<String, Publisher<Event>> getMapper() {
		return mapper;
	}
	
	@Override
	public ConnectionStatusEnum connect() {
		if (!ConnectionStatusEnum.CONNECTED.equals(connect)) {
			connect = ConnectionStatusEnum.CONNECTED;
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(connect);
			}
		}
		return connect;
		
	}

	@Override
	public void disconnect() {
		if (ConnectionStatusEnum.CONNECTED.equals(connect)) {
			connect = ConnectionStatusEnum.UNKNOWN_ERROR;
			for (OnConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onDisconnect(DisconnectionStatusEnum.CLOSED);
			}
		}
	}

	@Override
	public boolean isConnected() {
		return ConnectionStatusEnum.CONNECTED.equals(connect);
	}

	@Override
	public void addConnectionListener(OnConnectionListener listener) {
		connectionListenerList.add(listener);
	}

	@Override
	public void removeConnectionListener(OnConnectionListener listener) {
		connectionListenerList.remove(listener);
	}
}
