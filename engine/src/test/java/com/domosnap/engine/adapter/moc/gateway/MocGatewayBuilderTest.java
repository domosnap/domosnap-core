package com.domosnap.engine.adapter.moc.gateway;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
import com.domosnap.engine.gateway.GatewayBuilderTestGeneric;

public class MocGatewayBuilderTest {
	private GatewayBuilderTestGeneric t = new GatewayBuilderTestGeneric();
	
	@Test 
	public void testBuilder() {
		t.testBuilder(FullAdapterMoc.SCHEME, FullAdapterMoc.class);
	}
}
