package com.domosnap.engine.adapter.moc.gateway;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
import com.domosnap.engine.gateway.GatewayTestGeneric;

public class MocGatewayTest {

	GatewayTestGeneric t = new GatewayTestGeneric();

	@Test
	public void testGenericAdapter() {
		t.test(new FullAdapterMoc());
	}
}