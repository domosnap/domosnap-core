package com.domosnap.engine.adapter.moc.supervisor;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
import com.domosnap.engine.adapter.moc.core.MocTools;
import com.domosnap.engine.supervisor.DeviceFactoryTestGeneric;

public class MocDeviceFactoryTest {

	private final DeviceFactoryTestGeneric t = new DeviceFactoryTestGeneric();
	
	@BeforeAll
	public static void init() {
		MocTools.registerMocAdapter();
	}
	
	@Test
	public void test() {
		t.createDeviceTest(FullAdapterMoc.SCHEME + "://localhost:1234/12");
		t.createDevicesTest(FullAdapterMoc.SCHEME + "://localhost:1234/13", FullAdapterMoc.SCHEME + "://localhost:1234/14");
	}
}

// TODO HeatingTest.getSetOffsetTest » OnErrorNotImplemented The exception was not handled due to missing onError handler in the subscribe() method call. Further reading: https://github.com/ReactiveX/RxJava/wiki/Error-Handling | org.opentest4j.AssertionFailedError: expected: <scs://*****@localhost:1234/3> but was: <scs://*****@localhost:1234/12>

