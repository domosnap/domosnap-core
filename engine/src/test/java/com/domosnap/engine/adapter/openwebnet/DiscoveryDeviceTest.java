package com.domosnap.engine.adapter.openwebnet;

import java.net.URI;
import java.net.URISyntaxException;

import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DiscoveryDeviceGeneric;

import static org.junit.jupiter.api.Assumptions.assumeTrue;


class DiscoveryDeviceTest {

	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
	@Test
	void testscan() throws IllegalArgumentException, InterruptedException, UnsupportedAdapter, URISyntaxException {
		DiscoveryDeviceGeneric dt = new DiscoveryDeviceGeneric();
		dt.testScan(new URI("scs://12345@localhost:1234"));
	}
}
