package com.domosnap.engine.adapter.openwebnet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/*
 * #%L
 * MyDomoEngine
 * %%
 * Copyright (C) 2011 - 2013 A. de Giuli
 * %%
 * This file is part of MyDomo done by A. de Giuli (arnaud.degiuli(at)free.fr).
 * 
 *     MyDomo is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MyDomo is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with MyDomo.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.conversion.core.CommandEnum;
import com.domosnap.engine.adapter.openwebnet.conversion.core.WhereType;
import com.domosnap.engine.adapter.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.CommandParser;
import com.domosnap.engine.adapter.openwebnet.conversion.core.parser.ParseException;

class ParserTest {
	
	@Test
	void whoTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*12##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("1", p.getWho());
		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
		
		// Space case..
		String gatewayStatusCommand = "*#13**0##";
		p  = CommandParser.parse(gatewayStatusCommand);
		assertEquals("13", p.getWho());

	}

	@Test
	void whereGeneralTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*0##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("0", p.getWhere());
		assertEquals(WhereType.GENERAL, p.getWhereType());
	}

	@Test
	void whereAmbianceTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*1##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("1", p.getWhere());
		assertEquals( WhereType.ENVIRONMENT, p.getWhereType());
		assertEquals("1", p.getEnvironment());
	}

	@Test
	void whereGroupTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*#1##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("#1", p.getWhere());
		assertEquals("1", p.getGroup());
		assertEquals(WhereType.GROUP, p.getWhereType());
	}	

	@Test
	void wherePointToPointTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*12##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("12", p.getWhere());
		assertEquals("12", p.getPoint());
		assertEquals(WhereType.POINTTOPOINT, p.getWhereType());
	}

	@Test
	void whereGeneralOnBusTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*0#4#2##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("0#4#2", p.getWhere());
		assertEquals(WhereType.GENERALONLOCALBUS, p.getWhereType());
		assertEquals("2", p.getBus());
	}

	@Test
	void whereAmbianceOnBusTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*1#4#2##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("1#4#2", p.getWhere());
		assertEquals("1", p.getEnvironment());
		assertEquals(WhereType.ENVIRONMENTONLOCALBUS, p.getWhereType());
		assertEquals("2", p.getBus());
	}

	@Test
	void whereGroupOnBusTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*#1#4#2##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals("#1#4#2", p.getWhere());
		assertEquals("1", p.getGroup());
		assertEquals("2", p.getBus());
		assertEquals(WhereType.GROUPONLOCALBUS, p.getWhereType());
	}

	@Test
	void wherePointToPointOnBusTest() throws ParseException {
		// Standard          *WHO*WHAT*WHERE##
		String standard = "*1*1*12#4#2##";
		CommandParser p  = CommandParser.parse(standard);
		assertEquals(p.getWhere(), "12#4#2");
		assertEquals("12", p.getPoint());
		assertEquals(WhereType.POINTTOPOINT, p.getWhereType());
		assertEquals("2", p.getBus());
	}



	@Test
	void statusTest() throws ParseException {
		// Status request    *#WHO*WHERE##
		String status = "*#1*12##";
		CommandParser p  = CommandParser.parse(status);
		assertEquals("12", p.getWhere());
		assertEquals(CommandEnum.STANDARD_STATUS, p.getType());
	}
	
	@Test
	void dimensionRequestTest() throws ParseException {
		// Dimension request *#WHO*WHERE*DIMENSION##tring dimensionCommand = "*#12*1*#1*02*11*05*2012##";
		String dimensionStatus = "*#12*22*0##";
		CommandParser p  = CommandParser.parse(dimensionStatus);
		assertEquals("0", p.getDimension());
	}
	
	@Test
	void dimensionWriteTest() throws ParseException {
		// Dimension write   *#WHO*WHERE*#DIMENSION*VAL1*VAL2*...*VALn##
		String dimensionCommand = "*#12*1*#1*02*11*05*2012##";
		CommandParser p  = CommandParser.parse(dimensionCommand);
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertNotEquals(CommandEnum.STANDARD_COMMAND, p.getType());
		assertEquals("1", p.getDimension());
		assertEquals(4, p.getDimensionList().size());
		assertEquals("02", p.getDimensionList().get(0).getValue());
		assertEquals("2012", p.getDimensionList().get(3).getValue());
	}
	
	@Test
	void dimensionReadListTest() throws ParseException {
		// Dimension read *#WHO*WHERE*DIMENSION*VAL1*VAL2*...*VALn##
		List<DimensionValue> l = CommandParser.parse("*#4*6*0*0226##").getDimensionList();
		assertEquals(1, l.size());
		assertEquals("0226", l.get(0).getValue());
		
		// Read gateway date
		CommandParser parser = CommandParser.parse("*#13**0*19*51*02*001##");
		assertEquals(CommandEnum.DIMENSION_STATUS, parser.getType());
		assertEquals("13", parser.getWho());
		assertEquals("0", parser.getDimension());
		assertEquals("19", parser.getDimensionList().get(0).getValue());
		assertEquals("51", parser.getDimensionList().get(1).getValue());
		assertEquals("02", parser.getDimensionList().get(2).getValue());
		assertEquals("001", parser.getDimensionList().get(3).getValue());
	}
	
	@Test
	void heatingDimension20Test() throws ParseException {
		// Dimension write   *#WHO*WHERE*#DIMENSION*VAL1*VAL2*...*VALn##
		
		CommandParser p = CommandParser.parse("*#4*9#1*20*1##");
		
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		
		assertEquals("1", p.getActuator());
		assertEquals("9", p.getZone());

		assertEquals("20", p.getDimension());

		List<DimensionValue> l = p.getDimensionList();
		assertEquals(1, l.size());
		assertEquals("1", l.get(0).getValue());
	}
	
	
	@Test
	void gatewayDimensionTest() throws ParseException {
		// Dimension write   *#WHO*WHERE*#DIMENSION*VAL1*VAL2*...*VALn##
		
		CommandParser p = CommandParser.parse("*#13**#1*02*08*08*2017##");
		
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		
		assertEquals("13", p.getWho());

		assertEquals("1", p.getDimension());

		List<DimensionValue> l = p.getDimensionList();
		assertEquals(4, l.size());
		assertEquals("02", l.get(0).getValue());
	}
	
	@Test
	void gatewayDimension20Test() throws ParseException {
		// Dimension write   *#WHO*WHERE*#DIMENSION*VAL1*VAL2*...*VALn##
		
		CommandParser p = CommandParser.parse("*#13**1##");
		
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		
		assertEquals("13", p.getWho());

		assertEquals("1", p.getDimension());
	}
	
	
	@Test
	void gatewayAllTest() throws ParseException {
		// Time Request (dimension = 0)
		CommandParser p = CommandParser.parse("*#13**0##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("0", p.getDimension());
		// Time request Result (status)
		p = CommandParser.parse("*#13**0*12*30*10*102##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("0", p.getDimension());
		assertEquals("12", p.getDimensionList().get(0).getValue());
		assertEquals("30", p.getDimensionList().get(1).getValue());
		assertEquals("10", p.getDimensionList().get(2).getValue());
		assertEquals("102", p.getDimensionList().get(3).getValue());
		// Time set
		p = CommandParser.parse("*#13**#0*12*30*10*102##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("0", p.getDimension());
		assertEquals("12", p.getDimensionList().get(0).getValue());
		assertEquals("30", p.getDimensionList().get(1).getValue());
		assertEquals("10", p.getDimensionList().get(2).getValue());
		assertEquals("102", p.getDimensionList().get(3).getValue());
		
		// Date Request (dimension = 1)
		p = CommandParser.parse("*#13**1##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("1", p.getDimension());
		// Date request result
		p = CommandParser.parse("*#13**1*5*20*10*2021##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("1", p.getDimension());
		assertEquals("5", p.getDimensionList().get(0).getValue());
		assertEquals("20", p.getDimensionList().get(1).getValue());
		assertEquals("10", p.getDimensionList().get(2).getValue());
		assertEquals("2021", p.getDimensionList().get(3).getValue());	
		// Date set
		p = CommandParser.parse("*#13**#1*5*20*10*2021##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("1", p.getDimension());
		assertEquals("5", p.getDimensionList().get(0).getValue());
		assertEquals("20", p.getDimensionList().get(1).getValue());
		assertEquals("10", p.getDimensionList().get(2).getValue());
		assertEquals("2021", p.getDimensionList().get(3).getValue());
		
		// Ip Request (dimension = 10)
		p = CommandParser.parse("*#13**10##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("10", p.getDimension());
		// Ip request result
		p = CommandParser.parse("*#13**10*5*10*15*20##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("10", p.getDimension());
		assertEquals("5", p.getDimensionList().get(0).getValue());
		assertEquals("10", p.getDimensionList().get(1).getValue());
		assertEquals("15", p.getDimensionList().get(2).getValue());
		assertEquals("20", p.getDimensionList().get(3).getValue());
		
		// Netmask Request (dimension = 11)
		p = CommandParser.parse("*#13**11##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("11", p.getDimension());
		// Netmask request result
		p = CommandParser.parse("*#13**11*200*205*210*215##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("11", p.getDimension());
		assertEquals("200", p.getDimensionList().get(0).getValue());
		assertEquals("205", p.getDimensionList().get(1).getValue());
		assertEquals("210", p.getDimensionList().get(2).getValue());
		assertEquals("215", p.getDimensionList().get(3).getValue());
		
		// Mac Request (dimension = 12)
		p = CommandParser.parse("*#13**12##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("12", p.getDimension());
		// Mac Request result
		p = CommandParser.parse("*#13**12*10*255*255*255*210*210##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("12", p.getDimension());
		assertEquals("10", p.getDimensionList().get(0).getValue());
		assertEquals("255", p.getDimensionList().get(1).getValue());
		assertEquals("255", p.getDimensionList().get(2).getValue());
		assertEquals("255", p.getDimensionList().get(3).getValue());
		assertEquals("210", p.getDimensionList().get(4).getValue());
		assertEquals("210", p.getDimensionList().get(5).getValue());
		
		// Model Request (dimension = 15)
		p = CommandParser.parse("*#13**15##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("15", p.getDimension());
		// Model Request result
		p = CommandParser.parse("*#13**15*2##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("15", p.getDimension());
		assertEquals("2", p.getDimensionList().get(0).getValue());
		
		// Firmware version Request (dimension = 16)
		p = CommandParser.parse("*#13**16##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("16", p.getDimension());
		// Firmware version request result
		p = CommandParser.parse("*#13**16*1*2*3##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("16", p.getDimension());
		assertEquals("1", p.getDimensionList().get(0).getValue());
		assertEquals("2", p.getDimensionList().get(1).getValue());
		assertEquals("3", p.getDimensionList().get(2).getValue());
		
		// Uptime Request (dimension = 11)
		p = CommandParser.parse("*#13**19##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("19", p.getDimension());
		// uptime request result
		p = CommandParser.parse("*#13**19*161*17*36*35##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("19", p.getDimension());
		assertEquals("161", p.getDimensionList().get(0).getValue());
		assertEquals("17", p.getDimensionList().get(1).getValue());
		assertEquals("36", p.getDimensionList().get(2).getValue());
		assertEquals("35", p.getDimensionList().get(3).getValue());
		
		// datetime Request (dimension = 22)
		p = CommandParser.parse("*#13**22##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("22", p.getDimension());
		// datetime request result
		p = CommandParser.parse("*#13**22*19*22*08*001*02*11*05*2021##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("22", p.getDimension());
		assertEquals("19", p.getDimensionList().get(0).getValue());
		assertEquals("22", p.getDimensionList().get(1).getValue());
		assertEquals("08", p.getDimensionList().get(2).getValue());
		assertEquals("001", p.getDimensionList().get(3).getValue());
		assertEquals("02", p.getDimensionList().get(4).getValue());
		assertEquals("11", p.getDimensionList().get(5).getValue());
		assertEquals("05", p.getDimensionList().get(6).getValue());
		assertEquals("2021", p.getDimensionList().get(7).getValue());
		// datetime set request
		p = CommandParser.parse("*#13**#22*19*22*08*001*02*11*05*2021##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("22", p.getDimension());
		assertEquals("19", p.getDimensionList().get(0).getValue());
		assertEquals("22", p.getDimensionList().get(1).getValue());
		assertEquals("08", p.getDimensionList().get(2).getValue());
		assertEquals("001", p.getDimensionList().get(3).getValue());
		assertEquals("02", p.getDimensionList().get(4).getValue());
		assertEquals("11", p.getDimensionList().get(5).getValue());
		assertEquals("05", p.getDimensionList().get(6).getValue());
		assertEquals("2021", p.getDimensionList().get(7).getValue());
		
		// Kernel version Request (dimension = 16)
		p = CommandParser.parse("*#13**23##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("23", p.getDimension());
		// Kernel version request result
		p = CommandParser.parse("*#13**23*1*2*3##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("23", p.getDimension());
		assertEquals("1", p.getDimensionList().get(0).getValue());
		assertEquals("2", p.getDimensionList().get(1).getValue());
		assertEquals("3", p.getDimensionList().get(2).getValue());
		
		// Distribution version Request (dimension = 24)
		p = CommandParser.parse("*#13**24##");
		assertEquals(CommandEnum.DIMENSION_COMMAND, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("24", p.getDimension());
		// Distribution version request result
		p = CommandParser.parse("*#13**24*1*2*3##");
		assertEquals(CommandEnum.DIMENSION_STATUS, p.getType());
		assertEquals("13", p.getWho());
		assertEquals("24", p.getDimension());
		assertEquals("1", p.getDimensionList().get(0).getValue());
		assertEquals("2", p.getDimensionList().get(1).getValue());
		assertEquals("3", p.getDimensionList().get(2).getValue());
	}
	
	@Test
	void lightAllTest() throws ParseException {
		// Light off Request (what = 0)
		CommandParser p = CommandParser.parse("*1*0*10##");
		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
		assertEquals("1", p.getWho());
		assertEquals("0", p.getWhat());
		assertEquals("10", p.getWhere());

		// Light off at x speed Request (what = 0)
//		p = CommandParser.parse("*1*0#255*10##");
//		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
//		assertEquals("1", p.getWho());
//		assertEquals("0", p.getWhat());
//		assertEquals("10", p.getWhere());
//		p = CommandParser.parse("#1*10*1*100*255##");
//		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
//		assertEquals("1", p.getWho());
//		assertEquals("0", p.getWhat());
//		assertEquals("10", p.getWhere());
		
		// Light On Request (what = 1)
		p = CommandParser.parse("*1*1*10##");
		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
		assertEquals("1", p.getWho());
		assertEquals("1", p.getWhat());
		assertEquals("10", p.getWhere());
		
		// Light on at x speed Request (what = 0)
//		p = CommandParser.parse("*1*0#255*10##");
//		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
//		assertEquals("1", p.getWho());
//		assertEquals("0", p.getWhat());
//		assertEquals("10", p.getWhere());
//		p = CommandParser.parse("#1*10*1*100*255##");
//		assertEquals(CommandEnum.STANDARD_COMMAND, p.getType());
//		assertEquals("1", p.getWho());
//		assertEquals("0", p.getWhat());
//		assertEquals("10", p.getWhere());
	}
}