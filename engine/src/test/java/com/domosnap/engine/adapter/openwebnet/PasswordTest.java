package com.domosnap.engine.adapter.openwebnet;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.connector.Password;

class PasswordTest {

	private Password passwordEncoder = new Password();
	
	@Test
	void passwordTest() {
		testCalcPass (12345, "603356072", "25280520");
		testCalcPass (12345, "410501656", "119537670");
		testCalcPass (12345, "630292165", "4269684735");
		testCalcPass (12345, "523781130", "537331200");
	}
	
	private void testCalcPass (int pass, String nonce, String expected) {
		String res = passwordEncoder.calcPass (pass, nonce);
		assertEquals(expected, res);
	}
}
