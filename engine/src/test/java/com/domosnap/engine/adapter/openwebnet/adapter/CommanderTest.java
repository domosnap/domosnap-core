package com.domosnap.engine.adapter.openwebnet.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.connector.Commander;
import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import com.domosnap.engine.gateway.OnConnectionListener;
import com.domosnap.engine.gateway.ConnectionStatusEnum;
import com.domosnap.engine.gateway.DisconnectionStatusEnum;

public class CommanderTest {

	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
		
	final static String ip = "localhost";
	final static int port = 1234;
	final static int pwd = 12345;
	
	@Test
	void testCommanderConnection() {

		
		Commander conWithWrongPort = createCommander(ip, 80, pwd, ConnectionStatusEnum.CONNECTION_REFUSED, DisconnectionStatusEnum.CLOSED);
		conWithWrongPort.connect();
		assertFalse(conWithWrongPort.isConnected());
		conWithWrongPort.disconnect();
		assertFalse(conWithWrongPort.isConnected());
		
		Commander conWithWrongIp = createCommander("toto", port, pwd, ConnectionStatusEnum.INVALID_ADDRESS, DisconnectionStatusEnum.CLOSED);
		conWithWrongIp.connect();
		assertFalse(conWithWrongIp.isConnected());
		conWithWrongIp.disconnect();
		assertFalse(conWithWrongIp.isConnected());
		
		Commander conWithWrongPassword = createCommander(ip, port, 12, ConnectionStatusEnum.WRONG_ACKNOWLEDGEMENT, DisconnectionStatusEnum.CLOSED);
		conWithWrongPassword.connect();
		assertFalse(conWithWrongPassword.isConnected());
		conWithWrongPassword.disconnect();
		assertFalse(conWithWrongPassword.isConnected());
		
		
		Commander con = createCommander(ip, port, pwd, ConnectionStatusEnum.CONNECTED, DisconnectionStatusEnum.CLOSED);
		con.connect();
		con.sendCommand("*1*1*12##");
		con.disconnect();
		assertFalse(con.isConnected());
		con.sendCommand("*1*1*12##");
		
	}
	
	
	private Commander createCommander(String ip, int port, int pwd, ConnectionStatusEnum connect, DisconnectionStatusEnum close) {
		Commander c = new Commander(ip, port, pwd);
		c.addConnectionListener(new OnConnectionListener() {
			
			@Override
			public void onDisconnect(DisconnectionStatusEnum disconnectionStatus) {
				assertEquals(close, disconnectionStatus);
			}
			
			@Override
			public void onConnect(ConnectionStatusEnum connectionStatus) {
				assertEquals(connect,connectionStatus);
			}
			
		});

		return c;
	}
	
}
