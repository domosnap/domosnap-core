package com.domosnap.engine.adapter.openwebnet.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.OpenWebNetCommandConsumer;
import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;

class OpenWebNetCommanderConsumerTest {

	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
		
	final static String ip = "localhost";
	final static int port = 1234;
	final static int pwd = 12345;
	
	@Test
	void testOpenWebNetCommanderConsumer() {
		OpenWebNetCommandConsumer con = new OpenWebNetCommandConsumer(ip, port, pwd);
		assertEquals(ip, con.getIp());
		assertEquals(port, con.getPort());
	}
	
}
