package com.domosnap.engine.adapter.openwebnet.device;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import com.domosnap.engine.device.heating.HeatingZone;
import com.domosnap.engine.device.heating.statevalue.OffsetState;
import com.domosnap.engine.device.heating.statevalue.OffsetState.Mode;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

class HeatingTest {

	// TODO revoir si ce test ne peut pas etre remplacer par la sonde de temperature?
	
	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
	
	@Test
	void getDesiredTemperatureTest() throws IllegalArgumentException, UnsupportedAdapter {
		
		Where w = new Where("scs://12345@localhost:1234/1");
		
		try (DeviceFactory df = new DeviceFactory()) {
			HeatingZone hz = df.createDevice(HeatingZone.class, w).get();
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> hz.getDesiredTemperature()!=null);
	//		DesiredTemperature d = hz.getDesiredTemperature(HeatingZoneState.HEATING_MODE); TODO manage mode
			DoubleState d = hz.getDesiredTemperature();
			assertNotNull(d);
			assertEquals(19, d.getValue(), 0);
		}
	}

	
	// TODO Not supported actually...
//	@Test
//	public void setDesiredTemperatureTest() {
//		HeatingZone hz = s.createController(HeatingZone.class, new Where("11","11"));
//		System.out.println("setDesiredTemperatureTest");
//		
//		hz.addControllerChangeListener(new ControllerChangeListener() {
//
//			@Override
//			public void onStateChangeError(Controller controller,
//					What oldStatus, What newStatus) {
//				synchronized (lock) {
//					// When response from server is here we unlock the thread
//					System.out.println("Unlock...");
//					lock.notify();
//				}
//			}
//			
//			@Override
//			public void onStateChange(Controller controller,
//					What oldStatus, What newStatus) {
//				synchronized (lock) {
//					// When response from server is here we unlock the thread
//					System.out.println("Unlock...");
//					lock.notify();
//				}
//			}
//		});
//	
//		
//		hz.setDesiredTemperature(19, HeatingModeEnum.HEATING); TODO manage mode
//		hz.setDesiredTemperature(new DoubleState(19));
//		System.out.println("Wait desired temperature...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		DesiredTemperature r = hz.getDesiredTemperature(HeatingModeEnum.HEATING);
//		Assert.assertNotNull(r);
//		Assert.assertEquals(19, r.getDesiredTemperature(), 0);
//		
//		
//		hz.setDesiredTemperature(24, HeatingModeEnum.HEATING);
//		System.out.println("Wait desired temperature...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		r = hz.getDesiredTemperature(HeatingModeEnum.HEATING);
//		Assert.assertNotNull(r);
//		Assert.assertEquals(24, r.getDesiredTemperature(), 0);
//
//	}
	

	
	@Test
	void getMeasureTemperatureTest() throws IllegalArgumentException, UnsupportedAdapter, InterruptedException, ExecutionException {

		Where w = new Where("scs://12345@localhost:1234/2");
		
		try(DeviceFactory df = new DeviceFactory()) {
			HeatingZone hz = df.createDevice(HeatingZone.class, w).get();
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> hz.getMeasureTemperature()!=null);
			
			DoubleState m = hz.getMeasureTemperature();
			assertNotNull(m);
			assertEquals(20, m.getDoubleValue(), 0);
		}
	}

//	private ValvesStatus v = null;
//	
//	@Test
//	public void getValvesStatusTest() {
//		HeatingZone hz = s.createController(HeatingZone.class, "10");
//		getValvesStatus(hz);
//		s.onDestroy();
//	}
//	
//	public void getValvesStatus(HeatingZone hz) {
//		System.out.println("getValvesStatus");
//		DimensionStatusCallback<ValvesStatus> callback = new DimensionStatusCallback<ValvesStatus>() {
//			public void value(ValvesStatus value) {
//				v=value;
//				synchronized (lock) {
//					System.out.println("Unlock valve status...");
//					lock.notify();
//				}
//			}
//		};
//		
//		hz.getValvesStatus(callback);
//		System.out.println("Wait valve status...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		Assert.assertNotNull(v);
//		Assert.assertEquals(v.getHeatingValveStatus(), ValveStatusEnum.OFF);
//	}
	
	@Test
	void getSetOffsetTest() throws IllegalArgumentException, UnsupportedAdapter, InterruptedException, ExecutionException {
		Where w = new Where("scs://12345@localhost:1234/3");
		
		try (DeviceFactory df = new DeviceFactory()) {
			HeatingZone hz = df.createDevice(HeatingZone.class, w).get();
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> hz.getOffset()!=null);
			
			OffsetState offset = hz.getOffset();
			assertNotNull(offset);
			assertEquals(2, offset.getDegree());
			assertEquals(Mode.ON, offset.getMode());
			
		}
		
	}
}
