package com.domosnap.engine.adapter.openwebnet.device;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import com.domosnap.engine.device.ShutterGeneric;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;

class OpenWebNetShutterTest {

	private ShutterGeneric at = new ShutterGeneric();
	
	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
	
	@Test
	public void statusUpDownTest() throws IllegalArgumentException, UnsupportedAdapter {
		at.statusUpDownTest(new Where("scs://12345@localhost:1234/33"));
	}

}
