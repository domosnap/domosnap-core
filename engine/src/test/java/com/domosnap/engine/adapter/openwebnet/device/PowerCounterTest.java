package com.domosnap.engine.adapter.openwebnet.device;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.concurrent.TimeUnit;

import com.domosnap.engine.device.DeviceHelper;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import com.domosnap.engine.device.counter.PowerCounter;
import com.domosnap.engine.device.counter.PowerCounter.CounterStateName;
import com.domosnap.engine.device.what.impl.WattState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

class PowerCounterTest {
	
	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
	
	@Test
	void getActivePowerTest() throws IllegalArgumentException, UnsupportedAdapter {

		Where w = new Where("scs://12345@localhost:1234/5101");
		
		try(DeviceFactory df = new DeviceFactory()) {

			PowerCounter pc = df.createDevice(PowerCounter.class, w).get();
			pc.addControllerChangeListener((controller, oldStatus, newStatus) -> {
					// When response from server is here we unlock the thread
				if (CounterStateName.CURRENT_DAY.name().equals(newStatus.getName())) {
					System.out.println("Unlock...");
				}
			});
			
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> pc.getActivePower()!=null);
			// Wait the response from the server
			WattState a = pc.getActivePower();
			assertNotNull(a);
			// assertEquals(1362, d.getValue(), 0);
			DeviceHelper dh = new DeviceHelper(pc);
			assertNotNull(dh.getValue(CounterStateName.TOTAL.name()));
			assertNotNull(dh.getValue(CounterStateName.CURRENT_MONTH.name()));
			assertNotNull(dh.getValue(CounterStateName.CURRENT_DAY.name()));
			
		}
	}	
}
