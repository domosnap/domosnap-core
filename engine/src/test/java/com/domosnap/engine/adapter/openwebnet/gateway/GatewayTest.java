package com.domosnap.engine.adapter.openwebnet.gateway;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.adapter.openwebnet.OpenWebNetAdapter;
import com.domosnap.engine.adapter.openwebnet.connector.OpenWebNetTools;
import com.domosnap.engine.gateway.GatewayTestGeneric;

class GatewayTest  {

	@BeforeAll
	public static void assume() {
		assumeTrue(OpenWebNetTools.is1234Open());
	}
		
	@Test
	void testOpenWebNetAdapter() throws InterruptedException {
		GatewayTestGeneric t = new  GatewayTestGeneric();
		IAdapter adapter =  new OpenWebNetAdapter("scs://12345@localhost:1234");
		t.test(adapter);
		t.testConnection(adapter);
		t.testSendCommand(adapter, "scs://12345@localhost:1234/12");
	}
}