package com.domosnap.engine.device;

import com.domosnap.engine.device.what.State;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

public class DeviceTestTools {


    public static void sleep() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            fail(e);
        }
    }

    // Get a State from dvice. If state is null we retry 10 times all 100ms.
    public static void getState(Device device, String name) {

        DeviceHelper dh = new DeviceHelper(device);

        State<?> value = dh.get(name);

        if (value == null) { // If value = null I try to refresh
            Future<State<?>> result = Executors.newSingleThreadExecutor().submit(() -> {

                int i = 0;
                while (i < 10) { // after 10 try, I will return value (null if no state)
                    try {
                        i++;
                        if (device.get(name) != null) {
                            break;
                        }
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        fail(e);
                    }
                }

                return device.get(name);
            });

            try {
                value = result.get(); // TODO here we block thread in waiting result = pb since freez on android....
                // Maybe we need not to wait... or we need to ask only when we are connected = no information about gateway here

            } catch (InterruptedException | ExecutionException e) {
                fail(e);
            }
        }
    }
}
