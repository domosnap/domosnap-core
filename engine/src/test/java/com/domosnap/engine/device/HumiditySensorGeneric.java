package com.domosnap.engine.device;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

import com.domosnap.engine.device.humidity.HumiditySensor;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

public class HumiditySensorGeneric {

	
	public void statusHumidityTest(Where w) throws IllegalArgumentException, UnsupportedAdapter {
		try (DeviceFactory df = new DeviceFactory()) {
			
			final HumiditySensor humiditySensor = df.createDevice(HumiditySensor.class, w).get();
			
	
			// First we just wait 1 second to be sure the controller is initialize 
			Awaitility.waitAtMost(2, TimeUnit.SECONDS).until(() -> humiditySensor.getHumidity() != null);
	
			assertNotNull(humiditySensor.getHumidity());
			assertTrue(0 < humiditySensor.getHumidity().getValue());
	
			System.out.println("Finish...");
		}
	}
}
