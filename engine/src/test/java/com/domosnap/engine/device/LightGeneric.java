package com.domosnap.engine.device;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;
public class LightGeneric {

	
	private boolean isStateChanged = false;

	public void statusOnOffTest(Where w) throws IllegalArgumentException, UnsupportedAdapter {

		try (DeviceFactory df = new DeviceFactory()) {
			final Light light = df.createDevice(Light.class, w).get();
			
			// Listener will make us availabe to wait response from server
			light.addControllerChangeListener((controller, oldStatus, newStatus) -> {
				// When response from server is here we unlock the thread
				System.out.println("Unlock...");
				isStateChanged = true;
				}
			);

			Awaitility.waitAtMost(2, TimeUnit.SECONDS).until(() -> light.getStatus() != null);
			
			// By default simulation server send back a ON status (light 12 only). If value == null, it is a bug or just server have not enough time (1 second) to respond
			assertNotNull(light.getStatus());
			assertEquals(OnOffState.On().getValue() , light.getStatus().getValue());
			assertTrue( light.isReachable().getValue());
	
			// Now set the value to OFF
			light.setStatus(OnOffState.Off());
			// Wait the response from the server
			Awaitility.waitAtMost(10, TimeUnit.SECONDS).until(() -> isStateChanged);
			
			// Check that after the server response now the status is OFF
			assertNotNull(light.getStatus());
			assertEquals(OnOffState.Off().getValue() , light.getStatus().getValue());
			assertTrue( light.isReachable().getValue());
	
			// Switch on again
			isStateChanged = false;
			light.setStatus(OnOffState.On());
			Awaitility.waitAtMost(10, TimeUnit.SECONDS).until(() -> isStateChanged);
			
			assertNotNull(light.getStatus());
			assertEquals(OnOffState.On().getValue() , light.getStatus().getValue());
			assertTrue(light.isReachable().getValue());
			
			System.out.println("Finish...");
		}
	}
}
