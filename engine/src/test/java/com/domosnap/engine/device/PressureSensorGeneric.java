package com.domosnap.engine.device;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

import com.domosnap.engine.device.pressure.PressureSensor;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

public class PressureSensorGeneric {

	public void statusPressureTest(Where w) throws IllegalArgumentException, UnsupportedAdapter {

		try (DeviceFactory df = new DeviceFactory()) {
			final PressureSensor pressureSensor = df.createDevice(PressureSensor.class, w).get();
	
	
			// First we just wait 1 second to be sure the controller is initialize
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> pressureSensor.getPressure()!=null);
	
			assertNotNull(pressureSensor.getPressure());
			assertTrue(0 < pressureSensor.getPressure().getValue());
	
			System.out.println("Finish...");
		}
	}
}
