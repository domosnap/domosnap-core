package com.domosnap.engine.device;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

import com.domosnap.engine.device.shutter.Shutter;
import com.domosnap.engine.device.what.impl.UpDownState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

public class ShutterGeneric {

	private boolean isStateChanged = false;

	public void statusUpDownTest(Where w) throws IllegalArgumentException, UnsupportedAdapter {
		try (DeviceFactory df = new DeviceFactory()) {
			
			final Shutter automation = df.createDevice(Shutter.class, w).get();
	
			// Listener will make us availabe to wait response from server
			automation.addControllerChangeListener((controller, oldStatus, newStatus) -> {
				// When response from server is here we unlock the thread
				System.out.println("Unlock...");
				isStateChanged = true;
				}
			);
	
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> automation.getStatus()!=null);
			
			// By default server send back a OFF status. If value == null, it is a bug or
			// just server have not enough time (1 second) to respond
			assertNotNull(automation.getStatus());
			assertEquals(UpDownState.Stop, automation.getStatus());
	
			// Now set the value to AUTOMATION_DOWN
			isStateChanged = false;
			automation.setStatus(UpDownState.Down);
			System.out.println("Wait...");
	
			// Wait the response from the server
			Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> isStateChanged);
	
			// Check that after the server response now the status is ON
			assertNotNull(automation.getStatus());
			assertEquals(UpDownState.Down, automation.getStatus());
	
			// Switch UP now again
			isStateChanged = false;
			automation.setStatus(UpDownState.Up);
			System.out.println("Wait...");
	
			Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> isStateChanged);
	
			assertNotNull(automation.getStatus());
			assertEquals(UpDownState.Up, automation.getStatus());
	
			// Switch OFF now again
			isStateChanged = false;
			automation.setStatus(UpDownState.Stop);
			System.out.println("Wait...");
	
			Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> isStateChanged);
	
			assertNotNull(automation.getStatus());
			assertEquals(UpDownState.Stop, automation.getStatus());
	
			System.out.println("Finish...");
		}
	}
}
