package com.domosnap.engine.device;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

import com.domosnap.engine.device.temperature.TemperatureSensor;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

public class TemperatureSensorGeneric {

	public void statusTest(Where w) throws IllegalArgumentException, UnsupportedAdapter {
		try (DeviceFactory df = new DeviceFactory()) {
		
			final TemperatureSensor temperatureSensor = df.createDevice(TemperatureSensor.class, w).get();
			
			// First we just wait 1 second to be sure the controller is initialize 
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> temperatureSensor.getTemperature()!=null);
	
			// By default simulation server send back a ON status (light 12 only). If value == null, it is a bug or just server have not enough time (1 second) to respond
			assertNotNull(temperatureSensor.getTemperature());
			assertTrue(0 < temperatureSensor.getTemperature().getValue());
	
			System.out.println("Finish...");
		}
	}
}
