package com.domosnap.engine.device.serialize;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
import com.domosnap.engine.adapter.moc.core.MocTools;
import com.domosnap.engine.device.DeviceJsonCodec;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

class DeviceJsonSerializeTest {

	private final static String TITLE = "title";
	private final static String DESCRIPTION = "title";
	
	@BeforeAll
	public static void init() {
		MocTools.registerMocAdapter();
	}
	
	@Test
	void jsonTest() {
		
		try (DeviceFactory open = new DeviceFactory()) {
			Light l = open.createDevice(Light.class, new Where(FullAdapterMoc.SCHEME + "://localhost:1234/12")).get();

			l.setTitle(TITLE);
			l.setDescription(DESCRIPTION);
			String where = l.getWhere().toString();
					
			String json = DeviceJsonCodec.toJson(l);
			
			l.setTitle("rerewrwe");
			
			DeviceJsonCodec.fromJson(l, json);
			
			assertEquals(TITLE, l.getTitle());
			assertEquals(DESCRIPTION, l.getDescription());
			assertEquals(where, l.getWhere().toString());

		} catch (IllegalArgumentException | UnsupportedAdapter e) {
			fail(e.getMessage());
		}
	}
}
