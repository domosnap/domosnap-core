package com.domosnap.engine.device.serialize;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.DeviceMappingRegistry;
import com.domosnap.engine.device.light.Light;

class DeviceNameMappingRegistryTest {

	@Test
	void scanTest() {
//		new Light(null, null);
		assertEquals("Light", DeviceMappingRegistry.getMapping(Light.class));
		assertEquals(Light.class, DeviceMappingRegistry.getMapping("Light"));
		
		new FakeDevice(null, null, null);
		assertEquals(FakeDevice.class, DeviceMappingRegistry.getMapping("FakeDevice"));
		assertEquals("FakeDevice", DeviceMappingRegistry.getMapping(FakeDevice.class));		
	}
}
