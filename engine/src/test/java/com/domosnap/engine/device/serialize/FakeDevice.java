package com.domosnap.engine.device.serialize;

import java.util.List;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.device.what.StateName;
import com.domosnap.engine.gateway.Command;
import com.domosnap.engine.gateway.Event;

import com.domosnap.engine.gateway.Query;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.PublishProcessor;

public class FakeDevice extends Device {

	protected FakeDevice(Flowable<Event> ms, PublishProcessor<Command> cs, PublishProcessor<Query> q) {
		super(ms, cs, q);
	}

	@Override
	public List<StateName> getStateList() {
		return null;
	}

}
