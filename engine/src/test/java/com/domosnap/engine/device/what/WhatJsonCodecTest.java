package com.domosnap.engine.device.what;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.domosnap.engine.device.what.impl.*;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

class WhatJsonCodecTest {

	@Test
	void jsonNullSerialization() {
		What w = new What("test", null);
		String json = WhatJsonCodec.toJSon(w);
		List<What> w2 = WhatJsonCodec.fromJson(json);
		System.out.println(json);
		assertEquals(1, w2.size());
		assertEquals(w.getName(), w2.get(0).getName());
		assertEquals(w.getValue(), w2.get(0).getValue());
	}

	@Test
	void jsonBooleanSerialization() {
		What w = new What("test", BooleanState.FALSE);
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonDateSerialization() {
		What w = new What("test", new DateState(new Date()));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonDoubleSerialization() {
		What w = new What("test", new DoubleState(10.3));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());

		w = new What("test", new DoubleState(20.0));
		json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonIntegerSerialization() {
		What w = new What("test", new IntegerState(10));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonIpAddressSerialization() {
		What w = new What("test", new IpAddressState("192.168.1.1"));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonMacAddressSerialization() {
		What w = new What("test", new MacAddressState("aa.bb.cc.dd.ee.ff"));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonOnOffSerialization() {
		What w = new What("test", OnOffState.Off());
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonPercentageSerialization() {
		What w = new What("test", new PercentageState(10.0));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonPressureUnitSerialization() {
		What w = new What("test", PressureUnitState.pascal());
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonRGBSerialization() {
		What w = new What("test", new RGBState("#FFF"));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());


		w = new What("test", new RGBState("#0F0F0F"));
		json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());

		w = new What("test", new RGBState("F0F0F0"));
		json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonStringSerialization() {
		What w = new What("test", new StringState("test"));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonTemperatureSerialization() {
		What w = new What("test", TemperatureUnitState.celsius());
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());

		w = new What("test2", TemperatureUnitState.fahreinheit());
		json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonTimeSerialization() {
		What w = new What("test", new TimeState(10,20,35, TimeZone.getDefault()));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonUpDownSerialization() {
		What w = new What("test", UpDownState.Down);
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonVersionSerialization() {
		What w = new What("test", new VersionState(10,5, 1000));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}

	@Test
	void jsonWattSerialization() {
		What w = new What("test", new WattState(1000));
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		What w2 = WhatJsonCodec.oneFromJson(json);
		assertEquals(w.getName(), w2.getName());
		assertEquals(w.getValue(), w2.getValue());
	}
}
