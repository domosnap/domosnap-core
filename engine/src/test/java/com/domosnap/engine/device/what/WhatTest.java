package com.domosnap.engine.device.what;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.what.impl.OnOffState;

class WhatTest {

	@Test
	void what() {
		What w = new What("test", OnOffState.On());
		assertEquals(WhatJsonCodec.toJSon(w), w.toString());
	}
}
