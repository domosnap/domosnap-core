package com.domosnap.engine.gateway;


import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.where.Where;

import static org.junit.jupiter.api.Assertions.*;

class CommandJsonCodecTest {

	
	@Test
	void jsonTest() {
		
		Command c = new Command(Light.class, new Where("protocle://localhost/adress"), new What("boolean",BooleanState.TRUE));
		String json = CommandQueryJsonCodec.toJSon(c);
		System.out.println(json);
		Command c2 = CommandQueryJsonCodec.fromJson(json);
		
		assertEquals("protocle://localhost/adress", c2.getWhere().toString());
		assertEquals("boolean", c2.getWhatList().get(0).getName());
		assertEquals(1, c2.getWhatList().size());
		assertEquals(Light.class, c2.getWho());
		assertFalse(c2 instanceof Query);
		assertEquals(c.timestamp, c2.timestamp);
		assertEquals("null", CommandQueryJsonCodec.toJSon(null));
	}
}
