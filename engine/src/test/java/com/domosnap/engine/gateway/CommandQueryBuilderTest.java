//package com.domosnap.engine.gateway;
//
//import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
//import com.domosnap.engine.adapter.moc.core.MocTools;
//import com.domosnap.engine.device.light.Light;
//import com.domosnap.engine.device.what.What;
//import com.domosnap.engine.device.what.impl.OnOffState;
//import com.domosnap.engine.device.where.Where;
//import org.awaitility.Awaitility;
//import org.junit.jupiter.api.Test;
//
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//class CommandQueryBuilderTest {
//
//	private boolean commandEventReceived = false;
//	private Class<?> commandWho;
//	private Where commandWhere;
//	private int commandNumberOfStatus;
//
//	@Test
//	void commandBuilderTest() throws IllegalArgumentException, UnsupportedAdapter, InterruptedException, ExecutionException {
//
//		MocTools.registerMocAdapter();
//		GatewayBuilder gb = new GatewayBuilder();
//		Gateway g = gb.build(FullAdapterMoc.SCHEME + "://localhost:1234");
//		g.connect().get();
//		g.getMonitor().subscribe(event -> {commandEventReceived = true; commandWho = event.getWho(); commandWhere = event.getWhere(); commandNumberOfStatus = event.getWhatList().size();});
//		g.sendCommand(
//				CommandBuilder.create(Light.class)
//				.setWhere(new Where(FullAdapterMoc.SCHEME + "://localhost:1234/12"))
//				.addWhat(new What(Light.LightStateName.STATUS.name(), OnOffState.Off()))
//				.build()
//		);
//		Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> commandEventReceived);
//
//		g.disconnect();
//		assertTrue(commandEventReceived);
//		assertEquals(FullAdapterMoc.SCHEME + "://localhost:1234/12", commandWhere.toString());
//		assertEquals(Light.class, commandWho);
//		assertEquals(1, commandNumberOfStatus);
//
//	}
//
//	private boolean queryEventReceived = false;
//	private Class<?> queryWho;
//	private Where queryWhere;
//	private int queryNumberOfStatus;
//
//	@Test
//	void queryBuilderTest() throws IllegalArgumentException, UnsupportedAdapter, InterruptedException, ExecutionException {
//
//		MocTools.registerMocAdapter();
//		GatewayBuilder gb = new GatewayBuilder();
//		Gateway g = gb.build(FullAdapterMoc.SCHEME + "://localhost:1234");
//		g.connect().get();
//		g.getMonitor().subscribe(event -> {queryEventReceived = true; queryWho = event.getWho(); queryWhere = event.getWhere(); queryNumberOfStatus = event.getWhatList().size();});
//		g.sendCommand(
//				QueryBuilder.create(Light.class)
//						.setWhere(new Where(FullAdapterMoc.SCHEME + "://localhost:1234/12"))
//						.addWhatName(Light.LightStateName.STATUS)
//						.addWhatName(Light.LightStateName.REACHABLE)
//						.build()
//		);
//		Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> queryEventReceived);
//
//		g.disconnect();
//		assertTrue(queryEventReceived);
//		assertEquals(FullAdapterMoc.SCHEME + "://localhost:1234/12", queryWhere.toString());
//		assertEquals(Light.class, queryWho);
//		assertEquals(2, queryNumberOfStatus);
//	}
//}
