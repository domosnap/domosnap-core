package com.domosnap.engine.gateway;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.what.impl.DateState;
import com.domosnap.engine.device.what.impl.DoubleState;
import com.domosnap.engine.device.what.impl.IntegerState;
import com.domosnap.engine.device.what.impl.IpAddressState;
import com.domosnap.engine.device.what.impl.MacAddressState;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.what.impl.PercentageState;
import com.domosnap.engine.device.what.impl.PressureUnitState;
import com.domosnap.engine.device.what.impl.RGBState;
import com.domosnap.engine.device.what.impl.StringState;
import com.domosnap.engine.device.what.impl.TemperatureUnitState;
import com.domosnap.engine.device.what.impl.TimeState;
import com.domosnap.engine.device.what.impl.UpDownState;
import com.domosnap.engine.device.what.impl.VersionState;
import com.domosnap.engine.device.where.Where;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {

	
	@Test
	void commandTest() throws UnknownHostException {
		Command c = new Command(Light.class, new Where("protocle://localhost/adress"), new What("boolean",BooleanState.TRUE));
		
		assertEquals("protocle://localhost/adress", c.getWhere().toString());
		assertEquals("boolean", c.getWhatList().get(0).getName());
		assertEquals(Light.class, c.getWho());
		assertNotNull(c.timestamp);
		
		
		List<String> l = new ArrayList<>();
		l.add("statename");
		
		c = new Query(Light.class, new Where("protocle://localhost/adress"), l);
		
		assertEquals("protocle://localhost/adress", c.getWhere().toString());
		assertEquals("statename", c.getWhatList().get(0).getName());
		assertEquals(Light.class, c.getWho());
		assertNotNull(c.timestamp);
	}
	
	@Test
	void JsonTest() {
		List<What> l = new ArrayList<>();
		
		l.add(new What("boolean",BooleanState.TRUE));
		Command c = new Command(Light.class, new Where("protocle://localhost/adress"), l);
		String json = CommandQueryJsonCodec.toJSon(c);
		System.out.println(json);
		Command e2 = CommandQueryJsonCodec.fromJson(json);
		
		assertFalse(e2 instanceof Query);
		assertEquals("protocle://localhost/adress", e2.getWhere().toString());
		assertEquals("boolean", e2.getWhatList().get(0).getName());
		
		
		c = new Query(Light.class, new Where("protocle://localhost/adress"), "boolean");
		json = CommandQueryJsonCodec.toJSon(c);
		System.out.println(json);
		e2 = CommandQueryJsonCodec.fromJson(json);
		
		assertTrue(e2 instanceof Query);
		assertEquals("protocle://localhost/adress", e2.getWhere().toString());
		assertEquals("boolean", e2.getWhatList().get(0).getName());
	}
	
	@Test
	void JsonTest2() {
		List<What> l = new ArrayList<>();
		
		DateState dt = new DateState(new Date());
		DoubleState ds = new DoubleState(10);
		IntegerState is = new IntegerState(10);
		IpAddressState ips = new IpAddressState("192.168.1.1");
		MacAddressState mcs = new MacAddressState("FA.19.19.19.19.19");
		PercentageState ps = new PercentageState(50d);
		PressureUnitState pus = PressureUnitState.pascal();
		RGBState rgbs = new RGBState("FFFFFF");
		StringState ss = new StringState("String");
		TemperatureUnitState tus = TemperatureUnitState.celsius();
		TimeState ts = new TimeState(12,12,12, TimeZone.getDefault());
		VersionState vs = new VersionState(1,1,1);
		
		l.add(new What("boolean",BooleanState.TRUE));
		l.add(new What("date",dt));
		l.add(new What("double",ds));
		l.add(new What("integer",is));
		l.add(new What("ipaddress",ips));
		l.add(new What("macaddress",mcs));
		l.add(new What("onoff",OnOffState.On()));
		l.add(new What("percent",ps));
		l.add(new What("pressureunit",pus));
		l.add(new What("rgb",rgbs));
		l.add(new What("string",ss));
		l.add(new What("temperatureunit",tus));
		l.add(new What("time",ts));
		l.add(new What("updown",UpDownState.Stop));
		l.add(new What("version",vs));
		
		Command c = new Command(Light.class, new Where("protocle://localhost/adress"), l);
		String json = CommandQueryJsonCodec.toJSon(c);
		System.out.println(json);
		Command c2 = CommandQueryJsonCodec.fromJson(json);
		
		assertEquals(Light.class, c2.getWho());
		assertEquals(new Where("protocle://localhost/adress"), c2.getWhere());
		assertFalse(c2 instanceof Query);
		assertEquals(BooleanState.TRUE, c2.getWhatList().get(0).getValue());
		System.out.println(dt.getValue().getTime());
		System.out.println(((DateState) c2.getWhatList().get(1).getValue()).getValue().getTime());
		assertEquals(dt, c2.getWhatList().get(1).getValue());
		assertEquals(ds, c2.getWhatList().get(2).getValue());
		assertEquals(is, c2.getWhatList().get(3).getValue());
		assertEquals(ips, c2.getWhatList().get(4).getValue());
		assertEquals(mcs, c2.getWhatList().get(5).getValue());
		assertEquals(OnOffState.On(), c2.getWhatList().get(6).getValue());
		assertEquals(ps, c2.getWhatList().get(7).getValue());
		assertEquals(pus, c2.getWhatList().get(8).getValue());
		assertEquals(rgbs, c2.getWhatList().get(9).getValue());
		assertEquals(ss, c2.getWhatList().get(10).getValue());
		assertEquals(tus, c2.getWhatList().get(11).getValue());
		assertEquals(ts, c2.getWhatList().get(12).getValue());
		assertEquals(UpDownState.Stop, c2.getWhatList().get(13).getValue());
		assertEquals(vs, c2.getWhatList().get(14).getValue());
	}
}
