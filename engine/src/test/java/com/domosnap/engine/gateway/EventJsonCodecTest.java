package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.where.Where;

class EventJsonCodecTest {

	
	@Test
	void jsonTest() {
		
		assertEquals("", EventJsonCodec.toJSon(null));
		
		Event e = new Event(Light.class, new Where("protocle://localhost/adress"), new What("boolean",BooleanState.TRUE));
		String json = EventJsonCodec.toJSon(e);
		System.out.println(json);
		Event e2 = EventJsonCodec.fromJson(json);
		assertEquals("protocle://localhost/adress", e2.getWhere().toString());
		assertEquals("boolean", e2.getWhatList().get(0).getName());
		assertEquals(1, e2.getWhatList().size());
		assertEquals(Light.class, e2.getWho());
		assertEquals(e.timestamp, e2.timestamp);
	}
}
