package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.BooleanState;
import com.domosnap.engine.device.where.Where;

class EventTest {

	
	@Test
	void eventTest() throws UnknownHostException {
		Event e = new Event(Light.class, new Where("protocle://localhost/adress"), new What("boolean",BooleanState.TRUE));
		
		assertEquals("protocle://localhost/adress", e.getWhere().toString());
		assertEquals("boolean", e.getWhatList().get(0).getName());
		assertEquals(Light.class, e.getWho());
		assertNotNull(e.timestamp);
		
		
		List<What> l = new ArrayList<>();
		l.add(new What("boolean",BooleanState.TRUE));
		
		e = new Event(Light.class, new Where("protocle://localhost/adress"), l);
		
		assertEquals("protocle://localhost/adress", e.getWhere().toString());
		assertEquals("boolean", e.getWhatList().get(0).getName());
		assertEquals(Light.class, e.getWho());
		assertNotNull(e.timestamp);
	}
}
