package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;

class GatewayBuilderTest {

	
	@Test void testRegister() {
		try {
			GatewayBuilder.registerProtocole("A*", FullAdapterMoc.class);
			fail("GatewayBuilder must reject malformed schema.");
		} catch (UnsupportedScheme e) {
			// A malformed schema must raise an exception
			assertTrue(true);
		}
		
		try {
			GatewayBuilder.registerProtocole(FullAdapterMoc.SCHEME, FullAdapterMoc.class);
		} catch (UnsupportedScheme e) {
			fail("GatewayBuilder must support a correct schema.");
		}
	}
	
	@Test
	void testBuilderWithWrongParameter() {
		
		GatewayBuilder gb = new GatewayBuilder();
		
		// Null string
		try {
			String s = null;
			gb.build(s);
			fail("GatewayBuilder should raise an IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (UnsupportedAdapter e) {
			fail("GatewayBuilder should raise an IllegalArgumentException");
		}
		
		// Wrong uri
		try {
			gb.build("ssss*://");
			fail("GatewayBuilder should raise an IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		} catch (UnsupportedAdapter e) {
			fail("GatewayBuilder should raise an IllegalArgumentException");
		}
		
		// Good uri but unknown adapter
		try {
			gb.build("ssss://localhost:80/");
			fail("GatewayBuilder should raise an UnsupportedAdapter");
		} catch (IllegalArgumentException e) {
			fail("GatewayBuilder should raise an UnsupportedAdapter");
		} catch (UnsupportedAdapter e) {
			assertTrue(true);
		}
	}
}
