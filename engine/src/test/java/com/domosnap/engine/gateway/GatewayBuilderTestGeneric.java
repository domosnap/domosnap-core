package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;

public class GatewayBuilderTestGeneric {

	public void testBuilder(String scheme, Class<? extends IAdapter> adapterClass) {
		
		try {
			GatewayBuilder.registerProtocole(scheme, adapterClass);
			GatewayBuilder gb = new GatewayBuilder();
			Gateway gateway = gb.build(FullAdapterMoc.SCHEME + "://localhost");
			assertNotNull(gateway);
			assertEquals(FullAdapterMoc.SCHEME, gateway.getScheme());
			assertNotNull(gateway.getCommander());
			assertNotNull(gateway.getMonitor());
		} catch (IllegalArgumentException e) {
			fail("GatewayBuild must support a registered adapter.");
		} catch (UnsupportedAdapter e) {
			fail("GatewayBuild must support a registered adapter.");
		} catch (UnsupportedScheme e) {
			fail("GatewayBuild must support the adapter schema...");
		}
		
	}
}
