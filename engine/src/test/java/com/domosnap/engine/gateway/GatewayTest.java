package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class GatewayTest {

	@Test
	void noAdapter() {
		try {
			new Gateway(null);
			fail("Impossible to instanciate a gateway without an adaptater.");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}
	}
}