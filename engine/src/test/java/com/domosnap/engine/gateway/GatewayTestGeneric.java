package com.domosnap.engine.gateway;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.Log;
import io.reactivex.rxjava3.disposables.Disposable;
import org.awaitility.Awaitility;

import com.domosnap.engine.adapter.IAdapter;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;


public class GatewayTestGeneric {

	boolean commanderConnection = false;
	boolean monitorConnection = false;
	boolean receivedEvent = false;
	
	public void test(IAdapter adapter) {
		Gateway g = new Gateway(adapter);
		// Scheme
		assertEquals(adapter.getURI().getScheme(), g.getScheme());
		
		// Supported Device
		assertNotEquals(0, g.getSupportedDevice().size());


		OnGatewayConnectionListener clcommander = new OnGatewayConnectionListener() {
			
			@Override
			public void onConnect(Log.Session session, ConnectionStatusEnum connectionStatus) {
				if (Log.Session.COMMAND.equals(session)) {
					commanderConnection = true;
				}
			}
			
			@Override
			public void onDisconnect(Log.Session session, DisconnectionStatusEnum disconnectionStatus) {
				if (Log.Session.COMMAND.equals(session)) {
					commanderConnection = false;
				}
			}
		};


		OnGatewayConnectionListener clmonitor = new OnGatewayConnectionListener() {
			
			@Override
			public void onConnect(Log.Session session, ConnectionStatusEnum connectionStatus) {
				if (Log.Session.MONITOR.equals(session)) {
					monitorConnection = true;
				}
			}
			
			@Override
			public void onDisconnect(Log.Session session, DisconnectionStatusEnum disconnectionStatus) {
				if (Log.Session.MONITOR.equals(session)) {
					monitorConnection = false;
				}
			}
		};
		
		g.addConnectionListener(clcommander);
		g.addConnectionListener(clmonitor);

		assertNotNull(g.getCommander()); 
		assertNotNull(g.getMonitor());

		g.disconnect();

		assertFalse(commanderConnection);
		assertFalse(monitorConnection);
		
		try {
			assertTrue(g.connect().get());
		} catch (InterruptedException | ExecutionException e) {
			fail("Connection problem");
		}
		
		assertTrue(commanderConnection);
		assertTrue(monitorConnection);
		
	
		g.getMonitor().subscribe(event -> receivedEvent = true);
		
		g.getCommander().offer(new Command(Light.class, new Where("12"), new What("STATUS", OnOffState.On())));
	
		
		// Wait the response from the server
		Awaitility.await().atMost(10, TimeUnit.SECONDS).until(() -> receivedEvent);
		
		
		assertTrue(receivedEvent);
		
		g.disconnect();
		assertFalse(commanderConnection);
		assertFalse(monitorConnection);
		
		g.removeConnectionListener(clcommander);
		g.removeConnectionListener(clmonitor);
	}
	
	
	private boolean connectionOKMonitor = false;
	private boolean connectionOKCommander = false;
	
	public void testConnection(IAdapter adapter) throws InterruptedException{
		Gateway g = new Gateway(adapter);
		g.addConnectionListener(new OnGatewayConnectionListener() {
			
			@Override
			public void onConnect(Log.Session session, ConnectionStatusEnum connectionStatus) {
				if (Log.Session.MONITOR.equals(session)) {
					connectionOKMonitor = true;
				}
			}
			
			@Override
			public void onDisconnect(Log.Session session, DisconnectionStatusEnum disconnectionStatus) {
				if (Log.Session.MONITOR.equals(session)) {
					if (!connectionOKMonitor) {
						fail("receive on close but never call close on the connection and never start !!!!");
					}
					connectionOKMonitor = false;
				}
			}
		});
		
		g.addConnectionListener(new OnGatewayConnectionListener() {
			
			@Override
			public void onConnect(Log.Session session, ConnectionStatusEnum connectionStatus) {
				if (Log.Session.COMMAND.equals(session)) {
					connectionOKCommander = true;
				}
			}
			
			@Override
			public void onDisconnect(Log.Session session, DisconnectionStatusEnum disconnectionStatus) {
				if (Log.Session.COMMAND.equals(session)) {
					if (!connectionOKCommander) {
						fail("receive on close but never call close on the connection and never start !!!!");
					}
					connectionOKCommander = false;
				}
			}
		});
		
		//When
		g.connect();
		//Then
		Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> connectionOKCommander && connectionOKMonitor);
		assertTrue(connectionOKMonitor, "La connexion est un echecs");
		assertTrue(connectionOKCommander, "La connexion est un echecs");

		//When
		g.disconnect();
		Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> !(connectionOKCommander || connectionOKMonitor));
		// Then
		Thread.sleep(4000);
		assertFalse(connectionOKMonitor, "La déconnexion est un echecs");
		assertFalse(connectionOKCommander, "La déconnexion est un echecs");
		assertFalse(g.isConnected(), "isConnected ne marche pas");
				
		//When
		g.connect();
		//Then
		Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> connectionOKCommander && connectionOKMonitor);
		assertTrue(connectionOKMonitor, "La connexion est un echecs");
		assertTrue(connectionOKCommander, "La connexion est un echecs");
		assertTrue(g.isConnected(), "isConnected ne marche pas");
	}
	
	
	public void testSendCommand(IAdapter adapter, String lightUri) {
		Gateway g = new Gateway(adapter);
		final Where where = new Where(lightUri);
		
		Disposable d = g.getMonitor().subscribe(event ->  {
			assertEquals(where, event.getWhere());
			assertEquals(OnOffState.On(), event.getWhatList().get(0).getValue());
		});
		//When
		try {
			g.connect().get();
		} catch (InterruptedException | ExecutionException e) {
			fail(e);
		}
		
		g.sendCommand(null);
		g.sendCommand(new Command(Light.class, where, new What(LightStateName.STATUS.name(), OnOffState.On())));
		g.disconnect();
		d.dispose();
	}
}