package com.domosnap.engine.supervisor;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.domosnap.engine.device.DeviceHelper;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.Gateway;
import com.domosnap.engine.gateway.UnsupportedAdapter;

import static org.junit.jupiter.api.Assertions.*;

public class DeviceFactoryTestGeneric {
	
	boolean created = false;
	boolean destroyed = false;
	boolean gatewayCreated = false;
	
	public void createDeviceTest(String uri) {
		Gateway g = null;
		try (DeviceFactory df = new DeviceFactory()) {
			
			OnDeviceCreatedListener listener = d -> created = true; 
			OnGatewayCreatedListener listenerGateway = d -> gatewayCreated = true; 
			OnDeviceDestroyedListener deviceDestroyedListener = d -> destroyed = true;

			df.addOnDeviceCreatedListener(listener)
				.addOnGatewayCreatedListener(listenerGateway)
				.addOnDeviceDestroyedListener(deviceDestroyedListener);



			assertFalse(created);
			assertFalse(gatewayCreated);
			assertFalse(destroyed);
			assertEquals(0, df.getAllGateway().size());
			Light l = df.createDevice(Light.class, new Where(uri)).get();
			assertNotNull(l);
			l.setStatus(OnOffState.Off());
			assertEquals(OnOffState.Off(), new DeviceHelper(l).getValue(Light.LightStateName.STATUS.name()));
			assertTrue(created);
			assertTrue(gatewayCreated);
			assertFalse(destroyed);
			assertEquals(1, df.getAllGateway().size());

			g = df.getGateway(new URI(uri));
			assertTrue(g.isConnected());

			df.removeOnDeviceCreatedListener(listener)
					.removeOnGatewayCreatedListener(listenerGateway)
					.removeOnDeviceDestroyedListener(deviceDestroyedListener);
			
		} catch (Exception e) {
			fail(e);
		}

		assertNotNull(g);
		assertFalse(g.isConnected());
	}

	int i = 0;
	int j = 0;
	public void createDevicesTest(String uri, String uri2) {
		
		try (DeviceFactory df = new DeviceFactory()) {
			
			OnDeviceCreatedListener listener = d -> i++;
			OnDeviceDestroyedListener deviceDestroyedListener = d -> j++;
			
			df.addOnDeviceDestroyedListener(deviceDestroyedListener)
					.addOnDeviceCreatedListener(listener);
			
			assertEquals(0, i);
			List<Optional<Light>> ls = df.createDevices(Light.class, new Where(uri), new Where(uri2));
			assertNotNull(ls);
			Light l1 = ls.get(0).get();
			Light l2 = ls.get(1).get();
			l1.setStatus(OnOffState.Off());
			l2.setStatus(OnOffState.Off());
			assertEquals(OnOffState.Off(), new DeviceHelper(l1).getValue(Light.LightStateName.STATUS.name()));
			assertEquals(OnOffState.Off(),  new DeviceHelper(l2).getValue(Light.LightStateName.STATUS.name()));
			assertEquals(2, i);


			df.destroyDevice(l1);
			df.destroyDevice(l2);
			assertEquals(2, j);
			df.removeOnDeviceDestroyedListener(deviceDestroyedListener)
				.removeOnDeviceCreatedListener(listener);
		} catch (NoSuchElementException | UnsupportedAdapter e) {
			fail();
		}
	}
}