package com.domosnap.engine.supervisor;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.adapter.moc.core.FullAdapterMoc;
import com.domosnap.engine.adapter.moc.core.MocTools;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.gateway.UnsupportedAdapter;

import static org.junit.jupiter.api.Assertions.*;

class DeviceRepositoryGenericTest {
	
	boolean created = false;
	boolean gatewayCreated = false;
	boolean destroyed = false;
	int nbCreated = 0;
	int nbCreatedGateway = 0;
	
	@Test
	public void createDeviceTest() {
		MocTools.registerMocAdapter();
		String uri = FullAdapterMoc.SCHEME + "://localhost:1234/12";
		String uri2 = FullAdapterMoc.SCHEME + "://localhost:1234/13";
		String uri3 = FullAdapterMoc.SCHEME + "://localhost:1234/14";

		try (DeviceRepository dr = new DeviceRepository()) {

			OnDeviceCreatedListener listener = device -> {
				created = true;
				nbCreated++;
			};
			OnGatewayCreatedListener listenerGateway = d -> {
				nbCreatedGateway++;
				gatewayCreated = true;
			};
			OnDeviceDestroyedListener deviceDestroyedListener = d -> destroyed = true;


			dr.addOnDeviceCreatedListener(listener)
					.addOnGatewayCreatedListener(listenerGateway)
					.addOnDeviceDestroyedListener(deviceDestroyedListener)
					.addUnknownDeviceListener(d -> System.out.println("Nothing"));

			assertFalse(created);
			assertEquals(0, nbCreated);
			assertFalse(gatewayCreated);
			assertEquals(0, nbCreatedGateway);
			assertFalse(destroyed);

			Light l = dr.createDevice(Light.class, new Where(uri)).get();
			assertNotNull(l);
			assertTrue(created);
			assertTrue(gatewayCreated);
			assertFalse(destroyed);
			assertEquals(1, nbCreated);
			assertEquals(1, nbCreatedGateway);
			l = dr.createDevice(Light.class, new Where(uri)).get();
			assertNotNull(l);
			assertEquals(1, nbCreated);
			assertEquals(1, nbCreatedGateway);


			List<Optional<Light>> ls = dr.createDevices(Light.class, new Where(uri), new Where(uri2));
			assertEquals(2, nbCreated);
			assertEquals(2, ls.size());
			assertEquals(1, nbCreatedGateway);

			dr.removeOnDeviceCreatedListener(listener);


			Light l3 = dr.createDevice(Light.class, new Where(uri3)).get();
			assertNotNull(l3);
			// test that listener has been well removed!
			assertEquals(2, nbCreated);
			assertEquals(1, nbCreatedGateway);

			assertEquals(3, dr.getAllDevicesMap().size());
			assertEquals(3, dr.getAllDevicesList().size());
			assertEquals(3, dr.getAllDevicesByClass().get(Light.class).size());
			assertEquals(3, dr.getDevicesByClassMap(Light.class).size());


			Light l3bis = (Light) dr.getDevice(new Where(uri3));
			assertEquals(l3, l3bis);
			Light l3ter = dr.getDevice(Light.class, new Where(uri3));
			assertEquals(l3, l3ter);


			dr.close();
			assertEquals(0, dr.getAllDevicesMap().size());
			assertEquals(0, dr.getAllDevicesList().size());
		} catch (UnsupportedAdapter | IllegalArgumentException  e) {
			fail(e);
		}
	}

	private boolean unknown = false;
	@Test
	public void unknown() throws IllegalArgumentException, UnsupportedAdapter, URISyntaxException {
		MocTools.registerMocAdapter();
		String uri = FullAdapterMoc.SCHEME + "://localhost:1234/12";
		
		DeviceRepository dr = new DeviceRepository();
		OnUnknownDeviceListener listener = event -> unknown = true;
		OnDeviceCreatedListener lcreated = event -> System.out.println("Device created");
		dr.addUnknownDeviceListener(listener).addOnDeviceCreatedListener(lcreated);
		
		dr.connectGateway(new URI(uri));
		assertFalse(unknown);
		
		// Send an event which make the dr detect a new device
		try (DeviceFactory df = new DeviceFactory()) {
			Light lf = df.createDevice(Light.class, new Where(uri)).get();
			lf.setStatus(OnOffState.Off());
			Awaitility.await().atMost(2, TimeUnit.SECONDS).until(() -> unknown); // wait unknown become true
			assertTrue(unknown);
		}
		
		unknown = false;
		dr.removeUnknowDeviceListener(listener).removeOnDeviceCreatedListener(lcreated);
		// Send an event which make the dr detect a new device
		try (DeviceFactory df = new DeviceFactory()) {
			Light lf = df.createDevice(Light.class, new Where(uri)).get();
			lf.setStatus(OnOffState.Off());

			assertFalse(unknown);
		}
	}
	
	int nbCreatedForDelete = 0;
	@Test
	public void deleteDeviceTest() {
		MocTools.registerMocAdapter();
		String uri = FullAdapterMoc.SCHEME + "://localhost:1234/12";
		try {
			DeviceRepository dr = new DeviceRepository();
			
			OnDeviceCreatedListener listener = device -> nbCreatedForDelete++;
			dr.addOnDeviceCreatedListener(listener);
			
			assertEquals(0, nbCreatedForDelete);
					
			Where w = new Where(uri);
			Light l = dr.createDevice(Light.class, w).get();
			assertNotNull(l);
			assertEquals(1, nbCreatedForDelete);
			
			dr.deleteDevice(w);
			
			assertNull(dr.getDevice(w));
			assertEquals(0, dr.getAllDevicesMap().size());
			
			Light l2 = dr.createDevice(Light.class, w).get();
			assertNotNull(l2);
			assertEquals(2, nbCreatedForDelete);
			assertEquals(1, dr.getAllDevicesMap().size());
			
			dr.deleteDevice(Light.class, w);
			assertNull(dr.getDevice(w));
			assertEquals(0, dr.getAllDevicesMap().size());

			Light l3 = dr.createDevice(Light.class, w).get();
			assertNotNull(l3);
			assertEquals(3, nbCreatedForDelete);
			assertEquals(1, dr.getAllDevicesMap().size());


			
		} catch (Exception e) {
			fail(e);
		}
	}

	public void connectGatewayTest(){
		try (DeviceRepository dr = new DeviceRepository()) {
			URI uri = new URI(FullAdapterMoc.SCHEME + "://localhost:1234/12");
			gatewayCreated = false;
			OnGatewayCreatedListener listenerGateway = d -> {
				gatewayCreated = true;
			};

			assertTrue(dr.connectGateway(uri).get());

			assertTrue(gatewayCreated);

			assertTrue(dr.getGateway(uri).isConnected());
			dr.disconnectGateway(uri);
			assertFalse(dr.getGateway(uri).isConnected());

		} catch (UnsupportedAdapter | URISyntaxException | ExecutionException | InterruptedException e) {
			fail(e);
		}

	}
}
