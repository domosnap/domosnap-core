package com.domosnap.engine.supervisor;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.device.Device;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import org.junit.jupiter.api.Timeout;

public class DiscoveryDeviceGeneric {
	
	private boolean scanFinish = false; 
	private boolean foundAtLeastOneController = false;
	private boolean scanReallyFinish = false;

	@Timeout(value = 10, unit = TimeUnit.MINUTES)
	public void testScan(URI uri) throws InterruptedException, IllegalArgumentException, UnsupportedAdapter{
		DeviceDiscovery dd = new DeviceDiscovery();
		dd.scan(uri, new OnScanDeviceListener() {
			
			@Override
			public void progess(int percent) {
				if(percent==100) {
					scanFinish = true;
				}
			}
			
			@Override
			public void foundController(Device controller) {
				foundAtLeastOneController = true;
			}

			@Override
			public void scanFinished() {
				scanReallyFinish = true;
			}
		});
		
		Thread.sleep(5000);

		assertTrue(scanFinish, "Scan failed");
		assertTrue(scanReallyFinish, "Scan not finish");
		assertTrue(foundAtLeastOneController, "No controler found during scan");
	}
}
