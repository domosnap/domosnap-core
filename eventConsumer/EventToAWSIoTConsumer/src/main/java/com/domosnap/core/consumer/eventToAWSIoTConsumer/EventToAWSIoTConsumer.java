package com.domosnap.core.consumer.eventToAWSIoTConsumer;

import java.util.Hashtable;
import java.util.Map;

/*
 * #%L
 * DomoSnap Event to AWS IoT Consumer
 * %%
 * Copyright (C) 2019 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.domosnap.core.consumer.eventToAWSIoTConsumer.SampleUtil.KeyStorePasswordPair;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.gateway.Event;

public class EventToAWSIoTConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToAWSIoTConsumer.class.getSimpleName());

	private int errorCount = 0;
	private int retry = 5;

	private AWSIotMqttClient awsIotClient;
	private Map<String, String> mapping = new Hashtable<>();

	
	public void setRetry(int retry) {
		this.retry = retry;
	}
	
	public EventToAWSIoTConsumer(String clientEndpoint, String clientId, String certificateFile, String privateKeyFile) {
		
        if (awsIotClient == null && certificateFile != null && privateKeyFile != null) {
            KeyStorePasswordPair pair = SampleUtil.getKeyStorePasswordPair(certificateFile, privateKeyFile, null);
            awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, pair.keyStore, pair.keyPassword);
        }

        if (awsIotClient == null) {
            throw new IllegalArgumentException("Failed to construct client due to missing certificate or credentials.");
        }
        
        try {
			awsIotClient.connect();
		} catch (AWSIotException e) {
			log.severe(Session.OTHER, "Impossible to connect to AWS;" + e.getMessage() );
		}
	}
	
	
	public EventToAWSIoTConsumer(String clientEndpoint, String clientId, String awsAccessKeyId, String awsSecretAccessKey, String sessionToken) {

//      String awsAccessKeyId = "awsAccessKeyId";
//      String awsSecretAccessKey = "awsSecretAccessKey";
//      String sessionToken = "sessionToken";
		
        if (awsIotClient == null) {
            if (awsAccessKeyId != null && awsSecretAccessKey != null) {
                awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, awsAccessKeyId, awsSecretAccessKey,
                        sessionToken);
            }
        }

        if (awsIotClient == null) {
            throw new IllegalArgumentException("Failed to construct client due to missing credentials.");
        }
        
        try {
			awsIotClient.connect();
		} catch (AWSIotException e) {
			log.severe(Session.OTHER, "Impossible to connect to AWS;" + e.getMessage() );
		}
	}
	
	public void add(String where, String thingId) {
		mapping.put(where, thingId);
	}
	
	
	@Override
	public void accept(final Event event) {

		do {
			try {
				String domosnapId = event.getWhere().toString();
				String awsThingId = mapping.get(domosnapId);
				if (awsThingId != null) {
					awsIotClient.publish(getChannel(event, awsThingId) , getPayLoad(event));
				} else {
					log.info(Session.DEVICE, "No AWS shadow device for [" + domosnapId + "].");
				}
                Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.severe(Session.MONITOR, e.getMessage());
				errorCount++;
			    // Restore interrupted state...
			    Thread.currentThread().interrupt();
			 } catch (Exception e) {
				log.severe(Session.MONITOR, e.getMessage());
				errorCount++;
			}
		} while (errorCount > 0 && errorCount < retry);

	}
	
	private String getChannel(Event event, String id) {
		
		String channel = "/shadow/update"; // "/shadow/update"
		return "$aws/things/"+id+channel;
	}
	
	private String getPayLoad(Event event) {
		
		StringBuilder sb = new StringBuilder("{\"state\": {\"reported\": {");
		for (What what : event.getWhatList()) {
			String key = what.getName();
			String value = what.getValue().toString();
			
			sb.append("\"").append(key).append("\":\"").append(value).append("\",");
		}
		sb.setLength(sb.length()-1);
		String result = sb.append("}}}").toString();
		log.finest(Session.DEVICE, "Event Transformed to AWS format [" + result + "].");
		return result; // "{\"state\": {\"reported\": {\"color\": \"bleu\"}}}";
	}
}
