package com.domosnap.core.consumer.eventToAWSIoTConsumer;

/*
 * #%L
 * DomoSnap Event to AWS IoT Consumer
 * %%
 * Copyright (C) 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.light.Light.LightStateName;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.what.impl.RGBState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.event.EventFactory;
import com.domosnap.engine.gateway.Event;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		EventToAWSIoTConsumer consumer = new EventToAWSIoTConsumer("a2lbelcttbms3j-ats.iot.eu-west-1.amazonaws.com", "sdk-java", "Light2.cert.pem", "Light2.private.key");
		consumer.add("12", "Light2");
		
		EventFactory.addConsumer(consumer);
		
//		EventFactory.setChannel("/shadow/update");
		List<What> stateList = new ArrayList<>();
		stateList.add(new What(LightStateName.STATUS.name(), OnOffState.On()));
		stateList.add(new What(LightStateName.COLOR.name(), new RGBState("#FFFFFD")));
		EventFactory.sendEvent(new Event(Light.class, new Where("12"), stateList));
		
		
//		OpenWebNetControllerAdapter ownca = new OpenWebNetControllerAdapter("");
//		ownca.scan(new ScanListener() {
//			
//			@Override
//			public void scanFinished() {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void progess(int percent) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void foundController(Who who, Where where, Controller controller) {
//				// TODO Test if controller exist in AWS else, declare controller in AWS!
//				consumer.add(where.getPath(), "Light" + where.getPath());
//			}
//		});
		
		
		
	}

}
