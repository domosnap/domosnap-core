package com.domosnap.core.consumer.kafka;

import java.io.Closeable;
import java.io.IOException;

/*
 * #%L
 * DomoSnap Event to Kafka Consumer
 * %%
 * Copyright (C) 2018 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.gateway.Event;
import com.domosnap.engine.gateway.EventJsonCodec;

public class EventToKafkaConsumer implements Consumer<Event>, Closeable {

	private static final Log log = new Log(EventToKafkaConsumer.class.getSimpleName());

	private final KafkaProducer<String, String> producer;
	private String topic;
	
	public EventToKafkaConsumer(String bootStrapServer, String topic) {
	
		bootStrapServer = bootStrapServer == null ? "PLAINTEXT://localhost:9092" : bootStrapServer;
		this.topic =  topic == null ? "domosnap-event-topic" : topic ;
		
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
		props.put(ProducerConfig.ACKS_CONFIG, "all"); // message passé à tous mes broker "0"= udp, "1"= tcp = récupérer mais pas stocké. Parametre min.insync.replica = 2
		props.put(ProducerConfig.RETRIES_CONFIG, "3");
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100"); // value is for one partition!
		props.put(ProducerConfig.LINGER_MS_CONFIG, "500"); // Waiting time to send even if batch_siz not reached
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432"); //memory for all partition! here: 32Mo (dafault)... Knowing that:  130 octet < one message < 150 octet *100 =>
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<>(props);
	}
	
	public EventToKafkaConsumer(Map<String, Object> props) {
		producer = new KafkaProducer<>(props);
	}
	
	@Override
	public void accept(final Event event) {

		ProducerRecord<String, String> myRecord = new ProducerRecord<>(topic, event.getWhere().getURI().toString(), EventJsonCodec.toJSon(event, false));
		producer.send(myRecord, (metadata, exception) -> {
			if (exception != null) {
				log.severe(Session.MONITOR, exception.getMessage());
			} else {
				log.finest(Session.MONITOR, "Event send to Kafka with success [" + event + "]");
			}
		});
	}
	
	@Override
	public void close() throws IOException {
		producer.close();
	}
}
