package com.domosnap.core.consumer.mqtt;

import java.io.Closeable;
import java.io.IOException;

/*
 * #%L
 * DomoSnap Event to MQTT Consumer
 * %%
 * Copyright (C) 2018 - 2023 A. de Giuli
 * %%
 * This file is part of DomoSnap owned by Arnaud de Giuli (arnaud.degiuli(at)gmail.com)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     DomoSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     DomoSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.device.what.What;
import com.domosnap.engine.gateway.Event;

import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;

public class EventToMqttConsumer implements Consumer<Event>, Closeable {

	private static final Log log = new Log(EventToMqttConsumer.class.getSimpleName());

	private MqttClient client;
	private String configMqttTopicWrite = "controller/";

	private String hostname;
	private int port;
	private MqttClientOptions opts;
	private Vertx vertx;
	private boolean connected = false;
	
	public EventToMqttConsumer() {
	}
	
	// mosquitto_sub -h localhost -v -t test_channel -p 11112
	// mosquitto_pub -h localhost -t test_channel -m "Hello Raspberry Pi" -p 11112
	public EventToMqttConsumer(String host, int port, MqttClientOptions opt, Vertx vertx, String mainChannel, boolean autoConnect) {
		this.hostname = host;
		this.port = port;
		this.opts = opt;
		this.vertx = vertx;
		if (mainChannel != null) { this.configMqttTopicWrite = mainChannel; } 
		if (autoConnect) {connect();}
	}

	public EventToMqttConsumer(String host, int port, MqttClientOptions opt, String mainChannel, boolean autoConnect) {
		this(host, port, opt, Vertx.vertx(), mainChannel, autoConnect);
	}
	
	public void connect() {
		client = MqttClient.create(vertx,opts);

		client.connect(port, hostname, s -> {
			client.publishCompletionHandler(id -> 
				  log.finest(Session.SERVER, "Id of just received PUBACK or PUBCOMP packet is " + id)
				);
			connected = true;
			log.finest(Session.SERVER, "MqttClient connected.");
		});
	}
	
	public void disconnect() {
		connected = false;
		client.disconnect();
	}
	
	@Override
	public void accept(final Event event) {

		try {
			if (connected) {
				String where = event.getWhere().getPath();
				String who = event.getWho().getSimpleName();
				String channel = configMqttTopicWrite.concat(who).concat("/").concat(where);
				
				for (What what : event.getWhatList()) {
					log.finest(Session.MONITOR, "Channel [".concat(channel).concat("] - Value: ").concat(what.toString()));
					
					client.publish(channel,
							  Buffer.buffer(what.toString()),
							  MqttQoS.AT_LEAST_ONCE,
							  false,
							  false);	
				}
			} else {
				log.finest(Session.MONITOR, "MqttClient not connected. Event [".concat(event.toString()).concat("] will not be send."));
			}
		} catch (Exception e) {
			log.severe(Session.MONITOR, e.getMessage());
		}
	}
	
	public void setRootTopic(String rootTopic) {
		if (!rootTopic.endsWith("/")) {
			rootTopic = rootTopic + "/";
		}
		configMqttTopicWrite = rootTopic;
	}

	@Override
	public void close() throws IOException {
		disconnect();
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();

		vertx.deployVerticle(new Verticle() {
			private EventToMqttConsumer queue;
			
			@Override
			public void stop(Promise<Void> stopFuture) throws Exception {
				if (queue != null) {
					queue.disconnect();
				}
			}
			
			@Override
			public void start(Promise<Void> startFuture) throws Exception {
				MqttClientOptions opt = new MqttClientOptions();
				opt.setMaxInflightQueue(100);
				queue = new EventToMqttConsumer("env-5291014.hidora.com", 11112, opt, vertx, null, true);
			}
			
			@Override
			public void init(Vertx vertx, Context context) {
				// nothing to do
			}
			
			@Override
			public Vertx getVertx() {
				return vertx;
			}
		});
	}
}
