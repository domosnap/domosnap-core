package com.domosnap.core.consumer.eventToMqttConsumer;


import org.junit.Test;

import com.domosnap.core.consumer.mqtt.EventToMqttConsumer;
import com.domosnap.engine.device.light.Light;
import com.domosnap.engine.device.what.impl.OnOffState;
import com.domosnap.engine.device.where.Where;
import com.domosnap.engine.event.EventFactory;
import com.domosnap.engine.gateway.UnsupportedAdapter;
import com.domosnap.engine.supervisor.DeviceFactory;

public class EventToMqttConsumerTest {

	
	@Test
	public void testScan() throws IllegalArgumentException, UnsupportedAdapter {
				
		EventToMqttConsumer e = new EventToMqttConsumer();
		EventFactory.addConsumer(e);
		
		Where w = new Where("scs://12345@localhost:1234/12");
		try (DeviceFactory df = new DeviceFactory()) {
			Light l = df.createDevice(Light.class, w).get();
			int i = 0;
			while (i < 110) {
				l.setStatus(OnOffState.On());
				i++;
			}
			
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
}
